import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";
import _ from "lodash";

import userReducer from "./modules/user";
import themeReducer from "./modules/theme";
import searchReducer from "./modules/search";
import snackbarReducer from "./modules/snackbar";
import enumReducer from "./modules/enums";

import rootSaga from "./sagas/sagas";

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  userReducer: userReducer,
  themeReducer: themeReducer,
  searchReducer: searchReducer,
  snackbarReducer: snackbarReducer,
  enumReducer: enumReducer,
});

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("state", serializedState);
  } catch (err) {
    return;
  }
};

const loadState = () => {
  try {
    const serializedState = localStorage.getItem("state");

    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const configureStore = () => {
  const middlewareEnchancer = applyMiddleware(sagaMiddleware);
  const composedEnhancer = composeWithDevTools(...[middlewareEnchancer]);

  const store = createStore(rootReducer, loadState(), composedEnhancer);
  store.subscribe(
    _.throttle(() => {
      saveState({
        themeReducer: store.getState().themeReducer,
        userReducer: store.getState().userReducer,
      });
    }, 1000)
  );
  sagaMiddleware.run(rootSaga);

  return store;
};

export const store = configureStore();
