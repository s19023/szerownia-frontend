import { put, call, takeLatest } from "redux-saga/effects";
import { searchConstants } from "../modules/search";
import getCategoriesList from "../services/getCategoriesList";
import searchAds from "../services/searchAds";
import getFeaturesForCategory from "../services/getFeaturesForCategory";

function* getCategories() {
  try {
    const payload = yield call(getCategoriesList);
    yield put({
      type: searchConstants.GET_CATEGORIES.SUCCESS,
      payload: payload,
    });
  } catch (error) {
    yield put({ type: searchConstants.GET_CATEGORIES.FAILED, error });
  }
}

function* search({ category, q, filters, isSearch, page }) {
  try {
    const payload = yield call(searchAds, category, q, filters, isSearch, page);
    yield put({
      type: searchConstants.SEARCH.SUCCESS,
      payload: payload.data,
    });
  } catch (error) {
    yield put({ type: searchConstants.SEARCH.FAILED, error });
  }
}

function* getFeatures({ category }) {
  if (category < 1) {
    yield put({
      type: searchConstants.SET_SEARCH_CATEGORY.SUCCESS,
      payload: [],
    });
  } else {
    try {
      const payload = yield call(getFeaturesForCategory, category);
      yield put({
        type: searchConstants.SET_SEARCH_CATEGORY.SUCCESS,
        payload: payload,
      });
    } catch (error) {
      yield put({ type: searchConstants.SET_SEARCH_CATEGORY.FAILED, error });
    }
  }
}

function* watchSearch() {
  yield takeLatest(searchConstants.GET_CATEGORIES.ACTION, getCategories);
  yield takeLatest(searchConstants.SEARCH.ACTION, search);
  yield takeLatest(searchConstants.SET_SEARCH_CATEGORY.ACTION, getFeatures);
}

export default watchSearch;
