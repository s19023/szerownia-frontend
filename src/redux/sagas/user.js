import { put, call, takeLatest } from "redux-saga/effects";
import { userConstants } from "../modules/user";
import { snackbarConstants } from "../modules/snackbar";
import loginUser from "../services/loginUser";
import refreshAccessToken from "../services/refreshAccessToken";

function* login({ email, password }) {
  try {
    const payload = yield call(loginUser, email, password);
    yield put({
      type: userConstants.USER_LOGIN.SUCCESS,
      payload: {
        token: payload.token,
        refreshToken: payload.refreshToken,
      },
    });
    yield put({
      type: snackbarConstants.OPEN_SNACKBAR,
      error: false,
      message: "Logowanie udane, witamy 😀",
    });
  } catch (error) {
    yield put({ type: userConstants.USER_LOGIN.FAILED, error });
    yield put({
      type: snackbarConstants.OPEN_SNACKBAR,
      error: true,
      message:
        error && error.response && error.response.status === 401
          ? "Błędne dane logowania, spróbuj ponownie"
          : error.response.status === 418
          ? "Zostałeś zbanowany, nie możesz się zalogować"
          : undefined,
    });
  }
}

function* refreshToken({ token, refreshToken }) {
  try {
    const payload = yield call(refreshAccessToken, token, refreshToken);
    yield put({
      type: userConstants.REFRESH_TOKEN.SUCCESS,
      payload: payload.data,
    });
  } catch (error) {
    yield put({ type: userConstants.REFRESH_TOKEN.FAILED, error });
  }
}

function* watchUser() {
  yield takeLatest(userConstants.USER_LOGIN.ACTION, login);
  yield takeLatest(userConstants.REFRESH_TOKEN.ACTION, refreshToken);
}

export default watchUser;
