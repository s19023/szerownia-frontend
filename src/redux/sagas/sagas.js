import { all, fork } from "redux-saga/effects";
import watchSearch from "./search";

import watchUser from "./user";

function* rootSaga() {
  yield all([fork(watchUser), fork(watchSearch)]);
}

export default rootSaga;
