export const enumConstants = {
  SET_TICKET_SUBJECTS: "SET_TICKET_SUBJECTS",
  SET_CLAIMS_STATUSES: "SET_CLAIMS_STATUSES",
  SET_HIRE_STATUSES: "SET_HIRE_STATUSES",
  SET_COMPLAINT_REASONS: "SET_COMPLAINT_REASONS",
  SET_COMPLAINT_STATUSES: "SET_COMPLAINT_STATUSES",
};

export const enumAction = {
  setSubjects: ({ subjects }) => ({
    type: enumConstants.SET_TICKET_SUBJECTS,
    subjects,
  }),
  setClaimsStatuses: ({ statuses }) => ({
    type: enumConstants.SET_CLAIMS_STATUSES,
    statuses,
  }),
  setHireStatuses: ({ statuses }) => ({
    type: enumConstants.SET_HIRE_STATUSES,
    statuses,
  }),
  setComplaintReasons: ({ reasons }) => ({
    type: enumConstants.SET_COMPLAINT_REASONS,
    reasons,
  }),
  setComplaintStatuses: ({ statuses }) => ({
    type: enumConstants.SET_COMPLAINT_STATUSES,
    statuses,
  }),
};

const initialState = {
  ticketSubjects: [],
  claimsStatuses: [],
  hireStatuses: [],
  complaintReasons: [],
  complaintStatuses: [],
};

const enumReducer = (state = initialState, action) => {
  switch (action.type) {
    case enumConstants.SET_TICKET_SUBJECTS: {
      return {
        ...state,
        ticketSubjects: action.subjects,
      };
    }
    case enumConstants.SET_CLAIMS_STATUSES: {
      return {
        ...state,
        claimsStatuses: action.statuses,
      };
    }
    case enumConstants.SET_HIRE_STATUSES: {
      return {
        ...state,
        hireStatuses: action.statuses,
      };
    }
    case enumConstants.SET_COMPLAINT_REASONS: {
      return {
        ...state,
        complaintReasons: action.reasons,
      };
    }
    case enumConstants.SET_COMPLAINT_STATUSES: {
      return {
        ...state,
        complaintStatuses: action.statuses,
      };
    }

    default:
      return state;
  }
};

export default enumReducer;
