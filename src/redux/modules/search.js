import createSagaAction from "../util/createSagaAction";

export const searchConstants = {
  GET_CATEGORIES: createSagaAction("GET_CATEGORIES"),
  SET_SEARCH_CATEGORY: createSagaAction("SET_SEARCH_CATEGORY"),
  UPDATE_QUERY: "UPDATE_QUERY",
  UPDATE_FILTERS: "UPDATE_FILTERS",
  CHANGE_PAGE: "CHANGE_PAGE",
  SET_SEARCH: "SET_SEARCH",
  SEARCH: createSagaAction("SEARCH"),
};

export const searchAction = {
  getCategories: () => ({
    type: searchConstants.GET_CATEGORIES.ACTION,
  }),
  setSearchCategory: ({ category }) => ({
    type: searchConstants.SET_SEARCH_CATEGORY.ACTION,
    category,
  }),
  updateQuery: ({ q }) => ({
    type: searchConstants.UPDATE_QUERY,
    q,
  }),
  updateFilters: ({ id, value, op }) => ({
    type: searchConstants.UPDATE_FILTERS,
    id,
    value,
    op,
  }),
  search: ({ category, q, isSearch, filters, page, buttonPress }) => ({
    type: searchConstants.SEARCH.ACTION,
    category,
    q,
    isSearch,
    filters,
    buttonPress,
    page: page ? page : 1,
  }),
  changePage: ({ page }) => ({
    type: searchConstants.CHANGE_PAGE,
    page,
  }),
  setSearch: ({ isSearch, willLoad }) => ({
    type: searchConstants.SET_SEARCH,
    isSearch,
    willLoad,
  }),
};

const initialState = {
  isLoadingCategories: false,
  isSearching: false,

  categories: [],
  searchResults: [],
  features: [],
  filters: [],
  page: 1,
  pages: 1,
  isSearch: false,

  category: "",
  q: "",
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case searchConstants.GET_CATEGORIES.ACTION: {
      return { ...state, isLoadingCategories: true };
    }
    case searchConstants.GET_CATEGORIES.SUCCESS: {
      return {
        ...state,
        isLoadingCategories: false,
        categories: action.payload,
      };
    }
    case searchConstants.GET_CATEGORIES.FAILED: {
      return {
        ...state,
        isLoadingCategories: false,
        categories: [],
      };
    }

    case searchConstants.SET_SEARCH_CATEGORY.ACTION: {
      return {
        ...state,
        category: action.category,
        page: 1,
        filters: [],
      };
    }
    case searchConstants.SET_SEARCH_CATEGORY.SUCCESS: {
      return {
        ...state,
        features: action.payload,
      };
    }
    case searchConstants.SET_SEARCH_CATEGORY.FAILED: {
      return {
        ...state,
        features: [],
      };
    }

    case searchConstants.UPDATE_QUERY: {
      return {
        ...state,
        q: action.q ? action.q : "",
      };
    }

    case searchConstants.UPDATE_FILTERS: {
      const tmpFilters = [...state.filters].filter(
        (filter) => filter.id !== action.id || filter.op !== action.op
      );
      if (action.value !== undefined && action.value !== "") {
        const filter = {
          op: action.op,
          value: action.value,
          id: action.id,
        };
        tmpFilters.push(filter);
      }
      return {
        ...state,
        filters: tmpFilters,
        page: 1,
      };
    }

    case searchConstants.CHANGE_PAGE: {
      return {
        ...state,
        page: action.page,
      };
    }

    case searchConstants.SET_SEARCH: {
      return {
        ...state,
        isSearch: action.isSearch,
        page: 1,
        isSearching: !!action.willLoad,
        filters: [],
      };
    }

    case searchConstants.SEARCH.ACTION: {
      return {
        ...state,
        isSearching: true,
        page: action.buttonPress ? 1 : state.page,
      };
    }
    case searchConstants.SEARCH.SUCCESS: {
      return {
        ...state,
        isSearching: false,
        searchResults: action.payload.results,
        pages: state.isSearch
          ? action.payload.totalPages
          : action.payload.totalPages + 1,
      };
    }
    case searchConstants.SEARCH.FAILED: {
      return {
        ...state,
        isSearching: false,
        searchResults: [],
      };
    }

    default:
      return state;
  }
};

export default searchReducer;
