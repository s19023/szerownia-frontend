export const snackbarConstants = {
  OPEN_SNACKBAR: "OPEN_SNACKBAR",
  CLOSE_SNACKBAR: "CLOSE_SNACKBAR",
};

export const snackbarAction = {
  openSnackbar: ({ error, message }) => ({
    type: snackbarConstants.OPEN_SNACKBAR,
    error,
    message,
  }),
  closeSnackbar: () => ({
    type: snackbarConstants.CLOSE_SNACKBAR,
  }),
};

const initialState = {
  open: false,
  error: false,
  message: "",
};

const snackbarReducer = (state = initialState, action) => {
  switch (action.type) {
    case snackbarConstants.OPEN_SNACKBAR: {
      return {
        ...state,
        open: true,
        error: action.error,
        message: action.message
          ? action.message
          : action.error
          ? "Wystąpił nieznany błąd"
          : "Zapisano zmiany",
      };
    }

    case snackbarConstants.CLOSE_SNACKBAR: {
      return {
        ...state,
        open: false,
      };
    }

    default:
      return state;
  }
};

export default snackbarReducer;
