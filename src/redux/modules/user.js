import createSagaAction from "../util/createSagaAction";
import jwt_decode from "jwt-decode";
export const userConstants = {
  USER_LOGIN: createSagaAction("USER_LOGIN"),
  USER_LOGOUT: "USER_LOGOUT",
  REFRESH_TOKEN: createSagaAction("REFRESH_TOKEN"),
  SET_PREMIUM: "SET_PREMIUM",
};

export const userAction = {
  login: ({ email, password }) => ({
    type: userConstants.USER_LOGIN.ACTION,
    email,
    password,
  }),
  logout: () => ({
    type: userConstants.USER_LOGOUT,
  }),
  refreshToken: ({ accessToken, refreshToken }) => ({
    type: userConstants.REFRESH_TOKEN.ACTION,
    token: accessToken,
    refreshToken: refreshToken,
  }),
  setPremuim: ({ isPremium }) => ({
    type: userConstants.SET_PREMIUM,
    isPremium,
  }),
};

const initialState = {
  isLoggedIn: false,
  isLoggingIn: false,
  isRefreshingToken: false,
  refreshTokenError: false,
  loginError: false,
  sub: "",
  id: "",
  accessToken: "",
  refreshToken: "",
  exp: 0,
  role: "",
  isPremium: false,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case userConstants.USER_LOGIN.ACTION: {
      return { ...state, isLoggingIn: true };
    }
    case userConstants.USER_LOGIN.SUCCESS: {
      const token = action.payload.token;
      window.localStorage.setItem("apiKey", token);
      const { sub, id, role, exp, isPremium } = jwt_decode(token);
      return {
        ...state,
        isLoggingIn: false,
        isLoggedIn: true,
        sub,
        id,
        isPremium,
        accessToken: token,
        refreshToken: action.payload.refreshToken,
        exp: exp * 1000,
        role,
        loginError: false,
      };
    }
    case userConstants.USER_LOGIN.FAILED: {
      window.localStorage.removeItem("apiKey");
      return {
        ...initialState,
        loginError: true,
      };
    }

    case userConstants.USER_LOGOUT: {
      window.localStorage.removeItem("apiKey");
      return {
        ...initialState,
      };
    }

    case userConstants.REFRESH_TOKEN.ACTION: {
      return {
        ...state,
        isRefreshingToken: true,
        refreshTokenError: false,
      };
    }
    case userConstants.REFRESH_TOKEN.SUCCESS: {
      const token = action.payload.token;
      window.localStorage.setItem("apiKey", token);
      const { exp } = jwt_decode(token);
      return {
        ...state,
        accessToken: token,
        refreshToken: "token",
        exp: exp * 1000,
        refreshToken: action.payload.refreshToken
          ? action.payload.refreshToken
          : state.refreshToken,
        isRefreshingToken: false,
      };
    }
    case userConstants.REFRESH_TOKEN.FAILED: {
      window.localStorage.removeItem("apiKey");
      return {
        ...initialState,
        refreshTokenError: true,
      };
    }
    case userConstants.SET_PREMIUM: {
      return {
        ...state,
        isPremium: action.isPremium,
      };
    }
    default:
      return state;
  }
};

export default userReducer;
