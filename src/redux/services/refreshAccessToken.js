import axios from "axios";

const refreshAccessToken = (token, refreshToken) =>
  axios.post(
    "/refreshToken",
    { token, refreshToken },
    { baseURL: process.env.REACT_APP_BASE_URL }
  );

export default refreshAccessToken;
