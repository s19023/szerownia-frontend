import request from "../../config/request";

const getFeaturesForCategory = (categoryId) =>
  request.get(`/features/list/${categoryId}`).then((response) => response.data);

export default getFeaturesForCategory;
