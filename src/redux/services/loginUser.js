import axios from "axios";

const loginUser = (email, password) =>
  axios
    .post(
      "/login",
      { email, password },
      { baseURL: process.env.REACT_APP_BASE_URL }
    )
    .then((response) => response.data);

export default loginUser;
