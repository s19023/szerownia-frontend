import request from "../../config/request";

const getCategoriesList = () =>
  request
    .get("/categories/")
    .then((response) =>
      response.data.filter((category) => category.subCategories.length > 0)
    );

export default getCategoriesList;
