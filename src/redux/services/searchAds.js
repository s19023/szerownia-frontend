import request from "../../config/request";

const searchAds = (category, q, filters, isSearch, page) => {
  let tmpFilters = [];

  if (filters) {
    tmpFilters = filters.map((filter) => ({
      [filter.op]: filter.value,
      id: filter.id,
    }));
  }

  return request.post(
    isSearch ? "/searchingAd/find" : "/rentalAd/findByParams",
    {
      category,
      search: q ? q : "",
      filters: tmpFilters,
    },
    {
      params: {
        page: page - 1,
        rowsPerPage: 20,
      },
    }
  );
};

export default searchAds;
