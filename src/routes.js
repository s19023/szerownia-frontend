const pagesNameMap = {
  "/admin": {
    nameFunc: (role) =>
      role === "ADMIN" ? "Panel administratora" : "Panel moderatora",
    routes: {
      "vulgar-filter": "Filtr wulgaryzmów",
      blocked: "Zablokowane adresy IP",
      tickets: "Zgłoszenia do moderacji",
      complaints: "Zgłoszone reklamacje",
      categories: "Zarządzanie kategoriami",
      features: "Zarządzanie cechami",
    },
  },
};

export default pagesNameMap;
