export const validateForm = (fields, values) => {
  const errors = {};
  fields.forEach((field) => {
    const { id } = field;
    let value = values[id];
    if (!field.isRequired && (!value || value.length === 0)) {
      //skip checking not required field
      return;
    } else if (!value || value.length === 0) {
      errors[id] = field.requiredError
        ? field.requiredError
        : "To pole jest wymagane";
      return;
    }

    if (field.equalTo && value !== values[field.equalTo]) {
      errors[id] = field.equalToError
        ? field.equalToError
        : "Wprowadzone wartości nie są takie same";
    }

    if (field.customValidate) {
      errors[id] = field.customValidate(value);
    }

    if (field.regex && !field.regex.test(value)) {
      errors[id] = field.regexError ? field.regexError : "Nieprawidłowy format";
    }

    if (field.minLength && value.length < field.minLength) {
      errors[id] = `Minimalna ilość znaków: ${field.minLength}`;
    }

    if (field.maxLength && value.length > field.maxLength) {
      errors[id] = `Maksymalna ilość znaków: ${field.maxLength}`;
    }

    if (field.type === "number" || field.type === "float") {
      errors[id] = validateNumber(value, field, field.type);
    }
  });

  return errors;
};

export const hasErrors = (errors) => {
  let hasErrors = false;

  Object.keys(errors).forEach((key) => {
    if (errors[key]) {
      hasErrors = true;
    }
  });

  return hasErrors;
};

export const validateNumber = (value, field, type) => {
  const regex = type === "number" ? /^-?\d+$/ : /^-?\d+(\.\d+)?$/;
  if (type === "number" && !regex.test(value)) {
    return "Podana wartość nie jest liczbą całkowitą";
  } else if (!regex.test(value)) {
    return "Podana wartość nie jest liczbą";
  }

  value = type === "number" ? Number.parseInt(value) : Number.parseFloat(value);

  if ((field.minValue || field.minValue === 0) && value < field.minValue) {
    return `Minimalna wartość to ${field.minValue}${
      field.adornment ? field.adornment : ""
    }`;
  }

  if ((field.maxValue || field.maxValue === 0) && value > field.maxValue) {
    return `Maksymalna wartość to ${field.maxValue}${
      field.adornment ? field.adornment : ""
    }`;
  }

  if (field.otherType === "price" && !/^-?\d+(.\d{1,2})?$/.test(value)) {
    return "Podana wartość nie jest prawidałową ceną";
  }
};
