import axios from "axios";

const request = axios.create({ baseURL: `${process.env.REACT_APP_BASE_URL}/api` });
request.interceptors.request.use((config) => {
  const accessToken = window.localStorage.getItem("apiKey");
  config.headers = {
    Authorization: accessToken ? "Bearer " + accessToken : undefined,
  };
  return config;
});

export default request;
