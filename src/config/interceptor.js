import request from "./request";
import { snackbarConstants } from "../redux/modules/snackbar";
import { userConstants } from "../redux/modules/user";

const interceptor = (store) => {
  request.interceptors.response.use(
    (next) => Promise.resolve(next),
    (error) => {
      const message = error.response?.data?.message;
      store.dispatch({
        type:
          message ===
          "JWT strings must contain exactly 2 period characters. Found: 0"
            ? userConstants.USER_LOGOUT
            : snackbarConstants.OPEN_SNACKBAR,
        error: true,
        message: error.response?.data?.message,
      });
      return Promise.reject(error);
    }
  );
};

export default interceptor;
