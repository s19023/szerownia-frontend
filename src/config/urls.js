const rentalAdUrl = "/rentalAd";
export const rentalAds = {
  getAll: rentalAdUrl,
  getDates: rentalAdUrl + "/availableDates",
  getPickupMethods: rentalAdUrl + "/pickups",
  getHomePage: rentalAdUrl + "/homePage",
  getPromotedCount: rentalAdUrl + "/promotedCount",
  toCorrect: rentalAdUrl + "/myRentalAdsToCorrect",
};

export const searchAds = {
  getAll: "/searchingAd",
  toCorrect: "/searchingAd/mySearchAdsToCorrect",
};

export const users = {
  getAll: "/users",
  update: "/users/update",
  premium: "/users/premium",
  extendPremium: "/users/premium/more",
  getMyDetails: "/users/details",
  getMyAds: "/rentalAd/myRentalAds",
  getResetToken: "/users/rtoken",
  resetPassword: "/users/resetpass",
  getInspectors: "/users/inspectors",
  getSearchAds: (id) => `/users/${id}/searchAds`,
  getRentalAds: (id) => `/users/${id}/rentalAds`,
  getOpinions: (id) => `/users/${id}/opinions`,
};

export const opinions = {
  post: "/opinions",
  put: "/opinions/",
};

export const messages = {
  get: "/threads",
  thread: "/thread",
  request: "/newMessage",
  new: (thread) => "/message/" + thread,
};

export const modTickets = {
  getAll: "/tickets",
};

export const hires = {
  all: "/hires",
  userBorrows: "/hires/myBorrows",
  userShares: "/hires/myShares",
  sharesForUser: (id) => `/hires/userShares/${id}`,
  borrowsForUser: (id) => `/hires/userBorrows/${id}`,
  estimate: "/hires/estimate",
  outgoing: (id) => `hires/${id}/package/outgoing`,
  incoming: (id) => `hires/${id}/package/incoming`,
  confirm: (id) => `hires/${id}/package/confirm`,
  insuranceCost: (id) => `hires/${id}/insuranceCost`,
};

export const discounts = {
  getAll: "/discounts",
};

export const products = {
  getAll: "/products",
  userProducts: "/products/myProducts",
  suggestName: "/products/suggest",
};

export const admin = {
  vulgarWords: "/vulgarWords",
  deleteVulgarWord: "/vulgarWords/deleteById",
  updateVulgarWord: "/vulgerWords/updateById",
  blockedIPs: "/blockedIp",
  deleteBlockedIP: "/blockedIp/deleteById",
  tickets: {
    opinionTickets: "/tickets/opinions",
    adTickets: "/tickets/ads",
    userTickets: "/tickets/users",
    single: "/tickets",
  },
};

export const enums = {
  ticketSubjects: "/datasets/ticketSubject",
  claims: "/datasets/claims",
  hireStatuses: "/datasets/hireStatusTypes",
  complaintReasons: "/datasets/complaints",
  complaintStatues: "/datasets/complaintStatues",
};

export const images = {
  get: process.env.REACT_APP_BASE_URL + "/api/image",
  post: "/image",
};

export const paczkomaty = {
  get: "https://inpost.pl/sites/default/files/points.json",
  image: "https://geowidget.easypack24.net/uploads/pl/images",
};

export const claims = {
  get: "/claims",
  forUser: id => `/claims/allUserClaims/${id}`,
};

const insuranceAdjusterUrl = "/insuranceadjuster";
export const insuranceAdjuster = {
  assignClaim: insuranceAdjusterUrl + "/assign",
  decision: insuranceAdjusterUrl + "/decision",
  endInspections: (id) => `${insuranceAdjusterUrl}/endInspections/${id}`,
};

export const stats = {
  rentalAds: "/stats/createdRentalAdStats",
  rents: "/stats/rentStats",
  users: "/stats/createdUserStats",
};

export const complaints = {
  get: "/complaints",
  user: "/complaints/my",
};

export const insuranceCompanies = {
  get: "/insurancecompany",
};

const inspectionUrl = "/inspection";
export const inspections = {
  get: inspectionUrl + "/claimsForInspection",
  post: inspectionUrl,
};

const notificationsUrl = "/notification";
export const notifications = {
  get: notificationsUrl,
  unread: notificationsUrl + "/unread",
};

const categoriesUrl = "/categories/";
export const categories = {
  get: categoriesUrl,
  single: (id) => `${categoriesUrl}${id}`,
};

const featuresUrl = "/features";
export const features = {
  get: featuresUrl,
  single: (id) => `${featuresUrl}/${id}`,
  forCategory: (id) => `${featuresUrl}/list/${id}`,
};
