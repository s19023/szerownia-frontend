import React, { useMemo, useEffect } from "react";
import ReactDOM from "react-dom";
import App from "./screens/App";
import { Provider, useSelector, useDispatch } from "react-redux";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import blue from "@material-ui/core/colors/blue";
import red from "@material-ui/core/colors/red";
import { plPL } from "@material-ui/core/locale";
import { store } from "./redux/store";
import { themeAction } from "./redux/modules/theme";
import { enumAction } from "./redux/modules/enums";
import interceptor from "./config/interceptor";

import getTicketSubjects from "./services/enums/getTicketSubjects";
import getClaimsStatuses from "./services/enums/getClaimsStatuses";
import getHireStatuses from "./services/enums/getHireStatuses";
import getComplaintReasons from "./services/enums/getComplaintReasons";
import getComplaintStatuses from "./services/enums/getComplaintStatuses";

interceptor(store);

const Wrapper = () => {
  const dispatch = useDispatch();
  const theme = useSelector((state) => state.themeReducer.theme);
  const hasChanged = useSelector((state) => state.themeReducer.hasChanged);
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  useEffect(() => {
    if (!hasChanged) {
      dispatch(
        themeAction.changeTheme({
          theme: prefersDarkMode ? "dark" : "light",
          hasChanged: false,
        })
      );
    }
  }, [prefersDarkMode]);

  useEffect(() => {
    getTicketSubjects().then((response) => {
      dispatch(enumAction.setSubjects({ subjects: response }));
    });
  }, []);

  useEffect(() => {
    getClaimsStatuses().then((response) => {
      dispatch(enumAction.setClaimsStatuses({ statuses: response }));
    });
  }, []);

  useEffect(() => {
    getHireStatuses().then((response) => {
      dispatch(enumAction.setHireStatuses({ statuses: response }));
    });
  }, []);

  useEffect(() => {
    getComplaintReasons().then((response) => {
      dispatch(enumAction.setComplaintReasons({ reasons: response }));
    });
  }, []);

  useEffect(() => {
    getComplaintStatuses().then((response) => {
      dispatch(enumAction.setComplaintStatuses({ statuses: response }));
    });
  }, []);

  const themeDefinition = useMemo(
    () =>
      createMuiTheme(
        {
          palette: {
            type: theme,
            primary: {
              main: blue[700],
            },
            secondary: {
              main: red.A400,
            },
            text: {
              primary: theme === "dark" ? "#FAFAFA" : "#333333",
            },
            common: {
              darkBlue: theme === "dark" ? blue[700] : "#01277E",
            },
          },
          shape: {
            borderRadius: "8px",
          },
          overrides: {
            MuiTypography: {
              root: {
                color: theme === "dark" ? "#FAFAFA" : "#333333",
              },
            },
            MuiTabs: {
              flexContainerVertical: {
                "& .MuiTab-root": {
                  maxWidth: "unset",
                },
              },
            },
            MuiTab: {
              wrapper: {
                color: theme === "dark" ? "#FAFAFA" : "#333333",
              },
            },
            MuiLink: {
              underlineHover: {
                "&:hover": {
                  textDecoration: "underline !important",
                },
              },
            },
            MuiSnackbar: {
              anchorOriginBottomCenter: {
                top: "50%",
                bottom: "auto !important",
                transform: "translate(-50%, -50%) !important",
              },
            },
            MuiTooltip: {
              tooltip: {
                fontSize: "12px",
                padding: "8px 14px",
                borderRadius: "8px",
                border: "1px solid",
                borderColor: blue[700],
                backgroundColor: theme === "dark" ? "#303030" : "#FAFAFA",
                color: theme === "dark" ? "#FAFAFA" : "#333333",
                boxShadow: "0px 3px 6px #00000029",
              },
            },
          },
        },
        plPL
      ),
    [theme]
  );

  return (
    <ThemeProvider theme={themeDefinition}>
      <App />
    </ThemeProvider>
  );
};

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Wrapper />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
