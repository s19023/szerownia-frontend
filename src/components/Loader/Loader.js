import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    justifyContent: "center",
  },
}));

const Loader = ({ array, isLoading, label, margin, marginTop }) => {
  const classes = useStyles();

  if (!isLoading && (!array || array.length > 0)) {
    return null;
  }

  return (
    <Box className={classes.root} style={{ marginBottom: margin, marginTop }}>
      {isLoading ? (
        <CircularProgress size={60} />
      ) : (
        <Typography>{label ? label : "Nie znaleziono wyników"}</Typography>
      )}
    </Box>
  );
};

export default Loader;