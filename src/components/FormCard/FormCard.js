import React from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  header: {
    fontFamily: "Poppins, sans-serif",
    color: theme.palette.text.primary,
    fontSize: "24px",
    fontWeight: "bold",
    marginBottom: "5px",
  },
  card: {
    padding: "20px",
    boxSizing: "border-box",
    position: "relative",
    width: (props) => (props.width ? props.width : "400px"),
  },
}));

const FormCard = ({ title, width, ...props }) => {
  const classes = useStyles({ width });

  return (
    <Box {...props} width={width}>
      <Typography className={classes.header}>{title}</Typography>
      <Card elevation={2} className={classes.card}>
        {props.children}
      </Card>
    </Box>
  );
};

export default FormCard;
