import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";

import HomeIcon from "@material-ui/icons/Home";
import { useRouteMatch, Link as RouterLink } from "react-router-dom";
import pagesNameMap from "../../routes";

const useStyles = makeStyles(() => ({
  icon: {
    fill: "none",
    stroke: "#515151",
    strokeWidth: "1.5",
  },
  breadcrumbs: {
    marginBottom: "20px",
  },
  tabs: {
    marginBottom: (props) => props.marginBottom,
  },
  buttonBox: {
    display: "flex",
    flexDirection: "row-reverse",
    marginBottom: "20px",
  },
  button: {
    width: "190px",
    height: "30px",
    borderRadius: "9px",
  },
}));

const CustomTab = withStyles((theme) => ({
  root: {
    color: theme.palette.text.primary,
  },
  selected: {
    color: theme.palette.secondary.main,
  },
}))(Tab);

const getSecondLevelName = (url, path) => {
  const route = pagesNameMap[url];
  if (!route || !route.routes) {
    return null;
  }

  path = path.split("/");
  return path.length > 2 && <Typography>{route.routes[path[2]]}</Typography>;
};

const showTabs = (hideTabs, location) => {
  if (!hideTabs) {
    return true;
  }

  return !hideTabs(location);
};

const TabNavigation = ({ pages, updateValue, hideHome, hideTabs }) => {
  const match = useRouteMatch();
  const history = useHistory();
  const location = useLocation();
  const role = useSelector((state) => state.userReducer.role);
  pages = pages.filter((page) => page.allowedRoles.includes(role));

  const classes = useStyles({
    marginBottom: "20px",
  });
  const [value, setValue] = useState(0);

  const BreadcrumbLink = (props) => (
    <Link
      {...props}
      component={RouterLink}
      onClick={(e) => {
        e.preventDefault();
        history.push(props.to);
      }}
    />
  );

  const getName = (url) => {
    const route = pagesNameMap["/" + url.split("/")[1]];
    return (
      route &&
      (route.longName
        ? route.longName
        : route.nameFunc
        ? route.nameFunc(role)
        : route.name)
    );
  };

  const handleChange = (event, newValue) => {
    if (updateValue) {
      updateValue(newValue);
    }
    history.push(pages[newValue].href);
    setValue(newValue);
  };

  useEffect(() => {
    if (history.location.pathname.split("/").length === 2) {
      setValue(0);
      if (updateValue) {
        updateValue(0);
      }
    } else {
      const url = history.location.pathname.split("/")[2];
      const index = pages.findIndex((page) => page.href.split("/")[2] === url);
      if (index !== -1) {
        setValue(index);
        if (updateValue) {
          updateValue(index);
        }
      }
    }
  }, [location.pathname]);

  return (
    <React.Fragment>
      <Breadcrumbs className={classes.breadcrumbs}>
        {!hideHome && (
          <BreadcrumbLink to="/">
            <HomeIcon className={classes.icon} />
          </BreadcrumbLink>
        )}
        <BreadcrumbLink to={match.url}>{getName(match.url)}</BreadcrumbLink>
        {getSecondLevelName(match.url, history.location.pathname)}
      </Breadcrumbs>
      {showTabs(hideTabs, history.location.pathname) && (
        <Tabs
          indicatorColor="secondary"
          value={value}
          onChange={handleChange}
          className={classes.tabs}
        >
          {pages.map((page) => {
            return (
              <CustomTab
                label={page.nameFunc ? page.nameFunc(role) : page.name}
                key={`tab-${page.name}`}
                className={classes.tab}
              />
            );
          })}
        </Tabs>
      )}
    </React.Fragment>
  );
};

export default TabNavigation;
