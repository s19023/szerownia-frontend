import React from "react";
import InputMask from "react-text-mask";

const MaskedInput = ({ mask, ...props }) => {
  const { inputRef, ...other } = props;

  return (
    <InputMask
      mask={mask}
      {...other}
      ref={(ref) => inputRef(ref ? ref.inputElement : null)}
      placeholderChar={"\u2000"}
      guide={false}
    />
  );
};

export default MaskedInput;
