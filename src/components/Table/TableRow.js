import React from "react";
import MuiTableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import moment from "moment";

const TableRow = ({ row, fields }) => {
  const renderCell = (field) => {
    const value = field.getValue ? field.getValue(row) : row[field.id];
    return (
      <TableCell>
        {field.type === "date"
          ? moment(value).format(
              field.dateFormat ? field.dateFormat : "DD.MM.YYYY"
            )
          : value}
      </TableCell>
    );
  };

  return <MuiTableRow>{fields.map((field) => renderCell(field))}</MuiTableRow>;
};

export default TableRow;
