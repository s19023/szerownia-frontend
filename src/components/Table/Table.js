import React, { useState, useEffect } from "react";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import MuiTable from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import MuiTableRow from "@material-ui/core/TableRow";
import TableRow from "./TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableSortLabel from "@material-ui/core/TableSortLabel";

const Table = ({
  data,
  fields,
  defaultSort,
  rowComponent,
  disableStartPadding,
  disableEndPadding,
  marginBottom,
  handleRefresh,
}) => {
  const [sortedData, setSortedData] = useState([]);
  const [sortKey, setSortKey] = useState(defaultSort);
  const [sortDir, setSortDir] = useState("desc");

  useEffect(() => {
    let dataToSort = [...data];

    const sortField = fields.find((f) => f.id === sortKey);
    if (!sortField) {
      return;
    }

    dataToSort = dataToSort.sort(sortField.comparator);
    if (sortDir === "desc") {
      dataToSort.reverse();
    }

    setSortedData(dataToSort);
  }, [sortDir, sortKey, data]);

  const handleSort = (key) => {
    if (sortKey !== key) {
      setSortDir("asc");
      setSortKey(key);
    } else {
      setSortDir(sortDir === "asc" ? "desc" : "asc");
    }
  };

  const Row = rowComponent ? rowComponent : TableRow;

  return (
    <TableContainer component={Paper} style={{ marginBottom }}>
      <MuiTable size="small">
        <TableHead>
          <MuiTableRow>
            {!disableStartPadding && <TableCell />}
            {fields.map((field) => (
              <TableCell key={field.id}>
                <TableSortLabel
                  active={field.id === sortKey}
                  direction={field.id === sortKey ? sortDir : ""}
                  onClick={() => handleSort(field.id)}
                >
                  {field.name}
                </TableSortLabel>
              </TableCell>
            ))}
            {!disableEndPadding && <TableCell />}
          </MuiTableRow>
        </TableHead>
        <TableBody>
          {sortedData.map((row) => (
            <Row key={row.id} row={row} fields={fields} handleRefresh={handleRefresh} />
          ))}
          {sortedData.length === 0 && (
            <TableCell colSpan={fields.length + 2} align="center">
              Brak danych
            </TableCell>
          )}
        </TableBody>
      </MuiTable>
    </TableContainer>
  );
};

export default Table;
