import React, { useState } from "react";
import Popover from "@material-ui/core/Popover";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";

import createModTicket from "../../services/modTickets/createModTicket";

const useStyles = makeStyles(() => ({
  popper: {
    zIndex: "1300",
  },
  paper: {
    padding: "20px",
  },
  select: {
    width: "300px",
    marginBottom: "10px",
  },
}));

const FlagDialog = ({ adId, opinionId, userId, anchorEl, handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [value, setValue] = useState("");
  const [desc, setDesc] = useState("");
  const [disabled, setDisabled] = useState(false);
  const id = useSelector((state) => state.userReducer.id);
  const ticketSubjects = useSelector(
    (state) => state.enumReducer.ticketSubjects
  );

  const submit = () => {
    setDisabled(true);
    const ticket = {
      notifierId: id,
      adId,
      opinionId,
      reportedId: userId,
      desc,
      subject: value,
    };
    createModTicket(ticket)
      .then(() => {
        handleClose();
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: adId
              ? "Zgłoszono ogłoszenie do moderacji"
              : opinionId
              ? "Zgłoszono ocenę do moderacji"
              : "Zgłoszono użytkownika do moderacji",
          })
        );
        setDisabled(false);
      })
      .catch(() => {
        setDisabled(false);
        dispatch(snackbarAction.openSnackbar({ error: true }));
      });
  };

  return (
    <Popover
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      transformOrigin={{
        vertical: "center",
        horizontal: "center",
      }}
      open={anchorEl !== null}
      anchorEl={anchorEl}
      className={classes.popper}
      onClose={handleClose}
    >
      <Paper className={classes.paper} elevation={1}>
        <Box display="flex" flexDirection="column">
          <TextField
            multiline
            label="Opis zgłoszenia"
            className={classes.select}
            onChange={(e) => setDesc(e.target.value)}
            id="report-content"
          />
          <TextField
            select
            label="Powód zgłoszenia"
            className={classes.select}
            onChange={(e) => setValue(e.target.value)}
            id="report-topic"
          >
            {Object.keys(ticketSubjects).map((key) => (
              <MenuItem value={key} key={key}>
                {ticketSubjects[key]}
              </MenuItem>
            ))}
          </TextField>
          <Button
            disabled={value === "" || desc === "" || disabled}
            color="primary"
            variant="contained"
            id="submit-report"
            onClick={() => submit()}
          >
            Prześlij zgłoszenie
          </Button>
        </Box>
      </Paper>
    </Popover>
  );
};

export default FlagDialog;
