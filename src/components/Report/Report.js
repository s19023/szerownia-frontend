import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import FlagDialog from "./FlagDialog";
import FlagIcon from "@material-ui/icons/Flag";

const Report = ({ text, adId }) => {
  const [reportAnchor, setReportAdAnchor] = useState(null);

  return (
    <React.Fragment>
      <FlagDialog
        adId={adId}
        anchorEl={reportAnchor}
        handleClose={() => setReportAdAnchor(null)}
      />
        <Button
          color="secondary"
          startIcon={<FlagIcon />}
          id="report-button"
          onClick={(e) =>
            setReportAdAnchor(reportAnchor ? null : e.currentTarget)
          }
        >
          {text}
        </Button>
    </React.Fragment>
  );
};

export default Report;
