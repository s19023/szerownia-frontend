import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  ratingTitle: {
    color: theme.palette.primary.main,
  },
  rating: {
    marginRight: "10px",
  },
  ratingMain: {
    color: theme.palette.text.primary,
    fontWeight: "bold",
    fontSize: "20px",
    fontFamily: "Poppins, sans-serif",
  },
  ratingSecondary: {
    color: theme.palette.text.primary,
  },
}))

const UserRating = ({ rateSharer, rateBorrower }) => {
  const classes = useStyles();
  return (
    <Box>
      <Typography className={classes.ratingTitle}>
        Średnia ocena jako użyczający
      </Typography>
      <Box display="flex" alignItems="center">
        {rateSharer ? (
          <React.Fragment>
            <Rating
              value={rateSharer}
              precision={0.1}
              readOnly
              className={classes.rating}
            />
            <Typography className={classes.ratingMain}>
              {rateSharer.toFixed(1)}
            </Typography>
            <Typography className={classes.ratingSecondary}>/5</Typography>
          </React.Fragment>
        ) : (
          <Typography className={classes.ratingSecondary}>Brak ocen</Typography>
        )}
      </Box>
      <Typography className={classes.ratingTitle}>
        Średnia ocena jako wypożyczający
      </Typography>
      <Box display="flex" alignItems="center">
        {rateBorrower ? (
          <React.Fragment>
            <Rating
              value={rateBorrower}
              precision={0.1}
              readOnly
              className={classes.rating}
            />
            <Typography className={classes.ratingMain}>
              {rateBorrower.toFixed(1)}
            </Typography>
            <Typography className={classes.ratingSecondary}>/5</Typography>
          </React.Fragment>
        ) : (
          <Typography className={classes.ratingSecondary}>Brak ocen</Typography>
        )}
      </Box>
    </Box>
  );
};

export default UserRating;