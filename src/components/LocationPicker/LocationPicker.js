import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import SearchIcon from "@material-ui/icons/Search";
import RoomIcon from "@material-ui/icons/Room";
import { makeStyles } from "@material-ui/core/styles";
import { MapContainer, TileLayer, Circle, useMapEvents } from "react-leaflet";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";

import geocode from "../../services/geocoding/geocode";
import reverseGeocode from "../../services/geocoding/reverseGeocode";

const useStyles = makeStyles((theme) => ({
  title: {
    fontFamily: "Poppins, sans-serif",
    fontSize: (props) => (props.smallTitle ? "16px" : "24px"),
    fontWeight: "bold",
  },
  error: {
    fontSize: "12px",
    color: theme.palette.error.main,
    marginBottom: "5px",
  },
  mapContainer: {
    height: "100%",
    width: "100%",
  },
  iconRoot: {
    display: "flex",
    alignItems: "center",
    marginBottom: "20px",
  },
  input: {
    marginRight: "10px",
    flexGrow: "1",
  },
  location: {
    color: theme.palette.common.darkBlue,
    "&:not(:last-child)": {
      marginRight: "5px",
    },
  },
}));

const getAddressString = (address) => {
  if (address.suburb && (address.city || address.city_district)) {
    return `${address.suburb}, ${address.city || address.city_district}`;
  }

  let name = "";
  const cityTypes = ["city", "town", "village"];
  cityTypes.forEach((key) => {
    if (address[key]) {
      name = address[key];
      return;
    }
  });

  return `${name}, ${address.state && address.state.split(" ")[1]}`;
};

const checkAddress = (address) => {
  if (address.country !== "Polska") {
    return false;
  }

  let name = "";
  const cityTypes = ["city_district", "city", "town", "village"];
  cityTypes.forEach((key) => {
    if (address[key]) {
      name = address[key];
      return;
    }
  });

  if (name === "") {
    return false;
  }

  return true;
};

const MapMarker = ({ pos, setPos }) => {
  const map = useMapEvents({
    click(e) {
      setPos(e.latlng);
    },
    locationfound(e) {
      setPos(e.latlng);
      map.flyTo(e.latlng, 15, { animate: false });
    },
  });

  if (!pos) {
    map.locate();
  }

  useEffect(() => {
    if (pos) {
      map.flyTo(pos, map.getZoom());
    }
  }, [pos]);

  return pos ? <Circle center={pos} radius={600} /> : null;
};

const LocationPicker = ({ handleChange, title, error }) => {
  const classes = useStyles({ smallTitle: title });
  const [pos, setPos] = useState(null);
  const [address, setAddress] = useState(null);
  const [search, setSearch] = useState("");
  const [isAddressValid, setIsAddressValid] = useState(false);
  const dispatch = useDispatch();

  const setPosition = (newPos) => {
    const oldPos = pos;
    setPos(newPos);
    if (!newPos) {
      return;
    }

    reverseGeocode(newPos.lat, newPos.lng).then((address) => {
      const isValid = checkAddress(address.address);
      if (isValid) {
        setAddress(address.address);
        setIsAddressValid(isValid);
      } else {
        setPos(oldPos);
      }
    });
  };

  const handleSearch = (e) => {
    e.preventDefault();

    geocode(search).then((searchResults) => {
      if (searchResults.length > 0) {
        const address = searchResults[0];
        const isValid = checkAddress(address.address);
        if (isValid) {
          setPos({ lat: address.lat, lng: address.lon });
          setAddress(address.address);
        }
        setIsAddressValid(isValid);
      } else {
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message: "Nie znaleziono adresu dla wyszukiwanej frazy",
          })
        );
      }
    });
  };

  useEffect(() => {
    if (pos && isAddressValid) {
      handleChange("location", {
        latitude: pos.lat,
        longitude: pos.lng,
        name: getAddressString(address),
      });
    }
  }, [address, isAddressValid]);

  return (
    <Box width="100%">
      <Typography className={classes.title}>
        {title ? title : "Twoja lokalizacja"}
      </Typography>
      {error && <Typography className={classes.error}>{error}</Typography>}
      <Box className={classes.iconRoot}>
        <TextField
          id="location-search"
          placeholder="Wyszukaj"
          className={classes.input}
          onChange={(e) => setSearch(e.target.value)}
        />
        <Button
          startIcon={<SearchIcon />}
          id="loc-search-btn"
          color="primary"
          variant="contained"
          type="submit"
          onClick={handleSearch}
          disabled={search === "" || search === null}
        >
          Wyszukaj
        </Button>
      </Box>
      <Box className={classes.iconRoot}>
        {address && isAddressValid && <RoomIcon className={classes.location} />}
        <Typography className={classes.location}>
          {address && isAddressValid
            ? getAddressString(address)
            : "Wyszukaj adres lub zaznacz punkt na mapie"}
        </Typography>
      </Box>
      <Box width="100%" height="400px">
        <MapContainer
          zoom={15}
          maxZoom={16}
          minZoom={7}
          center={[52.23, 21]}
          className={classes.mapContainer}
        >
          <TileLayer
            attribution='Dane mapy &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <MapMarker pos={pos} setPos={setPosition} />
        </MapContainer>
      </Box>
    </Box>
  );
};

export default LocationPicker;
