import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { images } from "../../config/urls";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: theme.shadows[5],
    width: "100%",
    padding: "10px",
    boxSizing: "border-box",
    display: "flex",
    cursor: "pointer",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  image: {
    height: "185px",
    width: "310px",
    marginRight: "20px",
    backgroundColor: theme.palette.text.disabled,
    objectFit: "cover",
    objectPosition: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
      marginRight: "0px",
      marginBottom: "20px",
    },
  },
  searchIcon: {
    fontSize: "100px",
    lineHeight: "80px",
    color: theme.palette.background.paper,
    opacity: "0.6",
  },
  title: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
    flexGrow: "1",
    color: theme.palette.text.primary,
    marginRight: "20px",
  },
  price: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
    color: theme.palette.text.primary,
  },
  description: {
    fontSize: "18px",
    flexGrow: "1",
    color: theme.palette.text.primary,
  },
  status: {
    fontSize: "18px",
    color: theme.palette.text.disabled,
  },
}));

const AdDetails = ({ object, isSearchAd, showEdit }) => {
  const classes = useStyles();
  const [failedToLoad, setFailedToLoad] = useState(false);

  const handleClick = (e) => {
    e.stopPropagation();
  };

  return (
    <Box
      component="a"
      href={`/ad/rental/${object.id}`}
      target="_blank"
      className={classes.root}
    >
      {!isSearchAd ? (
        (object.adThumbnailId || object.imagesIdList) && !failedToLoad ? (
          <img
            src={`${images.get}/${
              object.adThumbnailId
                ? object.adThumbnailId
                : object.imagesIdList[0]
            }`}
            alt={object.title}
            className={classes.image}
            onError={() => setFailedToLoad(true)}
          />
        ) : (
          <Box className={classes.image}>
            <Typography className={classes.searchIcon}>?</Typography>
          </Box>
        )
      ) : (
        <Box className={classes.image}>
          <SearchIcon className={classes.searchIcon} />
        </Box>
      )}
      <Box flexGrow="1" display="flex" flexDirection="column">
        <Box display="flex">
          <Typography className={classes.title}>{object.title}</Typography>
          {!isSearchAd && (
            <Typography className={classes.price}>
              {object.pricePerDay}zł
            </Typography>
          )}
        </Box>
        <Typography className={classes.description}>
          {object.description}
        </Typography>
        {showEdit && (
          <Button
            onClick={handleClick}
            component={Link}
            to={{
              pathname: "/edit-ad",
              state: {
                adId: object.id,
                ticketId: object.modTicketId,
                isSearchAd,
              },
            }}
          >
            Edytuj ogłoszenie
          </Button>
        )}
      </Box>
    </Box>
  );
};

export default AdDetails;
