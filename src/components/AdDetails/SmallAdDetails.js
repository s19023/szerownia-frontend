import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import { images } from "../../config/urls";
import { makeStyles } from "@material-ui/core/styles";
import getRentalAd from "../../services/rentalAds/getRentalAd";
import getSearchAd from "../../services/searchAds/getSearchAd";

const useStyles = makeStyles((theme) => ({
  topText: {
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    marginBottom: "10px",
    color: theme.palette.text.primary,
  },
  root: {
    width: "250px",
    marginBottom: "10px",
  },
  progressRoot: {
    height: "130px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    height: "130px",
    width: "100%",
    backgroundColor: theme.palette.text.disabled,
    objectFit: "cover",
    objectPosition: "center",
  },
  textRoot: {
    padding: "10px",
  },
  header: {
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    color: theme.palette.text.primary,
  },
  description: {
    fontSize: "12px",
    color: theme.palette.text.primary,
  },
}));

const SmallAdDetails = ({ id, topText, isSearchAd }) => {
  const classes = useStyles();
  const [ad, setAd] = useState({});
  const [failedToLoad, setFailedToLoad] = useState(false);

  useEffect(() => {
    const getAd = isSearchAd ? getSearchAd : getRentalAd;
    getAd(id).then((ad) => {
      setAd(ad);
    });
  }, [id]);

  return (
    <Box position="sticky" top="70px">
      {topText && (
        <Typography className={classes.topText}>{topText}</Typography>
      )}
      <Card elevation={2} className={classes.root}>
        {Object.keys(ad).length === 0 ? (
          <Box className={classes.progressRoot}>
            <CircularProgress />
          </Box>
        ) : (
          <React.Fragment>
            {!isSearchAd &&
              (ad.imagesIdList && !failedToLoad ? (
                <img
                  src={`${images.get}/${ad.imagesIdList[0]}`}
                  alt={ad.title}
                  className={classes.image}
                  onError={() => setFailedToLoad(true)}
                />
              ) : (
                <Box className={classes.image} />
              ))}
            <Box className={classes.textRoot}>
              <Typography className={classes.header}>{ad.title}</Typography>
              <Typography className={classes.description}>
                {ad.description && ad.description.length > 300
                  ? ad.description.substring(0, 300) + "..."
                  : ad.description}
              </Typography>
            </Box>
          </React.Fragment>
        )}
      </Card>
    </Box>
  );
};

export default SmallAdDetails;
