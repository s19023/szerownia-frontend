import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import RoomIcon from "@material-ui/icons/Room";
import SearchIcon from "@material-ui/icons/Search";
import StarIcon from "@material-ui/icons/Star";
import { makeStyles } from "@material-ui/core/styles";
import { images } from "../../../config/urls";

const useStyles = makeStyles((theme) => ({
  grid: {
    display: "flex",
    justifyContent: "center",
    flex: "1",
    minWidth: "250px",
    maxWidth: "375px",
    flexBasis: "375px",
  },
  root: {
    display: "flex",
    flexGrow: "1",
    cursor: "pointer",
    flexDirection: "column",
    boxShadow: "0 3px 6px #00000029",
    border: "2px solid #707070",
    borderRadius: "10px",
    height: "376px",
    maxWidth: "350px",
    transition: "300ms transform",
    "&:hover": {
      transform: "scale(1.02)",
    },
  },
  image: {
    height: "185px",
    background: theme.palette.text.disabled,
    objectFit: "cover",
    objectPosition: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  searchIcon: {
    fontSize: "100px",
    lineHeight: "80px",
    color: theme.palette.background.paper,
    opacity: "0.6",
  },
  bottomRoot: {
    padding: "15px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    flexGrow: "1",
  },
  title: {
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    fontSize: "28px",
    lineHeight: "30px",
    height: "60px",
    marginBottom: "12px",
  },
  iconBox: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.common.darkBlue,
    fontSize: "18px",
  },
  icon: {
    width: "18px",
    color: theme.palette.common.darkBlue,
    position: "relative",
    left: "-3px",
  },
  iconText: {
    fontFamily: "Poppins, sans-serif",
  },
  priceBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  price: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "28px",
    fontWeight: "bold",
  },
  star: {
    marginRight: "5px",
    color: theme.palette.text.secondary,
  },
  promotedText: {
    fontSize: "14px",
    lineHeight: "14px",
    width: "min-content",
    textAlign: "center",
  },
}));

const MAX_TITLE_LENGTH = 40;

const Ad = ({ ad, isSearchAd }) => {
  const classes = useStyles({ isSearchAd });
  const [failedToLoad, setFailedToLoad] = useState(false);

  return (
    <Grid item className={classes.grid}>
      <Card
        component="a"
        href={`/ad/${isSearchAd ? "search" : "rental"}/${ad.id}`}
        target="_blank"
        elevation={0}
        className={classes.root}
        id={`rental-ad-${ad.id}`}
      >
        {!isSearchAd ? (
          ad.imagesIdList && !failedToLoad ? (
            <img
              src={`${images.get}/${ad.imagesIdList[0]}`}
              alt={ad.title}
              className={classes.image}
              onError={() => setFailedToLoad(true)}
            />
          ) : (
            <Box className={classes.image}>
              <Typography className={classes.searchIcon}>?</Typography>
            </Box>
          )
        ) : (
          <Box className={classes.image}>
            <SearchIcon className={classes.searchIcon} />
          </Box>
        )}
        <Box className={classes.bottomRoot}>
          <Typography className={classes.title}>
            {ad.title.length > MAX_TITLE_LENGTH
              ? ad.title.substring(0, MAX_TITLE_LENGTH - 3).trim() + "..."
              : ad.title}
          </Typography>
          <Box>
            <Box className={classes.iconBox}>
              <RoomIcon className={classes.icon} />
              <Typography className={classes.iconText}>
                {ad.location && ad.location.split(",").join(", ")}
              </Typography>
            </Box>
            <Box className={classes.priceBox}>
              {ad.pricePerDay && (
                <Typography className={classes.price}>
                  {ad.pricePerDay.toFixed(0)} zł
                </Typography>
              )}
              {ad.promoted && (
                <Box display="flex" alignItems="center">
                  <StarIcon className={classes.star} />
                  <Typography className={classes.promotedText}>
                    Ogłoszenie promowane
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
      </Card>
    </Grid>
  );
};

export default Ad;
