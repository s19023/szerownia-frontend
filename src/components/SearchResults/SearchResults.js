import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Pagination from "@material-ui/lab/Pagination";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Ad from "./components/Ad";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  padinationRoot: {
    display: "flex",
    justifyContent: "flex-end",
    marginBottom: "20px",
  },
  progressRoot: {
    display: "flex",
    justifyContent: "center",
  },
  grid: {
    maxWidth: "1500px",
  },
}));

const SearchResults = ({
  results,
  page,
  pages,
  changePage,
  isSearchAd,
  isLoading,
}) => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Box className={classes.padinationRoot}>
        <Pagination
          page={page}
          count={pages}
          showFirstButton
          showLastButton
          color="primary"
          onChange={(e, v) => changePage(v)}
        />
      </Box>
      {isLoading && (
        <Box className={classes.progressRoot}>
          <CircularProgress size={60} />
        </Box>
      )}
      {!isLoading && results.length === 0 && (
        <Box className={classes.progressRoot}>
          <Typography>Nie znaleziono wyników 😔</Typography>
        </Box>
      )}
      {!isLoading && results.length > 0 && (
        <Grid container spacing={3} className={classes.grid} justify="center">
          {results.map((ad) => (
            <Ad key={`ad-${ad.id}`} ad={ad} isSearchAd={isSearchAd} />
          ))}
        </Grid>
      )}
    </React.Fragment>
  );
};

export default SearchResults;
