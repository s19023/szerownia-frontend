import moment from "moment";
import { useSelector } from "react-redux";

export const useComplaintsFields = () => {
  const complaintTypes = useSelector((state) => state.enumReducer.complaintReasons);
  const complaintStatuses = useSelector((state) => state.enumReducer.complaintStatuses);

  return [
    {
      id: "title",
      name: "Nazwa ogłoszenia",
      comparator: (a, b) => a.hire.ad.title.localeCompare(b.hire.ad.title),
      getValue: (row) => row.hire.ad.title,
    },
    {
      id: "reason",
      name: "Powód zgłoszenia",
      comparator: (a, b) => a.reason.localeCompare(b.reason),
      getValue: (row) => (complaintTypes ? complaintTypes[row.reason] : ""),
    },
    {
      id: "status",
      name: "Status reklamacji",
      comparator: (a, b) => a.status.localeCompare(b.status),
      getValue: (row) => (complaintStatuses ? complaintStatuses[row.status] : ""),
    },
    {
      id: "sharer",
      name: "Użyczający",
      comparator: (a, b) =>
        `${a.hire.sharer.lastName} ${a.hire.sharer.firstName}`.localeCompare(
          `${b.hire.sharer.lastName} ${b.hire.sharer.firstName}`
        ),
      getValue: (row) =>
        `${row.hire.sharer.firstName} ${row.hire.sharer.lastName}`,
    },
    {
      id: "borrower",
      name: "Wypożyczający",
      comparator: (a, b) =>
        `${a.hire.borrower.lastName} ${a.hire.borrower.firstName}`.localeCompare(
          `${b.hire.borrower.lastName} ${b.hire.borrower.firstName}`
        ),
      getValue: (row) =>
        `${row.hire.borrower.firstName} ${row.hire.borrower.lastName}`,
    },
    {
      id: "reportDate",
      name: "Data zgłoszenia",
      comparator: (a, b) => new Date(a.reportDate) - new Date(b.reportDate),
      getValue: (row) => moment(row.reportDate).format("DD.MM.YYYY HH:mm"),
    },
    {
      id: "hireDate",
      name: "Data wypożyczenia",
      comparator: (a, b) =>
        new Date(a.hire.dateFrom) - new Date(b.hire.dateFrom),
      getValue: (row) => moment(row.hire.dateFrom).format("DD.MM.YYYY"),
    },
  ];
};
