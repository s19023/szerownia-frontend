import React, { useState } from "react";
import MuiAccordion from "@material-ui/core/Accordion";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import Avatar from "../Avatar/Avatar";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { images } from "../../config/urls";
import { fields } from "./fields";
import ShipmentDialog from "./Dialogs/ShipmentDialog";
import ConfirmDialog from "./Dialogs/ConfirmDialog";
import ClaimDialog from "./Dialogs/ClaimDialog";
import ComplaintDialog from "./Dialogs/ComplaintDialog";
import { Link } from "react-router-dom";
import OpinionDialog from "./Dialogs/OpinionDialog";
import { Rating } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  accordion: {
    background: theme.palette.background.paper,
    borderRadius: "5px",
    "&:not(:last-of-type)": {
      marginBottom: "5px",
    },
  },
  root: {
    display: "flex",
    alignItems: "center",
  },
  image: {
    minWidth: "64px",
    minHeight: "64px",
    maxWidth: "64px",
    maxHeight: "64px",
    borderRadius: "5px",
    background: theme.palette.text.secondary,
    marginRight: "10px",
    objectFit: "cover",
    objectPosition: "center",
  },
  title: {
    fontWeight: "bold",
    color: theme.palette.text.primary,
  },
  box: {
    flexGrow: "1",
    flexBasis: "20%",
    display: "flex",
    alignItems: "center",
  },
  ratingName: {
    color: theme.palette.text.primary,
    marginRight: "10px",
  },
  noRating: {
    color: theme.palette.text.secondary,
  },
  spacer: {
    flexBasis: "calc(40% + 46px)",
  },
  opinionBox: {
    flexGrow: "1",
    flexBasis: "20%",
  },
  details: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  header: {
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
  container: {
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  button: {
    marginBottom: "10px",
  },
  avatarRoot: {
    display: "flex",
    alignItems: "center",
    marginBottom: "20px",
  },
  avatar: {
    marginRight: "5px",
  },
  name: {
    fontSize: "16px",
    fontWeight: "bold",
    textIndent: "8px",
    marginRight: "5px",
  },
}));

const Accordion = withStyles(() => ({
  root: {
    "&:not(:last-of-type)": {
      marginBottom: "5px",
      "&.Mui-expanded": {
        margin: "16px 0",
      },
    },
    "&::before": {
      content: "unset",
    },
  },
}))(MuiAccordion);

const AccordionSummary = withStyles(() => ({
  root: {
    paddingTop: "10px",
    paddingBottom: "10px",
  },
  content: {
    margin: "unset",
    height: "100%",
  },
  expanded: {
    minHeight: "unset !important",
  },
}))(MuiAccordionSummary);

const ShareRow = ({ hire, modView, isBorrow, isPremium, handleRefresh }) => {
  const classes = useStyles();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [action, setAction] = useState("");

  const yourRating =
    !modView &&
    hire.opinions.find((opinion) => opinion.aboutSharer === !!isBorrow);
  const theirRating =
    !modView &&
    hire.opinions.find((opinion) => opinion.aboutSharer !== !!isBorrow);

  const getDialogTitle = () => {
    switch (hire.hireStatus) {
      case "WAITING_FOR_SHIPMENT":
        return "Nadaj przesyłkę do wypożyczającego";
      case "PENDING_HIRE":
        return action === "complaint"
          ? "Złóż reklamację"
          : "Nadaj przesyłkę do użyczającego";
      case "SHIPPED":
      case "SHIPPED_BACK":
        return "Potwierdź odebranie przesyłki";
      case "RETURNED":
        return action === "claim"
          ? "Zgłoś szkodę"
          : `Oceń ${isBorrow ? "użyczającego" : "wypożyczającego"}`;
    }
  };

  const getDialogContent = () => {
    const defaultProps = {
      hire,
      handleRefresh: refresh,
      handleClose: () => setDialogOpen(false),
      isBorrow,
    };
    switch (hire.hireStatus) {
      case "WAITING_FOR_SHIPMENT":
      case "PENDING_HIRE":
        return action === "complaint" ? (
          <ComplaintDialog {...defaultProps} />
        ) : (
          <ShipmentDialog {...defaultProps} />
        );
      case "SHIPPED":
      case "SHIPPED_BACK":
        return <ConfirmDialog {...defaultProps} />;
      case "RETURNED":
        return action === "claim" ? (
          <ClaimDialog {...defaultProps} />
        ) : (
          <OpinionDialog {...defaultProps} />
        );
    }
  };

  const renderButton = () => {
    if (modView) {
      return null;
    }

    const defaultProps = {
      variant: "contained",
      color: "primary",
      onClick: () => {
        setAction("");
        setDialogOpen(true);
      },
    };
    switch (hire.hireStatus) {
      case "WAITING_FOR_SHIPMENT":
        return !isBorrow ? (
          <Button {...defaultProps}>Nadaj przesyłkę</Button>
        ) : null;
      case "PENDING_HIRE":
        return isBorrow ? (
          <Button {...defaultProps}>Nadaj przesyłkę</Button>
        ) : null;
      case "SHIPPED":
        return isBorrow ? (
          <Button {...defaultProps}>Potwierdź odebranie przesyłki</Button>
        ) : null;
      case "SHIPPED_BACK":
        return !isBorrow ? (
          <Button {...defaultProps}>Potwierdź odebranie przesyłki</Button>
        ) : null;
      case "RETURNED":
        return (
          <React.Fragment>
            {!yourRating && (
              <Button {...defaultProps}>
                Oceń {isBorrow ? "użyczającego" : "wypożyczającego"}
              </Button>
            )}
            {!isBorrow && isPremium && (
              <Button
                {...defaultProps}
                onClick={() => {
                  setAction("claim");
                  setDialogOpen(true);
                }}
              >
                Zgłoś szkodę
              </Button>
            )}
          </React.Fragment>
        );
    }
  };

  const refresh = () => {
    setDialogOpen(false);
    handleRefresh();
  };

  return (
    <Accordion elevation={2} className={classes.accordion}>
      <AccordionSummary>
        <Box className={classes.root}>
          <img
            src={`${images.get}/${hire.ad.adThumbnailId}`}
            className={classes.image}
          />
          <Typography className={classes.title}>{hire.ad.title}</Typography>
        </Box>
      </AccordionSummary>
      <AccordionDetails>
        <Box className={classes.details}>
          <Box>
            {fields.map(
              (field) =>
                (!field.showWhen || field.showWhen(hire)) &&
                (!modView || !field.hideInModView) && (
                  <Box key={field.id} className={classes.container}>
                    <Typography className={classes.header}>
                      {field.name}
                    </Typography>
                    <Typography>
                      {field.getValue ? field.getValue(hire) : hire[field.id]}
                    </Typography>
                  </Box>
                )
            )}
            {theirRating && (
              <Box className={classes.container}>
                <Box display="flex" alignItems="center">
                  <Typography className={classes.header}>
                    Otrzymałeś ocenę
                  </Typography>
                  <Rating max={5} readOnly value={theirRating.rating} />
                </Box>
                <Typography>
                  {theirRating.comment
                    ? theirRating.comment
                    : "Brak komentarza"}
                </Typography>
              </Box>
            )}
            {yourRating && (
              <Box className={classes.container}>
                <Box display="flex" alignItems="center">
                  <Typography className={classes.header}>
                    Wystawiłeś ocenę
                  </Typography>
                  <Rating max={5} readOnly value={yourRating.rating} />
                </Box>
                <Typography>
                  {yourRating.comment ? yourRating.comment : "Brak komentarza"}
                </Typography>
              </Box>
            )}
            <Typography className={classes.header}>
              {isBorrow ? "Użyczający" : "Wypożyczający"}
            </Typography>
            <Box className={classes.avatarRoot}>
              <Avatar
                firstName={
                  isBorrow ? hire.sharer.firstName : hire.borrower.firstName
                }
                lastName={
                  isBorrow ? hire.sharer.lastName : hire.borrower.lastName
                }
                imageId={
                  isBorrow ? hire.sharer.userImageId : hire.borrower.userImageId
                }
                size={48}
              />
              <Box marginLeft="5px">
                <Typography className={classes.name}>
                  {isBorrow ? hire.sharer.firstName : hire.borrower.firstName}{" "}
                  {isBorrow ? hire.sharer.lastName : hire.borrower.lastName}
                </Typography>
                <Button
                  color="primary"
                  component={Link}
                  target="_blank"
                  to={`/user/${isBorrow ? hire.sharer.id : hire.borrower.id}`}
                >
                  Przejdź do profilu
                </Button>
              </Box>
            </Box>
            <Button
              color="primary"
              component={Link}
              target="_blank"
              to={`/ad/rental/${hire.ad.id}`}
            >
              Przejdź do ogłoszenia
            </Button>
          </Box>
          <Box display="flex" flexDirection="column">
            {isBorrow && hire.hireStatus === "PENDING_HIRE" && (
              <Button
                className={classes.button}
                variant="contained"
                color="secondary"
                onClick={() => {
                  setAction("complaint");
                  setDialogOpen(true);
                }}
              >
                Złóż reklamację
              </Button>
            )}
            {renderButton()}
          </Box>
        </Box>
      </AccordionDetails>
      <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
        <DialogTitle>{getDialogTitle()}</DialogTitle>
        {getDialogContent()}
      </Dialog>
    </Accordion>
  );
};

export default ShareRow;
