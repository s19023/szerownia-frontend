import moment from "moment";

const pickupMethods = {
  PARCEL_LOCKER: "Paczkomat",
  COURIER: "Kurier",
  PERSONAL_PICKUP: "Odbiór osobisty",
};

export const fields = [
  {
    id: "dateFrom",
    name: "Początek wypożyczenia",
    getValue: (hire) => moment(hire.dateFrom).format("DD.MM.YYYY"),
  },
  {
    id: "dateToPlanned",
    name: "Planowany koniec wypożyczenia",
    getValue: (hire) => moment(hire.dateToPlanned).format("DD.MM.YYYY"),
  },
  {
    id: "dateToPlanned",
    name: "Faktyczny koniec wypożyczenia",
    showWhen: (hire) => !!hire.dateToActual,
    getValue: (hire) => moment(hire.dateToActual).format("DD.MM.YYYY"),
  },
  {
    id: "selectedPickupMethod",
    name: "Sposób wysyłki",
    getValue: (hire) => pickupMethods[hire.selectedPickupMethod],
  },
  {
    id: "outgoingShipmentNumber",
    name: "Numer przesyłki do wypożyczającego",
    showWhen: (hire) =>
      hire.selectedPickupMethod !== "PERSONAL_PICKUP" &&
      hire.outgoingShipmentNumber,
    hideInModView: true,
  },
  {
    id: "incomingShipmentNumber",
    name: "Numer przesyłki do użyczającego",
    showWhen: (hire) =>
      hire.selectedPickupMethod !== "PERSONAL_PICKUP" &&
      hire.incomingShipmentNumber,
    hideInModView: true,
  },
];
