import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import Rating from "@material-ui/lab/Rating";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import postOpinion from "../../../services/opinions/postOpinion";

const useStyles = makeStyles(() => ({
  rating: {
    marginBottom: "10px",
  },
  input: {
    width: "400px",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
}));

const OpinionDialog = ({ hire, handleRefresh, handleClose, isBorrow }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [rating, setRating] = useState(0);
  const [opinion, setOpinion] = useState("");
  const userId = useSelector((state) => state.userReducer.id);

  const handleConfirm = () => {
    setIsLoading(true);
    const data = {
      aboutSharer: isBorrow,
      authorId: userId,
      takerId: isBorrow ? hire.sharer.id : hire.borrower.id,
      rating: rating,
      comment: opinion,
      hireId: hire.id,
    };

    postOpinion(data)
      .then(() => {
        handleRefresh();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <React.Fragment>
      <DialogContent>
        <Box
          width="100%"
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          <Rating
            max={5}
            value={rating}
            onChange={(e, v) => setRating(v)}
            className={classes.rating}
          />
          <TextField
            label="Komentarz do oceny"
            onChange={(e) => setOpinion(e.target.value)}
            className={classes.input}
            multiline
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={() => handleClose()}>
          Anuluj
        </Button>
        <Button
          color="primary"
          disabled={isLoading}
          startIcon={isLoading && <CircularProgress size={16} />}
          onClick={() => handleConfirm()}
        >
          Potwierdź
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default OpinionDialog;
