import React, { useState } from "react";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import confirmPackage from "../../../services/hires/confirmPackage";

const ConfirmDialog = ({ hire, handleRefresh, handleClose }) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleConfirm = () => {
    setIsLoading(true);
    confirmPackage(hire.id)
      .then(() => {
        handleRefresh();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <React.Fragment>
      <DialogContent>
        <DialogContentText>
          Potwierdź odebranie przesyłki od{" "}
          {hire.hireStatus === "SHIPPED" ? "użyczającego" : "wypożyczającego"}{" "}
          klikając w przycisk poniżej.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={() => handleClose()}>
          Anuluj
        </Button>
        <Button
          color="primary"
          disabled={isLoading}
          startIcon={isLoading && <CircularProgress size={16} />}
          onClick={() => handleConfirm()}
        >
          Potwierdź
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default ConfirmDialog;
