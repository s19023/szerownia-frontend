import React, { useState, useEffect } from "react";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Collapse from "@material-ui/core/Collapse";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import Button from "@material-ui/core/Button";
import outgoingShipment from "../../../services/hires/outgoingShipment";
import incomingShipment from "../../../services/hires/incomingShipment";
import getInsuranceCompanies from "../../../services/insuranceCompanies/getInsuranceCompanies";
import calculateInsuranceCost from "../../../services/hires/calculateInsuranceCost";

const ShipmentDialog = ({ hire, handleRefresh, handleClose }) => {
  const [loadingCompanies, setLoadingCompanies] = useState(false);
  const [companies, setCompanies] = useState([]);
  const [company, setCompany] = useState("");
  const [loadingCost, setLoadingCost] = useState(false);
  const [cost, setCost] = useState(false);
  const [shippingNumber, setShippingNumber] = useState("");
  const [getInsurance, setGetInsurance] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const isPersonalPickup = hire.selectedPickupMethod === "PERSONAL_PICKUP";
  const isReturnPackage = hire.hireStatus === "PENDING_HIRE";

  useEffect(() => {
    if (isReturnPackage) {
      return;
    }

    setLoadingCompanies(true);
    getInsuranceCompanies()
      .then((companies) => {
        setCompanies(companies);
        setLoadingCompanies(false);
      })
      .catch(() => {
        handleClose();
      });
  }, []);

  useEffect(() => {
    if (isReturnPackage) {
      return;
    }

    setLoadingCost(true);
    calculateInsuranceCost(hire.id)
      .then((cost) => {
        setCost(cost);
        setLoadingCost(false);
      })
      .catch(() => {
        setLoadingCost(false);
        handleClose();
      });
  }, []);

  const handleConfirm = () => {
    setIsLoading(true);
    const postFunc = isReturnPackage ? incomingShipment : outgoingShipment;
    postFunc(hire.id, shippingNumber, getInsurance, company)
      .then(() => {
        handleRefresh();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <React.Fragment>
      <DialogContent>
        <DialogContentText>
          {isPersonalPickup
            ? "Potwierdź przekazanie wypożyczenia klikając w przycisk poniżej."
            : "Potwierdź nadanie wypożyczenia wpisując numer przesyłki w pole poniżej."}
        </DialogContentText>
        <Box
          width="100%"
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          {!isPersonalPickup && (
            <TextField
              label="Numer przesyłki"
              onChange={(e) => setShippingNumber(e.target.value)}
              placeholder="Wprowadź numer przesyłki"
            />
          )}
          {!isReturnPackage && (
            <React.Fragment>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={getInsurance}
                    onChange={(e) => setGetInsurance(e.target.checked)}
                    color="primary"
                  />
                }
                label={`Chcę wykupić dodatkowe ubezpieczenie. ${
                  cost ? `Koszt: ${cost} zł.` : ""
                }`}
              />
              <Collapse in={getInsurance}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">
                    Firma ubezpieczeniowa
                  </FormLabel>
                  {loadingCompanies && (
                    <CircularProgress style={{ marginTop: "10px" }} />
                  )}
                  <RadioGroup
                    value={company}
                    onChange={(e) => setCompany(e.target.value)}
                  >
                    {companies.map((company) => (
                      <FormControlLabel
                        key={company.id}
                        label={company.name}
                        value={company.id?.toString()}
                        control={<Radio />}
                      />
                    ))}
                  </RadioGroup>
                </FormControl>
              </Collapse>
            </React.Fragment>
          )}
        </Box>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={() => handleClose()}>
          Anuluj
        </Button>
        <Button
          color="primary"
          disabled={
            isLoading ||
            (!isPersonalPickup && !shippingNumber) ||
            (getInsurance && !company)
          }
          startIcon={isLoading && <CircularProgress size={16} />}
          onClick={() => handleConfirm()}
        >
          Potwierdź
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default ShipmentDialog;
