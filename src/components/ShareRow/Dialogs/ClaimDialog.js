import React, { useState } from "react";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import DatePicker from "../../DatePicker/DatePicker";
import LocationPicker from "../../LocationPicker/LocationPicker";
import { makeStyles } from "@material-ui/core/styles";
import createClaim from "../../../services/claims/createClaim";

const useStyles = makeStyles(() => ({
  inputRoot: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  input: {
    width: "300px",
    marginBottom: "20px",
  },
}));

const ClaimDialog = ({ hire, handleRefresh, handleClose }) => {
  const classes = useStyles();
  const [data, setData] = useState({ idPolicy: hire.policyId });
  const [isLoading, setIsLoading] = useState(false);

  const handleConfirm = () => {
    setIsLoading(true);
    createClaim(data)
      .then(() => {
        handleRefresh();
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const handleChange = (key, val) => {
    const tmpData = { ...data };
    tmpData[key] = val;
    console.log(tmpData);
    setData(tmpData);
  };

  return (
    <React.Fragment>
      <DialogContent>
        <DialogContentText>
          Zgłoś szkodę do wypożyczenia. Poprzez wypełnienie formularza
          potwierdzasz że szkoda nastąpiła podczas tego wypożyczenia.
        </DialogContentText>
        <Box className={classes.inputRoot}>
          <TextField
            variant="outlined"
            label="Opis powstałej szkody"
            multiline
            onChange={(e) => handleChange("damageDescription", e.target.value)}
            className={classes.input}
          />
          <TextField
            variant="outlined"
            label="Okoliczności wystąpienia szkody"
            multiline
            onChange={(e) => handleChange("circumstances", e.target.value)}
            className={classes.input}
          />
          <DatePicker
            minDate={new Date(hire.dateFrom)}
            maxDate={
              new Date(
                hire.dateToActual ? hire.dateToActual : hire.dateToPlanned
              )
            }
            handleChange={(val) => handleChange("claimDate", val)}
            className={classes.input}
            label="Data wystąpienia szkody"
          />
          <LocationPicker
            handleChange={handleChange}
            title="Miejsce wystąpienia szkody"
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={() => handleClose()}>
          Anuluj
        </Button>
        <Button
          color="primary"
          disabled={isLoading}
          startIcon={isLoading && <CircularProgress size={16} />}
          onClick={() => handleConfirm()}
        >
          Potwierdź
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default ClaimDialog;
