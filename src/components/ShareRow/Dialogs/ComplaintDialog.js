import React, { useState } from "react";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector, useDispatch } from "react-redux";
import makeComplaint from "../../../services/complaints/makeComplaint";
import { snackbarAction } from "../../../redux/modules/snackbar";

const useStyles = makeStyles(() => ({
  input: {
    width: "400px",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
}));

const ComplaintDialog = ({ hire, handleRefresh, handleClose }) => {
  const classes = useStyles();
  const reasons = useSelector((state) => state.enumReducer.complaintReasons);
  const [reason, setReason] = useState("");
  const [details, setDetails] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleConfirm = () => {
    setIsLoading(true);
    makeComplaint(hire.id, reason, details)
      .then(() => {
        handleRefresh();
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Pomyślnie złożono reklamację",
          })
        );
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <React.Fragment>
      <DialogContent>
        <DialogContentText>
          Potwierdź chęć złożenia reklamacji i wypełnij dane poniżej.
        </DialogContentText>
        <Box
          width="100%"
          display="flex"
          flexDirection="column"
          alignItems="center"
        >
          <Select
            label="Powód zgłoszenia"
            onChange={(e) => setReason(e.target.value)}
            className={classes.input}
          >
            {Object.keys(reasons).map((key) => (
              <MenuItem value={key} key={key}>
                {reasons[key]}
              </MenuItem>
            ))}
          </Select>
          <TextField
            label="Szczegóły zgłoszenia"
            onChange={(e) => setDetails(e.target.value)}
            placeholder="Wprowadź szczegóły zgłoszenia"
            className={classes.input}
            multiline
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={() => handleClose()}>
          Anuluj
        </Button>
        <Button
          color="primary"
          disabled={isLoading || !reason || !details}
          startIcon={isLoading && <CircularProgress size={16} />}
          onClick={() => handleConfirm()}
        >
          Potwierdź
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default ComplaintDialog;
