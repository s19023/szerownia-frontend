import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import MomentUtils from "@date-io/moment";
import {
  DatePicker as MuiDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";

import moment from "moment";
import "moment/locale/pl";
moment.locale("pl");

const useStyles = makeStyles(() => ({
  input: {
    width: "100%",
  },
}));

const DatePicker = ({ label, handleChange, id, value, ...props }) => {
  const classes = useStyles();
  const [date, setDate] = useState(value ? value : null);

  const onChange = (date) => {
    setDate(date);
    handleChange(moment(date).format("YYYY-MM-DD"));
  };

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <MuiDatePicker
        id={id}
        value={date}
        onChange={onChange}
        inputVariant="outlined"
        label={label}
        disablePast={!props.minDate}
        views={["date"]}
        format="DD.MM.YYYY"
        className={classes.input}
        readOnly={value}
        {...props}
      />
    </MuiPickersUtilsProvider>
  );
};

export default DatePicker;
