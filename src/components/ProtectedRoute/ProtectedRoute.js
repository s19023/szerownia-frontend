import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useSelector } from "react-redux";

const ProtectedRoute = ({ noLogin, allowedRoles, children, ...props }) => {
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const role = useSelector((state) => state.userReducer.role);

  const validateAccess = () => {
    if (isLoggedIn && noLogin) {
      return false;
    }

    if (!isLoggedIn && !noLogin) {
      return false;
    }

    if (allowedRoles) {
      return allowedRoles.includes(role);
    }

    return true;
  };

  return (
    <Route
      {...props}
      render={() => (validateAccess() ? children : <Redirect to="/" />)}
    />
  );
};

export default ProtectedRoute;
