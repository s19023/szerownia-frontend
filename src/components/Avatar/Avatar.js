import React from "react";
import MuiAvatar from "@material-ui/core/Avatar";
import { makeStyles } from "@material-ui/core/styles";
import { images } from "../../config/urls";

const useStyles = makeStyles((theme) => ({
  avatar: {
    boxShadow: theme.shadows[5],
    width: (props) => props.size + "px",
    height: (props) => props.size + "px",
    marginRight: (props) => props.margin + "px",
  },
}));

const Avatar = ({ imageId, firstName, lastName, size, margin, ...props }) => {
  const classes = useStyles({
    size: size ? size : 40,
    margin: margin ? margin : 0,
  });

  return (
    <MuiAvatar
      src={`${images.get}/${imageId}`}
      alt={`${firstName} ${lastName}`}
      className={classes.avatar}
      {...props}
    >
      {firstName.charAt(0) + lastName.charAt(0)}
    </MuiAvatar>
  );
};

export default Avatar;
