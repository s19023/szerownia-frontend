import React, { useState, useEffect, useRef } from "react";
import Box from "@material-ui/core/Box";
import FormCard from "../FormCard/FormCard";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import ArrowDown from "@material-ui/icons/ArrowDownward";
import InputAdornment from "@material-ui/core/InputAdornment";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";

import { useDispatch } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";

import getCategoriesList from "../../services/categories/getCategoriesList";
import getFeaturesForCategory from "../../services/features/getFeaturesForCategory";
import suggestCategory from "../../services/products/suggestCategory";
import createProduct from "../../services/products/createProduct";
import ItemFeatureRow from "./ItemFeatureRow";
import CategorySuggestion from "./CategorySuggestion";
import getProduct from "../../services/products/getProduct";
import updateProduct from "../../services/products/updateProduct";
import {
  hasErrors,
  validateForm,
  validateNumber,
} from "../../utils/validateForm";
import { types } from "./types";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    alignItems: "center",
    minWidth: "500px",
  },
  inputRow: {
    padding: "10px 0",
  },
  input: {
    width: "300px",
  },
  subheader: {
    background: theme.palette.background.paper,
    fontWeight: "bold",
    fontSize: "16px",
  },
  item: {
    textIndent: "10px",
  },
  featureTitle: {
    width: "100%",
    padding: "10px 0",
  },
  collapse: {
    width: "100%",
  },
  icon: {
    color: theme.palette.primary.main,
    transform: (props) => (props.open ? "rotate(180deg)" : "rotate(0deg)"),
    transition: theme.transitions.create(
      "transform",
      theme.transitions.duration.standard
    ),
  },
}));

const marks = Array(5)
  .fill(0)
  .map((elem, index) => ({
    value: index + 1,
    label: index === 0 ? "Bardzo zły" : index === 4 ? "Bardzo dobry" : "",
  }));

const createValues = (isSearchAd) => {
  const obj = {};
  fields.forEach(
    (field) =>
      (!isSearchAd || field.showInSearch) &&
      (obj[field.id] = field.inputType === "slider" ? 3 : "")
  );
  return obj;
};

const AddNewItem = ({
  handleClose,
  handleSuccess,
  collapseOpen,
  isSearchAd,
  id,
}) => {
  const [values, setValues] = useState(createValues(isSearchAd));
  const [showMore, setShowMore] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [errors, setErrors] = useState({});
  const [featuresErrors, setFeatureErrors] = useState({});
  const [categories, setCategories] = useState([]);
  const [features, setFeatures] = useState([]);
  const [featureValues, setFeatureValues] = useState([]);
  const [suggestions, setSuggestions] = useState([]);
  const classes = useStyles({ open: showMore });
  const timeout = useRef(null);
  const dispatch = useDispatch();

  useEffect(() => {
    getCategoriesList().then((categories) => setCategories(categories));
  }, []);

  useEffect(() => {
    if (!id) {
      return;
    }

    getProduct(id).then((product) => {
      const tmpValues = { ...product };
      tmpValues.idCategory = product.categoryReferenceDTO.idCategory;
      getFeatures(tmpValues.idCategory);
      setValues(tmpValues);
      setFeatureValues(product.featureOccurrences);
    });
  }, [id]);

  useEffect(() => {
    if (!collapseOpen) {
      setValues(createValues(isSearchAd));
      setFeatures([]);
      setFeatureValues([]);
      setErrors({});
      setFeatureErrors({});
    }
  }, [collapseOpen]);

  useEffect(() => {
    clearTimeout(timeout.current);
    const name = values.name;
    if (name.length >= 3) {
      timeout.current = setTimeout(() =>
        suggestCategory(name).then(
          (suggestions) => setSuggestions(suggestions),
          300
        )
      );
    } else {
      setSuggestions([]);
    }
  }, [values.name]);

  const handleSave = () => {
    const filterFields = isSearchAd
      ? fields.filter((field) => field.showInSearch)
      : fields;
    const errors = validateForm(filterFields, values);
    const isValid = !hasErrors(errors);

    const featureErrors = {};
    features.forEach((feature) => {
      const outerValue = featureValues.find(
        (val) => val.idFeature === feature.idFeature
      );
      const value = outerValue?.[types[feature.type]];
      if (feature.required && feature.type !== "BOOLEAN" && !value) {
        featureErrors[feature.idFeature] = "Pole jest wymagane";
      } else if (!feature.required && outerValue && !value) {
        featureErrors[feature.idFeature] = "Pole nie zostało wypełnione";
        return;
      } else if (!feature.required && !outerValue) {
        return;
      }

      if (feature.type === "DOUBLE" || feature.type === "INTEGER") {
        featureErrors[feature.idFeature] = validateNumber(
          value,
          { minValue: 0 },
          feature.type === "INTEGER" ? "number" : "float"
        );
      }
    });

    let featuresValid = true;
    Object.keys(featureErrors).forEach((key) => {
      if (featureErrors[key]) {
        featuresValid = false;
      }
    });

    setErrors(errors);
    setFeatureErrors(featureErrors);

    if (!isValid || !featuresValid) {
      return;
    }
    setDisabled(true);
    const postFunc = id ? updateProduct : createProduct;
    postFunc(values, featureValues, id ? id : isSearchAd)
      .then(() => {
        handleSuccess();
        setDisabled(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: id ? "Zaktualizowano przedmiot" : "Dodano przedmiot",
          })
        );
      })
      .catch(() => {
        setDisabled(false);
        dispatch(snackbarAction.openSnackbar({ error: true }));
      });
  };

  const getFeatures = (idCategory) => {
    getFeaturesForCategory(idCategory).then((features) =>
      setFeatures(features)
    );
  };

  const changeCategory = (key, value, field) => {
    handleChange(key, value, field);
    getFeatures(value);
  };

  const updateFeature = (id, type, value, isCheckbox) => {
    const tmpFeatures = [...featureValues].filter(
      (feature) => feature.idFeature !== id
    );

    if (isCheckbox && value) {
      const feature = {
        idFeature: id,
      };
      tmpFeatures.push(feature);
    } else if (!isCheckbox) {
      const feature = {
        [type]: isCheckbox && value ? "" : value,
        idFeature: id,
      };
      tmpFeatures.push(feature);
    }

    setFeatureValues(tmpFeatures);
  };

  const handleChange = (key, value, field) => {
    const tmpValues = { ...values };
    tmpValues[key] = field.type === "float" ? value.replace(",", ".") : value;
    setValues(tmpValues);
  };

  const getInput = (field) => {
    const value = values[field.id];

    switch (field.inputType) {
      case "select": {
        return (
          <TextField
            id={`product-${field.id}`}
            select
            label="Kategoria"
            variant="outlined"
            value={value}
            className={classes.input}
            onChange={(e) => changeCategory(field.id, e.target.value, field)}
            error={!!errors[field.id]}
            helperText={errors[field.id]}
          >
            {categories.map((category) => renderCategory(category))}
          </TextField>
        );
      }
      case "slider": {
        return (
          <React.Fragment>
            <Typography>{field.name}</Typography>
            <Slider
              id={field.id}
              marks={marks}
              min={1}
              max={5}
              step={1}
              value={value ? value : 3}
              className={classes.input}
              valueLabelDisplay="auto"
              onChange={(e, v) => handleChange(field.id, v, field)}
              error={!!errors[field.id]}
              helperText={errors[field.id]}
            />
          </React.Fragment>
        );
      }
      case "suggest": {
        return (
          <CategorySuggestion
            suggestions={suggestions}
            setSuggestion={(val) => changeCategory("idCategory", val, field)}
          />
        );
      }
      default:
        return (
          <TextField
            id={`product-${field.id}`}
            variant="outlined"
            className={classes.input}
            label={field.name}
            value={value}
            onChange={(e) => handleChange(field.id, e.target.value, field)}
            InputProps={{
              endAdornment: field.endAdornment ? (
                <InputAdornment position="end">
                  {field.endAdornment}
                </InputAdornment>
              ) : undefined,
            }}
            error={!!errors[field.id]}
            helperText={errors[field.id]}
          />
        );
    }
  };

  const renderCategory = (category) => {
    return [
      <ListSubheader
        value={""}
        onClick={(e) => e.preventDefault()}
        className={classes.subheader}
        key={`category-${category.idCategory}`}
      >
        {category.name}
      </ListSubheader>,
      <Divider />,
      category.subCategories.map((subCategory) => (
        <MenuItem
          className={classes.item}
          key={`sub-${subCategory.idCategory}`}
          value={subCategory.idCategory}
        >
          {subCategory.name}
        </MenuItem>
      )),
    ];
  };

  return (
    <Box className={classes.root}>
      <FormCard
        width="100%"
        flexGrow="1"
        title={id ? "Edytuj przedmiot" : "Dodaj nowy przedmiot"}
      >
        <Box className={classes.root}>
          {fields.map((field) => {
            if (isSearchAd && !field.showInSearch) {
              return null;
            }
            return (
              <Box
                key={field.id}
                className={
                  field.type !== "suggest" || suggestions.length > 0
                    ? classes.inputRow
                    : ""
                }
              >
                {getInput(field)}
              </Box>
            );
          })}
          {features.length > 0 && (
            <React.Fragment>
              <Typography className={classes.featureTitle}>
                Dodaj cechy produktu
              </Typography>
              {features
                .filter((feature) => feature.required)
                .map((feature) => (
                  <ItemFeatureRow
                    key={feature.idFeature}
                    feature={feature}
                    required
                    updateFeature={updateFeature}
                    value={featureValues.find(
                      (occurrence) => occurrence.idFeature === feature.idFeature
                    )}
                    error={featuresErrors[feature.idFeature]}
                  />
                ))}
              <Button
                id="open-more-item"
                color="primary"
                startIcon={<ArrowDown className={classes.icon} />}
                onClick={() => setShowMore(!showMore)}
              >
                {showMore ? "Pokaż mniej" : "Pokaż więcej"}
              </Button>
              <Collapse in={showMore} className={classes.collapse}>
                {features
                  .filter((feature) => !feature.required)
                  .map((feature) => (
                    <ItemFeatureRow
                      key={feature.idFeature}
                      feature={feature}
                      updateFeature={updateFeature}
                      value={featureValues.find(
                        (occurrence) =>
                          occurrence.idFeature === feature.idFeature
                      )}
                      error={featuresErrors[feature.idFeature]}
                    />
                  ))}
              </Collapse>
            </React.Fragment>
          )}
          <Box className={classes.inputRow}>
            <Button
              id="conform-item"
              className={classes.input}
              variant="contained"
              onClick={handleClose}
            >
              Anuluj
            </Button>
          </Box>
          <Box className={classes.inputRow}>
            <Button
              id="cancel-item"
              className={classes.input}
              variant="contained"
              color="primary"
              onClick={() => handleSave()}
              disabled={disabled}
            >
              Zapisz zmiany
            </Button>
          </Box>
        </Box>
      </FormCard>
    </Box>
  );
};

export default AddNewItem;
