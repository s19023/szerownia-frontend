export const fields = [
  {
    id: "name",
    name: "Nazwa",
    showInSearch: true,
    isRequired: true,
  },
  {
    id: "categorySuggestion",
    showInSearch: true,
    inputType: "suggest",
  },
  {
    id: "idCategory",
    name: "Kategoria",
    inputType: "select",
    showInSearch: true,
    isRequired: true,
  },
  {
    id: "make",
    name: "Marka",
    isRequired: true,
  },
  {
    id: "model",
    name: "Model",
    isRequired: true,
  },
  {
    id: "estimatedValue",
    name: "Szacowana wartość",
    endAdornment: "zł",
    type: "float",
    isRequired: true,
  },
  {
    id: "condition",
    name: "Stan",
    inputType: "slider",
    type: "number",
    showInSearch: true,
  },
];
