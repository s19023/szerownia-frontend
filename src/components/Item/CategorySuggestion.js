import React from "react";
import Collapse from "@material-ui/core/Collapse";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
  },
  button: {
    "&:not(:last-of-type)": {
      marginRight: "10px",
    },
  },
}));

const CategorySuggestion = ({ suggestions, setSuggestion }) => {
  const classes = useStyles();

  return (
    <Box width="300px">
      <Collapse in={suggestions.length > 0} appear>
        <Typography>Propozycje kategorii</Typography>
        <Box className={classes.root}>
          {suggestions.map((suggestion) => (
            <Button
              color="primary"
              key={suggestion.categoryId}
              onClick={() => setSuggestion(suggestion.categoryId)}
            >
              {suggestion.categoryName}
            </Button>
          ))}
        </Box>
      </Collapse>
    </Box>
  );
};

export default CategorySuggestion;
