import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core/styles";
import { types } from "./types";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    padding: "10px 0",
    display: "flex",
    alignItems: "center",
  },
  title: {
    fontWeight: "bold",
    width: "50%",
    color: (props) =>
      props.value ? theme.palette.text.primary : theme.palette.text.disabled,
  },
  input: {
    flexGrow: "1",
  },
}));

const ItemFeatureRow = ({ feature, updateFeature, required, value, error }) => {
  const type = types[feature.type];
  const classes = useStyles({ value: value || required });

  return (
    <Box className={classes.root}>
      {!required && (
        <Checkbox
          checked={!!value}
          onChange={(e) =>
            updateFeature(
              feature.idFeature,
              type,
              !value,
              true
            )
          }
        />
      )}
      <Typography className={classes.title}>
        {feature.name}
        {required && "*"}
      </Typography>
      {feature.type !== "BOOLEAN" ? (
        <TextField
          id={`feature-${feature.idFeature}`}
          className={classes.input}
          onChange={(e) =>
            updateFeature(feature.idFeature, type, e.target.value)
          }
          value={value ? value[type] : ""}
          disabled={!required && !value}
          error={!!error}
          helperText={error}
        />
      ) : (
        <Checkbox
          checked={value ? !!value[type] : false}
          id={`feature-${feature.idFeature}`}
          color="primary"
          onChange={(e) =>
            updateFeature(feature.idFeature, type, e.target.checked)
          }
          disabled={!required && !value}
        />
      )}
    </Box>
  );
};

export default ItemFeatureRow;
