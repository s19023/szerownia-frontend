export const types = {
  BOOLEAN: "booleanValue",
  INTEGER: "decimalNumberValue",
  DOUBLE: "floatingNumberValue",
  TEXT: "textValue",
};