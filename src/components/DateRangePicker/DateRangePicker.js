import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import MomentUtils from "@date-io/moment";
import {
  DatePicker as MuiDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import clsx from "clsx";
import getAvailableDates from "../../services/rentalAds/getAvailableDates";
import moment from "moment";
import "moment/locale/pl";
moment.locale("pl");

const useStyles = makeStyles((theme) => ({
  input: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  day: {
    width: "36px",
    height: "36px",
    padding: "0 2px",
    color: "inherit",
  },
  button: {
    fontSize: theme.typography.caption.fontSize,
    width: "100%",
    height: "100%",
    color: "inherit",
  },
  today: {
    color: theme.palette.primary.main,
  },
  nonCurrentMonthDay: {
    color: theme.palette.text.disabled,
  },
  highlightNonCurrentMonth: {
    color: theme.palette.text.secondary,
  },
  highlight: {
    backgroundColor: theme.palette.primary.main,
    color: "#FFFFFF",
    borderRadius: "0",
  },
  highlightStart: {
    borderTopLeftRadius: "50%",
    borderBottomLeftRadius: "50%",
  },
  highlightEnd: {
    borderTopRightRadius: "50%",
    borderBottomRightRadius: "50%",
  },
  highlightOne: {
    borderRadius: "50%",
    backgroundColor: theme.palette.primary.main,
    color: "#FFFFFF",
  },
  highlightSelected: {
    borderRadius: "50%",
    border: "1px dashed " + theme.palette.primary.main,
  },
}));

const inifiniteRange = 2;

const DateRangePicker = ({
  id,
  updateMinDate,
  updateMaxDate,
  downloadDates,
  minStartDate,
  disabledDates,
}) => {
  const classes = useStyles();
  const [minDate, setMinDate] = useState(null);
  const [maxDate, setMaxDate] = useState(null);
  const [prevMaxDate, setPrevDate] = useState(true);
  const [availableDates, setAvailableDates] = useState([]);
  const [isLoading, setIsLoading] = useState(!!downloadDates);

  const onChange = (date) => {
    if (!isDisabled(date)) {
      if (prevMaxDate || date.isBefore(minDate, "day")) {
        setMinDate(date);
        setMaxDate(null);
        setPrevDate(false);
      } else {
        const tmpDate = moment(minDate);
        let canProceed = true;

        if (downloadDates) {
          while (downloadDates && tmpDate.isBefore(date) && canProceed) {
            canProceed = availableDates.includes(tmpDate.format("YYYY-MM-DD"));
            tmpDate.add(1, "day");
          }
        } else if (disabledDates) {
          while (tmpDate.isBefore(date) && canProceed) {
            canProceed = !disabledDates.includes(tmpDate.format("YYYY-MM-DD"));
            tmpDate.add(1, "day");
          }
        }

        if (canProceed) {
          setMaxDate(date);
          setPrevDate(true);
        } else {
          setMinDate(date);
          setMaxDate(null);
        }
      }
    }
  };

  useEffect(() => {
    updateMinDate(minDate);
  }, [minDate]);

  useEffect(() => {
    updateMaxDate(maxDate);
  }, [maxDate]);

  useEffect(() => {
    if (minStartDate) {
      setMinDate(null);
    }
  }, [minStartDate]);

  useEffect(() => {
    if (!downloadDates) {
      return;
    }

    getAvailableDates(id)
      .then((response) => {
        const availableDates = [];
        response.forEach((slot) => {
          const endDate = slot.to
            ? moment(slot.to).add(1, "day")
            : moment().add(inifiniteRange, "years");
          let currDate = moment(slot.from);

          while (endDate.isAfter(currDate, "day")) {
            availableDates.push(currDate.format("YYYY-MM-DD"));
            console.log(currDate.format("YYYY-MM-DD"));
            currDate.add(1, "day");
          }
        });

        setAvailableDates(availableDates);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  const isDisabled = (day) => {
    return (
      (downloadDates && !availableDates.includes(day.format("YYYY-MM-DD"))) ||
      (disabledDates && disabledDates.includes(day.format("YYYY-MM-DD"))) ||
      day.isBefore(minStartDate ? moment(minStartDate) : moment(), "day")
    );
  };

  const renderDay = (day, selectedDate, isInCurrentMonth) => {
    const isBetween =
      day.isSameOrAfter(minDate, "day") && day.isSameOrBefore(maxDate, "day");
    const isStartDate = day.isSame(minDate, "day");
    const isEndDate = day.isSame(maxDate, "day");
    const isOne = day.isSame(minDate, "day") && day.isSame(maxDate, "day");

    const wrapperClassName = clsx(classes.day, {
      [classes.highlight]: isBetween && !isOne,
      [classes.highlightStart]: isStartDate && !isOne,
      [classes.highlightEnd]: isEndDate && !isOne,
    });

    const buttonClassName = clsx(classes.button, {
      [classes.today]: day.isSame(moment(), "day") && !isBetween,
      [classes.nonCurrentMonthDay]: !isInCurrentMonth,
      [classes.highlightNonCurrentMonth]: !isInCurrentMonth && isBetween,
      [classes.highlightSelected]: isStartDate && !maxDate,
      [classes.highlightOne]: isOne,
    });

    return (
      <Box className={wrapperClassName}>
        <IconButton className={buttonClassName} disabled={isDisabled(day)}>
          <span>{day.format("D")}</span>
        </IconButton>
      </Box>
    );
  };

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <MuiDatePicker
        value={prevMaxDate ? maxDate : minDate}
        loading={isLoading}
        rightArrowButtonProps={{ disabled: isLoading }}
        leftArrowButtonProps={{ disabled: isLoading }}
        onChange={onChange}
        variant="static"
        orientation="landscape"
        disablePast
        maxDate={moment().add(inifiniteRange, "years").toDate()}
        disableToolbar
        renderDay={renderDay}
      />
    </MuiPickersUtilsProvider>
  );
};

export default DateRangePicker;
