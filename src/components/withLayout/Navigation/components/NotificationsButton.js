import React, { useState, useRef, useEffect } from "react";
import { useSelector } from "react-redux";
import Badge from "@material-ui/core/Badge";
import IconButton from "@material-ui/core/IconButton";
import Popover from "@material-ui/core/Popover";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import NotificationsIcon from "@material-ui/icons/Notifications";
import { makeStyles } from "@material-ui/core/styles";
import Loader from "../../../Loader/Loader";
import getNotifications from "../../../../services/notifications/getNotifications";
import getUnreadNotifications from "../../../../services/notifications/getUnreadNotifications";
import clsx from "clsx";
import moment from "moment";

const anchorSettings = {
  anchorOrigin: {
    vertical: "top",
    horizontal: "center",
  },
  transformOrigin: {
    vertical: "top",
    horizontal: "center",
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    height: "36px",
    width: "36px",
    marginRight: "10px",
    [theme.breakpoints.down("sm")]: {
      margin: "0px",
    },
  },
  notificationsRoot: {
    width: "300px",
    [theme.breakpoints.down(375)]: {
      width: "100%",
    },
  },
  titleRoot: {
    padding: "10px",
  },
  title: {
    fontWeight: "bold",
    fontFamily: "Poppins",
    textAlign: "center",
  },
  borderBottom: {
    borderBottom: "1px solid " + theme.palette.divider,
  },
  listRoot: {
    maxHeight: "300px",
    overflow: "auto",
    marginBottom: "10px",
  },
  notification: {
    padding: "5px",
    display: "flex",
    "&:not(:last-child)": {
      borderBottom: "1px solid " + theme.palette.divider,
    },
  },
  content: {
    fontSize: "14px",
  },
  unread: {
    fontWeight: "bold",
  },
  date: {
    fontSize: "12px",
    color: theme.palette.text.disabled,
  },
  unreadDotRoot: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    minWidth: "20px",
  },
  unreadDot: {
    borderRadius: "50%",
    width: "12px",
    height: "12px",
    backgroundColor: theme.palette.primary.main,
    boxShadow: theme.shadows[3],
  },
}));

const NotificationsButton = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(null);
  const [unread, setUnread] = useState(0);
  const [notifications, setNotifications] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const timeout = useRef();

  useEffect(() => {
    if (isLoggedIn) {
      downloadUnread();
    } else {
      clearTimeout(timeout.current);
    }
    return () => clearTimeout(timeout.current);
  }, [isLoggedIn]);

  useEffect(() => {
    downloadNotifications();
  }, [open]);

  const toggleOpen = (e) => {
    setOpen(open ? null : e.currentTarget);
  };

  const downloadUnread = () => {
    clearTimeout(timeout.current);
    getUnreadNotifications()
      .then((count) => {
        setUnread(count);
      })
      .finally(() => {
        timeout.current = setTimeout(() => {
          downloadUnread();
        }, 5000);
      });
  };

  const downloadNotifications = () => {
    if (!open || isLoading) {
      return;
    }

    setIsLoading(true);
    getNotifications()
      .then((notifications) => {
        setNotifications(notifications.results);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <React.Fragment>
      <IconButton className={classes.root} onClick={toggleOpen} color="inherit">
        <Badge badgeContent={unread} color="secondary">
          <NotificationsIcon />
        </Badge>
      </IconButton>
      <Popover
        open={!!open}
        anchorEl={open}
        onClose={toggleOpen}
        {...anchorSettings}
      >
        <Box className={classes.notificationsRoot}>
          {notifications.length === 0 || isLoading ? (
            <Box padding="10px" overflow="hidden">
              <Loader
                array={notifications}
                isLoading={isLoading}
                label="Brak nieodczytanych powiadomień"
              />
            </Box>
          ) : (
            <React.Fragment>
              <Box className={clsx(classes.borderBottom, classes.titleRoot)}>
                <Typography className={classes.title}>
                  Ostatnie powiadomienia
                </Typography>
              </Box>
              <Box className={clsx(classes.borderBottom, classes.listRoot)}>
                {notifications.map((n) => (
                  <Box key={n.id} className={classes.notification}>
                    <Box flexGrow="1">
                      <Typography
                        className={clsx(classes.content, {
                          [classes.unread]: n.notificationStatus === "UNREAD",
                        })}
                      >
                        {n.content}
                      </Typography>
                      <Typography className={classes.date}>
                        {moment(n.createTime).format("DD.MM.YYYY HH:mm")}
                      </Typography>
                    </Box>
                    {n.notificationStatus === "UNREAD" && (
                      <Box className={classes.unreadDotRoot}>
                        <div className={classes.unreadDot} />
                      </Box>
                    )}
                  </Box>
                ))}
              </Box>
            </React.Fragment>
          )}
        </Box>
      </Popover>
    </React.Fragment>
  );
};

export default NotificationsButton;
