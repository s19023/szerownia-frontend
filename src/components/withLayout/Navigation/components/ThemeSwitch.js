import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Zoom from "@material-ui/core/Zoom";
import SunIcon from "@material-ui/icons/WbSunny";
import MoonIcon from "@material-ui/icons/Brightness2";
import { makeStyles } from "@material-ui/core/styles";
import { themeAction } from "../../../../redux/modules/theme";
import { useDispatch, useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  icon: {
    position: "absolute",
  },
  root: {
    height: "36px",
    width: "36px",
    marginRight: "10px",
    position: "relative",
    [theme.breakpoints.down("sm")]: {
      margin: "0px",
    },
  },
}));

const ThemeSwitch = () => {
  const classes = useStyles();
  const theme = useSelector((state) => state.themeReducer.theme);
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(
      themeAction.changeTheme({
        theme: theme === "dark" ? "light" : "dark",
        hasChanged: true,
      })
    );
  };

  return (
    <IconButton
      id="theme-switch"
      className={classes.root}
      onClick={() => handleClick()}
      color="inherit"
    >
      <Zoom in={theme === "light"}>
        <SunIcon className={classes.icon} />
      </Zoom>
      <Zoom in={theme === "dark"}>
        <MoonIcon className={classes.icon} />
      </Zoom>
    </IconButton>
  );
};

export default ThemeSwitch;
