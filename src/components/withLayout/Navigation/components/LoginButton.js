import React, { useEffect, useRef } from "react";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import PersonIcon from "@material-ui/icons/Person";
import AddIcon from "@material-ui/icons/Add";
import SettingsIcon from "@material-ui/icons/Settings";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import MailIcon from "@material-ui/icons/Mail";
import { makeStyles } from "@material-ui/core/styles";

import { useHistory, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { userAction } from "../../../../redux/modules/user";

const useStyles = makeStyles((theme) => ({
  addNewButton: {
    marginLeft: "10px",
    backgroundColor: theme.palette.background.paper,
    boxShadow: "none",
    color: theme.palette.text.primary,
    "&:hover": {
      backgroundColor: theme.palette.background.default,
    },
  },
}));

const redirects = {
  ADMIN: "/admin",
  MODERATOR: "/admin",
  INSURANCE_ADJUSTER: "/insurance/adjuster",
  APPRAISER: "/insurance/appraiser",
};

const allowedRoutes = {
  INSURANCE_ADJUSTER: [
    "/insurance/adjuster",
    "/insurance/claim",
    "/user",
    "/ad",
  ],
  APPRAISER: ["/insurance/appraiser", "/insurance/claim", "/user", "/ad"],
  MODERATOR: ["/admin", "/user", "/ad"],
};

const LoginButton = () => {
  const classes = useStyles();
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const isRefreshingToken = useSelector(
    (state) => state.userReducer.isRefreshingToken
  );
  const accessToken = useSelector((state) => state.userReducer.accessToken);
  const refreshToken = useSelector((state) => state.userReducer.refreshToken);
  const refreshTokenError = useSelector(
    (state) => state.userReducer.refreshTokenError
  );
  const exp = useSelector((state) => state.userReducer.exp);
  const role = useSelector((state) => state.userReducer.role);
  const timer = useRef();
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const isTokenExpired = () => {
    const date = new Date();
    const currentTime = date.getTime();

    return !!(exp && currentTime > exp);
  };

  const handleLogout = () => {
    dispatch(userAction.logout());
  };

  useEffect(() => {
    clearInterval(timer.current);
    timer.current = setInterval(() => {
      !isRefreshingToken &&
        isLoggedIn &&
        isTokenExpired() &&
        dispatch(userAction.refreshToken({ accessToken, refreshToken }));
    }, 1000);
    return () => {
      clearInterval(timer.current);
    };
  }, [isLoggedIn, exp]);

  useEffect(() => {
    if (refreshTokenError) {
      clearInterval(timer.current);
      dispatch(userAction.logout());
    }
  }, [refreshTokenError]);

  useEffect(() => {
    if (isLoggedIn && role !== "USER") {
      const routes = allowedRoutes[role];
      if (
        (routes &&
          !routes.find((route) => history.location.pathname.includes(route))) ||
        (!routes && !history.location.pathname.includes(redirects[role]))
      ) {
        history.push(redirects[role]);
      }
    }
  }, [isLoggedIn, location]);

  const renderLeftButton = () => {
    if (!isLoggedIn) {
      return (
        <Button
          id="login"
          color="inherit"
          onClick={() => history.push("/login")}
        >
          Zaloguj się
        </Button>
      );
    } else {
      return (
        <Button
          id="logout"
          color="inherit"
          onClick={() => handleLogout()}
          startIcon={<ExitToAppIcon />}
        >
          Wyloguj się
        </Button>
      );
    }
  };

  const renderRightButton = () => {
    switch (role) {
      case "USER": {
        return (
          <React.Fragment>
            <IconButton
              id="messages-inbox"
              onClick={() => history.push("/messages")}
              color="inherit"
            >
              <MailIcon />
            </IconButton>
            <Button
              id="my-profile"
              startIcon={<PersonIcon />}
              color="inherit"
              onClick={() => history.push("/my-profile")}
            >
              Twoje konto
            </Button>
            <Button
              id="create-ad"
              className={classes.addNewButton}
              startIcon={<AddIcon />}
              variant="contained"
              onClick={() => history.push("/create-ad")}
            >
              Dodaj ogłoszenie
            </Button>
          </React.Fragment>
        );
      }
      case "ADMIN":
      case "MODERATOR": {
        return (
          <Button
            id="admin-panel"
            className={classes.addNewButton}
            startIcon={<SettingsIcon />}
            variant="contained"
            onClick={() => history.push("/admin")}
          >
            Panel {role === "ADMIN" ? "administatora" : "moderatora"}
          </Button>
        );
      }
      case "APPRAISER":
      case "INSURANCE_ADJUSTER": {
        return (
          <Button
            id="admin-panel"
            className={classes.addNewButton}
            startIcon={<SettingsIcon />}
            variant="contained"
            onClick={() => history.push("/insurance/adjuster")}
          >
            Panel {role === "APPRAISER" ? "rzeczoznawcy" : "likwidatora"}
          </Button>
        );
      }
      default:
        return null;
    }
  };

  return (
    <React.Fragment>
      {renderLeftButton()}
      {renderRightButton()}
    </React.Fragment>
  );
};

export default LoginButton;
