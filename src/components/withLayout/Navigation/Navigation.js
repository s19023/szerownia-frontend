import React, { useState, useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import Zoom from "@material-ui/core/Zoom";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import Collapse from "@material-ui/core/Collapse";
import ThemeSwitch from "./components/ThemeSwitch";
import { makeStyles } from "@material-ui/core/styles";
import LoginButton from "./components/LoginButton";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import NotificationsButton from "./components/NotificationsButton";

const useStyles = makeStyles((theme) => ({
  text: {
    fontSize: "24px",
    flexGrow: "1",
    color: "#FAFAFA",
  },
  name: {
    fontFamily: "Poppins, sans-serif",
  },
  iconButton: {
    position: "relative",
  },
  spacer: {
    width: "24px",
    height: "24px",
  },
  icon: {
    position: "absolute",
    color: theme.palette.common.white,
  },
  collapse: {
    padding: "16px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const Navigation = () => {
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const role = useSelector((state) => state.userReducer.role);
  const [open, setOpen] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    if (!isMobile) {
      setOpen(false);
    }
  }, [isMobile]);

  return (
    <AppBar>
      <Toolbar>
        <Typography className={classes.text}>
          <Link
            id="home-button"
            color="inherit"
            underline="none"
            href="/"
            onClick={(e) => {
              e.preventDefault();
              history.push("/");
            }}
          >
            <strong className={classes.name}>Szerownia</strong> - dziel się!
          </Link>
        </Typography>
        <Hidden smDown>
          <ThemeSwitch />
          {isLoggedIn && role === "USER" && <NotificationsButton />}
          <LoginButton />
        </Hidden>
        <Hidden mdUp>
          <IconButton
            onClick={() => setOpen(!open)}
            className={classes.iconButton}
          >
            <div className={classes.spacer} />
            <Zoom in={open}>
              <CloseIcon className={classes.icon} />
            </Zoom>
            <Zoom in={!open}>
              <MenuIcon className={classes.icon} />
            </Zoom>
          </IconButton>
        </Hidden>
      </Toolbar>
      <Collapse in={open}>
        <Box className={classes.collapse}>
          <ThemeSwitch />
          {isLoggedIn && role === "USER" && <NotificationsButton />}
          <LoginButton />
        </Box>
      </Collapse>
    </AppBar>
  );
};

export default Navigation;
