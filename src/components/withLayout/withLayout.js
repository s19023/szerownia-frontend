import React from "react";
import Box from "@material-ui/core/Box";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";
import { BrowserRouter as Router } from "react-router-dom";
import Navigation from "./Navigation/Navigation";
import Footer from "./Footer/Footer";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    minHeight: "100vh",
    padding: "80px 5% 20px",
    boxSizing: "border-box",
    backgroundColor: theme.palette.background.default,
  },
}));

const withLayout = (WrappedComponent) => {
  const HocComponent = ({ ...props }) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const snackbarOpen = useSelector((state) => state.snackbarReducer.open);
    const error = useSelector((state) => state.snackbarReducer.error);
    const message = useSelector((state) => state.snackbarReducer.message);

    const handleClose = () => {
      dispatch(snackbarAction.closeSnackbar());
    };

    return (
      <Router>
        <Navigation />
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          open={snackbarOpen}
          onClose={handleClose}
          autoHideDuration={5000}
        >
          <Alert
            severity={error ? "error" : "success"}
            elevation={6}
            variant="filled"
          >
            {message}
          </Alert>
        </Snackbar>
        <Box className={classes.root}>
          <WrappedComponent {...props} />
        </Box>
        <Footer />
      </Router>
    );
  };

  return HocComponent;
};

export default withLayout;
