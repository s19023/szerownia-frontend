import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import preval from "preval.macro";
import moment from "moment";
import logo from "../../../static/szerownia_logo_512_transparent.png";

const buildDate = preval`module.exports = new Date().getTime()`;

const loggedInLinks = [
  {
    to: "/my-profile/ads",
    name: "Twoje konto",
    id: "profile",
  },
  {
    to: "/create-ad",
    name: "dodaj ogłoszenie",
    id: "newAd",
  },
];

const notLoggedIn = [
  {
    to: "/login",
    name: "Zaloguj się",
    id: "login",
  },
  {
    to: "/register",
    name: "Załóż konto",
    id: "register",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "200px",
    width: "100%",
    padding: "20px 100px",
    boxSizing: "border-box",
    background: theme.palette.primary.main,
    [theme.breakpoints.down("md")]: {
      padding: "20px 32px",
    },
    [theme.breakpoints.down("sm")]: {
      padding: "16px",
      flexDirection: "column",
      alignItems: "center",
      height: "unset",
    },
  },
  logo: {
    display: "flex",
    justifyItems: "center",
    marginRight: "100px",
    overflow: "hidden",
    maxWidth: "160px",
    [theme.breakpoints.down("md")]: {
      marginRight: "30px",
    },
    [theme.breakpoints.down("sm")]: {
      marginRight: "0px",
    },
  },
  linkRoot: {
    display: "flex",
    justifyContent: "space-around",
    marginRight: "100px",
    flexGrow: "1",
    [theme.breakpoints.down("md")]: {
      marginRight: "30px",
    },
    [theme.breakpoints.down("sm")]: {
      marginRight: "0px",
      flexDirection: "column",
      alignItems: "center",
    },
  },
  link: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "20px",
    textTransform: "uppercase",
    color: "#FAFAFA",
    textDecoration: "none",
    [theme.breakpoints.down("sm")]: {
      padding: "10px 0px",
    },
  },
  address: {
    color: "#FAFAFA",
  },
  version: {
    marginTop: "20px",
    color: "#FAFAFA",
    fontSize: "10px",
  },
}));

const Footer = () => {
  const classes = useStyles();
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const links = isLoggedIn ? loggedInLinks : notLoggedIn;

  return (
    <Box className={classes.root}>
      <img className={classes.logo} src={logo} alt="Logo serwisu Szerownia" />
      <Box className={classes.linkRoot}>
        <Link to="/terms" className={classes.link}>
          Regulamin
        </Link>
        {links.map((link) => (
          <Link key={link.id} className={classes.link} to={link.to}>
            {link.name}
          </Link>
        ))}
      </Box>
      <Box>
        <Typography className={classes.link}>Kontakt</Typography>
        <Typography className={classes.address}>
          Szerownia sp. z o.o.
        </Typography>
        <Typography className={classes.address}>
          ul. Lorem Ipsum 11, 11-111 Warszawa
        </Typography>
        <a href="mailto:szerownia@gmail.com" className={classes.address}>
          szerownia@gmail.com
        </a>
        <Typography className={classes.version}>
          Wersja: {moment(buildDate).format("YYYYMMDD.HHmm")}
        </Typography>
      </Box>
    </Box>
  );
};

export default Footer;
