import React, { useEffect } from "react";
import Box from "@material-ui/core/Box";
import MuiInput from "@material-ui/core/Input";
import MuiSelect from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ScrollToTop from "../ScrollToTop/ScrollToTop";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { searchAction } from "../../redux/modules/search";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    marginBottom: "30px",
    maxWidth: "100%",
  },
  search: {
    height: "80px",
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    boxShadow: "0 3px 6px #00000029",
    borderRadius: "40px",
    display: "flex",
    alignItems: "center",
    overflow: "hidden",
  },
  selectBox: {
    width: "20%",
    height: "100%",
    borderRight: "2px solid #707070",
  },
  select: {
    width: "100%",
    height: "100%",
  },
  subheader: {
    display: "flex",
    alignItems: "center",
    background: theme.palette.background.paper,
    fontWeight: "bold",
    fontSize: "16px",
  },
  item: {
    textIndent: "10px",
  },
  button: {
    width: "10%",
    height: "100%",
    boxShadow: "none !important",
    borderRadius: "0px",
  },
  icon: {
    fontSize: "48px !important",
  },
  categoryIcon: {
    marginRight: "10px",
  },
}));

const Select = withStyles(() => ({
  root: {
    width: "100%",
    height: "100%",
    padding: "0 20px",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    fontSize: "20px",
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    whiteSpace: "normal",
    textAlign: "right",
  },
}))(MuiSelect);

const Input = withStyles(() => ({
  root: {
    flexGrow: "1",
    paddingLeft: "20px",
    fontSize: "20px",
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
  },
}))(MuiInput);

const Search = () => {
  const classes = useStyles();
  const categories = useSelector((state) => state.searchReducer.categories);
  const category = useSelector((state) => state.searchReducer.category);
  const q = useSelector((state) => state.searchReducer.q);
  const filters = useSelector((state) => state.searchReducer.filters);
  const page = useSelector((state) => state.searchReducer.page);
  const isSearch = useSelector((state) => state.searchReducer.isSearch);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(searchAction.getCategories());
  }, []);

  useEffect(() => {
    if (!q && !category) {
      history.push("/");
    }
  }, [q, category]);

  useEffect(() => {
    dispatch(searchAction.search({ category, q, isSearch, filters, page }));
  }, [page]);

  useEffect(() => {
    if (history.location.pathname === "/") {
      return;
    }

    dispatch(searchAction.search({ category, q, isSearch, filters }));
  }, [isSearch]);

  useEffect(() => {
    if (history.location.pathname === "/") {
      dispatch(searchAction.setSearchCategory({ category: "" }));
      dispatch(searchAction.updateQuery({ q: "" }));
    }
  }, [history && history.location.pathname]);

  const handleCategoryChange = (e) => {
    const value = e.target.value;
    if (value < 0) {
      return;
    }

    dispatch(searchAction.setSearchCategory({ category: value }));

    if (q || value) {
      dispatch(searchAction.search({ category: e.target.value, q, isSearch }));
    }

    if (history.location.pathname === "/") {
      history.push("/search");
    }
  };

  const handleUpdateQuery = (e) => {
    dispatch(searchAction.updateQuery({ q: e.target.value }));
  };

  const handleClick = () => {
    dispatch(
      searchAction.search({ category, q, filters, isSearch, buttonPress: true })
    );
    history.push("/search");
  };

  // array provided as Select doesn't like fragments as children
  const renderCategory = (category) => {
    return [
      <ListSubheader
        value={-1}
        onClick={(e) => e.preventDefault()}
        className={classes.subheader}
        key={`category-${category.idCategory}`}
      >
        <Icon
          style={{ color: category.iconColor }}
          className={classes.categoryIcon}
        >
          {category.iconName}
        </Icon>
        {category.name}
      </ListSubheader>,
      <Divider />,
      category.subCategories.map((subCategory) => (
        <MenuItem
          key={`sub-${subCategory.idCategory}`}
          value={subCategory.idCategory}
          className={classes.item}
        >
          {subCategory.name}
        </MenuItem>
      )),
    ];
  };

  return (
    <Box className={classes.root}>
      <ScrollToTop />
      <Box className={classes.search}>
        <Box className={classes.selectBox}>
          <Select
            disableUnderline
            displayEmpty
            className={classes.select}
            onChange={handleCategoryChange}
            value={category}
          >
            <MenuItem value="">Wszystko</MenuItem>
            {categories.map((category) => renderCategory(category))}
          </Select>
        </Box>
        <Input
          placeholder="Wyszukaj"
          disableUnderline
          value={q}
          onChange={(e) => handleUpdateQuery(e)}
        />
        <Button
          disabled={!category && !q}
          variant="contained"
          color="primary"
          className={classes.button}
          startIcon={<ArrowForwardIcon className={classes.icon} />}
          onClick={() => handleClick()}
        />
      </Box>
    </Box>
  );
};

export default Search;
