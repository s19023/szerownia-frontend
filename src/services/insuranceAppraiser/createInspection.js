import moment from "moment";
import request from "../../config/request";
import { inspections } from "../../config/urls";

const createInspection = (data, claimId) => {
  data.claimId = claimId;
  data.inspectionDate = moment().format("YYYY-MM-DD");
  return request.post(inspections.post, data).then((response) => response.data);
};

export default createInspection;
