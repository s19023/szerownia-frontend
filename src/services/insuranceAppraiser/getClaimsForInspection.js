import request from "../../config/request";
import { inspections } from "../../config/urls";

const getClaimsForInspection = () =>
  request.get(inspections.get).then((response) => response.data);

export default getClaimsForInspection;
