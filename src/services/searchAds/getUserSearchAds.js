import request from "../../config/request";
import { users } from "../../config/urls";

const getUserSearchAds = (id, page) =>
  request
    .get(users.getSearchAds(id), { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getUserSearchAds;
