import request from "../../config/request";
import { searchAds } from "../../config/urls";

const updateSearchAd = (data, id) =>
  request
    .put(`${searchAds.getAll}/${id}`, data)
    .then((response) => response.data);

export default updateSearchAd;
