import request from "../../config/request";
import { searchAds } from "../../config/urls";

const getSearchAd = (id) => request.get(`${searchAds.getAll}/${id}`).then((response) => response.data);

export default getSearchAd;