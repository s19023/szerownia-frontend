import request from "../../config/request";
import { searchAds } from "../../config/urls";

const getSearchAdsListToCorrect = (_, page) =>
  request
    .get(searchAds.toCorrect, { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getSearchAdsListToCorrect;
