import request from "../../config/request";
import { searchAds } from "../../config/urls";

const postSearchAd = (data) =>
  request
    .post(`${searchAds.getAll}/${data.products.join(",")}`, data)
    .then((response) => response.data);

export default postSearchAd;
