import request from "../../config/request";
import { searchAds } from "../../config/urls";

const getSearchAdList = (page) =>
  request
    .get(searchAds.getAll, { params: { page } })
    .then((response) => response.data);

export default getSearchAdList;
