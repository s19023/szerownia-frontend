import request from "../../config/request";
import { products } from "../../config/urls";

const createProduct = (data, featureValues, searchProduct) => {
  data.productFeatures = featureValues;
  data.searchProduct = searchProduct ? true : false;
  return request.post(products.getAll, data);
}

export default createProduct;
