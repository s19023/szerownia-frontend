import request from "../../config/request";
import { products } from "../../config/urls";

const getProduct = (id) => {
  return request.get(products.getAll + `/${id}`).then((response) => response.data);
}

export default getProduct;
