import request from "../../config/request";
import { products } from "../../config/urls";

const suggestCategory = (name) =>
  request.get(products.suggestName + `/${name}`).then((respnse) => respnse.data);

export default suggestCategory;
