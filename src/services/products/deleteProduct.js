import request from "../../config/request";
import { products } from "../../config/urls";

const deleteProduct = (id) => {
  return request.delete(products.getAll + `/${id}`);
};

export default deleteProduct;
