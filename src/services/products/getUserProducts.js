import request from "../../config/request";
import { products } from "../../config/urls";

const getUserProducts = (showSearch) =>
  request.get(products.userProducts + `?showSearch=${showSearch ? true : false}`).then((respnse) => respnse.data);

export default getUserProducts;
