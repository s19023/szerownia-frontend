import request from "../../config/request";
import { products } from "../../config/urls";

const updateProduct = (data, featureValues, id) => {
  delete data.featureOccurrences;
  data.featureOccurrenceUpdateRequestDTO = featureValues;
  return request.put(products.getAll + `/${id}`, data);
};

export default updateProduct;
