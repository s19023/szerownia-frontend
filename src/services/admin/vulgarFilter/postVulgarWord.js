import request from "../../../config/request";
import { admin } from "../../../config/urls";

const postVulgarWord = (word) =>
  request.post(`${admin.vulgarWords}`, { vulgarWord: word }).then((response) => response.data);

export default postVulgarWord;
