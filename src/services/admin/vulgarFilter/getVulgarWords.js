import request from "../../../config/request";
import { admin } from "../../../config/urls";

const getVulgarWords = (page, rowsPerPage, q) =>
  request
    .get(admin.vulgarWords, {
      params: { howManyRecord: rowsPerPage, page: page, vulgarWord: q },
    })
    .then((response) => response.data);

export default getVulgarWords;
