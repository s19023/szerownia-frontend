import request from "../../../config/request";
import { admin } from "../../../config/urls";

const deleteVulgarWord = (id) =>
  request.delete(`${admin.deleteVulgarWord}/${id}`).then((response) => response.data);

export default deleteVulgarWord;
