import request from "../../../config/request";
import { admin } from "../../../config/urls";

const postBlockedIP = (ip) =>
  request.post(`${admin.blockedIPs}`, { ip: ip }).then((response) => response.data);

export default postBlockedIP;
