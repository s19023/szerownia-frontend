import request from "../../../config/request";
import { admin } from "../../../config/urls";

const deleteBlockedIP = (id) =>
  request.delete(`${admin.deleteBlockedIP}/${id}`).then((response) => response.data);

export default deleteBlockedIP;
