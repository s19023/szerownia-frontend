import request from "../../../config/request";
import { admin } from "../../../config/urls";

const getBlockedIPs = (page, rowsPerPage, q) =>
  request
    .get(admin.blockedIPs, {
      params: { howManyRecord: rowsPerPage, page: page, vulgarWord: q },
    })
    .then((response) => response.data);

export default getBlockedIPs;
