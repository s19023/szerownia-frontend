import moment from "moment";

const labels = Array(5)
  .fill(0)
  .map((elem, index) => moment().subtract(4 - index, "d").toDate());
const data = Array(5).fill(0).map((elem) => Math.floor(Math.random() * 100 + 50));

const getStats = () =>
  new Promise((res) =>
    setTimeout(() => res({ dates: labels, counts: data }), Math.random() * 500 + 800)
  );

export default getStats;
