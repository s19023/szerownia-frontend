import request from "../../../config/request";
import { admin } from "../../../config/urls";

const getAdTickets = (page) =>
  request
    .get(admin.tickets.adTickets, { params: { page } })
    .then((response) => response.data);

export default getAdTickets;
