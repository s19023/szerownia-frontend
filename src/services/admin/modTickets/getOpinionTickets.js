import request from "../../../config/request";
import { admin } from "../../../config/urls";

const getOpinionTickets = (page) =>
  request
    .get(admin.tickets.opinionTickets, { params: { page } })
    .then((response) => response.data);

export default getOpinionTickets;
