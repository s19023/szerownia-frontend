import request from "../../../config/request";
import { admin } from "../../../config/urls";

const getSingleTicket = (id) =>
  request.get(`${admin.tickets.single}/${id}`).then((response) => response.data);

export default getSingleTicket;
