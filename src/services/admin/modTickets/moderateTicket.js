import request from "../../../config/request";
import { admin } from "../../../config/urls";

const moderateTicket = (id, command, message) =>
  request
    .post(`${admin.tickets.single}/${id}`, command, { params: { message: message ? message : "Zaakceptowano ogłoszenia" } })
    .then((response) => response.data);

export default moderateTicket;
