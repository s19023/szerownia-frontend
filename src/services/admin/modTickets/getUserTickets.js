import request from "../../../config/request";
import { admin } from "../../../config/urls";

const getUserTickets = (page) =>
  request
    .get(admin.tickets.userTickets, { params: { page } })
    .then((response) => response.data);

export default getUserTickets;
