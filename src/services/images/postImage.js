import request from "../../config/request";
import { images } from "../../config/urls";

const postImage = (image, progressCb) => {
  const formData = new FormData();
  formData.append("imageList", image, image.name.replace(/\s/g, ""));
  return request({
    method: "post",
    url: images.post,
    data: formData,
    headers: { "Content-Type": "multipart/form-data" },
    onUploadProgress: (progress) => progressCb(progress)
  });
};

export default postImage;
