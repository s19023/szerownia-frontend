import request from "../../config/request";
import { enums } from "../../config/urls";

const getClaimsStatuses = () =>
  request.get(enums.claims).then((response) => response.data);

export default getClaimsStatuses;
