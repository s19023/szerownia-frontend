import request from "../../config/request";
import { enums } from "../../config/urls";

const getComplaintReasons = () =>
  request.get(enums.complaintReasons).then((response) => response.data);

export default getComplaintReasons;
