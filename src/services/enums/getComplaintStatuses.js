import request from "../../config/request";
import { enums } from "../../config/urls";

const getComplaintStatuses = () =>
  request.get(enums.complaintStatues).then((response) => response.data);

export default getComplaintStatuses;
