import request from "../../config/request";
import { enums } from "../../config/urls";

const getTicketSubjects = () =>
  request.get(enums.ticketSubjects).then((response) => response.data);

export default getTicketSubjects;
