import request from "../../config/request";
import { enums } from "../../config/urls";

const getHireStatuses = () =>
  request.get(enums.hireStatuses).then((response) => response.data);

export default getHireStatuses;
