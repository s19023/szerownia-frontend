import axios from "axios";
import { paczkomaty } from "../../config/urls";

const getAllPaczkomaty = () =>
  axios
    .get(paczkomaty.get)
    .then((response) => response.data.items.filter((paczkomat) => paczkomat.t === 1));

export default getAllPaczkomaty;
