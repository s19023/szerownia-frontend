import createDiscount from "./createDiscount";

const createDiscounts = (discounts) => {
  const mappedDiscounts = discounts.map((discount) => ({
    minNumDays: discount.minDays,
    rentalAdId: discount.id,
    value: discount.discount,
  }));
  
  const promises = mappedDiscounts.map((discount) => createDiscount(discount));
  return Promise.all(promises);
};

export default createDiscounts;
