import request from "../../config/request";
import { discounts } from "../../config/urls";

const createDiscount = (data) => request.post(`${discounts.getAll}`, data).then((response) => response.data);

export default createDiscount;