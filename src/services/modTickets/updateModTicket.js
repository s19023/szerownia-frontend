import request from "../../config/request";
import { modTickets } from "../../config/urls";

const updateModTicket = (id) =>
  request
    .post(`${modTickets.getAll}/verify/${id}`)
    .then((response) => response.data);

export default updateModTicket;
