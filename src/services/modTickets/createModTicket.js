import request from "../../config/request";
import { modTickets } from "../../config/urls";

const createModTicket = (data) =>
  request.post(`${modTickets.getAll}`, data).then((response) => response.data);

export default createModTicket;
