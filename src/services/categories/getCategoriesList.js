import request from "../../config/request";
import { categories } from "../../config/urls";

const getCategoriesList = () =>
  request
    .get(categories.get)
    .then((response) =>
      response.data.filter((category) => category.subCategories.length > 0)
    );

export default getCategoriesList;
