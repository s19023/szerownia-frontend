import request from "../../config/request";
import { categories } from "../../config/urls";

const createCategory = (data) => request.post(categories.single, data);

export default createCategory;
