import request from "../../config/request";
import { categories } from "../../config/urls";

const updateCategory = (data, id) => request.put(categories.single(id), data);

export default updateCategory;
