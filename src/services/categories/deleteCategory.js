import request from "../../config/request";
import { categories } from "../../config/urls";

const deleteCategory = (id) => request.delete(categories.single(id));

export default deleteCategory;
