import axios from "axios";

const reverseGeocode = (lat, lon) =>
  axios
    .get("https://nominatim.openstreetmap.org/reverse", {
      params: {
        lat: lat,
        lon: lon,
        zoom: 14,
        format: "json",
      },
    })
    .then((response) => response.data);

export default reverseGeocode;
