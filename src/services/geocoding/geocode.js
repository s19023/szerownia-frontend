import axios from "axios";

const geocode = (search) =>
  axios
    .get("https://nominatim.openstreetmap.org/search", {
      params: {
        q: search,
        zoom: 14,
        format: "json",
        countrycodes: "pl",
        addressdetails: 1,
        limit: 1,
      },
    })
    .then((response) => response.data);

export default geocode;
