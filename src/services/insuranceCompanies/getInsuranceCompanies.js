import request from "../../config/request";
import { insuranceCompanies } from "../../config/urls";

const getInsuranceCompanies = () =>
  request
    .get(insuranceCompanies.get)
    .then((response) => response.data);

export default getInsuranceCompanies;
