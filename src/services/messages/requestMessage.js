import request from "../../config/request";
import { messages } from "../../config/urls";

const requestMessage = (adId, modTicketId, recepient, content) => {
  return request
    .post(
      `${process.env.REACT_APP_MESSAGES_URL}${messages.request}/${recepient}${
        adId ? "A" : "M"
      }${adId ? adId : modTicketId}`,
      content
    )
    .then((response) => response.data);
};

export default requestMessage;
