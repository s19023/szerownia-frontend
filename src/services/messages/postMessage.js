import request from "../../config/request";
import { messages } from "../../config/urls";

const postMessage = (id, content) => {
  return request
    .post(`${process.env.REACT_APP_MESSAGES_URL}${messages.new(id)}`, content)
    .then((response) => response.data);
};

export default postMessage;
