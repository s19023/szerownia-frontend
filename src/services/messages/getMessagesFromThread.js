import request from "../../config/request";
import { messages } from "../../config/urls";

const getMessagesFromThread = (id) =>
  request
    .get(`${process.env.REACT_APP_MESSAGES_URL}${messages.thread}/${id}`)
    .then((response) => response.data);

export default getMessagesFromThread;
