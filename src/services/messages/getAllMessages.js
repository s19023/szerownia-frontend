import request from "../../config/request";
import { messages } from "../../config/urls";

const getAllMessages = () =>
  request.get(`${process.env.REACT_APP_MESSAGES_URL}${messages.get}`).then((response) => response.data);

export default getAllMessages;
