import request from "../../config/request";
import { complaints } from "../../config/urls";

const makeComplaint = (hireId, reason, details) =>
  request
    .post(complaints.get, { hireId, reason, details })
    .then((response) => response.data);

export default makeComplaint;
