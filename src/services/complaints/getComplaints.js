import request from "../../config/request";
import { complaints } from "../../config/urls";

const getComplaints = (page) =>
  request
    .get(complaints.get, { params: { howManyRecord: 10, page } })
    .then((response) => response.data);

export default getComplaints;
