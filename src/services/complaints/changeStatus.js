import request from "../../config/request";
import { complaints } from "../../config/urls";

const changeStatus = (id, status) =>
  request
    .put(complaints.get, { complaintId: id, status })
    .then((response) => response.data);

export default changeStatus;
