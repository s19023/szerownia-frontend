import request from "../../config/request";
import { complaints } from "../../config/urls";

const getUserComplaints = (page) =>
  request
    .get(complaints.user, { params: { howManyRecord: 10, page } })
    .then((response) => response.data);

export default getUserComplaints;
