import request from "../../config/request";
import { insuranceAdjuster } from "../../config/urls";

const updateClaimStatus = (claimId, decision, justification) =>
  request.post(insuranceAdjuster.decision, {
    claimId,
    decision,
    justification,
  });

export default updateClaimStatus;
