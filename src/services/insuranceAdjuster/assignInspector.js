import request from "../../config/request";
import { insuranceAdjuster } from "../../config/urls";

const assignInspector = (claimId, appraiserId) =>
  request.post(insuranceAdjuster.assignClaim, { claimId, appraiserId });

export default assignInspector;
