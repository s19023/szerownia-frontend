import request from "../../config/request";
import { users } from "../../config/urls";

const getInspectors = () =>
  request.get(users.getInspectors).then((response) => response.data);

export default getInspectors;
