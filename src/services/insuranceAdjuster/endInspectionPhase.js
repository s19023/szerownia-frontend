import request from "../../config/request";
import { insuranceAdjuster } from "../../config/urls";

const endInspectionPhase = (claimId) =>
  request.post(insuranceAdjuster.endInspections(claimId));

export default endInspectionPhase;
