import request from "../../config/request";
import { insuranceAdjuster } from "../../config/urls";

const sendToInsuranceCompany = (id) => request.post(insuranceAdjuster.insuranceCompany + id);

export default sendToInsuranceCompany;