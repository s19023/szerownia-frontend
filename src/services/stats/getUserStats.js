import request from "../../config/request";
import { stats } from "../../config/urls";

const getUserStats = () => request.get(stats.users).then((response) => response.data);

export default getUserStats;