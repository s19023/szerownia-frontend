import request from "../../config/request";
import { stats } from "../../config/urls";

const getRentStats = () => request.get(stats.rents).then((response) => response.data);

export default getRentStats;