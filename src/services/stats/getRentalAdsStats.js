import request from "../../config/request";
import { stats } from "../../config/urls";

const getRentalAdsStats = () => request.get(stats.rentalAds).then((response) => response.data);

export default getRentalAdsStats;