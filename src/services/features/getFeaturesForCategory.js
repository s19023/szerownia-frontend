import request from "../../config/request";
import { features } from "../../config/urls";

const getFeaturesForCategory = (categoryId) =>
  request.get(features.forCategory(categoryId)).then((response) => response.data);

export default getFeaturesForCategory;
