import request from "../../config/request";
import { features } from "../../config/urls";

const getAllFeatures = () =>
  request.get(features.get).then((response) => response.data);

export default getAllFeatures;
