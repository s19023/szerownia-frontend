import request from "../../config/request";
import { features } from "../../config/urls";

const deleteFeature = (id) =>
  request.delete(features.single(id)).then((response) => response.data);

export default deleteFeature;
