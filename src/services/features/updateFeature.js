import request from "../../config/request";
import { features } from "../../config/urls";

const updateFeature = (values) =>
  request.put(features.single(values.idFeature), values).then((response) => response.data);

export default updateFeature;
