import request from "../../config/request";
import { features } from "../../config/urls";

const createFeature = (values) =>
  request.post(features.get + "/", values).then((response) => response.data);

export default createFeature;
