import request from "../../config/request";
import { hires } from "../../config/urls";

const createNewHire = (id, dateFrom, dateTo, pickupMethod) =>
  request
    .post(hires.all, {
      adId: id,
      dateFrom: dateFrom.format("YYYY-MM-DD"),
      dateToPlanned: dateTo.format("YYYY-MM-DD"),
      selectedPickupMethod: pickupMethod,
    })
    .then((response) => response.data);

export default createNewHire;
