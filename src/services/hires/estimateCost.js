import request from "../../config/request";
import { hires } from "../../config/urls";

const estimateCost = (rentalAdId, startDate, endDate) =>
  request
    .post(hires.estimate, { rentalAdId, startDate, endDate })
    .then((response) => response.data);

export default estimateCost;
