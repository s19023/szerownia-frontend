import request from "../../config/request";
import { hires } from "../../config/urls";

const getUserShares = (hireStatus) =>
  request
    .get(hires.userShares, { params: { hireStatus } })
    .then((response) => response.data);

export default getUserShares;
