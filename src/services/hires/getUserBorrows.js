import request from "../../config/request";
import { hires } from "../../config/urls";

const getUserBorrows = (hireStatus) =>
  request
    .get(hires.userBorrows, { params: { hireStatus } })
    .then((response) => response.data);

export default getUserBorrows;
