import request from "../../config/request";
import { hires } from "../../config/urls";

const getBorrowsForUser = (id, page) =>
  request
    .get(hires.borrowsForUser(id), { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getBorrowsForUser;
