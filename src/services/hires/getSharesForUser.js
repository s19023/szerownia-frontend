import request from "../../config/request";
import { hires } from "../../config/urls";

const getSharesForUser = (id, page) =>
  request
    .get(hires.sharesForUser(id), { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getSharesForUser;
