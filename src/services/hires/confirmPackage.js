import request from "../../config/request";
import { hires } from "../../config/urls";

const confirmPackage = (id) =>
  request.put(hires.confirm(id)).then((response) => response.data);

export default confirmPackage;
