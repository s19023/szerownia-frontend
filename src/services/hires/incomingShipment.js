import request from "../../config/request";
import { hires } from "../../config/urls";

const incomingShipment = (id, shipmentNumber) =>
  request
    .post(hires.incoming(id), shipmentNumber ? shipmentNumber : -1)
    .then((response) => response.data);

export default incomingShipment;
