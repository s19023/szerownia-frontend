import request from "../../config/request";
import { hires } from "../../config/urls";

const calculateInsuranceCost = (id) =>
  request
    .get(hires.insuranceCost(id))
    .then((response) => response.data);

export default calculateInsuranceCost;
