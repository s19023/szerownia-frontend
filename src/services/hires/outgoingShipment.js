import request from "../../config/request";
import { hires } from "../../config/urls";

const outgoingShipment = (id, shipmentNumber, insure, insuranceCompanyId) =>
  request
    .post(hires.outgoing(id), {
      outgoingShipmentNumber: shipmentNumber,
      insure,
      insuranceCompanyId,
    })
    .then((response) => response.data);

export default outgoingShipment;
