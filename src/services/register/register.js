import axios from "axios";

const register = (data) => axios.post(`${process.env.REACT_APP_BASE_URL}/api/users`, data);

export default register;
