import request from "../../config/request";
import { users } from "../../config/urls";

const getPasswordResetToken = (email) =>
  request.post(users.getResetToken, email);

export default getPasswordResetToken;
