import request from "../../config/request";
import { users } from "../../config/urls";

const getOtherUserDetails = (id) =>
  request.get(`${users.getAll}/${id}`).then((response) => response.data);

export default getOtherUserDetails;
