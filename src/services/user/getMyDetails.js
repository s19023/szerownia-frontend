import request from "../../config/request";
import { users } from "../../config/urls";

const getMyDetails = () =>
  request.get(users.getMyDetails).then((response) => response.data);

export default getMyDetails;
