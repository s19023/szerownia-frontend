import request from "../../config/request";
import { users } from "../../config/urls";

const getMyRentalAds = () =>
  request.get(users.getMyAds).then((response) => response.data);

export default getMyRentalAds;
