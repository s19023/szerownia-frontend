import request from "../../config/request";
import { users } from "../../config/urls";

const updatePremium = () =>
  request.post(users.premium).then((response) => response.data);

export default updatePremium;
