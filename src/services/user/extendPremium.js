import request from "../../config/request";
import { users } from "../../config/urls";

const extendPremium = () =>
  request.post(users.extendPremium).then((response) => response.data);

export default extendPremium;
