import request from "../../config/request";
import { users } from "../../config/urls";

const updateProfilePicture = (file, fileName) => {
  const data = new FormData();
  data.append("image", file, fileName.replace(/\s/g, ""));

  return request({
    method: "put",
    url: users.getAll,
    data: data,
    headers: { "Content-Type": "multipart/form-data" },
  });
}

export default updateProfilePicture;
