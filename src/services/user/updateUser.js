import request from "../../config/request";
import { users } from "../../config/urls";

const updateUser = (id, data) =>
  request.post(users.update, data).then((response) => response.data);

export default updateUser;
