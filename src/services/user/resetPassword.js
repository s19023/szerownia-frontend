import request from "../../config/request";
import { users } from "../../config/urls";

const resetPassword = (password, token) =>
  request.post(users.resetPassword, { newpassword: password, token });

export default resetPassword;
