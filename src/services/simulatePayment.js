const simulatePayment = () => {
  return new Promise((res, rej) => {
    if (Math.random() > 0.25) {
      setTimeout(() => res(), 5000);
    } else {
      setTimeout(() => rej("Payment rejected"), 5000);
    }
  });
};

export default simulatePayment;
