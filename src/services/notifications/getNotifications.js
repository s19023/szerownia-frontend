import request from "../../config/request";
import { notifications } from "../../config/urls";

const getNotifications = () => {
  return request
    .get(notifications.get, { params: { howManyRecord: 10 } })
    .then((response) => response.data);
};

export default getNotifications;
