import request from "../../config/request";
import { notifications } from "../../config/urls";

const getUnreadNotifications = () => {
  return request
    .get(notifications.unread)
    .then((response) => response.data);
};

export default getUnreadNotifications;
