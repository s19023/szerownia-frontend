import request from "../../config/request";
import { claims } from "../../config/urls";

const getClaim = (id) =>
  request.get(claims.get + `/${id}`).then((response) => response.data);

export default getClaim;
