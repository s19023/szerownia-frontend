import request from "../../config/request";
import { claims } from "../../config/urls";

const getClaims = () =>
  request.get(claims.get).then((response) => response.data);

export default getClaims;
