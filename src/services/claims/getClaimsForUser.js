import request from "../../config/request";
import { claims } from "../../config/urls";

const getClaimsForUser = (id, page) =>
  request
    .get(claims.forUser(id), { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getClaimsForUser;
