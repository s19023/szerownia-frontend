import request from "../../config/request";
import { claims } from "../../config/urls";

const createClaim = (data) =>
  request.post(`${claims.get}`, data).then((response) => response.data);

export default createClaim;
