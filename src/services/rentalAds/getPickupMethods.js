import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const getPickupMethods = (id) => request.get(`${rentalAds.getPickupMethods}/${id}`).then((response) => response.data);

export default getPickupMethods;