import request from "../../config/request";
import { users } from "../../config/urls";

const getUserRentalAds = (id, page) =>
  request
    .get(users.getRentalAds(id), { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getUserRentalAds;
