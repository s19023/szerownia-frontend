import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const getAvailableDates = (id) =>
  request.get(`${rentalAds.getDates}/${id}`).then((response) => response.data);

export default getAvailableDates;
