import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const postRentalAd = (data) =>
  request
    .post(`${rentalAds.getAll}/${data.products.join(",")}`, data)
    .then((response) => response.data);

export default postRentalAd;
