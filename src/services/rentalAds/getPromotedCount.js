import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const getPromotedCount = (id) => request.get(rentalAds.getPromotedCount).then((response) => response.data);

export default getPromotedCount;