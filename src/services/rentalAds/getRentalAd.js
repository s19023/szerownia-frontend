import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const getRentalAd = (id) => request.get(`${rentalAds.getAll}/${id}`).then((response) => response.data);

export default getRentalAd;