import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const updateRentalAd = (data, id) => {
  data.pickupMethodList = data.pickupMethod;

  return request
    .put(`${rentalAds.getAll}/${id}`, data)
    .then((response) => response.data);
}

export default updateRentalAd;
