import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const getRentalAdsList = (page) =>
  request
    .get(rentalAds.getAll, { params: { page } })
    .then((response) => response.data);

export default getRentalAdsList;
