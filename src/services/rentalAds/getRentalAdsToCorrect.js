import request from "../../config/request";
import { rentalAds } from "../../config/urls";

const getRentalAdsListToCorrect = (_, page) =>
  request
    .get(rentalAds.toCorrect, { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getRentalAdsListToCorrect;
