import request from "../../config/request";
import { opinions } from "../../config/urls";

const putOpinion = (data) => request.put(opinions.put, data);

export default putOpinion;
