import request from "../../config/request";
import { opinions } from "../../config/urls";

const postOpinion = (data) => request.post(opinions.post, data);

export default postOpinion;
