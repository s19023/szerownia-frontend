import request from "../../config/request";
import { users } from "../../config/urls";

const getUserOpinions = (id, page) =>
  request
    .get(users.getOpinions(id), { params: { page, howManyRecord: 10 } })
    .then((response) => response.data);

export default getUserOpinions;
