import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import NewMessage from "./NewMessage";
import MailIcon from "@material-ui/icons/Mail";

const MessageButton = ({ adId, userId, isLoggedIn }) => {
  const [messageAnchor, setMessageAnchor] = useState(null);

  return (
    <React.Fragment>
      <NewMessage
        adId={adId}
        anchorEl={messageAnchor}
        userId={userId}
        handleClose={() => setMessageAnchor(null)}
      />
      <Button
        disabled={!isLoggedIn}
        color="primary"
        variant="contained"
        id="new-message"
        startIcon={<MailIcon />}
        onClick={(e) =>
          setMessageAnchor(messageAnchor ? null : e.currentTarget)
        }
      >
        Wyślij wiadomość
      </Button>
    </React.Fragment>
  );
};

export default MessageButton;
