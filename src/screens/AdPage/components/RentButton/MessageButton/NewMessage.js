import React, { useState } from "react";
import Popover from "@material-ui/core/Popover";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../../../redux/modules/snackbar";

import requestMessage from "../../../../../services/messages/requestMessage";

const useStyles = makeStyles(() => ({
  popper: {
    zIndex: "1300",
  },
  paper: {
    padding: "20px",
  },
  header: {
    fontWeight: "bold",
  },
  content: {
    width: "300px",
    marginBottom: "10px",
  },
}));

const NewMessage = ({ adId, userId, anchorEl, handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [content, setContent] = useState("");
  const [disabled, setDisabled] = useState(false);

  const submit = () => {
    setDisabled(true);
    requestMessage(adId, null, userId, content)
      .then(() => {
        handleClose();
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Wysłano wiadomość",
          })
        );
        setDisabled(false);
      })
      .catch(() => {
        setDisabled(false);
        dispatch(snackbarAction.openSnackbar({ error: true }));
      });
  };

  return (
    <Popover
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      transformOrigin={{
        vertical: "center",
        horizontal: "center",
      }}
      open={anchorEl !== null}
      anchorEl={anchorEl}
      className={classes.popper}
      onClose={handleClose}
    >
      <Paper className={classes.paper} elevation={1}>
        <Box display="flex" flexDirection="column">
          <Typography className={classes.header}>Napisz wiadomość</Typography>
          <TextField
            multiline
            id="message-content"
            label="Treść wiadomości"
            className={classes.content}
            onChange={(e) => setContent(e.target.value)}
          />
          <Button
            disabled={content === "" || disabled}
            color="primary"
            variant="contained"
            onClick={() => submit()}
            id="send-message"
          >
            Wyślij wiadomość
          </Button>
        </Box>
      </Paper>
    </Popover>
  );
};

export default NewMessage;
