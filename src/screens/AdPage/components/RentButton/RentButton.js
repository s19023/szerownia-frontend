import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import MuiButton from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import MessageButton from "./MessageButton/MessageButton";
import Zoom from "@material-ui/core/Zoom";
import { withStyles } from "@material-ui/core/styles";

const RentButton = ({ id, userId, isSearchAd }) => {
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const history = useHistory();

  const Button = () => (
    <MuiButton
      disabled={!isLoggedIn}
      variant="contained"
      color="primary"
      onClick={() =>
        history.push(
          isSearchAd ? "/create-ad?searchAd=" + id : "/hire?rentalAd=" + id
        )
      }
    >
      {isSearchAd ? "Chcę użyczyć" : "Wypożycz"}
    </MuiButton>
  );

  if (isLoggedIn) {
    return (
      <React.Fragment>
        <Button />
        <MessageButton adId={id} userId={userId} isLoggedIn={isLoggedIn} />
      </React.Fragment>
    );
  } else {
    return (
      <React.Fragment>
        <Tooltip
          title={
            isSearchAd
              ? "Aby odpowiedzieć na ogłoszenie musisz się zalogowoać"
              : "Aby dokonać wypożyczenia musisz się zalogować"
          }
          TransitionComponent={Zoom}
        >
          <span>
            <Button />
          </span>
        </Tooltip>
        <Tooltip title="Aby móc wysyłać wiadomości musisz się zalogować">
          <span>
            <MessageButton adId={id} isLoggedIn={isLoggedIn} />
          </span>
        </Tooltip>
      </React.Fragment>
    );
  }
};

export default RentButton;
