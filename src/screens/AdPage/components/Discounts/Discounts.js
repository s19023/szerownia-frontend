import React, { useMemo } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "50%",
    paddingLeft: "30px",
    boxSizing: "border-box",
  },
  noDiscountsRoot: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  noDiscounts: {
    color: theme.palette.text.disabled,
    fontSize: "18px",
  },
  header: {
    fontSize: "18px",
    fontWeight: "bold",
  },
}));

const Discounts = ({ discounts }) => {
  const classes = useStyles();

  discounts = useMemo(
    () => discounts && discounts.sort((a, b) => b.minNumDays - a.minNumDays),
    [discounts]
  );

  if (!discounts || discounts.length === 0) {
    return (
      <Box className={classes.root}>
        <Typography className={classes.header}>Rabaty</Typography>
        <Box className={classes.noDiscountsRoot}>
          <Typography className={classes.noDiscounts}>
            Brak rabatów w ogłoszeniu 😔
          </Typography>
        </Box>
      </Box>
    );
  }

  return (
    <Box className={classes.root}>
      <Typography className={classes.header}>Rabaty</Typography>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Minimalna ilość dni</TableCell>
            <TableCell>Wysokość zniżki</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {discounts.map((discount) => (
            <TableRow key={discount.id}>
              <TableCell>{discount.minNumDays}</TableCell>
              <TableCell>{discount.value}%</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Box>
  );
};

export default Discounts;
