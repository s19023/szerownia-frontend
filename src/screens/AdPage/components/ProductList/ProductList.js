import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Product from "./Product";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "50%",
    borderRight: "1px solid " + theme.palette.divider,
    paddingRight: "30px",
    boxSizing: "border-box",
  },
  noBorder: {
    borderRight: "unset",
    width: "100%",
    paddingRight: "unset",
  },
  header: {
    fontSize: "18px",
    marginBottom: "10px",
    fontWeight: "bold",
  },
}));

const ProductList = ({ products, isSearchAd }) => {
  const classes = useStyles();

  return (
    <Box className={isSearchAd ? classes.noBorder : classes.root}>
      <Typography className={classes.header}>
        {isSearchAd ? "Poszukiwane przedmioty" : "Przedmioty"}
      </Typography>
      {products.map((product) => (
        <Product product={product} key={product.id} />
      ))}
    </Box>
  );
};

export default ProductList;
