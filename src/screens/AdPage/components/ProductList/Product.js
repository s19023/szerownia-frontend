import React from "react";
import Typography from "@material-ui/core/Typography";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Checkbox from "@material-ui/core/Checkbox";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import { makeStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const fields = [
  "booleanValue",
  "decimalNumberValue",
  "floatingNumberValue",
  "textValue",
];

const findNotNull = (feature) => {
  const key = fields.find((key) => feature[key] !== null);
  return key;
};

const useStyles = makeStyles((theme) => ({
  name: {
    fontWeight: "bold",
    width: "50%",
  },
  make: {
    color: theme.palette.text.secondary,
  },
  checkbox: {
    padding: "0",
  },
}));

const Product = ({ product }) => {
  const classes = useStyles();
  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.name}>{product.name}</Typography>
        <Typography className={classes.make}>
          {product.make} {product.model}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>Kategoria produktu</TableCell>
              <TableCell align="center">{product.categoryReferenceDTO.name}</TableCell>
            </TableRow>
            {product.featureOccurrences.map((feature) => {
              const key = findNotNull(feature);
              return (
                <TableRow key={feature.idFeatureOccurence}>
                  <TableCell>{feature.featureName}</TableCell>
                  <TableCell align="center">
                    {key === "booleanValue" ? (
                      <Checkbox
                        className={classes.checkbox}
                        color="primary"
                        disableRipple
                        size="small"
                        checked={feature[key]}
                        readOnly
                      />
                    ) : (
                      feature[key]
                    )}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </AccordionDetails>
    </Accordion>
  );
};

export default Product;
