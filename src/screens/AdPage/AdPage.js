import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import Report from "../../components/Report/Report";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import RentButton from "./components/RentButton/RentButton";
import { MapContainer, TileLayer, Circle, useMapEvents } from "react-leaflet";
import ProductList from "./components/ProductList/ProductList";
import Discounts from "./components/Discounts/Discounts";

import { makeStyles } from "@material-ui/core/styles";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import MailIcon from "@material-ui/icons/Mail";
import EmojiPeopleIcon from "@material-ui/icons/EmojiPeople";
import LocalShippingIcon from "@material-ui/icons/LocalShipping";
import RoomIcon from "@material-ui/icons/Room";

import { images } from "../../config/urls";
import getRentalAd from "../../services/rentalAds/getRentalAd";
import getSearchAd from "../../services/searchAds/getSearchAd";
import { useSelector } from "react-redux";
import { useParams, useHistory, Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  dialogContent: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    marginTop: "-16px",
    maxWidth: "1200px",
  },
  imageRoot: {
    position: "relative",
    height: "520px",
    display: "flex",
    justifyContent: "center",
    marginBottom: "20px",
  },
  ownAdTitle: {
    fontSize: "20px",
    color: theme.palette.primary.main,
    marginBottom: "10px",
  },
  searchAdTitle: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "24px",
    color: theme.palette.text.primary,
  },
  imageButtons: {
    position: "absolute",
    right: "0",
    left: "0",
    bottom: "0",
    top: "0",
    display: "flex",
    alignItems: "center",
  },
  spacer: {
    display: "flex",
    flexGrow: "1",
    height: "100%",
    justifyContent: "center",
    alignItems: "flex-end",
    marginBottom: "10px",
  },
  circle: {
    height: "16px",
    width: "16px",
    borderRadius: "50%",
    border: "2px solid #FFFFFF",
    boxShadow: theme.shadows[5],
    "&:not(:last-of-type)": {
      marginRight: "10px",
    },
  },
  fill: {
    backgroundColor: "#FFFFFF",
  },
  image: {
    width: "100%",
    objectFit: "contain",
  },
  flexRow: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  largeText: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "32px",
    fontWeight: "bold",
    lineHeight: "32px",
    display: "flex",
    alignItems: "flex-end",
  },
  price: {
    fontSize: "24px",
    fontWeight: "bold",
    lineHeight: "24px",
  },
  avatar: {
    width: "64px",
    height: "64px",
    marginRight: "20px",
    boxShadow: theme.shadows[5],
  },
  name: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "24px",
    fontWeight: "bold",
  },
  iconRoot: {
    display: "flex",
    alignItems: "center",
    marginBottom: "10px",
  },
  icon: {
    fontSize: "24px",
    color: theme.palette.common.darkBlue,
    position: "relative",
    left: "-7px",
  },
  location: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "16px",
    color: theme.palette.common.darkBlue,
  },
  pickupIcon: {
    fontSize: "48px",
    marginRight: "10px",
    color: theme.palette.text.primary,
  },
  pickupType: {
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    fontSize: "20px",
    lineHeight: "20px",
    width: "min-content",
    maxWidth: "190px",
  },
  disabled: {
    color: theme.palette.text.disabled,
  },
  mapContainer: {
    width: "300px",
    height: "300px",
    borderRadius: "10px",
    overflow: "hidden",
    boxShadow: theme.shadows[5],
    cursor: "pointer",
  },
  map: {
    height: "300px",
  },
  header: {
    fontSize: "24px",
    fontWeight: "bold",
  },
  description: {
    fontSize: "16px",
  },
}));

const ClickHandler = ({ center }) => {
  const map = useMapEvents({
    click() {
      window.open(
        `https://www.openstreetmap.org/?mlat=${center[0]}&mlon=${center[1]}#map=15/${center[0]}/${center[1]}`,
        "_blank"
      );
    },
  });

  return null;
};

const isPickupTypeActive = (ad, type, isSearchAd) => {
  if (isSearchAd) {
    return ad.pickupMethod && ad.pickupMethod.includes(type);
  } else {
    return ad.pickupMethodList && ad.pickupMethodList.includes(type);
  }
};

const AdPage = () => {
  const classes = useStyles();
  const [ad, setAd] = useState({});
  const [error, setError] = useState(false);
  const [selectedId, setSelectedId] = useState(0);
  const userId = useSelector((state) => state.userReducer.id);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const role = useSelector((state) => state.userReducer.role);
  const { type, id } = useParams();
  const history = useHistory();
  const isSearchAd = type === "search";

  useEffect(() => {
    if (type !== "search" && type !== "rental") {
      history.push("/");
    }

    const getAd = isSearchAd ? getSearchAd : getRentalAd;
    getAd(id)
      .then((data) => setAd(data))
      .catch(() => setError(true));
  }, [id]);

  const handleChange = (change) => {
    let index = selectedId + change;
    if (ad.imagesIdList.length <= index) {
      index = 0;
    } else if (index === -1) {
      index = ad.imagesIdList.length - 1;
    }

    setSelectedId(index);
  };

  const personalPickup = isPickupTypeActive(ad, "PERSONAL_PICKUP", isSearchAd);
  const paczkomatPickup = isPickupTypeActive(ad, "PARCEL_LOCKER", isSearchAd);
  const courierPickup = isPickupTypeActive(ad, "COURIER", isSearchAd);

  const center = ad.location && [ad.location.latitude, ad.location.longitude];

  return (
    <Box className={classes.dialogContent}>
      {!isSearchAd && (
        <Box className={classes.imageRoot}>
          {ad.imagesIdList && (
            <React.Fragment>
              <img
                src={`${images.get}/${ad.imagesIdList[selectedId]}`}
                alt={ad.title}
                className={classes.image}
              />
              <Box className={classes.imageButtons}>
                <IconButton onClick={() => handleChange(-1)}>
                  <ArrowBackIcon />
                </IconButton>
                <Box className={classes.spacer}>
                  {ad.imagesIdList.map((id, index) => (
                    <Box
                      className={`${classes.circle} ${
                        index === selectedId ? classes.fill : ""
                      }`}
                      key={index}
                    />
                  ))}
                </Box>
                <IconButton onClick={() => handleChange(1)}>
                  <ArrowForwardIcon />
                </IconButton>
              </Box>
            </React.Fragment>
          )}
        </Box>
      )}
      {!!Object.keys(ad).length && (
        <React.Fragment>
          {isLoggedIn && role === "USER" && ad.user.id !== userId ? (
            <Box padding="10px 0">
              <Report text="Zgłoś ogłoszenie" adId={id} />
            </Box>
          ) : (
            ad.user.id === userId && (
              <Typography className={classes.ownAdTitle}>
                Oglądasz własne ogłoszenie
              </Typography>
            )
          )}
          <Box className={classes.flexRow} marginBottom="30px">
            <Box>
              {!!isSearchAd && (
                <Typography className={classes.searchAdTitle}>
                  Użytkownik {ad.user.firstName} poszukuje
                </Typography>
              )}
              <Typography className={classes.largeText}>{ad.title}</Typography>
              {ad.pricePerDay && (
                <Typography className={classes.largeText}>
                  {ad.pricePerDay.toFixed(0)} zł
                  <Typography className={classes.price}>/dzień</Typography>
                </Typography>
              )}
            </Box>
            <Box>
              <Link
                to={`/user/${ad.user.id}`}
                target="_blank"
                className={classes.flexRow}
                style={{ marginBottom: "20px" }}
              >
                <Avatar
                  className={classes.avatar}
                  src={`${images.get}/${ad.imageUserId}`}
                  alt={`${ad.user.firstName} ${ad.user.lastName}`}
                />
                <Typography className={classes.name}>
                  {ad.user.firstName}
                </Typography>
              </Link>
              <Button
                variant="contained"
                color="primary"
                component={Link}
                to={`/user/${ad.user.id}`}
                target="_blank"
              >
                Przejdź do profilu
              </Button>
            </Box>
          </Box>
          {(!isLoggedIn || role === "USER") && ad.user.id !== userId && (
            <Box className={classes.flexRow} marginBottom="30px">
              <RentButton
                id={ad.id}
                userId={ad.user.id}
                isSearchAd={isSearchAd}
              />
            </Box>
          )}
          <Box className={classes.flexRow} marginBottom="30px">
            <Box className={classes.flexRow}>
              <Box className={classes.flexRow} marginRight="20px">
                {courierPickup && <MailIcon className={classes.pickupIcon} />}
                <Typography
                  className={`${classes.pickupType} ${
                    !courierPickup && classes.disabled
                  }`}
                >
                  Kurier
                </Typography>
              </Box>
              <Box className={classes.flexRow} marginRight="20px">
                {personalPickup && (
                  <EmojiPeopleIcon className={classes.pickupIcon} />
                )}
                <Typography
                  className={`${classes.pickupType} ${
                    !personalPickup && classes.disabled
                  }`}
                >
                  Odbiór osobisty
                </Typography>
              </Box>
              <Box className={classes.flexRow}>
                {paczkomatPickup && (
                  <LocalShippingIcon className={classes.pickupIcon} />
                )}
                <Typography
                  className={`${classes.pickupType} ${
                    !paczkomatPickup && classes.disabled
                  }`}
                >
                  Paczkomat
                </Typography>
              </Box>
            </Box>
          </Box>
          <Box display="flex" marginBottom="30px">
            <Box flexGrow="1" marginRight="20px">
              <Typography className={classes.header}>Opis</Typography>
              <Typography className={classes.description}>
                {ad.description}
              </Typography>
            </Box>
            <Box>
              <Box className={classes.iconRoot}>
                <RoomIcon className={classes.icon} />
                <Typography className={classes.location}>
                  {ad.location.name}
                </Typography>
              </Box>
              <Box className={classes.mapContainer}>
                <MapContainer
                  center={center}
                  zoom={13}
                  className={classes.map}
                  zoomControl={false}
                  scrollWheelZoom={false}
                  doubleClickZoom={false}
                  boxZoom={false}
                  dragging={false}
                  eventHandlers={{}}
                >
                  <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                  />
                  <Circle center={center} radius={600} />
                  <ClickHandler center={center} />
                </MapContainer>
              </Box>
            </Box>
          </Box>
          <Box display="flex" alignItems="strech" marginBottom="30px">
            <ProductList
              products={ad.products}
              isSearchAd={isSearchAd}
              isSearchAd={isSearchAd}
            />
            {!isSearchAd && <Discounts discounts={ad.discounts} />}
          </Box>
        </React.Fragment>
      )}
    </Box>
  );
};

export default AdPage;
