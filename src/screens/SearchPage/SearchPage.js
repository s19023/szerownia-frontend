import React from "react";
import Box from "@material-ui/core/Box";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from "@material-ui/core/styles";
import Search from "../../components/Search/Search";
import SearchBox from "./components/SearchBox";
import SearchResults from "../../components/SearchResults/SearchResults";
import { useSelector, useDispatch } from "react-redux";
import { searchAction } from "../../redux/modules/search";

const useStyles = makeStyles(() => ({
  tabs: {
    marginBottom: "30px",
  },
  flex: {
    justifyContent: "center",
  },
}));

const SearchPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const searchResults = useSelector(
    (state) => state.searchReducer.searchResults
  );
  const isLoading = useSelector((state) => state.searchReducer.isSearching);
  const page = useSelector((state) => state.searchReducer.page);
  const pages = useSelector((state) => state.searchReducer.pages);
  const isSearchAd = useSelector((state) => state.searchReducer.isSearch);

  const setIsSearchAd = (val) => {
    dispatch(searchAction.setSearch({ isSearch: val !== 0, willLoad: true }));
  };

  const handleChangePage = (page) => {
    dispatch(searchAction.changePage({ page }));
  };

  return (
    <Box width="100%" maxWidth="1500px">
      <Search />
      <SearchBox />
      <Tabs
        className={classes.tabs}
        classes={{ flexContainer: classes.flex }}
        value={isSearchAd ? 1 : 0}
        onChange={(e, v) => setIsSearchAd(v)}
      >
        <Tab value={0} label="Ogłoszenia użyczenia" />
        <Tab value={1} label="Ogłoszenia poszukiwania" />
      </Tabs>
      <SearchResults
        results={searchResults}
        isLoading={isLoading}
        isSearchAd={isSearchAd}
        page={page}
        pages={pages}
        changePage={handleChangePage}
      />
    </Box>
  );
};

export default SearchPage;
