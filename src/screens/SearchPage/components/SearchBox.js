import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import Typography from "@material-ui/core/Typography";
import MuiTextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import ArrowDown from "@material-ui/icons/ArrowDownward";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { useSelector, useDispatch } from "react-redux";
import { searchAction } from "../../../redux/modules/search";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    width: "100%",
  },
  inputBox: {
    boxSizing: "border-box",
    width: "20%",
    padding: "5px 10px",
  },
  title: {
    fontSize: "12px",
    fontFamily: "Poppins, sans-serif",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    color: theme.palette.text.primary,
  },
  input: {
    width: "100%",
  },
  numberRoot: {
    width: "100%",
    display: "flex",
  },
  numberInput: {
    flexBasis: "1px",
    flexGrow: "1",
    "&:not(:last-of-type)": {
      marginRight: "10px",
    },
  },
  collapse: {
    marginBottom: "10px",
  },
  icon: {
    color: theme.palette.primary.main,
    transform: (props) => (props.open ? "rotate(180deg)" : "rotate(0deg)"),
    transition: theme.transitions.create(
      "transform",
      theme.transitions.duration.standard
    ),
  },
}));

const TextField = withStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    "& .MuiOutlinedInput-root": {
      fontFamily: "Poppins, sans-serif !important",
      "& fieldset": {
        border: "none !important",
      },
    },
  },
}))(MuiTextField);

const createField = (classes, feature, updateFeature, filters) => {
  const filter = filters.filter((f) => f.id === feature.idFeature);

  switch (feature.type) {
    case "BOOLEAN": {
      const value = filter[0]?.value;

      console.log(value);

      const showBoth = value === undefined;
      return (
        <Box className={classes.numberRoot}>
          <FormControlLabel
            label="Tak"
            control={
              <Checkbox
                checked={showBoth || value}
                color="primary"
                size="small"
                onChange={(e) =>
                  updateFeature(
                    feature.idFeature,
                    e.target.checked,
                    "bool",
                    true,
                    value
                  )
                }
              />
            }
          />
          <FormControlLabel
            label="Nie"
            control={
              <Checkbox
                checked={showBoth || !value}
                color="primary"
                size="small"
                onChange={(e) =>
                  updateFeature(
                    feature.idFeature,
                    e.target.checked,
                    "bool",
                    false,
                    value
                  )
                }
              />
            }
          />
        </Box>
      );
    }
    case "TEXT": {
      return (
        <TextField
          className={classes.input}
          variant="outlined"
          size="small"
          onChange={(e) =>
            updateFeature(feature.idFeature, e.target.value, "eq")
          }
        />
      );
    }
    default: {
      return (
        <Box className={classes.numberRoot}>
          <TextField
            variant="outlined"
            size="small"
            className={classes.numberInput}
            placeholder="Od"
            onChange={(e) =>
              updateFeature(feature.idFeature, e.target.value, "gt")
            }
          />
          <TextField
            variant="outlined"
            size="small"
            className={classes.numberInput}
            placeholder="Do"
            onChange={(e) =>
              updateFeature(feature.idFeature, e.target.value, "lt")
            }
          />
        </Box>
      );
    }
  }
};

const SearchBox = () => {
  const [hasCahnged, setHasChanged] = useState(false);
  const [showMore, setShowMore] = useState(false);
  const features = useSelector((state) => state.searchReducer.features);
  const category = useSelector((state) => state.searchReducer.category);
  const q = useSelector((state) => state.searchReducer.q);
  const isSearch = useSelector((state) => state.searchReducer.isSearch);
  const page = useSelector((state) => state.searchReducer.page);
  const filters = useSelector((state) => state.searchReducer.filters);
  const classes = useStyles({ open: showMore });
  const dispatch = useDispatch();

  useEffect(() => {
    if (hasCahnged) {
      dispatch(searchAction.search({ category, q, filters, isSearch }));
    }
  }, [filters]);

  useEffect(() => {
    setShowMore(false);
  }, [category]);

  const updateFeature = (id, value, op, checkboxVal, oldVal) => {
    setHasChanged(true);

    console.log(value, checkboxVal, oldVal);

    if (checkboxVal !== undefined) {
      if (oldVal === undefined && value) {
        value = checkboxVal;
      } else if (value && ((checkboxVal && !oldVal) || (!checkboxVal && oldVal))) {
        value = undefined;
      } else if (!value && (checkboxVal === oldVal)) {
        value = undefined;
      } else if (!value && !checkboxVal) {
        value = true;
      } else if (!value && checkboxVal) {
        value = false;
      }
    }

    dispatch(searchAction.updateFilters({ id, value, op }));
  };

  if (isSearch) {
    return null;
  }

  return (
    <Box marginBottom="30px">
      <Box className={classes.root}>
        {features
          .filter((feature) => feature.required)
          .map((feature) => (
            <Box key={feature.idFeature} className={classes.inputBox}>
              <Typography className={classes.title}>{feature.name}</Typography>
              {createField(classes, feature, updateFeature, filters)}
            </Box>
          ))}
      </Box>
      <Collapse in={showMore} className={classes.collapse}>
        <Box className={classes.root}>
          {features
            .filter((feature) => !feature.required)
            .map((feature) => (
              <Box key={feature.idFeature} className={classes.inputBox}>
                <Typography className={classes.title}>
                  {feature.name}
                </Typography>
                {createField(classes, feature, updateFeature, filters)}
              </Box>
            ))}
        </Box>
      </Collapse>
      {features.filter((feature) => !feature.required).length > 0 && (
        <Button
          color="primary"
          startIcon={<ArrowDown className={classes.icon} />}
          onClick={() => setShowMore(!showMore)}
        >
          {showMore ? "Pokaż mniej" : "Pokaż więcej"}
        </Button>
      )}
    </Box>
  );
};

export default SearchBox;
