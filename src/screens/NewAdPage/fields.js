import ImageDropzone from "./components/ImageDropzone/ImageDropzone";
import ItemsAccordion from "./components/ItemsAccordion/ItemsAccordion";
import LocationPicker from "../../components/LocationPicker/LocationPicker";
import TimeSlots from "./components/TimeSlots/TimeSlots";
import PickupTypeAccordion from "./components/PickupTypeAccordion/PickupTypeAccordion";
import RequirementsAccordion from "./components/RequirementsAccordion/RequirementsAccordion";
import PromoteAd from "./components/PromoteAd/PromoteAd";

export const fields = [
  {
    name: "Tytuł",
    id: "title",
    showInSearch: true,
    showInEdit: true,
    isRequired: true,
  },
  {
    name: "Opis",
    id: "description",
    formType: "textArea",
    showInSearch: true,
    isRequired: true,
    showInEdit: true,
  },
  {
    name: "Cena za dzień",
    id: "pricePerDay",
    adornment: "zł",
    isRequired: true,
    type: "float",
    otherType: "price",
    minValue: 1,
    showInEdit: true,
  },
  {
    name: "Kaucja",
    id: "depositAmount",
    adornment: "zł",
    isRequired: true,
    type: "number",
    otherType: "price",
    minValue: 1,
    showInEdit: true,
  },
  {
    name: "Cena za dzień opóźnienia",
    id: "penaltyForEachDayOfDelayInReturns",
    helperText: "Cena zostanie dodana do ceny za dzień wypożyczenia",
    adornment: "zł",
    isRequired: true,
    type: "float",
    otherType: "price",
    minValue: 0,
    showInEdit: true,
  },
  {
    name: "Promować ogłoszenie?",
    id: "promoted",
    component: PromoteAd,
    formType: "custom",
    showInEdit: true,
  },
  {
    name: "Zdjęcia",
    id: "imageNames",
    formType: "custom",
    component: ImageDropzone,
    isRequired: true,
    type: "array",
  },
  {
    name: "Lokalizacja",
    id: "location",
    formType: "custom",
    showInSearch: true,
    component: LocationPicker,
    isRequired: true,
    type: "array",
    customValidate: (value) => {
      if (!value.latitude || !value.longitude) {
        return "To pole jest wymagane";
      }
    },
  },
  {
    name: "Okres dostępności ogłoszenia",
    id: "timeSlotList",
    formType: "custom",
    showInSearch: true,
    component: TimeSlots,
    isRequired: true,
    type: "array",
  },
  {
    name: "Przedmioty",
    id: "products",
    formType: "custom",
    showInSearch: true,
    component: ItemsAccordion,
    isRequired: true,
    type: "array",
    showInEdit: true,
  },
  {
    name: "Sposób wysyłki",
    id: "pickupMethod",
    formType: "custom",
    showInSearch: true,
    component: PickupTypeAccordion,
    isRequired: true,
    type: "array",
    showInEdit: true,
  },
  {
    name: "Wymagania",
    id: "requirements",
    formType: "custom",
    component: RequirementsAccordion,
    type: "array",
  },
];
