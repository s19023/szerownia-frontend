export const defaultValues = {
  averageOpinion: {
    id: "averageOpinion",
  },
  averageRentalPrice: {
    id: "averageRentalPrice",
  },
  customRequirement: {
    id: "customRequirement",
  },
  depositAmount: {
    id: "depositAmount",
    required: true,
  },
  description: { id: "description", required: true },
  endDate: {
    id: "endDate",
  },
  timeSlotList: {
    id: "timeSlotList",
    type: "array",
  },
  imageNames: {
    id: "imageNames",
    type: "array",
  },
  location: {
    id: "location",
    required: true,
    type: "object",
    validate: (location) => {
      if (!location) {
        return false;
      }
      const { latitude, longitude, name } = location;

      return latitude !== null && longitude !== null && name !== null;
    },
  },
  minRentalHistory: { id: "minRentalHistory" },
  penaltyForEachDayOfDelayInReturns: {
    id: "penaltyForEachDayOfDelayInReturns",
    required: true,
  },
  pickupMethod: { id: "pickupMethod", required: true, type: "array" },
  pricePerDay: { id: "pricePerDay", required: true },
  products: { id: "products", required: true, type: "array" },
  publicationDate: { id: "publicationDate", required: true },
  title: { id: "title", required: true },
  searchAdId: { id: "searchAdId" },
};
