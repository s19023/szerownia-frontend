import React, { useEffect, useState, useRef } from "react";
import Box from "@material-ui/core/Box";
import FormCard from "../../components/FormCard/FormCard";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import TextField from "@material-ui/core/TextField";
import DatePicker from "../../components/DatePicker/DatePicker";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import DiscountDialog from "./components/DiscountDialog/DiscountDialog";
import SmallAdDetails from "../../components/AdDetails/SmallAdDetails";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";
import Loader from "../../components/Loader/Loader";
import { useHistory, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";
import { fields as requirementsFields } from "./components/RequirementsAccordion/fields";
import { hasErrors, validateForm } from "../../utils/validateForm";
import { defaultValues } from "./values";
import useQuery from "../../utils/useQuery";
import { snackbarAction } from "../../redux/modules/snackbar";

import postRentalAd from "../../services/rentalAds/postRentalAd";
import postSearchAd from "../../services/searchAds/postSearchAd";
import getSearchAd from "../../services/searchAds/getSearchAd";
import getRentalAd from "../../services/rentalAds/getRentalAd";
import updateSearchAd from "../../services/searchAds/updateSearchAd";
import updateRentalAd from "../../services/rentalAds/updateRentalAd";
import updateModTicket from "../../services/modTickets/updateModTicket";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    flexGrow: "1",
    justifyContent: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  tabs: {
    marginBottom: "10px",
  },
  tabsContainer: {
    justifyContent: "center",
  },
  inputRow: {
    width: "100%",
    padding: "10px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    width: "100%",
    marginBottom: "10px",
  },
  buttonCancel: {
    width: "100%",
  },
  input: {
    width: "300px",
  },
}));

const createValues = (fields) => {
  const values = {};
  Object.keys(fields).forEach((key) => {
    const field = fields[key];
    switch (field.type) {
      case "array":
        values[field.id] = [];
        break;
      case "object":
        values[field.id] = {};
        break;
      default:
        values[field.id] = "";
    }
  });

  return values;
};

const mapAdToValues = (ad) => {
  return {
    ...ad,
    timeSlotsList: ad.timeSlotList,
    products: ad.products.map((p) => p.id),
    pickupMethod: ad.pickupMethodList,
  };
};

const FormField = ({
  field,
  changeValues,
  isSearchAd,
  error,
  errors,
  values,
}) => {
  const classes = useStyles();
  const value = values[field.id];

  switch (field.formType) {
    case "date": {
      return (
        <Box className={classes.input}>
          <DatePicker
            label={field.name}
            handleChange={(date) => changeValues(field.id, date)}
            id={field.id}
          />
        </Box>
      );
    }
    case "custom": {
      const Component = field.component;
      return (
        <Component
          handleChange={changeValues}
          isSearchAd={isSearchAd}
          error={error}
          errors={errors}
          values={values}
        />
      );
    }
    case "textArea": {
      return (
        <TextField
          id={field.id}
          value={value || ""}
          multiline
          rows={4}
          rowsMax={4}
          label={field.name}
          className={classes.input}
          variant="outlined"
          onChange={(e) => changeValues(field.id, e.target.value)}
          error={!!error}
          helperText={error}
        />
      );
    }
    default: {
      return (
        <TextField
          id={field.id}
          label={field.name}
          className={classes.input}
          value={value || ""}
          variant="outlined"
          onChange={(e) => changeValues(field.id, e.target.value)}
          error={!!error}
          helperText={error}
          InputProps={{
            endAdornment: field.adornment ? (
              <InputAdornment position="end">{field.adornment}</InputAdornment>
            ) : undefined,
          }}
        />
      );
    }
  }
};

const NewAdPage = ({ edit }) => {
  const classes = useStyles();
  const { state } = useLocation();
  const { adId: id, ticketId, isSearchAd: search } = (state || {});
  const [values, setValues] = useState(createValues(defaultValues));
  const [errors, setErrors] = useState({});
  const [disabled, setDisabled] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadingAd, setLoadingAd] = useState(edit && !!id);
  const [open, setOpen] = useState(false);
  const timeout = useRef(null);
  const [adId, setAdId] = useState(null);
  const [isSearchAd, setIsSearchAd] = useState(search ? 1 : 0);
  const searchAd = useQuery().get("searchAd");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    if (edit && !id) {
      history.length ? history.goBack() : history.push("/");
    }

    if (!edit) {
      return;
    }

    const getAd = isSearchAd ? getSearchAd : getRentalAd;
    getAd(id)
      .then((result) => {
        setValues(mapAdToValues(result));
        setLoadingAd(false);
      })
      .catch(() => {
        history.length ? history.goBack() : history.push("/");
      });
  }, [edit, id]);

  const changeValues = (key, value) => {
    const tmpValues = { ...values };
    const field = fields
      .concat(requirementsFields)
      .find((field) => field.id === key);
    tmpValues[key] = field.type === "float" ? value.replace(",", ".") : value;
    setValues(tmpValues);
  };

  useEffect(() => {
    if (!searchAd) {
      return;
    }

    const tmpValues = { ...values };
    tmpValues.searchAdId = searchAd;
    setValues(tmpValues);
  }, [searchAd]);

  const submit = () => {
    let filterFields = fields.filter((field) => showField(field));
    if (!edit && !isSearchAd) {
      filterFields = filterFields.concat(requirementsFields)
    }

    const errors = validateForm(filterFields, values);
    setErrors(errors);

    if (hasErrors(errors)) {
      dispatch(
        snackbarAction.openSnackbar({
          error: true,
          message:
            "Formularz zawiera błędy, popraw je przed wystawieniem ogłoszenia",
        })
      );
      return;
    }

    setDisabled(true);
    setLoading(true);
    timeout.current = setTimeout(() => setOpen(true), 500);
    const postFunc = isSearchAd
      ? edit
        ? updateSearchAd
        : postSearchAd
      : edit
      ? updateRentalAd
      : postRentalAd;

    postFunc(values, id)
      .then((id) => {
        if (isSearchAd || (edit && !ticketId)) {
          dispatch(
            snackbarAction.openSnackbar({
              error: false,
              message: `Ogłoszenie ${edit ? "edytowano" : "dodano"} pomyślnie`,
            })
          );
          history.length ? history.goBack() : history.push("/");
        } else if (edit && ticketId) {
          updateModTicket(ticketId).then(() => {
            dispatch(
              snackbarAction.openSnackbar({
                error: false,
                message: "Ogłoszenie edytowane i wysłane do poprawy",
              })
            );
            history.length ? history.goBack() : history.push("/");
          });
        } else {
          setAdId(id);
          setTimeout(() => setLoading(false), 500);
        }
      })
      .catch((error) => {
        if (error.response.status === 400) {
          history.push("/");
          dispatch(
            snackbarAction.openSnackbar({
              error: true,
              message:
                "Ogłoszenie zawiera wulgaryzmy i zostało wysłane do moderacji",
            })
          );
        } else {
          setOpen(false);
          setLoading(false);
          setDisabled(false);
          clearTimeout(timeout.current);
          dispatch(
            snackbarAction.openSnackbar({
              error: true,
              message: "Wystąpił nieznany błąd",
            })
          );
        }
      });
  };

  const showField = (field) => {
    return (
      (isSearchAd === 0 || field.showInSearch) &&
      (edit ? field.showInEdit : true)
    );
  };

  if (loadingAd) {
    return <Loader isLoading={loadingAd} />;
  }

  return (
    <Box className={classes.root}>
      <ScrollToTop />
      <DiscountDialog
        open={open && isSearchAd === 0}
        isLoading={loading}
        id={adId}
      />
      <FormCard
        title={edit ? "Edytuj ogłoszenie" : "Dodaj nowe ogłoszenie"}
        width="750px"
      >
        {!searchAd && !edit && (
          <Tabs
            color="secondary"
            value={isSearchAd}
            onChange={(e, v) => setIsSearchAd(v)}
            className={classes.tabs}
            classes={{ flexContainer: classes.tabsContainer }}
          >
            <Tab label="Ogłoszenie użyczenia" value={0} />
            <Tab label="Ogłoszenie poszukiwania" value={1} />
          </Tabs>
        )}
        <form className={classes.form} onSubmit={(e) => e.preventDefault()}>
          {fields.map((field) => {
            const show = showField(field);
            if (show) {
              return (
                <Box key={field.id} className={classes.inputRow}>
                  <FormField
                    field={field}
                    changeValues={changeValues}
                    isSearchAd={isSearchAd}
                    error={errors[field.id]}
                    errors={errors}
                    values={values}
                  />
                </Box>
              );
            } else {
              return null;
            }
          })}
          <Button
            id="add-new-confirm"
            className={classes.button}
            variant="contained"
            color="primary"
            disabled={disabled}
            onClick={(e) => submit(e)}
            type="sumbit"
          >
            {edit ? "Edytuj" : "Dodaj nowe"} ogłoszenie
          </Button>
        </form>
        <Button
          id="cancel-form"
          className={classes.buttonCancel}
          variant="contained"
          onClick={() => setTimeout(() => history.push("/"), 400)}
        >
          Anuluj
        </Button>
      </FormCard>
      {searchAd && !edit && (
        <Box marginLeft="20px" marginTop="40px">
          <SmallAdDetails id={searchAd} isSearchAd topText="Odpowiadasz na" />
        </Box>
      )}
    </Box>
  );
};

export default NewAdPage;
