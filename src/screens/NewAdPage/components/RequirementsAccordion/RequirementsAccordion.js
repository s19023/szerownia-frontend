import React from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Requirement from "./components/Requirement";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  header: {
    flexBasis: "33.33%",
    flexShrink: "0",
  },
  secondaryHeading: {
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  detailsRoot: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    boxSizing: "border-box",
  },
  itemRow: {
    width: "100%",
    textTransform: "unset",
  },
  itemBox: {
    display: "flex",
    flexGrow: "1",
  },
  itemName: {
    fontWeight: "bold",
    maxWidth: "30%",
    width: "30%",
  },
  itemMake: {
    color: theme.palette.text.secondary,
  },
}));

const RequirementsAccordion = ({ handleChange, errors }) => {
  const classes = useStyles();

  return (
    <Accordion
      className={classes.root}
      elevation={2}
      id="requirements-accordion"
    >
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.header}>Wymagania</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.detailsRoot}>
        {fields.map((field) => (
          <Requirement
            field={field}
            key={field.id}
            handleChange={handleChange}
            error={errors[field.id]}
          />
        ))}
      </AccordionDetails>
    </Accordion>
  );
};

export default RequirementsAccordion;
