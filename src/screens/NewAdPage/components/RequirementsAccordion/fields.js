export const fields = [
  {
    formType: "slider",
    id: "averageOpinion",
    name: "Wymagana średnia opinia",
    type: "float",
    maxValue: 5,
    minValue: 1,
  },
  {
    id: "averageRentalPrice",
    name: "Wymagana średnia kwota wypożyczeń",
    adornment: "zł",
    type: "number",
    minValue: 20,
  },
  {
    id: "minRentalHistory",
    name: "Wymagana liczba wypożczeń",
    type: "number",
    minValue: 1,
  },
  {
    id: "customRequirement",
    name: "Dodatkowe wymaganie",
  },
];
