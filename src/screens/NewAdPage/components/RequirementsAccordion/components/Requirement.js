import React, { useState, useRef } from "react";
import Box from "@material-ui/core/Box";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Slider from "@material-ui/core/Slider";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  label: {
    minWidth: "50%",
  },
  input: {
    flexGrow: "1",
  },
}));

const marks = Array(5)
  .fill(0)
  .map((number, index) => ({
    value: index + 1,
    label: index + 1,
  }));

const Requirement = ({ field, handleChange, error }) => {
  const classes = useStyles();
  const [isSelected, setIsSelected] = useState(false);
  const [value, setValue] = useState("");
  const timeout = useRef();

  const updateValue = (value) => {
    setValue(value);
    if (field.type === "slider") {
      clearTimeout(timeout.current);
      timeout.current = setTimeout(() => handleChange(field.id, value), 300);
    } else {
      handleChange(field.id, value);
    }
  };

  const handleCheck = (selected) => {
    setIsSelected(selected);
    handleChange(field.id, selected ? value : "");
  };

  return (
    <Box className={classes.root}>
      <FormControlLabel
        className={classes.label}
        label={field.name}
        control={
          <Checkbox
            id={field.id}
            color="primary"
            onChange={(e, c) => handleCheck(c)}
          />
        }
      />
      {field.type === "slider" ? (
        <Slider
          id={field.id}
          className={classes.input}
          disabled={!isSelected}
          step={0.1}
          min={1}
          max={5}
          defaultValue={4}
          valueLabelDisplay="auto"
          marks={marks}
          onChange={(e, v) => updateValue(v)}
        />
      ) : (
        <TextField
          id={field.id}
          className={classes.input}
          disabled={!isSelected}
          onChange={(e) => updateValue(e.target.value)}
          InputProps={{
            endAdornment: field.endAdornment ? (
              <InputAdornment position="end">
                {field.endAdornment}
              </InputAdornment>
            ) : undefined,
          }}
          error={!!error}
          helperText={error}
        />
      )}
    </Box>
  );
};

export default Requirement;
