import React, { useEffect, useState } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { makeStyles } from "@material-ui/core/styles";
import { types } from "./types";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  header: {
    flexBasis: "33.33%",
    flexShrink: "0",
  },
  secondaryHeading: {
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    flexGrow: "1",
  },
  error: {
    fontSize: "12px",
    color: theme.palette.error.main,
    flex: "none",
  },
  detailsRoot: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
  },
}));

const PickupTypeAccordion = ({ handleChange, error, values }) => {
  const classes = useStyles();
  const [selectedTypes, setSelected] = useState([]);
  const [selectedNames, setSelectedNames] = useState([]);

  useEffect(() => {
    setSelected(values.pickupMethod || []);
  }, [values.pickupMethod]);

  useEffect(() => {
    const names = [];
    types.forEach((type) => {
      if (selectedTypes.includes(type.id)) {
        names.push(type.accordionName);
      }
    });

    setSelectedNames(names);
  }, [selectedTypes]);

  const handleSelect = (selected, field) => {
    const { id } = field;
    let tmpSelected = [...selectedTypes];

    if (selected) {
      tmpSelected.push(id);
    } else {
      const index = tmpSelected.findIndex((elem) => elem === id);

      tmpSelected = tmpSelected
        .slice(0, index)
        .concat(tmpSelected.slice(index + 1));
    }

    handleChange("pickupMethod", tmpSelected);
  };

  return (
    <Accordion className={classes.root} elevation={2} id="pickup-type">
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.header}>Sposób wysyłki</Typography>
        <Typography className={classes.secondaryHeading}>
          {selectedNames.length !== 0
            ? `Wybrano: ${selectedNames.join(", ")}`
            : "Nie wybrano żadnego"}
        </Typography>
        {error && <Typography className={classes.error}>{error}</Typography>}
      </AccordionSummary>
      <AccordionDetails className={classes.detailsRoot}>
        {types.map((type) => (
          <FormControlLabel
            onChange={(e, checked) => handleSelect(checked, type)}
            key={type.id}
            label={type.name}
            control={
              <Checkbox
                id={type.id}
                color="primary"
                checked={selectedTypes.includes(type.id)}
              />
            }
          />
        ))}
      </AccordionDetails>
    </Accordion>
  );
};

export default PickupTypeAccordion;
