export const types = [
  {
    name: "Odbiór osobisty",
    accordionName: "odbiór osobisty",
    id: "PERSONAL_PICKUP",
  },
  {
    name: "Paczkomat",
    accordionName: "paczkomat",
    id: "PARCEL_LOCKER",
  },
  {
    name: "Przesyłka kurierska",
    accordionName: "przesyłkę kurierską",
    id: "COURIER",
  },
];
