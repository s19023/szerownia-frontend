import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import DatePicker from "../../../../components/DatePicker/DatePicker";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import ClearIcon from "@material-ui/icons/Clear";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  grid: {
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  }
}))

const TimeSlot = ({ minDate, dateFrom, dateTo, handleAdd, handleDelete, index }) => {
  const classes = useStyles();
  const [from, setFrom] = useState(dateFrom);
  const [to, setTo] = useState(dateTo);

  const add = () => {
    handleAdd({ startDate: from, endDate: to });
  };

  return (
    <React.Fragment>
      <Grid item xs={5}>
        <DatePicker
          label="Data od"
          value={dateFrom}
          id={`date-from-${index}`}
          minDate={minDate ? minDate : new Date()}
          maxDate={to}
          handleChange={(date) => setFrom(date)}
        />
      </Grid>
      <Grid item xs={5}>
        <DatePicker
          label="Data do"
          id={`date-to-${index}`}
          value={dateTo}
          minDate={from}
          handleChange={(date) => setTo(date)}
        />
      </Grid>
      {handleAdd && (
        <Grid item xs={2} className={classes.grid}>
          <Button
            variant="contained"
            color="primary"
            id="add-new-slot"
            disabled={!from || !to}
            onClick={() => add()}
          >
            Dodaj
          </Button>
        </Grid>
      )}
      {handleDelete && (
        <Grid item xs={2} className={classes.grid}>
          <IconButton onClick={() => handleDelete()} id={`delete-slot-${index}`}>
            <ClearIcon />
          </IconButton>
        </Grid>
      )}
    </React.Fragment>
  );
};

export default TimeSlot;
