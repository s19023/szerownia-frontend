import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TimeSlot from "./TimeSlot";
import DateRangePicker from "../../../../components/DateRangePicker/DateRangePicker";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  header: {
    flexGrow: "1",
  },
  secondaryHeading: {
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  error: {
    fontSize: "12px",
    color: theme.palette.error.main,
    flex: "none",
  },
  detailsRoot: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    boxSizing: "border-box",
  },
  helper: {
    fontSize: "12px",
    color: theme.palette.text.secondary,
  },
}));

const TimeSlots = ({ handleChange, error }) => {
  const classes = useStyles();
  const [timeSlots, setTimeSlots] = useState([]);
  const [disabledDates, setDisabledDates] = useState([]);
  const [newStartDate, setStartDate] = useState(null);
  const [newEndDate, setEndDate] = useState(null);

  const handleAdd = () => {
    let tmpSlots = [...timeSlots];
    tmpSlots.push({
      startDate: newStartDate.format("YYYY-MM-DD"),
      endDate: newEndDate.format("YYYY-MM-DD"),
    });
    tmpSlots = tmpSlots.sort((a, b) =>
      moment(a.startDate).diff(moment(b.startDate))
    );
    const tmpDates = [...disabledDates];
    const date = moment(newStartDate);
    tmpDates.push(date.format("YYYY-MM-DD"));
    while (date.isBefore(newEndDate)) {
      date.add(1, "day");
      tmpDates.push(date.format("YYYY-MM-DD"));
    }

    console.log(tmpDates);
    setDisabledDates(tmpDates);
    setTimeSlots(tmpSlots);
    setStartDate(null);
    setEndDate(null);
  };

  const handleDelete = (index) => {
    const slot = timeSlots[index];
    const tmpSlots = [...timeSlots].filter((obj, idx) => idx !== index);
    const startDate = moment(slot.startDate);
    const endDate = moment(slot.endDate);
    const tmpDates = [...disabledDates].filter(
      (date) => !moment(date).isBetween(startDate, endDate, "day", "[]")
    );
    setTimeSlots(tmpSlots);
    setDisabledDates(tmpDates);
  };

  useEffect(() => {
    handleChange("timeSlotList", timeSlots);
  }, [timeSlots]);

  return (
    <Accordion className={classes.root} elevation={2} id="timeslots-accordion">
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.header}>Okresy dostępności</Typography>
        {error && <Typography className={classes.error}>{error}</Typography>}
      </AccordionSummary>
      <AccordionDetails className={classes.detailsRoot}>
        <Grid container spacing={2} justify="center" alignItems="center">
          {timeSlots.map((slot, index) => (
            <TimeSlot
              handleDelete={() => handleDelete(index)}
              index={index}
              dateFrom={slot.startDate}
              dateTo={slot.endDate}
              key={slot.endDate}
            />
          ))}
          <Grid item>
            <Typography className={classes.helper}>
              Wybierz nowy okres
            </Typography>
            <DateRangePicker
              updateMinDate={setStartDate}
              updateMaxDate={setEndDate}
              disabledDates={disabledDates}
            />
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              startIcon={<AddIcon />}
              disabled={!newStartDate || !newEndDate}
              onClick={() => handleAdd()}
            >
              Dodaj okres
            </Button>
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};

export default TimeSlots;
