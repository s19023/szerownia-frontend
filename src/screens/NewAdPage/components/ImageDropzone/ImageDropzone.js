import React, { useState, useRef, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Dropzone from "react-dropzone";
import Image from "./Image";
import { makeStyles } from "@material-ui/core/styles";
import { randomBytes } from "crypto";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    marginBottom: "5px",
  },
  title: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
  error: {
    fontSize: "12px",
    color: theme.palette.error.main,
    marginBottom: "5px",
  },
  noFile: {
    display: "flex",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    fontSize: "24px",
    marginRight: "5px",
    color: theme.palette.primary.main,
  },
  noFileText: {
    fontSize: "20px",
    color: theme.palette.text.disabled,
  },
  helperText: {
    fontSize: "12px",
    "&:not(:last-child)": {
      marginBottom: "5px",
    },
  },
}));

const ImageDropzone = ({ handleChange, error }) => {
  const classes = useStyles();
  const [files, setFiles] = useState([]);
  const images = useRef(Array(8).fill(""));

  const addFiles = (filesToAdd) => {
    const tmpFiles = [...files];
    filesToAdd.forEach((file) => {
      if (tmpFiles.length < 8) {
        const tmpFile = file;
        tmpFile.id = randomBytes(16).toString("hex");
        tmpFiles.push(tmpFile);
      }
    });
    setFiles(tmpFiles);
  };

  const deleteImage = (index) => {
    const tmpFiles = [...files].filter((elem, idx) => idx !== index);
    const tmpImages = [...images.current].filter((elem, idx) => idx !== index);
    setFiles(tmpFiles);
    images.current = tmpImages;
  };

  const addImage = (fileName, index) => {
    const tmpImages = [...images.current];
    if (index >= images.length) {
      tmpImages.push(fileName);
    } else {
      tmpImages[index] = fileName;
    }
    images.current = tmpImages;
  };

  useEffect(() => {
    handleChange(
      "imageNames",
      images.current.filter((elem) => elem !== "")
    );
  }, [images.current]);

  return (
    <Box width="100%">
      <Typography className={classes.title}>Zdjęcia</Typography>
      <Typography className={classes.helperText}>
        Pierwsze zdjęcie będzie zdjęciem głównym
      </Typography>
      {error && <Typography className={classes.error}>{error}</Typography>}
      <Dropzone
        onDrop={(files) => addFiles(files)}
        onDropRejected={() => alert("Przesłane pliki nie spełniają wymagań")}
        accept={["image/jpeg", "image/png"]}
        maxSize={5 * 1024 * 1024}
      >
        {({ getRootProps, getInputProps }) => (
          <Box className={classes.root} {...getRootProps()}>
            <input {...getInputProps()} />
            <Grid container spacing={2}>
              {Array(8)
                .fill(0)
                .map((_, index) => (
                  <Image
                    srcFile={files[index]}
                    addImage={(fileName) => addImage(fileName, index)}
                    deleteImage={() => deleteImage(index)}
                    key={files[index] ? files[index].id : index}
                  />
                ))}
            </Grid>
          </Box>
        )}
      </Dropzone>
      <Typography className={classes.helperText}>
        Zdjęcia przyjmujemy w formacie .jpg, albo .png. Maksymalny rozmiar
        zdjęcia to 5MB.
      </Typography>
    </Box>
  );
};

export default ImageDropzone;
