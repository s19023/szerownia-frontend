import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Fade from "@material-ui/core/Fade";
import IconButton from "@material-ui/core/IconButton";
import CircularProgress from "@material-ui/core/CircularProgress";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
import Compressor from "compressorjs";

import postImage from "../../../../services/images/postImage";

const useStyles = makeStyles((theme) => ({
  emptyRoot: {
    height: "120px",
    borderRadius: "5px",
    border: "1px dashed #707070",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    height: "120px",
    borderRadius: "5px",
    overflow: "hidden",
    position: "relative",
  },
  image: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },
  overlay: {
    position: "absolute",
    backgroundColor: theme.palette.grey[900] + "AA",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
  },
}));

const Image = ({ srcFile, addImage, deleteImage }) => {
  const classes = useStyles();
  const [src, setSrc] = useState("");
  const [file, setFile] = useState(null);
  const [loaded, setLoaded] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [showHover, setShowHover] = useState(false);

  useEffect(() => {
    if (!srcFile) {
      return;
    }

    setIsLoading(true);
    const time = new Date();
    new Compressor(srcFile, {
      quality: 0.6,
      success: (result) => {
        setFile(result);
        const reader = new FileReader();
        reader.onload = () => {
          setSrc(reader.result);
          setLoaded(true);
        };
        reader.readAsDataURL(result);
      },
    });
  }, [srcFile]);

  useEffect(() => {
    if (!file) {
      return;
    }

    postImage(file, (progress) =>
      setUploadProgress((progress.loaded / progress.total) * 100)
    )
      .then((response) => {
        addImage(response.data[0].imageId);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [file]);

  const handleDelete = (e) => {
    e.stopPropagation();
    deleteImage();
  };

  if (loaded) {
    return (
      <Grid item xs={3}>
        <Box
          className={classes.root}
          onMouseEnter={() => setShowHover(true)}
          onMouseOver={() => setShowHover(true)}
          onMouseLeave={() => setShowHover(false)}
        >
          <img
            src={src}
            alt="Zdjęcie dodane przez Ciebie"
            className={classes.image}
          />
          <Fade in={showHover || isLoading}>
            <Box className={classes.overlay}>
              {isLoading ? (
                <CircularProgress
                  variant="determinate"
                  value={uploadProgress}
                />
              ) : (
                <IconButton color="secondary" onClick={handleDelete}>
                  <DeleteIcon />
                </IconButton>
              )}
            </Box>
          </Fade>
        </Box>
      </Grid>
    );
  }

  return (
    <Grid item xs={3}>
      <Box className={classes.emptyRoot}>
        {isLoading ? <CircularProgress /> : <AddAPhotoIcon />}
      </Box>
    </Grid>
  );
};

export default Image;
