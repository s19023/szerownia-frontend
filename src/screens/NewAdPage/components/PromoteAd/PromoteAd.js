import React, { useState, useEffect } from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Tooltip from "@material-ui/core/Tooltip";
import { useSelector } from "react-redux";
import getPromotedCount from "../../../../services/rentalAds/getPromotedCount";

const getCountText = (count) => {
  if (count === 5) return "ogłoszeń";
  if (count > 1) return "ogłoszenia";
  return "ogłoszenie";
};

const PromoteAd = ({ handleChange }) => {
  const isPremium = useSelector((state) => state.userReducer.isPremium);
  const [promoted, setPromoted] = useState(0);

  useEffect(() => {
    if (!isPremium) {
      return;
    }

    getPromotedCount().then((count) => {
      setPromoted(count);
    });
  }, [isPremium]);

  const getPopupContent = () => {
    if (!isPremium) {
      return "Aby promować ogłoszenia zakup konto premium.";
    } else {
      return promoted >= 5
        ? "Nie możesz już promować ogłoszeń."
        : `Możesz promować jeszcze ${5 - promoted} ${getCountText(
            5 - promoted
          )}.`;
    }
  };

  return (
    <Tooltip title={getPopupContent()}>
      <FormControlLabel
        label="Promuj ogłoszenie"
        control={
          <Checkbox
            disabled={!isPremium || promoted >= 5}
            onChange={(e, checked) => handleChange("promoted", checked)}
          />
        }
      />
    </Tooltip>
  );
};

export default PromoteAd;
