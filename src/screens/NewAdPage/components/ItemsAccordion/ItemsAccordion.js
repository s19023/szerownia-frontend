import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import Typography from "@material-ui/core/Typography";
import Collapse from "@material-ui/core/Collapse";
import Fade from "@material-ui/core/Fade";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { makeStyles } from "@material-ui/core/styles";

import getUserProducts from "../../../../services/products/getUserProducts";
import AddNewItem from "../../../../components/Item/AddNewItem";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  header: {
    flexBasis: "33.33%",
    flexShrink: "0",
  },
  secondaryHeading: {
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    flexGrow: "1",
  },
  error: {
    fontSize: "12px",
    color: theme.palette.error.main,
    flex: "none",
  },
  detailsRoot: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    boxSizing: "border-box",
  },
  itemRow: {
    width: "100%",
    textTransform: "unset",
  },
  itemBox: {
    display: "flex",
    flexGrow: "1",
  },
  itemName: {
    fontWeight: "bold",
    maxWidth: "30%",
    width: "30%",
  },
  itemMake: {
    color: theme.palette.text.secondary,
  },
}));

const getEnding = (len) => {
  if (len === 1) return "";
  if (len >= 10 && len < 20) return "ów";
  const lastDigit = len % 10;
  if (lastDigit >= 2 && lastDigit < 5) return "y";
  return "ów";
};

const ItemsAccordion = ({ handleChange, isSearchAd, error, values }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [selected, setSelected] = useState([]);
  const [addProductOpen, setAddProductOpen] = useState(false);
  const [collapseOpen, setCollapseOpen] = useState(false);

  const getProducts = () => {
    getUserProducts(isSearchAd)
      .then((products) => {
        setProducts(products);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  useEffect(() => {
    setSelected(values.products || []);
  }, [values.products])

  useEffect(() => {
    getProducts();
  }, [isSearchAd]);

  const handleSave = () => {
    setAddProductOpen(false);
    getProducts();
  };

  const selectNewItem = (index) => {
    const elemIndex = selected.findIndex((elem) => elem === index);
    let tmpSelected = [...selected];
    if (elemIndex === -1) {
      tmpSelected.push(index);
    } else {
      tmpSelected = tmpSelected.filter((elem) => elem !== index);
    }

    handleChange("products", tmpSelected);
    setSelected(tmpSelected);
  };

  return (
    <Accordion className={classes.root} elevation={2} id="items-accordion">
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.header}>Przedmioty</Typography>
        <Typography className={classes.secondaryHeading}>
          Wybrano {selected.length} przedmiot{getEnding(selected.length)}
        </Typography>
        {error && <Typography className={classes.error}>{error}</Typography>}
      </AccordionSummary>
      <AccordionDetails className={classes.detailsRoot}>
        {products.map((product) => (
          <Button
            id={product.idProduct}
            key={product.idProduct}
            className={classes.itemRow}
            onClick={() => selectNewItem(product.idProduct)}
            startIcon={
              selected.findIndex((elem) => product.idProduct === elem) !==
              -1 ? (
                <CheckBoxIcon color="primary" />
              ) : (
                <CheckBoxOutlineBlankIcon />
              )
            }
          >
            <Box className={classes.itemBox}>
              <Typography className={classes.itemName}>
                {product.name}
              </Typography>
              <Typography className={classes.itemMake}>
                {product.make} {product.model}
              </Typography>
            </Box>
          </Button>
        ))}
        <Collapse in={addProductOpen} onExited={() => setCollapseOpen(false)}>
          <AddNewItem
            handleClose={() => setAddProductOpen(false)}
            collapseOpen={collapseOpen}
            handleSuccess={() => handleSave()}
            isSearchAd={isSearchAd}
          />
        </Collapse>
        <Fade in={!addProductOpen}>
          <Button
            color="primary"
            onClick={() => {
              setAddProductOpen(true);
              setCollapseOpen(true);
            }}
          >
            Dodaj nowy przedmiot
          </Button>
        </Fade>
      </AccordionDetails>
    </Accordion>
  );
};

export default ItemsAccordion;
