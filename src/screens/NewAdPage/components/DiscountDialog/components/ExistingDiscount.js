import React from "react";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";

const useStyles = makeStyles(() => ({
  input: {
    margin: "10px",
  },
  buttonBox: {
    width: "80px",
    display: "flex",
    justifyContent: "center",
  },
}));

const ExistingDiscount = ({ discount, handleDelete }) => {
  const classes = useStyles();

  return (
    <Box display="flex" alignItems="center" marginBottom="10px">
      {fields.map((field) => (
        <TextField
          readOnly
          className={classes.input}
          key={field.id}
          label={field.label}
          value={discount[field.id]}
          InputProps={{
            endAdornment: field.adornment ? (
              <InputAdornment position="end">{field.adornment}</InputAdornment>
            ) : undefined,
          }}
        />
      ))}
      <Box className={classes.buttonBox}>
        <IconButton
          id={`delete-discount-${discount.id}`}
          onClick={() => handleDelete()}
        >
          <CloseIcon />
        </IconButton>
      </Box>
    </Box>
  );
};

export default ExistingDiscount;
