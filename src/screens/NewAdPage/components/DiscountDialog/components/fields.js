export const fields = [
  {
    id: "discount",
    isRequired: true,
    label: "Wysokość zniżki",
    adornment: "%",
    type: "number",
    minValue: 0,
    maxValue: 99,
  },
  {
    id: "minDays",
    isRequired: true,
    label: "Minimalna ilość dni",
    type: "number",
    minValue: 1,
  },
];
