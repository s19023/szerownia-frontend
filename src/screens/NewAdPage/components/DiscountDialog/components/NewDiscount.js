import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { fields as defaultFields } from "./fields";
import { hasErrors, validateForm } from "../../../../../utils/validateForm";

const useStyles = makeStyles(() => ({
  input: {
    margin: "10px",
  },
  button: {
    width: "80px",
  },
}));

const NewDiscount = ({ id, handleClose, lastValue }) => {
  const classes = useStyles();
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const [fields, setFields] = useState(defaultFields);

  useEffect(() => {
    const tmpFields = [...fields];
    tmpFields.forEach((field) => {
      if (!lastValue) {
        return;
      }
      field.minValue = Number.parseInt(lastValue[field.id]) + 1;
    });

    setFields(tmpFields);
  }, [lastValue]);

  const handleChange = (key, value) => {
    const tmpValues = { ...values };
    tmpValues[key] = value;
    setValues(tmpValues);
  };

  const handleSubmit = () => {
    const errors = validateForm(fields, values);
    const isValid = !hasErrors(errors);
    if (isValid) {
      handleClose({ id, minDays: values.minDays, discount: values.discount });
    } else {
      setErrors(errors);
    }
  };

  return (
    <Box display="flex" alignItems="center">
      <Box display="flex">
        {fields.map((field) => (
          <TextField
            className={classes.input}
            key={field.id}
            onChange={(e) => handleChange(field.id, e.target.value)}
            label={field.label}
            error={!!errors[field.id]}
            helperText={errors[field.id]}
            InputProps={{
              endAdornment: field.adornment ? (
                <InputAdornment position="end">
                  {field.adornment}
                </InputAdornment>
              ) : undefined,
            }}
          />
        ))}
      </Box>
      <Button
        className={classes.button}
        variant="contained"
        onClick={() => handleSubmit()}
        id="add-discount"
      >
        Dodaj
      </Button>
    </Box>
  );
};

export default NewDiscount;
