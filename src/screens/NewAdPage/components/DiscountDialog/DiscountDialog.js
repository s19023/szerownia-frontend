import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import NewDiscount from "./components/NewDiscount";
import ExistingDiscount from "./components/ExistingDiscount";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../../redux/modules/snackbar";

import createDiscounts from "../../../../services/discounts/createDiscounts";

const DiscountDialog = ({ open, isLoading, id }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [discounts, setDiscounts] = useState([]);
  const [disabled, setDisabled] = useState(false);
  const [newDiscountOpen, setNewDiscountOpen] = useState(false);

  const handleSubmit = () => {
    setDisabled(true);
    createDiscounts(discounts)
      .then(() => handleCancel())
      .catch(() => {
        setDisabled(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message: "Wystąpił błąd poczas tworzenia rabatów ",
          })
        );
      });
  };

  const handleCancel = () => {
    setTimeout(() => {
      dispatch(
        snackbarAction.openSnackbar({
          error: false,
          message: "Ogłoszenie dodano pomyślnie ",
        })
      );
      history.push("/");
    }, 400);
  };

  const handleDelete = (index) => {
    let tmpDiscounts = [...discounts];
    tmpDiscounts = tmpDiscounts
      .slice(0, index)
      .concat(tmpDiscounts.slice(index + 1));
    setDiscounts(tmpDiscounts);
  };

  const handleClose = (discount) => {
    const tmpDiscounts = [...discounts];
    tmpDiscounts.push(discount);
    setDiscounts(tmpDiscounts);
    setNewDiscountOpen(false);
  };

  return (
    <Dialog open={open}>
      {isLoading ? (
        <Box padding="40px">
          <CircularProgress size={100} />
        </Box>
      ) : (
        <React.Fragment>
          <DialogTitle>Dodać rabaty do ogłoszenia?</DialogTitle>
          <DialogContent>
            <Box marginBottom="20px">
              Dzięki rabatom ogłoszenie może wiele zyskać w oczach potencjalnych
              klientów 😉
            </Box>
            <Box display="flex" flexDirection="column" alignItems="center">
              {discounts.map((discount, index) => (
                <ExistingDiscount
                  discount={discount}
                  handleDelete={() => handleDelete(index)}
                />
              ))}
              {newDiscountOpen ? (
                <NewDiscount
                  id={id}
                  handleClose={handleClose}
                  lastValue={discounts[discounts.length - 1]}
                />
              ) : (
                <Button
                  id="add-another-discount"
                  color="primary"
                  variant="contained"
                  onClick={() => setNewDiscountOpen(true)}
                >
                  {discounts.length > 0 ? "Dodaj kolejny rabat" : "Dodaj rabat"}
                </Button>
              )}
            </Box>
          </DialogContent>
          <DialogActions>
            <Button
              color="secondary"
              id="discount-cancel"
              disabled={disabled}
              onClick={() => handleCancel()}
            >
              {discounts.length > 0 ? "Anuluj" : "Nie, dziękuję"}
            </Button>
            {discounts.length > 0 && (
              <Button
                color="primary"
                disabled={disabled}
                onClick={() => handleSubmit()}
                id="discount-continue"
              >
                Kontynuuj
              </Button>
            )}
          </DialogActions>
        </React.Fragment>
      )}
    </Dialog>
  );
};

export default DiscountDialog;
