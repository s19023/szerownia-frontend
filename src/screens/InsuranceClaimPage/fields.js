import moment from "moment";

export const fields = [
  {
    name: "Status zgłoszenia",
    getValue: (claim, statuses) => statuses && statuses[claim.status],
  },
  {
    row: [
      {
        name: "Data zgłoszenia szkody",
        getValue: (claim) =>
          moment(claim.reportDate).format("DD.MM.YYYY HH:mm"),
      },
      {
        name: "Data wystąpienia szkody",
        getValue: (claim) => moment(claim.claimDate).format("DD.MM.YYYY"),
      },
    ],
  },
  {
    name: "Suma ubezpieczenia",
    getValue: (claim) => `${claim.policy.insuranceTotal} zł`,
  },
  {
    row: [
      {
        name: "Data zlecenia oględzin",
        getValue: (claim) =>
          claim.dateOfAssignToTheAppraiser
            ? moment(claim.dateOfAssignToTheAppraiser).format("DD.MM.YYYY HH:mm")
            : "Nie zlecono jeszcze oględzin",
      },
      {
        name: "Rzeczoznawca odpowiedzialny za oględziny",
        getValue: (claim) =>
          claim.appraiser
            ? `${claim.appraiser.firstName} ${claim.appraiser.lastName}`
            : "Nie zlecono jeszcze oględzin",
      },
    ],
  },
  {
    name: "Uzasadnienie decyzji",
    getValue: (claim) => claim.justification,
  },
  {
    name: "Numer szkody w TU",
    getValue: (claim) => claim.insuranceCaseId,
  },
];

export const secondaryFields = [
  {
    name: "Opis szkody",
    getValue: (claim) => claim.damageDescription,
  },
  {
    name: "Opis okoliczności",
    getValue: (claim) => claim.circumstances,
  },
];
