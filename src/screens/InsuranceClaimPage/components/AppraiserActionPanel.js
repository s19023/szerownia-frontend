import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Adornment from "@material-ui/core/InputAdornment";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogHeader from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router";
import createInspection from "../../../services/insuranceAppraiser/createInspection";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";
import { hasErrors, validateForm } from "../../../utils/validateForm";

const fields = [
  {
    id: "damageAssessment",
    type: "number",
    adornment: "zł",
    minValue: "1",
  },
  {
    id: "description",
    minLength: "30",
  },
];

const useStyles = makeStyles(() => ({
  card: {
    display: "flex",
    justifyContent: "center",
    padding: "20px",
    "&:not(:last-of-type)": {
      marginBottom: "20px",
    },
  },
  flexRoot: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  input: {
    width: "100%",
    maxWidth: "400px",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  dialogContent: {
    width: "500px",
  },
}));

const AppraiserActionPanel = ({ id }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const history = useHistory();
  const dispatch = useDispatch();

  const handleEdit = (key, value) => {
    const tmpValues = { ...values };
    tmpValues[key] = value;
    setValues(tmpValues);
  };

  const renderDialog = () => {
    return (
      <React.Fragment>
        <DialogHeader>Wyceń szkodę</DialogHeader>
        <DialogContent className={classes.dialogContent}>
          <Box className={classes.flexRoot}>
            <TextField
              className={classes.input}
              onChange={(e) => handleEdit("damageAssessment", e.target.value)}
              label="Wartość szkody"
              variant="outlined"
              error={!!errors.damageAssessment}
              helperText={errors.damageAssessment}
              InputProps={{
                endAdornment: <Adornment position="end">zł</Adornment>,
              }}
            />
            <TextField
              className={classes.input}
              onChange={(e) => handleEdit("description", e.target.value)}
              label="Opis szkody"
              variant="outlined"
              multiline
              error={!!errors.description}
              helperText={errors.description}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            color="secondary"
            onClick={() => setDialogOpen(false)}
            style={{ marginRight: "10px" }}
          >
            Anuluj
          </Button>
          <Button
            color="primary"
            disabled={isLoading}
            onClick={() => handleSubmit()}
            startIcon={isLoading && <CircularProgress size={16} />}
          >
            Zapisz
          </Button>
        </DialogActions>
      </React.Fragment>
    );
  };

  const handleSubmit = () => {
    const errors = validateForm(fields, values);
    console.log(errors);
    setErrors(errors);

    if (hasErrors(errors)) {
      return;
    }

    setIsLoading(true);
    createInspection(values, id)
      .then(() => {
        history.push("/insurance/appraiser");
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Dodano oględziny",
          })
        );
      })
      .catch((error) => {
        setIsLoading(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message: error.response.data.message,
          })
        );
      });
  };

  return (
    <React.Fragment>
      <Card elevation={2} className={classes.card}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setDialogOpen(true)}
        >
          Dokonaj oględzin
        </Button>
      </Card>
      <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
        {renderDialog()}
      </Dialog>
    </React.Fragment>
  );
};

export default AppraiserActionPanel;
