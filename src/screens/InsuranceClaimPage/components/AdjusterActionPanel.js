import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Avatar from "../../../components/Avatar/Avatar";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogHeader from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router";
import getInspectors from "../../../services/insuranceAdjuster/getInspectors";
import assignInspector from "../../../services/insuranceAdjuster/assignInspector";
import updateClaimStatus from "../../../services/insuranceAdjuster/updateClaimStatus";
import sendToInsuranceCompany from "../../../services/insuranceAdjuster/sendToInsuranceCompany";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";
import endInspectionPhase from "../../../services/insuranceAdjuster/endInspectionPhase";

const useStyles = makeStyles(() => ({
  card: {
    padding: "20px",
    "&:not(:last-of-type)": {
      marginBottom: "20px",
    },
  },
  input: {
    width: "100%",
  },
  buttonRoot: {
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
    marginRight: "10px",
    "&:not(:last-of-type)": {
      marginRight: "10px",
    },
  },
  button: {
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  dialogContent: {
    width: "500px",
  },
}));

const useMenuStyles = makeStyles(() => ({
  selectMenu: {
    display: "flex",
    alignItems: "center",
  },
}));

const AdjusterActionPanel = ({ loadClaim, setLoading, claim, id }) => {
  const classes = useStyles();
  const menuStyles = useMenuStyles();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [action, setAction] = useState("");
  const [inspectors, setInspectors] = useState([]);
  const [selectedInspector, setSelectedInspector] = useState("");
  const [justification, setJustification] = useState("");
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    getInspectors()
      .then((inspectors) => {
        setInspectors(inspectors);
      })
      .catch(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message:
              "Nie można pobrać listy rzeczoznawców, spróbuj ponownie później.",
          })
        );
        history.push("/insurance/adjuster");
      });
  }, []);

  useEffect(() => {
    if (action) {
      setDialogOpen(true);
    }
  }, [action]);

  useEffect(() => {
    if (!dialogOpen) {
      setAction("");
    }
  }, [dialogOpen]);

  const renderDialog = () => {
    switch (action) {
      case "assign":
        return (
          <React.Fragment>
            <DialogHeader>Przypisz rzeczoznawcę</DialogHeader>
            <DialogContent className={classes.dialogContent}>
              <Select
                variant="outlined"
                value={selectedInspector}
                onChange={(e) => setSelectedInspector(e.target.value)}
                displayEmpty
                className={classes.input}
                classes={menuStyles}
              >
                <MenuItem value="">Wybierz</MenuItem>
                {renderInspectors()}
              </Select>
            </DialogContent>
            <DialogActions>
              <Button
                color="secondary"
                onClick={() => setDialogOpen(false)}
                style={{ marginRight: "10px" }}
              >
                Anuluj
              </Button>
              <Button
                color="primary"
                disabled={!selectedInspector}
                onClick={() => handleAssignClaim()}
              >
                Przypisz
              </Button>
            </DialogActions>
          </React.Fragment>
        );
      case "accepted":
      case "rejected":
        return (
          <React.Fragment>
            <DialogHeader>
              {action === "accepted"
                ? "Potwierdzenie szkody"
                : "Odrzucenie szkody"}
            </DialogHeader>
            <DialogContent className={classes.dialogContent}>
              <DialogContentText>
                Uzupełnij powód{" "}
                {action === "rejected" ? "odrzucenia" : "przyjęcia"} szkody w
                polu poniżej.
              </DialogContentText>
              <TextField
                className={classes.input}
                multiline
                variant="outlined"
                onChange={(e) => setJustification(e.target.value)}
                placeholder="Wprowadź uzasadnienie"
              />
            </DialogContent>
            <DialogActions>
              <Button
                color="secondary"
                onClick={() => setDialogOpen(false)}
                style={{ marginRight: "10px" }}
              >
                Anuluj
              </Button>
              <Button
                color="primary"
                disabled={!justification}
                onClick={() => handleUpdateStatus(action)}
              >
                {action === "accepted" ? "Potwierdź" : "Odrzuć"}
              </Button>
            </DialogActions>
          </React.Fragment>
        );
      default:
        return null;
    }
  };

  const handleAssignClaim = () => {
    setLoading(true);
    setDialogOpen(false);
    assignInspector(claim.id, selectedInspector)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Pomyślnie zlecono oględziny",
          })
        );
      })
      .finally(() => {
        loadClaim();
        setSelectedInspector("");
      });
  };

  const handleUpdateStatus = (status) => {
    setLoading(true);
    setDialogOpen(false);
    updateClaimStatus(id, status, justification)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Pomyślnie zmieniono status szkody",
          })
        );
      })
      .finally(() => {
        loadClaim();
      });
  };

  const handleEndInspections = () => {
    setLoading(true);
    setDialogOpen(false);
    endInspectionPhase(id)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Pomyślnie zmieniono status szkody",
          })
        );
      })
      .finally(() => {
        loadClaim();
      });
  };

  const handleSendToInsuranceCompany = () => {
    setLoading(true);
    sendToInsuranceCompany(id)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Wysłano szkodę do Towarzystwa Ubezpieczeniowego",
          })
        );
      })
      .finally(() => {
        loadClaim();
      });
  };

  const renderInspectors = () => {
    return inspectors.map((user) => (
      <MenuItem value={user.id}>
        <ListItemAvatar>
          <Avatar
            imageId={user.userImageId}
            firstName={user.firstName}
            lastName={user.lastName}
          />
        </ListItemAvatar>
        <ListItemText>
          {user.firstName} {user.lastName}
        </ListItemText>
      </MenuItem>
    ));
  };

  return (
    <React.Fragment>
      {(claim.status === "SUBMITTED" ||
        claim.status === "IN_PROGRESS_AFTER_INSPECTION") && (
        <Card elevation={2} className={classes.card}>
          <Box display="flex">
            <Box className={classes.buttonRoot}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                onClick={() => setAction("rejected")}
              >
                Odrzuć szkodę
              </Button>
              <Button
                variant="contained"
                color="primary"
                disabled={!claim.insuranceCaseId}
                onClick={() => setAction("accepted")}
              >
                Zaakceptuj szkodę
              </Button>
            </Box>
            <Box className={classes.buttonRoot}>
              {claim.status === "IN_PROGRESS_AFTER_INSPECTION" && (
                <Button
                  variant="contained"
                  className={classes.button}
                  disabled={!!claim.insuranceCaseId}
                  onClick={() => handleSendToInsuranceCompany()}
                >
                  Przekaż szkodę do TU
                </Button>
              )}
              {claim.status === "SUBMITTED" && (
                <Button
                  variant="contained"
                  className={classes.button}
                  onClick={() => setAction("assign")}
                >
                  Zleć oględziny
                </Button>
              )}
              {claim.claimInspections.length > 0 &&
                claim.status === "SUBMITTED" && (
                  <Button
                    variant="contained"
                    onClick={() => handleEndInspections()}
                  >
                    Zakończ etap oględzin
                  </Button>
                )}
            </Box>
          </Box>
        </Card>
      )}
      <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
        {renderDialog()}
      </Dialog>
    </React.Fragment>
  );
};

export default AdjusterActionPanel;
