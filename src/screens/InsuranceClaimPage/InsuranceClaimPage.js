import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Avatar from "../../components/Avatar/Avatar";
import UserRating from "../../components/UserRating/UserRating";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import ArrowDown from "@material-ui/icons/ArrowDownward";
import { makeStyles } from "@material-ui/core/styles";
import { MapContainer, TileLayer, Marker } from "react-leaflet";
import { useParams, useHistory, Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";
import getClaim from "../../services/claims/getClaim";
import { fields, secondaryFields } from "./fields";
import AdjusterActionPanel from "./components/AdjusterActionPanel";
import AppraiserActionPanel from "./components/AppraiserActionPanel";
import clsx from "clsx";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: "20px",
  },
  root: {
    width: "100%",
    maxWidth: "1250px",
  },
  column: {
    flexGrow: "1",
    marginRight: "20px",
  },
  card: {
    padding: "20px",
    "&:not(:last-child)": {
      marginBottom: "20px",
    },
  },
  field: {
    flexBasis: "50%",
    flexGrow: "1",
  },
  name: {
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
    marginBottom: "5px",
  },
  value: {
    "&:not(:last-of-type)": {
      marginBottom: "15px",
    },
  },
  avatarRoot: {
    display: "flex",
    alignItems: "center",
    marginBottom: "15px",
  },
  avatar: {
    width: "64px",
    height: "64px",
    marginRight: "10px",
  },
  userName: {
    fontSize: "20px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
  columnRight: {
    display: "flex",
    flexDirection: "column",
    flexShrink: "1",
    flexGrow: "1",
    maxWidth: "500px",
    minWidth: "350px",
  },
  mapRoot: {
    borderRadius: "10px",
    boxShadow: theme.shadows[2],
    flexGrow: "1",
    maxHeight: "600px",
    overflow: "hidden",
    "&:not(:last-of-type)": {
      marginBottom: "20px",
    },
  },
  map: {
    width: "100%",
    height: "100%",
  },
  statusInfo: {
    textAlign: "center",
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
  },
  icon: {
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.standard,
    }),
  },
  open: {
    transform: "rotate(180deg)",
  },
  button: {
    transition: theme.transitions.create("margin", {
      duration: theme.transitions.duration.standard,
    }),
  },
  buttonOpen: {
    marginBottom: "10px",
  },
}));

const InsuranceClaimPage = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [claim, setClaim] = useState({});
  const [open, setOpen] = useState(false);
  const center = claim.location
    ? [claim.location.latitude, claim.location.longitude]
    : [0, 0];
  const { id } = useParams();
  const statuses = useSelector((state) => state.enumReducer.claimsStatuses);
  const userRole = useSelector((state) => state.userReducer.role);
  const dispatch = useDispatch();
  const history = useHistory();

  const loadClaim = () => {
    setLoading(true);
    getClaim(id)
      .then((claim) => {
        setClaim(claim);
        setLoading(false);
      })
      .catch(() => {
        dispatch(snackbarAction.openSnackbar({ error: true }));
        history.push("/insurance/adjuster");
      });
  };

  useEffect(() => {
    if (!id) {
      history.push("/insurance/adjuster");
    }

    loadClaim();
  }, [id]);

  const renderField = (field) => {
    return (
      field.getValue(claim, statuses) && (
        <Box key={field.name} className={classes.field}>
          <Typography className={classes.name}>{field.name}</Typography>
          <Typography className={classes.value}>
            {field.getValue(claim, statuses)}
          </Typography>
        </Box>
      )
    );
  };

  if (loading) {
    return (
      <Box>
        <CircularProgress size={60} />
      </Box>
    );
  }

  return (
    <Box className={classes.root}>
      <Typography component="h1" variant="h4" className={classes.header}>
        Zgłoszenie szkody dotyczącej ogłoszenia {claim.title}
      </Typography>
      <Box display="flex">
        <Box className={classes.column}>
          <Card elevation={2} className={classes.card}>
            {fields.map((field) =>
              field.row ? (
                <Box display="flex">
                  {field.row.map((_field) => renderField(_field))}
                </Box>
              ) : (
                renderField(field)
              )
            )}
          </Card>
          <Card elevation={2} className={classes.card}>
            {secondaryFields.map((field) => (
              <React.Fragment>
                <Typography className={classes.name}>{field.name}</Typography>
                <Typography className={classes.value}>
                  {field.getValue(claim, statuses)}
                </Typography>
              </React.Fragment>
            ))}
          </Card>
          {claim.claimInspections.length > 0 && (
            <Box marginBottom="20px">
              <Button
                className={clsx(classes.button, { [classes.buttonOpen]: open })}
                color="primary"
                onClick={() => setOpen(!open)}
                startIcon={
                  <ArrowDown
                    className={clsx(classes.icon, { [classes.open]: open })}
                  />
                }
              >
                {open ? "Schowaj" : "Pokaż"} inspekcje
              </Button>
              <Collapse in={open}>
                {claim.claimInspections.map((inspection) => (
                  <Card
                    elevation={2}
                    className={classes.card}
                    key={inspection.id}
                  >
                    <Typography className={classes.name}>
                      Inspekcja z dnia{" "}
                      {moment(inspection.inspectionDate).format("DD.MM.YYYY")}
                    </Typography>
                    <Box className={classes.field}>
                      <Typography className={classes.name}>
                        Oszacowanie szkody
                      </Typography>
                      <Typography className={classes.value}>
                        {inspection.damageAssessment} zł
                      </Typography>
                    </Box>
                    <Box className={classes.field}>
                      <Typography className={classes.name}>
                        Opis szkody
                      </Typography>
                      <Typography className={classes.value}>
                        {inspection.description}
                      </Typography>
                    </Box>
                  </Card>
                ))}
              </Collapse>
            </Box>
          )}
          <Card elevation={2} className={classes.card}>
            <Typography className={classes.name}>Użyczający</Typography>
            <Box className={classes.avatarRoot}>
              <Avatar
                className={classes.avatar}
                imageId={claim.user.userImageId}
                firstName={claim.user.firstName}
                lastName={claim.user.lastName}
              />
              <Typography className={classes.userName}>
                {claim.user.firstName} {claim.user.lastName}
              </Typography>
            </Box>
            <Box marginBottom="15px">
              <UserRating
                rateSharer={claim.user.sharerRate}
                rateBorrower={claim.user.borrowerRate}
              />
            </Box>
            <Button
              variant="contained"
              color="primary"
              component={Link}
              to={`/user/${claim.user.id}`}
            >
              Przejdź do profilu
            </Button>
          </Card>
          <Card elevation={2} className={classes.card}>
            <Typography className={classes.name}>Wypożyczający</Typography>
            <Box className={classes.avatarRoot}>
              <Avatar
                className={classes.avatar}
                imageId={claim.borrower.userImageId}
                firstName={claim.borrower.firstName}
                lastName={claim.borrower.lastName}
              />
              <Typography className={classes.userName}>
                {claim.borrower.firstName} {claim.borrower.lastName}
              </Typography>
            </Box>
            <Box marginBottom="15px">
              <UserRating
                rateSharer={claim.borrower.sharerRate}
                rateBorrower={claim.borrower.borrowerRate}
              />
            </Box>
            <Button
              variant="contained"
              color="primary"
              component={Link}
              to={`/user/${claim.borrower.id}`}
            >
              Przejdź do profilu
            </Button>
          </Card>
        </Box>
        <Box className={classes.columnRight}>
          <Box className={classes.mapRoot}>
            <MapContainer center={center} zoom={14} className={classes.map}>
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker position={center} />
            </MapContainer>
          </Box>
          {userRole === "INSURANCE_ADJUSTER" ? (
            <AdjusterActionPanel
              claim={claim}
              loadClaim={loadClaim}
              id={id}
              setLoading={setLoading}
            />
          ) : (
            <AppraiserActionPanel id={id} />
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default InsuranceClaimPage;
