import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Table from "../../components/Table/Table";
import Claim from "./components/Claim/Claim";
import { makeStyles } from "@material-ui/core/styles";
import getClaims from "../../services/claims/getClaims";
import getClaimsForInspection from "../../services/insuranceAppraiser/getClaimsForInspection";
import { snackbarAction } from "../../redux/modules/snackbar";
import { fields as adjusterFields } from "./adjusterFields";
import { fields as appraiserFields } from "./appraiserFields";

const useStyles = makeStyles({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  header: {
    width: "100%",
    marginBottom: "30px",
  },
  indicatorRoot: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
});

const InsurancePanel = ({ isAppraiser }) => {
  const classes = useStyles();
  const [claims, setClaims] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    setIsLoading(true);
    const getClaimsFunc = isAppraiser ? getClaimsForInspection : getClaims;
    getClaimsFunc()
      .then((claims) => {
        setClaims(claims);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message: "Wystąpił błąd podczas pobierania zgłoszeń",
          })
        );
      });
  }, []);

  return (
    <Box className={classes.root}>
      <Typography className={classes.header} component="h1" variant="h2">
        Panel {isAppraiser ? "rzeczoznawcy" : "likwidatora"}
      </Typography>
      {isLoading ? (
        <Box className={classes.indicatorRoot}>
          <CircularProgress size={48} />
        </Box>
      ) : (
        <Table
          fields={isAppraiser ? appraiserFields : adjusterFields}
          data={claims}
          defaultSort={
            isAppraiser ? "dateOfAssignToTheAppraiser" : "reportDate"
          }
          rowComponent={({ row }) => (
            <Claim row={row} isAppraiser={isAppraiser} />
          )}
        />
      )}
    </Box>
  );
};

export default InsurancePanel;
