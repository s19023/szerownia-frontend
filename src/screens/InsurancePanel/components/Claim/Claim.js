import React, { useState } from "react";
import { useSelector } from "react-redux";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import ArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { makeStyles } from "@material-ui/core/styles";
import { MapContainer, TileLayer, Marker } from "react-leaflet";
import moment from "moment";
import clsx from "clsx";
import { images } from "../../../../config/urls";
import { fields as adjusterFields } from "../../adjusterFields";
import { fields as appraiserFields } from "../../appraiserFields";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "700px",
    minWidth: "fit-content",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  span: {
    fontWeight: "bold",
  },
  rowRoot: {
    "& > *": {
      borderBottom: "unset",
    },
  },
  icon: {
    color: theme.palette.text.primary,
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.standard,
    }),
  },
  expanded: {
    transform: "rotate(180deg)",
  },
  bottomRoot: {
    display: "flex",
    padding: "15px",
  },
  leftRoot: {
    marginRight: "20px",
    width: "50%",
  },
  header: {
    color: theme.palette.text.primary,
    fontSize: "16px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
    marginBottom: "5px",
  },
  content: {
    color: theme.palette.text.primary,
    marginBottom: "10px",
  },
  avatarRoot: {
    display: "flex",
    alignItems: "center",
    marginBottom: "10px",
  },
  avatar: {
    width: "32px",
    height: "32px",
    boxShadow: theme.shadows[5],
    marginRight: "10px",
  },
  description: {
    color: theme.palette.text.primary,
    marginBottom: "10px",
  },
  map: {
    flexGrow: "1",
    width: "0px",
    height: "300px",
  },
}));

const Claim = ({ row, isAppraiser }) => {
  const classes = useStyles();
  const statuses = useSelector((state) => state.enumReducer.claimsStatuses);
  const [expanded, setExpanded] = useState(false);
  const claim = row;
  const [center] = useState([
    claim.location.latitude,
    claim.location.longitude,
  ]);
  const fields = isAppraiser ? appraiserFields : adjusterFields;

  return (
    <React.Fragment>
      <TableRow className={classes.rowRoot}>
        <TableCell align="center">
          <IconButton onClick={() => setExpanded(!expanded)} size="small">
            <ExpandMoreIcon
              className={clsx(classes.icon, { [classes.expanded]: expanded })}
            />
          </IconButton>
        </TableCell>
        <TableCell>{statuses[claim.status]}</TableCell>
        <TableCell>{claim.title}</TableCell>
        <TableCell>
          {claim.user.lastName} {claim.user.firstName}
        </TableCell>
        <TableCell>
          {moment(claim.reportDate).format("DD.MM.YYYY HH:mm")}
        </TableCell>
        <TableCell>{moment(claim.claimDate).format("DD.MM.YYYY")}</TableCell>
        <TableCell>
          {claim.dateOfAssignToTheAppraiser
            ? moment(claim.dateOfAssignToTheAppraiser).format("DD.MM.YYYY")
            : "Nie zlecono oględzin"}
        </TableCell>
        <TableCell>
          {isAppraiser
            ? `${claim.adjuster.lastName} ${claim.adjuster.firstName}`
            : claim.appraiser
            ? `${claim.appraiser.lastName} ${claim.appraiser.firstName}`
            : "Nie zlecono oględzin"}
        </TableCell>
        <TableCell align="right">{claim.policy.insuranceTotal} zł</TableCell>
        <TableCell>
          <IconButton
            size="small"
            color="primary"
            component={Link}
            to={`/insurance/claim/${claim.id}`}
          >
            <ArrowRightIcon />
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={fields.length + 2} style={{ padding: 0 }}>
          <Collapse in={expanded}>
            <Box className={classes.bottomRoot}>
              <Box className={classes.leftRoot}>
                <Typography className={classes.header}>Opis szkody</Typography>
                <Typography className={classes.content}>
                  {claim.damageDescription}
                </Typography>
                <Typography className={classes.header}>
                  Opis okoliczności
                </Typography>
                <Typography className={classes.content}>
                  {claim.circumstances}
                </Typography>
                <Typography className={classes.header}>Użytkownik</Typography>
                <Box className={classes.avatarRoot}>
                  <Avatar
                    className={classes.avatar}
                    src={`${images.get}/${claim.user.userImageId}`}
                    alt={`${claim.user.firstName} ${claim.user.lastName}`}
                  />
                  <Typography>
                    {claim.user.firstName} {claim.user.lastName}
                  </Typography>
                </Box>
                <Button
                  variant="contained"
                  color="primary"
                  endIcon={<ArrowForwardIcon />}
                  component="a"
                  href={`/user/${claim.user.id}`}
                  target="_blank"
                >
                  Przejdź do profilu
                </Button>
              </Box>
              <MapContainer
                center={center}
                zoom={14}
                className={classes.map}
                zoomControl={false}
                scrollWheelZoom={false}
                doubleClickZoom={false}
                boxZoom={false}
                dragging={false}
              >
                <TileLayer
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={center} />
              </MapContainer>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export default Claim;
