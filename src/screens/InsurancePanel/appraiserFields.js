export const fields = [
  {
    id: "status",
    name: "Status zgłoszenia",
    comparator: (a, b) => a.status.localeCompare(b.status),
  },
  {
    id: "title",
    name: "Nazwa ogłoszenia",
    comparator: (a, b) => a.title.localeCompare(b.title),
  },
  {
    id: "name",
    name: "Użyczający",
    comparator: (a, b) =>
      `${a.user.lastName} ${a.user.firstName}`.localeCompare(
        `${b.user.lastName} ${b.user.firstName}`
      ),
  },
  {
    id: "reportDate",
    name: "Data zgłoszenia",
    comparator: (a, b) => new Date(a.reportDate) - new Date(b.reportDate),
  },
  {
    id: "claimDate",
    name: "Data szkody",
    comparator: (a, b) => new Date(a.claimDate) - new Date(b.claimDate),
  },
  {
    id: "dateOfAssignToTheAppraiser",
    name: "Data przekazania rzeczoznawcy",
    comparator: (a, b) =>
      new Date(a.dateOfAssignToTheAppraiser) - new Date(b.dateOfAssignToTheAppraiser),
  },
  {
    id: "adjuster",
    name: "Likwidator",
    comparator: (a, b) => {
      if (!a.adjuster) {
        return -1;
      }
      if (!b.adjuster) {
        return 1;
      }

      return `${a.adjuster.lastName} ${a.adjuster.firstName}`.localeCompare(
        `${b.adjuster.lastName} ${b.adjuster.firstName}`
      );
    },
  },
  {
    id: "insuranceTotal",
    name: "Suma ubezpieczenia",
    comparator: (a, b) =>
      Number.parseFloat(a.policy.insuranceTotal) -
      Number.parseFloat(b.policy.insuranceTotal),
  },
];