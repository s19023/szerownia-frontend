import React, { useState } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import MuiLink from "@material-ui/core/Link";
import { Link } from "react-router-dom";

const AcceptTerms = ({ updateValue, error, helperText }) => {
  return (
    <FormControl error={error}>
      <FormControlLabel
        label={
          <React.Fragment>
            Przeczytałem i akceptuję 
            <MuiLink to="/terms" target="_blank" component={Link}>regulamin</MuiLink>
          </React.Fragment>
        }
        control={<Checkbox color="primary" onChange={(e) => updateValue(e.target.checked)} />}
      />
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  );
};

export default AcceptTerms;
