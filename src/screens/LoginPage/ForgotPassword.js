import React, { useState, useEffect } from "react";
import Fade from "@material-ui/core/Fade";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import FormCard from "../../components/FormCard/FormCard";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";
import getPasswordResetToken from "../../services/user/getPasswordResetToken";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    flexGrow: "1",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  inputRow: {
    padding: "10px 0",
    display: "flex",
    alignItems: "center",
    marginBottom: "20px",
  },
  label: {
    fontWeight: "bold",
    width: "200px",
    marginRight: "10px",
  },
  input: {
    width: "300px",
  },
  bottomRow: {
    marginTop: "20px",
    display: "flex",
    alignItems: "center",
  },
  bottomText: {
    marginRight: "5px",
  },
  link: {
    color: theme.palette.primary.main,
    fontWeight: "bold",
    textDecoration: "none !important",
    transition: "300ms font-size",
    "&:hover": {
      fontSize: "18px",
    },
  },
}));

const ForgotPassword = () => {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [disableSave, setDisableSave] = useState(true);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);

  const dispatch = useDispatch();
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    getPasswordResetToken(email)
      .then((response) => {
        setSuccess(true);
      })
      .catch((e) => {
        if (e.response.status === 404) {
          setError(true);
        } else {
          dispatch(snackbarAction.openSnackbar({ error: true }));
        }
      });
  };

  const handleChange = (e) => {
    setError(false);
    setEmail(e.target.value);
  };

  useEffect(() => {
    const disableSave = email === "" || !email || error;
    setDisableSave(disableSave);
  }, [email, error]);

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/");
    }
  }, [isLoggedIn]);

  if (success) {
    return (
      <Fade in appear>
        <Box className={classes.root}>
          <ScrollToTop />
          <FormCard title="Sprawdź pocztę">
            <Box className={classes.form}>
              <Typography className={classes.inputRow}>
                Link do odzyskiwania hasła został wysłany na Twój adres e-mail.
                Postępuj zgodnie ze wskazówkami by odzyskać hasło.
              </Typography>
              <Button
                id="success-btn"
                className={classes.input}
                variant="contained"
                color="primary"
                onClick={() => history.push("/")}
              >
                Wróć do strony głównej
              </Button>
            </Box>
          </FormCard>
        </Box>
      </Fade>
    );
  }

  return (
    <Fade in>
      <Box className={classes.root}>
        <ScrollToTop />
        <FormCard title="Nie pamiętasz hasła?">
          <form onSubmit={handleSubmit} className={classes.form}>
            <Typography>
              Wprowadź adres e-mail a następnie postępuj zgodnie z wskazówkami
              otrzymanymi na swoją skrzynkę.
            </Typography>
            <Box className={classes.inputRow}>
              <TextField
                id="email"
                onChange={handleChange}
                variant="outlined"
                label="Adres e-mail"
                type="email"
                className={classes.input}
                error={error}
                helperText={
                  error && "Nie znaleziono użytkownika z takim adresem e-mail"
                }
              />
            </Box>
            <Button
              id="password-retrive-btn"
              disabled={disableSave}
              className={classes.input}
              type="submit"
              variant="contained"
              color="primary"
            >
              Przypomnij hasło
            </Button>
          </form>
        </FormCard>
      </Box>
    </Fade>
  );
};

export default ForgotPassword;
