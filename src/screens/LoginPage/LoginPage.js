import React, { useState, useEffect } from "react";
import Fade from "@material-ui/core/Fade";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import FormCard from "../../components/FormCard/FormCard";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory, Link as RouterLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { userAction } from "../../redux/modules/user";
import { fields } from "./fields";
import { hasErrors, validateForm } from "../../utils/validateForm";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    flexGrow: "1",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  inputRow: {
    padding: "10px 0",
    display: "flex",
    alignItems: "center",
  },
  label: {
    fontWeight: "bold",
    width: "200px",
    marginRight: "10px",
  },
  input: {
    width: "300px",
  },
  forgotLinkRoot: {
    display: "flex",
    alignItems: "center",
    height: "24px",
    marginBottom: "10px",
  },
  bottomRow: {
    marginTop: "20px",
    display: "flex",
    alignItems: "center",
  },
  bottomText: {
    marginRight: "5px",
  },
  link: {
    color: theme.palette.primary.main,
    fontWeight: "bold",
    textDecoration: "none !important",
    transition: "300ms font-size",
    "&:hover": {
      fontSize: "18px",
    },
  },
}));

const LoginPage = () => {
  const classes = useStyles();
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const loginFields = fields.filter((field) => field.showInLogin);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/");
    }
  }, [isLoggedIn]);

  const handleChange = (key, value) => {
    const tmpValues = { ...values };
    tmpValues[key] = value;
    setValues(tmpValues);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const errors = validateForm(loginFields, values);
    const isValid = !hasErrors(errors);
    if (isValid) {
      dispatch(
        userAction.login({ email: values.email, password: values.password })
      );
    } else {
      setErrors(errors);
    }
  };

  return (
    <Fade in>
      <Box className={classes.root}>
        <ScrollToTop />
        <FormCard title="Witamy ponownie!">
          <form onSubmit={handleSubmit} className={classes.form}>
            {loginFields.map((field) => (
              <Box key={field.id} className={classes.inputRow}>
                <TextField
                  id={field.id}
                  onChange={(e) => handleChange(field.id, e.target.value)}
                  variant="outlined"
                  label={field.name}
                  type={field.type}
                  className={classes.input}
                  error={!!errors[field.id]}
                  helperText={errors[field.id]}
                />
              </Box>
            ))}
            <Box className={classes.forgotLinkRoot}>
              <Link
                underline="none"
                className={classes.link}
                component={RouterLink}
                to="/forgot-password"
              >
                Nie pamiętasz hasła?
              </Link>
            </Box>
            <Button
              id="login-btn"
              className={classes.input}
              type="submit"
              variant="contained"
              color="primary"
            >
              Zaloguj się
            </Button>
            <Box className={classes.bottomRow}>
              <Typography className={classes.bottomText}>
                Jesteś u nas po raz pierwszy?{" "}
              </Typography>
              <Link
                underline="none"
                className={classes.link}
                component={RouterLink}
                to="/register"
              >
                Załóż konto!
              </Link>
            </Box>
          </form>
        </FormCard>
      </Box>
    </Fade>
  );
};

export default LoginPage;
