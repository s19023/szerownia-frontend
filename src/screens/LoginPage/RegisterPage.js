import React, { useState } from "react";
import { Link as RouterLink, useHistory } from "react-router-dom";
import Fade from "@material-ui/core/Fade";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import FormCard from "../../components/FormCard/FormCard";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";

import register from "../../services/register/register";
import { hasErrors, validateForm } from "../../utils/validateForm";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    flexGrow: "1",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  inputRow: {
    padding: "10px 0",
    display: "flex",
    alignItems: "center",
  },
  input: {
    width: "300px",
  },
  bottomRow: {
    marginTop: "20px",
    display: "flex",
    alignItems: "center",
  },
  bottomText: {
    marginRight: "5px",
  },
  link: {
    color: theme.palette.primary.main,
    fontWeight: "bold",
    textDecoration: "none !important",
    transition: "300ms font-size",
    "&:hover": {
      fontSize: "18px",
    },
  },
}));

const createValues = (fields) => {
  const values = {};
  fields.forEach((field) => {
    values[field.id] = "";
  });

  return values;
};

const RegisterPage = () => {
  const [data, setData] = useState(createValues(fields));
  const [errors, setErrors] = useState(createValues(fields));
  const [isDisabled, setIsDisabled] = useState(true);
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const handleUpdate = (key, value) => {
    const tmpData = { ...data };
    tmpData[key] = value;
    setData(tmpData);
  };

  const handleUpload = (e) => {
    e.preventDefault();
    const errors = validateForm(fields, data);
    const isValid = !hasErrors(errors);
    if (!isValid) {
      setErrors(errors);
      return;
    }
    setIsDisabled(true);

    register(data)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Konto zostało utworzone pomyślnie",
          })
        );
        history.push("/login");
      })
      .catch(() => {
        setIsDisabled(false);
      });
  };

  return (
    <Fade in>
      <Box className={classes.root}>
        <ScrollToTop />
        <FormCard title="Stwórz konto">
          <form className={classes.form}>
            {fields.map((field) => {
              const CustomComponent = field.component;
              return (
                <Box key={field.id} className={classes.inputRow}>
                  {CustomComponent ? (
                    <CustomComponent
                      error={!!errors[field.id]}
                      helperText={errors[field.id]}
                      updateValue={(value) => handleUpdate(field.id, value)}
                    />
                  ) : (
                    <TextField
                      id={field.id}
                      className={classes.input}
                      error={!!errors[field.id]}
                      helperText={errors[field.id]}
                      onChange={(e) => handleUpdate(field.id, e.target.value)}
                      value={data[field.id]}
                      type={field.type}
                      label={field.name}
                      variant="outlined"
                      InputProps={{
                        inputComponent: field.maskedInput
                          ? field.maskedInput
                          : "input",
                      }}
                    />
                  )}
                </Box>
              );
            })}
            <Button
              id="register-btn"
              variant="contained"
              color="primary"
              type="submit"
              className={classes.input}
              onClick={(e) => handleUpload(e)}
            >
              Zarejestruj się
            </Button>
            <Box className={classes.bottomRow}>
              <Typography className={classes.bottomText}>
                Masz już u nas konto?{" "}
              </Typography>
              <Link
                underline="none"
                className={classes.link}
                component={RouterLink}
                to="/login"
              >
                Zaloguj się!
              </Link>
            </Box>
          </form>
        </FormCard>
      </Box>
    </Fade>
  );
};

export default RegisterPage;
