import MaskedInput from "../../components/MaskedInput/MaskedInput";
import AcceptTerms from "./AcceptTerms";

export const fields = [
  {
    name: "Adres e-mail",
    id: "email",
    type: "email",
    regex: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    regexError: "Wprowadzono nieprawidłowy adres e-mail",
    isRequired: true,
    showInLogin: true,
  },
  {
    name: "Hasło",
    id: "password",
    type: "password",
    minLength: 8,
    isRequired: true,
    showInLogin: true,
  },
  {
    name: "Powtórz hasło",
    id: "repeatPassword",
    type: "password",
    minLength: 8,
    equalTo: "password",
    equalToError: "Wprowadzone hasła nie są takie same",
    isRequired: true,
  },
  {
    name: "Imię",
    id: "firstName",
    minLength: 2,
    isRequired: true,
  },
  {
    name: "Nazwisko",
    id: "lastName",
    minLength: 2,
    isRequired: true,
  },
  {
    name: "Telefon",
    type: "tel",
    id: "telephoneNumber",
    maskedInput: ({ inputRef, ...other }) => {
      return (
        <MaskedInput
          inputRef={inputRef}
          mask={[
            /\d/,
            /\d/,
            /\d/,
            " ",
            /\d/,
            /\d/,
            /\d/,
            " ",
            /\d/,
            /\d/,
            /\d/,
          ]}
          {...other}
        />
      );
    },
    regex: /^(\d{3} ){2}\d{3}$/,
    regexError: "Wprowadzono nieprawidłowy numer telefonu",
    isRequired: true,
  },
  {
    name: "Regulamin",
    id: "acceptTerms",
    component: AcceptTerms,
    isRequired: true,
    requiredError: "Musisz zaakceptować regulamin",
  },
];
