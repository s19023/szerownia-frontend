import React, { useState, useEffect } from "react";
import Fade from "@material-ui/core/Fade";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import FormCard from "../../components/FormCard/FormCard";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";
import resetPassword from "../../services/user/resetPassword";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    flexGrow: "1",
    justifyContent: "center",
    alignItems: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  inputRow: {
    padding: "10px 0",
    display: "flex",
    alignItems: "center",
  },
  label: {
    fontWeight: "bold",
    width: "200px",
    marginRight: "10px",
  },
  input: {
    width: "300px",
  },
  bottomRow: {
    marginTop: "20px",
    display: "flex",
    alignItems: "center",
  },
  bottomText: {
    marginRight: "5px",
  },
  link: {
    color: theme.palette.primary.main,
    fontWeight: "bold",
    textDecoration: "none !important",
    transition: "300ms font-size",
    "&:hover": {
      fontSize: "18px",
    },
  },
}));

const PasswordReset = () => {
  const classes = useStyles();
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [disableSave, setDisableSave] = useState(true);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);

  const dispatch = useDispatch();
  const history = useHistory();
  const { token } = useParams();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (password !== repeatPassword) {
      setError(true);
      return;
    }

    resetPassword(password, token)
      .then((response) => {
        setSuccess(true);
      })
      .catch((e) => {
        if (e.response.status === 404) {
          dispatch(
            snackbarAction.openSnackbar({
              error: true,
              message: "Podany token resetowania hasła nie istnieje.",
            })
          );
        } else {
          dispatch(snackbarAction.openSnackbar({ error: true }));
        }
      });
  };

  const handleChange = (e, type) => {
    setError(false);
    type === "password" ? setPassword(e.target.value) : setRepeatPassword(e.target.value);
  };

  useEffect(() => {
    const disableSave =
      password === "" ||
      !password ||
      repeatPassword === "" ||
      !repeatPassword ||
      error;
    setDisableSave(disableSave);
  }, [error, password, repeatPassword]);

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/");
    }
  }, [isLoggedIn]);

  if (success) {
    return (
      <Fade in appear>
        <Box className={classes.root}>
          <ScrollToTop />
          <FormCard title="Sukces 🎉">
            <Box className={classes.form}>
              <Typography className={classes.inputRow}>
                Hasło zostało zmienione pomyślnie. Możesz się teraz zalogować.
              </Typography>
              <Button
                id="success-btn"
                className={classes.input}
                variant="contained"
                color="primary"
                onClick={() => history.push("/login")}
              >
                Przejdź do strony logowania
              </Button>
            </Box>
          </FormCard>
        </Box>
      </Fade>
    );
  }

  return (
    <Fade in>
      <Box className={classes.root}>
        <ScrollToTop />
        <FormCard title="Wprowadź nowe hasło">
          <form onSubmit={handleSubmit} className={classes.form}>
            <Box className={classes.inputRow}>
              <TextField
                id="password"
                onChange={(e) => handleChange(e, "password")}
                variant="outlined"
                label="Hasło"
                type="password"
                className={classes.input}
              />
            </Box>
            <Box className={classes.inputRow}>
              <TextField
                id="retype-password"
                onChange={(e) => handleChange(e, "repeatPassword  ")}
                variant="outlined"
                label="Powtórz hasło"
                type="password"
                className={classes.input}
                error={error}
                helperText={error && "Podane hasła nie są takie same"}
              />
            </Box>
            <Button
              id="password-retrive-btn"
              disabled={disableSave}
              className={classes.input}
              type="submit"
              variant="contained"
              color="primary"
            >
              Zmień hasło
            </Button>
          </form>
        </FormCard>
      </Box>
    </Fade>
  );
};

export default PasswordReset;
