import React from "react";
import withLayout from "../components/withLayout/withLayout";
import { Route, Switch } from "react-router-dom";
import ProtectedRoute from "../components/ProtectedRoute/ProtectedRoute";
import AdminPanel from "./AdminPanel/AdminPanel";
import LoginPage from "./LoginPage/LoginPage";
import RegisterPage from "./LoginPage/RegisterPage";
import HomePage from "./HomePage/HomePage";
import HirePage from "./HirePage/HirePage";
import SearchPage from "./SearchPage/SearchPage";
import NewAdPage from "./NewAdPage/NewAdPage";
import UserProfilePage from "./UserProfilePage/UserProfilePage";
import UserPage from "./UserPage/UserPage";
import MessagesPage from "./MessagesPage/MessagesPage";
import TermsPage from "./TermsPage/TermsPage";
import InsurancePanel from "./InsurancePanel/InsurancePanel";
import InsuranceClaimPage from "./InsuranceClaimPage/InsuranceClaimPage";
import AdPage from "./AdPage/AdPage";
import ForgotPassword from "./LoginPage/ForgotPassword";
import PasswordReset from "./LoginPage/PasswordReset";

const App = () => (
  <Switch>
    <ProtectedRoute allowedRoles={["ADMIN", "MODERATOR"]} path="/admin">
      <AdminPanel />
    </ProtectedRoute>
    <ProtectedRoute noLogin path="/login">
      <LoginPage />
    </ProtectedRoute>
    <ProtectedRoute noLogin path="/register">
      <RegisterPage />
    </ProtectedRoute>
    <ProtectedRoute noLogin path="/forgot-password">
      <ForgotPassword />
    </ProtectedRoute>
    <ProtectedRoute noLogin path="/passwordreset/:token">
      <PasswordReset />
    </ProtectedRoute>
    <Route path="/search">
      <SearchPage />
    </Route>
    <ProtectedRoute path="/hire">
      <HirePage />
    </ProtectedRoute>
    <ProtectedRoute path="/create-ad">
      <NewAdPage />
    </ProtectedRoute>
    <ProtectedRoute path="/edit-ad">
      <NewAdPage edit />
    </ProtectedRoute>
    <ProtectedRoute path="/my-profile">
      <UserProfilePage />
    </ProtectedRoute>
    <Route path="/ad/:type/:id">
      <AdPage />
    </Route>
    <Route path="/user/:id">
      <UserPage />
    </Route>
    <ProtectedRoute path="/messages">
      <MessagesPage />
    </ProtectedRoute>
    <ProtectedRoute
      allowedRoles={["INSURANCE_ADJUSTER"]}
      path="/insurance/adjuster"
    >
      <InsurancePanel />
    </ProtectedRoute>
    <ProtectedRoute allowedRoles={["APPRAISER"]} path="/insurance/appraiser">
      <InsurancePanel isAppraiser />
    </ProtectedRoute>
    <ProtectedRoute
      allowedRoles={["INSURANCE_ADJUSTER", "APPRAISER"]}
      path="/insurance/claim/:id"
    >
      <InsuranceClaimPage />
    </ProtectedRoute>
    <Route path="/terms">
      <TermsPage />
    </Route>
    <Route path="/">
      <HomePage />
    </Route>
  </Switch>
);

export default withLayout(App);
