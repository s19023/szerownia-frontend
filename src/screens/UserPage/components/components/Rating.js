import React, { useState } from "react";
import { useSelector } from "react-redux";
import Box from "@material-ui/core/Box";
import MuiRating from "@material-ui/lab/Rating";
import MuiLink from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import FlagIcon from "@material-ui/icons/Flag";
import Avatar from "../../../../components/Avatar/Avatar";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import FlagDialog from "../../../../components/Report/FlagDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  flexRow: {
    display: "flex",
    alignItems: "center",
    marginBottom: "5px",
  },
  spaceBetween: {
    justifyContent: "space-between",
    marginBottom: "0px",
  },
  name: {
    fontWeight: "bold",
  },
  sharerInfo: {
    marginLeft: "5px",
    fontSize: "12px",
    color: theme.palette.text.secondary,
  },
  comment: {
    marginBottom: "5px",
  },
}));

const Rating = ({ opinion, showDivider }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(null);
  const id = useSelector((state) => state.userReducer.id);
  const role = useSelector((state) => state.userReducer.role);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);

  return (
    <Box className={classes.root}>
      <Box className={clsx(classes.flexRow, classes.spaceBetween)}>
        <Box className={classes.flexRow}>
          <Avatar
            firstName={opinion.authorFirstName}
            lastName={opinion.authorLastName}
            imageId={opinion.idAuthorAvatar}
            size={24}
            margin={10}
          />
          <MuiLink
            component={Link}
            to={`/user/${opinion.idAuthor}`}
            target="_blank"
            underline="hover"
            color="textPrimary"
          >
            <Typography className={classes.name}>
              {opinion.authorFirstName} {opinion.authorLastName}
            </Typography>
          </MuiLink>
        </Box>
        {isLoggedIn && role === "USER" && opinion.idAuthor !== id && (
          <IconButton
            size="small"
            color="secondary"
            onClick={(e) => setOpen(e.currentTarget)}
          >
            <FlagIcon />
          </IconButton>
        )}
        <FlagDialog
          opinionId={opinion.idOpinion}
          anchorEl={open}
          handleClose={() => setOpen(null)}
        />
      </Box>
      <Box className={classes.flexRow}>
        <MuiRating max={5} value={opinion.rating} readOnly />
        <Typography className={classes.sharerInfo}>
          jako {opinion.aboutSharer ? "użyczający" : "wypożyczający"}
        </Typography>
      </Box>
      <Typography
        className={clsx({
          [classes.comment]: showDivider,
        })}
      >
        {opinion.comment}
      </Typography>
      {showDivider && <Divider />}
    </Box>
  );
};

export default Rating;
