import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Pagination from "@material-ui/lab/Pagination";
import Loader from "../../../components/Loader/Loader";
import Ad from "../../../components/SearchResults/components/Ad";
import ClaimList from "./components/CialmList";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import getUserSearchAds from "../../../services/searchAds/getUserSearchAds";
import getUserRentalAds from "../../../services/rentalAds/getUserRentalAds";
import { useSelector } from "react-redux";
import ShareRow from "../../../components/ShareRow/ShareRow";
import getSharesForUser from "../../../services/hires/getSharesForUser";
import getBorrowsForUser from "../../../services/hires/getBorrowsForUser";
import getClaimsForUser from "../../../services/claims/getClaimsForUser";

const useStyles = makeStyles(() => ({
  tabs: {
    marginBottom: "20px",
  },
  paginationRoot: {
    display: "flex",
    justifyContent: "flex-end",
    marginBottom: "20px",
  },
}));

const additionalTabs = [
  {
    id: "shares",
    name: "Użyczenia",
    index: 2,
  },
  {
    id: "borrows",
    name: "Wypożyczenia",
    index: 3,
  },
  {
    id: "claims",
    name: "Szkody",
    index: 4,
  },
];

const downloadFuncs = {
  0: getUserRentalAds,
  1: getUserSearchAds,
  2: getSharesForUser,
  3: getBorrowsForUser,
  4: getClaimsForUser,
};

const notFoundText = {
  2: "Nie znaleziono użyczeń",
  3: "Nie znaleziono wypożyczeń",
  4: "Nie znaleziono zgłoszonych szkód",
};

const AdList = ({ id }) => {
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [tab, setTab] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [loaded, setLoaded] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [ads, setAds] = useState([]);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const role = useSelector((state) => state.userReducer.role);

  useEffect(() => {
    if (!isLoggedIn || role === "USER") {
      setTab(0);
    }
  }, [isLoggedIn, role]);

  useEffect(() => {
    loadData();
  }, [page]);

  useEffect(() => {
    setPage(1);
    loadData();
  }, [tab]);

  const handleTabChange = (v) => {
    setLoaded(false);
    setTab(v);
  };

  const loadData = () => {
    setIsLoading(true);
    const getAds = downloadFuncs[tab];
    getAds(id, page - 1)
      .then((response) => {
        setTotalPages(response.totalPages > 0 ? response.totalPages : 1);
        setAds(response.results);
        setLoaded(true);
        setIsLoading(false);
      })
      .catch(() => {
        setTotalPages(1);
        setAds([]);
        setIsLoading(false);
      });
  };

  return (
    <React.Fragment>
      <Tabs
        value={tab}
        onChange={(e, v) => handleTabChange(v)}
        className={classes.tabs}
        orientation={isMobile ? "vertical" : "horizontal"}
      >
        <Tab value={0} label="Ogłoszenia użyczenia" />
        <Tab value={1} label="Ogłoszenia poszukiwania" />
        {isLoggedIn &&
          role !== "USER" &&
          additionalTabs.map((tab) => (
            <Tab value={tab.index} label={tab.name} key={tab.id} />
          ))}
      </Tabs>
      <Box className={classes.paginationRoot}>
        <Pagination
          page={page}
          onChange={(e, page) => setPage(page)}
          count={totalPages}
          showFirstButton
          showLastButton
        />
      </Box>
      <Loader
        array={ads}
        isLoading={isLoading || !loaded}
        label={
          notFoundText[tab] ? notFoundText[tab] : "Nie znaleziono ogłoszeń"
        }
      />
      <Grid
        container
        spacing={tab <= 1 ? 2 : 0}
        justifyContent={isMobile ? "center" : undefined}
        direction={tab <= 1 ? "row" : "column"}
      >
        {loaded &&
          !isLoading &&
          tab <= 1 &&
          ads.map((ad) => <Ad key={ad.id} ad={ad} isSearchAd={tab === 1} />)}
        {loaded &&
          !isLoading &&
          tab > 1 &&
          tab < 4 &&
          ads.map((hire) => (
            <ShareRow key={hire.id} hire={hire} isBorrow={tab === 3} modView />
          ))}
        {loaded && !isLoading && tab === 4 && ads.length > 0 && (
          <ClaimList claims={ads} />
        )}
      </Grid>
    </React.Fragment>
  );
};

export default AdList;
