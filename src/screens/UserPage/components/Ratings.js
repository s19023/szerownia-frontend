import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Pagination from "@material-ui/lab/Pagination";
import Loader from "../../../components/Loader/Loader";
import Rating from "./components/Rating";
import { makeStyles } from "@material-ui/core/styles";
import getUserOpinions from "../../../services/opinions/getUserOpinions";

const useStyles = makeStyles((theme) => ({
  paginationRoot: {
    display: "flex",
    justifyContent: "center",
    marginBottom: "20px",
  },
  root: {
    width: "100%",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  flexRow: {
    display: "flex",
    alignItems: "center",
    marginBottom: "5px",
  },
  name: {
    fontWeight: "bold",
  },
  sharerInfo: {
    marginLeft: "5px",
    fontSize: "12px",
    color: theme.palette.text.secondary,
  },
  comment: {
    marginBottom: "5px",
  },
}));

const Ratings = ({ id }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [opinions, setOpinions] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    getUserOpinions(id, page - 1)
      .then((response) => {
        setTotalPages(response.totalPages > 0 ? response.totalPages : 1);
        setOpinions(response.results);
        setIsLoading(false);
      })
      .catch(() => {
        setTotalPages(1);
        setOpinions([]);
        setIsLoading(false);
      });
  }, [page]);

  return (
    <React.Fragment>
      <Box className={classes.paginationRoot}>
        <Pagination
          page={page}
          onChange={(e, v) => setPage(v)}
          count={totalPages}
          size="small"
          siblingCount={1}
          boundaryCount={1}
        />
      </Box>
      <Loader
        array={opinions}
        isLoading={isLoading}
        label="Nie znaleziono ocen"
      />
      {!isLoading &&
        opinions.map((opinion, index) => (
          <Rating
            key={opinion.idOpinion}
            showDivider={index !== opinions.length - 1}
            opinion={opinion}
          />
        ))}
    </React.Fragment>
  );
};

export default Ratings;
