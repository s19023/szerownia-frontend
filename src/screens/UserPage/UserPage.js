import React, { useEffect, useState } from "react";
import { useParams, Redirect } from "react-router";
import { useSelector } from "react-redux";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import UserRating from "../../components/UserRating/UserRating";
import Button from "@material-ui/core/Button";
import FlagIcon from "@material-ui/icons/Flag";
import AdList from "./components/AdList";
import Ratings from "./components/Ratings";
import Collapse from "@material-ui/core/Collapse";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import getOtherUserDetails from "../../services/user/getOtherUserDetails";
import { images } from "../../config/urls";
import clsx from "clsx";
import FlagDialog from "../../components/Report/FlagDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    maxWidth: "1200px",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },
  card: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px",
    width: "300px",
    marginRight: "20px",
    height: "fit-content",
    [theme.breakpoints.down("sm")]: {
      marginRight: "0px",
      marginBottom: "20px",
    },
  },
  avatar: {
    width: "128px",
    height: "128px",
    boxShadow: theme.shadows[5],
    marginBottom: "10px",
  },
  name: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
    color: theme.palette.text.primary,
  },
  header: {
    fontSize: "12px",
    color: theme.palette.text.secondary,
  },
  list: {
    padding: "20px",
    minWidth: "500px",
  },
  icon: {
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.standard,
    }),
  },
  open: {
    transform: "rotate(180deg)",
  },
  collapse: {
    width: "100%",
  },
  listRoot: {
    flexGrow: "1",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
}));

const UserPage = () => {
  const classes = useStyles();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const { id } = useParams();
  const userId = useSelector((state) => state.userReducer.id);
  const role = useSelector((state) => state.userReducer.role);
  const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);
  const [reportOpen, setReportOpen] = useState(null);

  useEffect(() => {
    if (id == userId) {
      return;
    }

    getOtherUserDetails(id).then((data) => {
      setData(data);
    });
  }, []);

  if (id == userId) {
    return <Redirect to="/my-profile/ads" />;
  }

  return (
    <Box className={classes.root}>
      <Card elevation={2} className={classes.card}>
        <Avatar
          src={`${images.get}/${data.userImageId}`}
          className={classes.avatar}
        >
          {data.firstName} {data.lastName}
        </Avatar>
        <Typography className={classes.name}>
          {data.firstName} {data.lastName}
        </Typography>
        {isLoggedIn && role === "USER" && (
          <Button
            startIcon={<FlagIcon />}
            color="secondary"
            onClick={(e) => setReportOpen(e.currentTarget)}
          >
            Zgłoś użytkownika
          </Button>
        )}
        <FlagDialog
          userId={id}
          anchorEl={reportOpen}
          handleClose={() => setReportOpen(null)}
        />
        <Box>
          <Typography className={classes.header}>Adres e-mail</Typography>
          <Typography>{data.email}</Typography>
          <Typography className={classes.header}>Telefon</Typography>
          <Typography>{data.telephoneNumber}</Typography>
          <UserRating
            rateSharer={data.sharerRate}
            rateBorrower={data.borrowerRate}
          />
        </Box>
        <Button
          color="primary"
          onClick={() => setOpen(!open)}
          endIcon={
            <ArrowDownwardIcon
              className={clsx(classes.icon, { [classes.open]: open })}
            />
          }
        >
          {open ? "Schowaj" : "Pokaż"} oceny
        </Button>
        <Collapse in={open} className={classes.collapse}>
          <Ratings id={id} />
        </Collapse>
      </Card>
      <Box className={classes.listRoot}>
        {!isMobile && (
          <Typography className={classes.name}>
            Ogłoszenia użytkownika
          </Typography>
        )}
        <AdList id={id} />
      </Box>
    </Box>
  );
};

export default UserPage;
