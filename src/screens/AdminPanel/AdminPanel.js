import React from "react";
import Box from "@material-ui/core/Box";
import TabNavigation from "../../components/TabNavigation/TabNavigation";
import { Route, Switch } from "react-router-dom";
import VulgarWords from "./VulgarWords/VulgarWords";
import BlockedIPs from "./BlockedIPs/BlockedIPs";
import ModTickets from "./ModTickets/ModTickets";
import Stats from "./Stats/Stats";
import Complaints from "./Complaints/Complaints";
import Categories from "./Categories/Categories";
import Features from "./Features/Features";

const pages = [
  {
    nameFunc: (role) =>
      role === "ADMIN" ? "Panel administratora" : "Panel moderatora",
    href: "/admin",
    allowedRoles: ["ADMIN", "MODERATOR"],
  },
  {
    name: "Filtr wulgaryzmów",
    href: "/admin/vulgar-filter",
    allowedRoles: ["ADMIN"],
  },
  {
    name: "Zablokowane IP",
    href: "/admin/blocked",
    allowedRoles: ["ADMIN"],
  },
  {
    name: "Zarządzanie kategoriami",
    href: "/admin/categories",
    allowedRoles: ["ADMIN"],
  },
  {
    name: "Zarządzanie cechami",
    href: "/admin/features",
    allowedRoles: ["ADMIN"],
  },
  {
    name: "Zgłoszenia",
    href: "/admin/tickets",
    allowedRoles: ["MODERATOR"],
  },
  {
    name: "Reklamacje",
    href: "/admin/complaints",
    allowedRoles: ["MODERATOR"],
  },
];

const AdminPanel = () => {
  return (
    <Box width="100%" flexGrow="1" display="flex" flexDirection="column">
      <TabNavigation pages={pages} hideHome />
      <Switch>
        <Route path="/admin/vulgar-filter">
          <VulgarWords />
        </Route>
        <Route path="/admin/blocked">
          <BlockedIPs />
        </Route>
        <Route path="/admin/categories">
          <Categories />
        </Route>
        <Route path="/admin/features">
          <Features />
        </Route>
        <Route path="/admin/tickets">
          <ModTickets />
        </Route>
        <Route path="/admin/complaints">
          <Complaints />
        </Route>
        <Route path="/admin">
          <Stats />
        </Route>
      </Switch>
    </Box>
  );
};

export default AdminPanel;
