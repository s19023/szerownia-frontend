import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import CircularProgress from "@material-ui/core/CircularProgress";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";
import { types } from "../types";
import clsx from "clsx";
import updateFeature from "../../../services/features/updateFeature";
import createFeature from "../../../services/features/createFeature";
import deleteFeature from "../../../services/features/deleteFeature";

const useStyles = makeStyles(() => ({
  root: {
    padding: "20px",
    justifyContent: "center",
    flexDirection: "column",
    width: "500px",
    "&:first-of-type": {
      marginBottom: "20px",
    },
    "&:not(:last-child)": {
      marginBottom: "10px",
    },
  },
  header: {
    fontSize: "18px",
    marginBottom: "20px",
    width: "100%",
  },
  row: {
    display: "flex",
    alignItems: "center",
  },
  inputRow: {
    display: "contents",
  },
  input: {
    width: "300px",
    marginBottom: "10px",
  },
  margin: {
    marginRight: "10px",
  },
  name: {
    fontWeight: "bold",
  },
  button: {
    marginRight: "5px",
  },
}));

const SingleFeature = ({ feature, handleRefresh }) => {
  const classes = useStyles();
  const [values, setValues] = useState({ ...(feature || {}) });
  const [editing, setEditing] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [menuOpen, setMenuOpen] = useState(null);
  const [deleting, setDeleting] = useState(false);

  const handleValueChange = (key, value) => {
    const tmpValues = { ...values };
    tmpValues[key] = value;
    setValues(tmpValues);
  };

  const handleSave = () => {
    setIsEditing(true);
    const postFunction = feature ? updateFeature : createFeature;
    postFunction(values)
      .then(() => {
        handleRefresh();
      })
      .catch(() => {
        setIsEditing(false);
      });
  };

  const handleDelete = () => {
    setDeleting(true);
    deleteFeature(feature.idFeature)
      .then(() => {
        handleRefresh();
      })
      .catch(() => {
        setDeleting(false);
      });
  };

  return (
    <Card elevation={3} className={clsx(classes.root, classes.row)}>
      {!feature && (
        <Typography className={classes.header}>Dodaj nową cechę</Typography>
      )}
      <Box className={editing || !feature ? classes.inputRow : classes.row}>
        {!editing && feature ? (
          <React.Fragment>
            <Typography className={clsx(classes.margin, classes.name)}>
              {feature.name}
            </Typography>
            <Typography>{types[feature.type]}</Typography>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <TextField
              variant="outlined"
              label="Nazwa"
              readOnly={!feature && !editing}
              value={values.name || ""}
              className={clsx(classes.margin, classes.input)}
              onChange={(e) => handleValueChange("name", e.target.value)}
            />
            <TextField
              variant="outlined"
              label="Typ cechy"
              readOnly={!feature && !editing}
              value={values.type || ""}
              select
              className={clsx(classes.margin, classes.input)}
              onChange={(e) => handleValueChange("type", e.target.value)}
            >
              {Object.keys(types).map((key) => (
                <MenuItem value={key} key={key}>
                  {types[key]}
                </MenuItem>
              ))}
            </TextField>
          </React.Fragment>
        )}
      </Box>
      <Box className={classes.row}>
        {feature ? (
          editing ? (
            <React.Fragment>
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleSave()}
                className={classes.button}
                startIcon={isEditing && <CircularProgress size={24} />}
                disabled={isEditing}
              >
                Zapisz
              </Button>
              <Button
                variant="contained"
                color="secondary"
                onClick={() => setEditing(false)}
              >
                Anuluj
              </Button>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <IconButton
                color="primary"
                onClick={() => setEditing(true)}
                className={classes.button}
              >
                <EditIcon />
              </IconButton>
              <IconButton
                color="secondary"
                onClick={(e) => setMenuOpen(e.currentTarget)}
                disabled={deleting}
              >
                <DeleteIcon />
              </IconButton>
              <Menu
                open={!!menuOpen}
                anchorEl={menuOpen}
                onClose={() => setMenuOpen(null)}
              >
                <MenuItem onClick={() => handleDelete()}>
                  <ListItemIcon>
                    <DeleteIcon />
                  </ListItemIcon>
                  <ListItemText
                    primary="Usuń kategorię"
                    secondary="Ta akcja jest nieodwracalna"
                  />
                </MenuItem>
              </Menu>
            </React.Fragment>
          )
        ) : (
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleSave()}
            startIcon={isEditing && <CircularProgress size={24} />}
            disabled={isEditing}
          >
            Dodaj cechę
          </Button>
        )}
      </Box>
    </Card>
  );
};

export default SingleFeature;
