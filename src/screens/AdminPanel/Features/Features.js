import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Loader from "../../../components/Loader/Loader";
import getAllFeatures from "../../../services/features/getAllFeatures";
import SingleFeature from "./SingleFeature";

const Features = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [features, setFeatures] = useState([]);

  const loadData = () => {
    setIsLoading(true);
    getAllFeatures()
      .then((features) => {
        setFeatures(features);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <Box display="flex" flexDirection="column" alignItems="center">
      <Loader
        array={features}
        isLoading={isLoading}
        label="Nie znaleziono cech, spróbuj ponownie później."
      />
      {!isLoading && <SingleFeature handleRefresh={loadData} />}
      {!isLoading &&
        features.map((feature) => (
          <SingleFeature
            feature={feature}
            key={feature.idFeature}
            handleRefresh={loadData}
          />
        ))}
    </Box>
  );
};

export default Features;
