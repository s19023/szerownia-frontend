import React, { useState, useEffect, useMemo } from "react";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Collapse from "@material-ui/core/Collapse";
import Card from "@material-ui/core/Card";
import Zoom from "@material-ui/core/Zoom";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TablePagination from "@material-ui/core/TablePagination";
import Pagination from "@material-ui/lab/Pagination";
import VulgarWord from "./VulgarWord";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";
import getVulgarWords from "../../../services/admin/vulgarFilter/getVulgarWords";
import postVulgarWord from "../../../services/admin/vulgarFilter/postVulgarWord";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: "10px",
    padding: "10px",
  },
  cardTitle: {
    fontWeight: "bold",
    fontSize: "12px",
    marginBottom: "5px",
    color: theme.palette.primary.main,
  },
  input: {
    width: "400px",
    marginRight: "10px",
  },
  button: {
    marginRight: "10px",
  },
}));

const usePaginationStyles = makeStyles(() => ({
  spacer: {
    flex: "unset",
  },
}));

const VulgarWords = () => {
  const classes = useStyles();
  const [vulgarWords, setVulgarWords] = useState([]);
  const [newWordOpen, setNewWordOpen] = useState(false);
  const [newWord, setNewWord] = useState("");
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const dispatch = useDispatch();

  useEffect(() => {
    downloadData();
  }, [page, rowsPerPage, search]);

  const downloadData = () => {
    getVulgarWords(page - 1, rowsPerPage, search).then((words) => {
      setCount(words.count);
      setVulgarWords(words.results);
    });
  };

  const handleChange = (search) => {
    setTimeout(() => {
      setSearch(search);
      setPage(1);
    }, 300);
  };

  const openDialog = () => {
    setNewWordOpen(true);
    setNewWord("");
  };

  const handleAddWord = () => {
    postVulgarWord(newWord)
      .then(() => {
        setNewWordOpen(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Dodano nowy wulgaryzm",
          })
        );
        if (page !== 1) {
          setPage(1);
        } else {
          downloadData();
        }
      })
      .catch(() => {
        dispatch(snackbarAction.openSnackbar({ error: true }));
      });
  };

  const handleDelete = (error) => {
    if (error) {
      dispatch(snackbarAction.openSnackbar({ error: true }));
    } else {
      dispatch(
        snackbarAction.openSnackbar({
          error: false,
          message: "Usunięto wulgaryzm",
        })
      );
      if (page !== 1) {
        setPage(1);
      } else {
        downloadData();
      }
    }
  };

  const PaginationAction = () => (
    <Pagination
      showFirstButton
      showLastButton
      count={Math.ceil(count / rowsPerPage)}
      page={page}
      onChange={(e, page) => setPage(page)}
    />
  );

  return (
    <Box width="100%" display="flex" flexDirection="column" alignItems="center">
      <Box
        display="flex"
        alignItems="center"
        marginBottom="10px"
        width="700px"
        justifyContent="space-between"
      >
        <TextField
          className={classes.input}
          placeholder="Wyszukaj słowo"
          onChange={(e) => handleChange(e.target.value)}
        />
        <Zoom in={!newWordOpen}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => openDialog()}
          >
            Dodaj nowy wulgaryzm
          </Button>
        </Zoom>
      </Box>
      <Collapse in={newWordOpen} unmountOnExit>
        <Card elevation={1} className={classes.card}>
          <Typography className={classes.cardTitle}>
            Dodaj nowy wulgaryzm
          </Typography>
          <Box display="flex" alignItems="center">
            <TextField
              className={classes.input}
              placeholder="Wpisz nowe słowo"
              onChange={(e) => setNewWord(e.target.value)}
            />
            <Box>
              <Button
                className={classes.button}
                variant="contained"
                onClick={() => setNewWordOpen(false)}
              >
                Anuluj
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleAddWord()}
              >
                Dodaj
              </Button>
            </Box>
          </Box>
        </Card>
      </Collapse>
      <TablePagination
        component="div"
        rowsPerPage={rowsPerPage}
        rowPerPageOptions={[25, 50, 100]}
        onChangeRowsPerPage={(e) => {
          setRowsPerPage(e.target.value);
          setPage(1);
        }}
        page={page - 1}
        count={count}
        ActionsComponent={PaginationAction}
        classes={usePaginationStyles()}
      />
      <Box>
        {vulgarWords.map((word) => (
          <VulgarWord
            key={word.id}
            word={word}
            classes={classes}
            handleDelete={handleDelete}
          />
        ))}
      </Box>
    </Box>
  );
};

export default VulgarWords;
