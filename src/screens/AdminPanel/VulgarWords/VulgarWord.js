import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";

import deleteVulgarWord from "../../../services/admin/vulgarFilter/deleteVulgerWord";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "10px",
    width: "700px",
    borderRadius: "5px",
    backgroundColor: theme.palette.background.paper,
    "&:nth-of-type(even)": {
      backgroundColor: theme.palette.background.default,
    },
  },
  vulgarWord: {
    width: "300px",
  },
}));

const VulgarWords = ({ word, handleDelete }) => {
  const classes = useStyles();
  const [disabled, setDisabled] = useState(false);

  const onDelete = () => {
    setDisabled(true);
    deleteVulgarWord(word.id)
      .then(() => handleDelete(false))
      .catch(() => {
        setDisabled(false);
        handleDelete(true);
      });
  };

  return (
    <Box className={classes.root}>
      <Typography color="textPrimary" className={classes.vulgarWord}>
        {word.vulgarWord}
      </Typography>
      <IconButton disabled={disabled} onClick={() => onDelete()} size="small">
        <CloseIcon />
      </IconButton>
    </Box>
  );
};

export default VulgarWords;
