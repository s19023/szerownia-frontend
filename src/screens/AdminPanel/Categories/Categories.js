import React, { useState, useEffect } from "react";
import Loader from "../../../components/Loader/Loader";
import TreeView from "@material-ui/lab/TreeView";
import TreeItem from "@material-ui/lab/TreeItem";
import Icon from "@material-ui/core/Icon";
import Typography from "@material-ui/core/Typography";
import SingleCategory from "./SingleCategory";
import AddNewCategory from "./AddNewCategory";
import { makeStyles } from "@material-ui/core/styles";
import getCategoriesList from "../../../services/categories/getCategoriesList";
import getAllFeatures from "../../../services/features/getAllFeatures";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  header: {
    fontFamily: "Poppins, sans-serif",
    color: theme.palette.text.primary,
    fontSize: "24px",
    fontWeight: "bold",
    marginBottom: "5px",
    width: "100%",
    maxWidth: "550px",
  },
  treeRoot: {
    marginBottom: "20px",
    width: "100%",
    maxWidth: "400px",
  },
  itemRoot: {
    display: "flex",
    alignItems: "center",
    padding: "5px",
  },
}));

const Categories = () => {
  const classes = useStyles();
  const [categories, setCategories] = useState([]);
  const [isLoadingCategories, setLoadingCategories] = useState(true);
  const [features, setFeatures] = useState([]);

  useEffect(() => {
    getAllFeatures().then((features) => {
      setFeatures(features);
    });
  }, []);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    getCategoriesList()
      .then((categories) => {
        setCategories(categories.filter((category) => category.abstract));
        setLoadingCategories(false);
      })
      .catch(() => {
        setLoadingCategories(false);
      });
  };

  return (
    <div className={classes.root}>
      <Typography className={classes.header} component="h2">
        Kategorie
      </Typography>
      <Loader
        isLoading={isLoadingCategories}
        array={categories}
        label="Nie znaleziono kategorii. Spróbuj ponownie później."
      />
      <TreeView className={classes.treeRoot}>
        {categories.map((category) => (
          <TreeItem
            icon={
              <Icon style={{ color: category.iconColor, marginRight: "5px" }}>
                {category.iconName}
              </Icon>
            }
            key={category.idCategory}
            nodeId={category.idCategory.toString()}
            label={<div className={classes.itemRoot}>{category.name}</div>}
          >
            {category.subCategories.map((subCategory) => (
              <TreeItem
                key={subCategory.idCategory}
                nodeId={subCategory.idCategory.toString()}
                label={
                  <div className={classes.itemRoot}>{subCategory.name}</div>
                }
              >
                <SingleCategory
                  id={subCategory.idCategory}
                  category={subCategory}
                  parentCategories={categories}
                  parentCategory={category}
                  allFeatures={features}
                  handleRefresh={getData}
                />
              </TreeItem>
            ))}
          </TreeItem>
        ))}
      </TreeView>
      <AddNewCategory
        parentCategories={categories}
        handleRefresh={getData}
        features={features}
      />
    </div>
  );
};

export default Categories;
