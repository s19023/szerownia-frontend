import React, { useState, useEffect, useMemo } from "react";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";
import Loader from "../../../components/Loader/Loader";
import Box from "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import CircularProgress from "@material-ui/core/CircularProgress";
import DeleteIcon from "@material-ui/icons/Close";
import getFeaturesForCategory from "../../../services/features/getFeaturesForCategory";
import deleteCategory from "../../../services/categories/deleteCategory";
import AddNewCategory from "./AddNewCategory";
import { types } from "../types";

const SingleCategory = ({
  id,
  parentCategories,
  category,
  allFeatures,
  parentCategory,
  handleRefresh,
}) => {
  const [features, setFeatures] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isDeleting, setDeleting] = useState(false);
  const [editing, setEditing] = useState(false);
  const [open, setOpen] = useState(null);
  const dispatch = useDispatch();
  const categoryWithFeatures = useMemo(() => {
    const tmpFeatures = [];
    features.forEach((feature) => {
      feature.featureId = feature.idFeature;
      feature.isRequired = feature.required;
      tmpFeatures.push(feature);
    });

    const tmpCategory = {
      name: category.name,
      idParentCategory: parentCategory.idCategory,
      features: tmpFeatures,
    };
    return tmpCategory;
  }, [features, category, parentCategory]);

  useEffect(() => {
    getFeatures(id);
  }, [id]);

  const getFeatures = () => {
    getFeaturesForCategory(id)
      .then((features) => {
        setFeatures(features.sort((a, b) => (a.required ? -1 : 1)));
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const handleUpdate = () => {
    setEditing(false);
    getFeatures();
    handleRefresh();
  };

  const handleDelete = () => {
    setDeleting(true);
    deleteCategory(id)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: `Usunięto kategorię ${category.name}`,
          })
        );
        handleRefresh();
      })
      .catch(() => {
        setDeleting(false);
      });
  };

  return (
    <Box style={{ marginBottom: "10px" }}>
      <Loader
        array={features}
        isLoading={isLoading}
        label="Nie znaleziono cech dla danej kategorii"
        marginTop={10}
      />
      {!isLoading && features.length > 0 && !editing && (
        <React.Fragment>
          <Table size="small" style={{ marginBottom: "10px" }}>
            <TableHead>
              <TableRow>
                <TableCell>Nazwa cechy</TableCell>
                <TableCell>Typ cechy</TableCell>
                <TableCell>Cecha wymagana?</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {features.map((feature) => (
                <TableRow key={feature.idFeature}>
                  <TableCell>{feature.name}</TableCell>
                  <TableCell>{types[feature.type]}</TableCell>
                  <TableCell>{feature.required ? "TAK" : "NIE"}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <Box>
            <Button
              color="primary"
              onClick={() => setEditing(true)}
              disabled={isDeleting}
            >
              Edytuj kategorię
            </Button>
            <Button
              color="secondary"
              onClick={(e) => setOpen(e.currentTarget)}
              startIcon={isDeleting && <CircularProgress size={24} />}
              disabled={isDeleting}
            >
              Usuń kategorię
            </Button>
          </Box>
          <Menu open={!!open} onClose={() => setOpen(null)} anchorEl={open}>
            <MenuItem onClick={() => handleDelete()}>
              <ListItemIcon>
                <DeleteIcon />
              </ListItemIcon>
              <ListItemText
                primary="Usuń kategorię"
                secondary="Tej akcji nie da się odwrócić."
              />
            </MenuItem>
          </Menu>
        </React.Fragment>
      )}
      {editing && (
        <AddNewCategory
          parentCategories={parentCategories}
          category={categoryWithFeatures}
          features={allFeatures}
          id={id}
          handleRefresh={handleUpdate}
        />
      )}
    </Box>
  );
};

export default SingleCategory;
