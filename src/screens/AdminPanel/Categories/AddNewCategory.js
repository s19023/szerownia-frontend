import React, { useEffect, useState } from "react";
import FormCard from "../../../components/FormCard/FormCard";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";
import createCategory from "../../../services/categories/createCategory";
import NewFeatureRow from "./components/NewFeatureRow";
import FeatureRow from "./components/FeatureRow";
import updateCategory from "../../../services/categories/updateCategory";

const fields = [
  {
    id: "name",
    isRequired: true,
  },
  {
    id: "idParentCategory",
    isRequired: true,
  },
  {
    id: "features",
    isRequired: true,
    type: "array",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  inputRow: {
    marginBottom: "10px",
  },
  input: {
    width: "300px",
  },
}));

const AddNewCategory = ({
  handleRefresh,
  parentCategories,
  category,
  features,
  id,
}) => {
  const classes = useStyles();
  const [values, setValues] = useState(category || { features: [] });
  const [key, setKey] = useState(new Date().getTime());
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (key, value) => {
    const tmpValues = { ...values };
    tmpValues[key] = value;
    setValues(tmpValues);
  };

  const handleFeaturesChange = (value, deleteFeature) => {
    let tmpFeatures = [...(values.features || [])];
    const index = tmpFeatures.findIndex(
      (feature) => feature.featureId === value.featureId
    );

    if (!deleteFeature) {
      if (index !== -1) {
        tmpFeatures[index] = {
          featureId: value.featureId,
          idFeature: value.featureId,
          featureRequired: value.required,
          isRequired: value.required,
        };
      } else {
        tmpFeatures.push({
          featureId: value.featureId,
          idFeature: value.featureId,
          featureRequired: value.required,
          isRequired: value.required,
        });
        setKey(new Date().getTime());
      }
    } else {
      tmpFeatures = tmpFeatures.filter(
        (feature) => feature.featureId !== value.featureId
      );
    }
    handleChange("features", tmpFeatures);
  };

  const handleCreate = () => {
    setIsLoading(true);
    const postFunc = category ? updateCategory : createCategory;
    postFunc(values, id)
      .then(() => {
        handleRefresh();
        setIsLoading(false);
        setValues({ features: [] });
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <FormCard
      title={
        category ? `Edytuj kategorię ${category.name}` : "Dodaj nową kategorię"
      }
      width={550}
    >
      <Box className={classes.root}>
        <Box className={classes.inputRow}>
          <TextField
            value={category ? values.name : undefined}
            variant="outlined"
            label="Nazwa kategorii"
            onChange={(e) => handleChange("name", e.target.value)}
            className={classes.input}
          />
        </Box>
        <Box className={classes.inputRow}>
          <TextField
            select
            variant="outlined"
            label="Nadrzędna kategoria"
            onChange={(e) => handleChange("idParentCategory", e.target.value)}
            value={values.idParentCategory || ""}
            className={classes.input}
          >
            {parentCategories.map((category) => (
              <MenuItem key={category.idCategory} value={category.idCategory}>
                {category.name}
              </MenuItem>
            ))}
          </TextField>
        </Box>
        {values.features.map((feature) => (
          <FeatureRow
            key={feature.featureId}
            feature={feature}
            features={features}
            handleFeaturesChange={handleFeaturesChange}
          />
        ))}
        <NewFeatureRow
          features={features.filter(
            (f) =>
              !values.features.find(
                (feature) => feature.featureId === f.idFeature
              )
          )}
          handleFeaturesChange={handleFeaturesChange}
          key={key}
        />
        <Button
          startIcon={isLoading && <CircularProgress size={24} />}
          disabled={isLoading}
          variant="contained"
          color="primary"
          onClick={() => handleCreate()}
        >
          {category ? "Edytuj" : "Dodaj"} kategorię
        </Button>
      </Box>
    </FormCard>
  );
};

export default AddNewCategory;
