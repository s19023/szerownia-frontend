import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  input: {
    width: "300px",
    marginRight: "16px",
  },
  row: {
    display: "flex",
    alignItems: "center",
    marginTop: "5px",
    marginBottom: "10px",
  },
  button: {
    marginBottom: "10px",
  },
}));

const NewFeatureRow = ({ features, handleFeaturesChange }) => {
  const classes = useStyles();
  const [selectedFeature, setSelectedFeature] = useState("");
  const [required, setRequired] = useState(false);

  const handleSubmit = () => {
    const feature = { featureId: selectedFeature, required };
    handleFeaturesChange(feature);
  };

  return (
    <React.Fragment>
      <Box className={classes.row}>
        <TextField
          select
          variant="outlined"
          label="Cecha"
          onChange={(e) => setSelectedFeature(e.target.value)}
          value={selectedFeature}
          className={classes.input}
        >
          {features.map((feature) => (
            <MenuItem key={feature.idFeature} value={feature.idFeature}>
              {feature.name}
            </MenuItem>
          ))}
        </TextField>
        <FormControlLabel
          label="Cecha wymagana"
          control={<Checkbox onChange={(e, checked) => setRequired(checked)} />}
        />
      </Box>
      <Button
        disabled={!selectedFeature}
        variant="contained"
        color="primary"
        onClick={() => handleSubmit()}
        className={classes.button}
      >
        Dodaj cechę
      </Button>
    </React.Fragment>
  );
};

export default NewFeatureRow;
