import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Close";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  inputRow: {
    marginBottom: "10px",
  },
  input: {
    width: "300px",
  },
  row: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    padding: "5px",
    justifyContent: "space-between",
    borderBottom: "1px solid " + theme.palette.text.disabled,
  },
  selectRow: {
    marginTop: "5px",
  },
  select: {
    marginRight: "10px",
  },
  name: {
    fontWeight: "bold",
  }
}));

const FeatureRow = ({ feature, features, handleFeaturesChange }) => {
  const classes = useStyles();

  const handleCheck = (required, id) => {
    const feature = { featureId: id, required: required };
    handleFeaturesChange(feature);
  };

  const handleDelete = (id) => {
    const feature = { featureId: id };
    handleFeaturesChange(feature, true);
  };

  return (
    <Box key={feature.featureId} className={classes.row}>
      <Typography>
        {
          features.find(
            (f) =>
              f.idFeature === feature.featureId ||
              f.idFeature === feature.idFeature
          )?.name
        }
      </Typography>
      <FormControlLabel
        label="Cecha wymagana"
        control={
          <Checkbox
            checked={feature.featureRequired || feature.required}
            onChange={(e) => handleCheck(e.target.checked, feature.featureId)}
          />
        }
      />
      <IconButton
        size="small"
        color="primary"
        onClick={() => handleDelete(feature.featureId)}
      >
        <DeleteIcon />
      </IconButton>
    </Box>
  );
};

export default FeatureRow;
