export const types = {
  TEXT: "Wartość tekstowa",
  DOUBLE: "Liczba dziesiętna",
  BOOLEAN: "Wartość prawda/fałsz",
  INTEGER: "Liczba całkowita",
};