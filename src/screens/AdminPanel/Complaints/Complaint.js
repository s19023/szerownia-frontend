import React, { useState } from "react";
import { useSelector } from "react-redux";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import { useComplaintsFields } from "../../../components/useComplaintsFields/useComplaintsFields";
import changeStatus from "../../../services/complaints/changeStatus";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "700px",
    minWidth: "fit-content",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  span: {
    fontWeight: "bold",
  },
  rowRoot: {
    height: "42.5px",
    "& > *": {
      borderBottom: "unset",
    },
  },
  icon: {
    color: theme.palette.text.primary,
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.standard,
    }),
  },
  expanded: {
    transform: "rotate(180deg)",
  },
  bottomRoot: {
    display: "flex",
    padding: "15px",
    justifyContent: "center",
  },
  header: {
    color: theme.palette.text.primary,
    fontSize: "16px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
    marginBottom: "5px",
  },
  button: {
    marginRight: "10px",
  },
}));

const Complaint = ({ row, handleRefresh }) => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const [isAccepting, setIsAccepting] = useState(false);
  const [isRejecting, setIsRejecting] = useState(false);
  const fields = useComplaintsFields();

  const handleAccept = () => {
    setIsAccepting(true);
    changeStatus(row.id, "ACCEPTED")
      .then(() => {
        handleRefresh();
        setExpanded(false);
      })
      .catch(() => {
        setIsAccepting(false);
      });
  };

  const handleReject = () => {
    setIsRejecting(true);
    changeStatus(row.id, "REJECTED")
      .then(() => {
        handleRefresh();
        setExpanded(false);
      })
      .catch(() => {
        setIsRejecting(false);
      });
  };

  return (
    <React.Fragment>
      <TableRow className={classes.rowRoot}>
        <TableCell align="center">
          {row.status === "SUBMITTED" && (
            <IconButton onClick={() => setExpanded(!expanded)} size="small">
              <ExpandMoreIcon
                className={clsx(classes.icon, { [classes.expanded]: expanded })}
              />
            </IconButton>
          )}
        </TableCell>
        {fields.map((field) => (
          <TableCell key={field.id}>{field.getValue(row)}</TableCell>
        ))}
      </TableRow>
      <TableRow>
        <TableCell colSpan={fields.length + 2} style={{ padding: 0 }}>
          <Collapse in={expanded}>
            <Box className={classes.bottomRoot}>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                disabled={isRejecting || isAccepting}
                startIcon={isAccepting && <CircularProgress size={16} />}
                onClick={handleAccept}
              >
                Zaakceptuj
              </Button>
              <Button
                variant="contained"
                color="secondary"
                disabled={isRejecting || isAccepting}
                startIcon={isRejecting && <CircularProgress size={16} />}
                onClick={handleReject}
              >
                Odrzuć
              </Button>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export default Complaint;
