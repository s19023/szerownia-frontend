import React, { useState, useEffect } from "react";
import Loader from "../../../components/Loader/Loader";
import Table from "../../../components/Table/Table";
import Pagination from "@material-ui/lab/Pagination";
import Complaint from "./Complaint";
import getComplaints from "../../../services/complaints/getComplaints";
import { useComplaintsFields } from "../../../components/useComplaintsFields/useComplaintsFields";

const Complaints = () => {
  const [complaints, setComplaints] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const fields = useComplaintsFields();

  useEffect(() => {
    downloadData();
  }, [page]);

  const downloadData = () => {
    getComplaints(page - 1)
      .then((response) => {
        setTotalPages(response.totalPages);
        setComplaints(response.results);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return isLoading || complaints.length === 0 ? (
    <Loader
      array={complaints}
      isLoading={isLoading}
      label="Nie znaleziono żadnych reklamacji"
    />
  ) : (
    <React.Fragment>
      <Table
        fields={fields}
        data={complaints}
        rowComponent={Complaint}
        defaultSort="reportDate"
        marginBottom="10px"
        disableEndPadding
        handleRefresh={downloadData}
      />
      <Pagination
        count={totalPages}
        page={page}
        onChange={(e, v) => setPage(v)}
        showFirstButton
        showLastButton
      />
    </React.Fragment>
  );
};

export default Complaints;
