import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Line } from "react-chartjs-2";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import getStats from "../../../services/admin/stats/getStats";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  card: {
    padding: "15px",
    backgroundColor: (props) =>
      props.color ? props.color : theme.palette.background.paper,
    borderRadius: "10px",
    transition: theme.transitions.create(
      "background-color",
      theme.transitions.duration.short
    ),
  },
  loading: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "260px",
  },
  largeText: {
    fontSize: "48px",
    lineHeight: "60px",
    fontFamily: "Poppins, sans-serif",
    marginRight: "10px",
    fontWeight: "bold",
    color: (props) => props.color && theme.palette.getContrastText(props.color),
  },
  smallText: {
    fontSize: "24px",
    lineHeight: "28px",
    color: (props) => props.color && theme.palette.getContrastText(props.color),
  },
}));

const options = (theme, color) => ({
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    mode: "index",
    displayColors: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          display: false,
          drawBorder: false,
        },
        ticks: {
          fontColor: theme.palette.getContrastText(color),
          fontFamily: "Poppins, sans-serif",
          fontSize: 14,
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          drawBorder: false,
          borderDash: [5, 5],
          borderDashOffset: 5,
          color: theme.palette.getContrastText(color),
        },
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 4,
          minTicksLimit: 4,
          fontSize: 14,
          fontFamily: "Poppins, sans-serif",
          fontColor: theme.palette.getContrastText(color),
        },
      },
    ],
  },
});

const createData = (labels, data, theme, color, tooltip) => {
  const dataset = {
    label: tooltip,
    fill: false,
    data: data,
    pointRadius: 0,
    pointHitRadius: 5,
    borderColor: theme.palette.getContrastText(color),
    lineTension: 0,
  };

  return {
    labels: labels,
    datasets: [dataset],
  };
};

const StatBox = ({ color, title, tooltip, getStatsFunc }) => {
  const theme = useTheme();
  const [isLoading, setIsLoading] = useState(true);
  const [labels, setLabels] = useState([]);
  const [data, setData] = useState([]);
  const classes = useStyles({ color: isLoading ? null : color });

  useEffect(() => {
    setIsLoading(true);
    const statsFunc = getStatsFunc ? getStatsFunc : getStats;
    statsFunc().then((stats) => {
      setIsLoading(false);
      setLabels(stats.dates.map((date) => moment(date).format("DD.MM")));
      setData(stats.counts);
    });
  }, []);

  return (
    <Grid item xs={4}>
      <Card elevation={2} className={classes.card}>
        {isLoading ? (
          <Box className={classes.loading}>
            <CircularProgress />
          </Box>
        ) : (
          <React.Fragment>
            <Box display="flex" alignItems="center" height="60px">
              <Typography className={classes.largeText}>
                {data[data.length - 1]}
              </Typography>
              <Typography className={classes.smallText}>
                {title(data[data.length - 1])}
              </Typography>
            </Box>
            <Box height="200px">
              <Line
                data={createData(labels, data, theme, color, tooltip)}
                options={options(theme, color)}
              />
            </Box>
          </React.Fragment>
        )}
      </Card>
    </Grid>
  );
};

export default StatBox;
