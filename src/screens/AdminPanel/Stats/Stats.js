import React from "react";
import Grid from "@material-ui/core/Grid";
import StatBox from "./StatBox";
import getUserStats from "../../../services/stats/getUserStats";
import getRentalAdsStats from "../../../services/stats/getRentalAdsStats";
import getRentStats from "../../../services/stats/getRentStats";

const getAccounts = (count) => {
  if ((count < 10 || count > 20) && count % 10 >= 2 && count % 10 <= 4)
    return "konta założone dzisiaj";
  if (count === 1) return "konto założone dzisiaj";
  return "kont założonych dzisiaj";
};

const getAds = (count) => {
  if ((count < 10 || count > 20) && count % 10 >= 2 && count % 10 <= 4)
    return "ogłoszenia dodane dzisiaj";
  if (count === 1) return "ogłoszenie dodane dzisiaj";
  return "ogłoszeń dodane dzisiaj";
};

const getHires = (count) => {
  if ((count < 10 || count > 20) && count % 10 >= 2 && count % 10 <= 4)
    return "wypożyczenia dokonane dzisiaj";
  if (count === 1) return "wypożyczenie dokonane dzisiaj";
  return "wypożyczeń dokonanych dzisiaj";
};

const Stats = () => {
  return (
    <Grid container spacing={3}>
      <StatBox
        color="#2F61ED"
        title={getAccounts}
        tooltip="Nowych kont"
        getStatsFunc={getUserStats}
      />
      <StatBox
        color="#AA53EB"
        title={getAds}
        tooltip="Nowych ogłoszeń"
        getStatsFunc={getRentalAdsStats}
      />
      <StatBox
        color="#F5D653"
        title={getHires}
        tooltip="Dokonanych wypożyczeń"
        getStatsFunc={getRentStats}
      />
    </Grid>
  );
};

export default Stats;
