import React, { useState, useEffect, useMemo } from "react";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Collapse from "@material-ui/core/Collapse";
import Card from "@material-ui/core/Card";
import Zoom from "@material-ui/core/Zoom";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TablePagination from "@material-ui/core/TablePagination";
import Pagination from "@material-ui/lab/Pagination";
import BlockedIP from "./BlockedIP";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";
import getBlockedIPs from "../../../services/admin/blockedIPs/getBlockedIPs";
import postBlockedIP from "../../../services/admin/blockedIPs/postBlockedIP";
import { hasErrors, validateForm } from "../../../utils/validateForm";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: "10px",
    padding: "10px",
  },
  cardTitle: {
    fontWeight: "bold",
    fontSize: "12px",
    marginBottom: "5px",
    color: theme.palette.primary.main,
  },
  input: {
    width: "400px",
    marginRight: "10px",
  },
  button: {
    marginRight: "10px",
  },
}));

const usePaginationStyles = makeStyles(() => ({
  spacer: {
    flex: "unset",
  },
}));

const fields = [
  {
    id: "ip",
    required: true,
    regex:
      /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
    regexError: "Wprowadź adres IP w prawidłowym formacie",
  },
];

const BlockedIPs = () => {
  const classes = useStyles();
  const [blockedIPs, setBlockedIPs] = useState([]);
  const [newAddressOpen, setNewAddressOpen] = useState(false);
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const dispatch = useDispatch();

  useEffect(() => {
    downloadData();
  }, [page, rowsPerPage, search]);

  const downloadData = () => {
    getBlockedIPs(page - 1, rowsPerPage, search).then((words) => {
      setCount(words.count);
      setBlockedIPs(words.results);
    });
  };

  const handleChange = (search) => {
    setTimeout(() => {
      setSearch(search);
      setPage(1);
    }, 300);
  };

  const openDialog = () => {
    setNewAddressOpen(true);
    setValues({});
    setErrors({});
  };

  const handleValueChange = (value) => {
    const tmpValues = { ...values};
    tmpValues.ip = value;
    setValues(tmpValues);
  }

  const handleAddWord = () => {
    const errors = validateForm(fields, values);
    setErrors(errors);
    if (hasErrors(errors)) {
      return;
    }

    postBlockedIP(values.ip)
      .then(() => {
        setNewAddressOpen(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Zablokowano adres IP",
          })
        );
        if (page !== 1) {
          setPage(1);
        } else {
          downloadData();
        }
      })
      .catch(() => {
        dispatch(snackbarAction.openSnackbar({ error: true }));
      });
  };

  const handleDelete = (error) => {
    if (error) {
      dispatch(snackbarAction.openSnackbar({ error: true }));
    } else {
      dispatch(
        snackbarAction.openSnackbar({
          error: false,
          message: "Odblokowano adres IP",
        })
      );
      if (page !== 1) {
        setPage(1);
      } else {
        downloadData();
      }
    }
  };

  const PaginationAction = () => (
    <Pagination
      showFirstButton
      showLastButton
      count={Math.ceil(count / rowsPerPage)}
      page={page}
      onChange={(e, page) => setPage(page)}
    />
  );

  return (
    <Box width="100%" display="flex" flexDirection="column" alignItems="center">
      <Box
        display="flex"
        alignItems="center"
        marginBottom="10px"
        width="700px"
        justifyContent="space-between"
      >
        <TextField
          className={classes.input}
          placeholder="Wyszukaj adres IP"
          onChange={(e) => handleChange(e.target.value)}
        />
        <Zoom in={!newAddressOpen}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => openDialog()}
          >
            Zablokuj adres IP
          </Button>
        </Zoom>
      </Box>
      <Collapse in={newAddressOpen} unmountOnExit>
        <Card elevation={1} className={classes.card}>
          <Typography className={classes.cardTitle}>Zablokuj adres</Typography>
          <Box display="flex" alignItems="center">
            <TextField
              className={classes.input}
              placeholder="Wpisz nowy adres"
              onChange={(e) => handleValueChange(e.target.value)}
              error={!!errors.ip}
              helperText={errors.ip}
            />
            <Box>
              <Button
                className={classes.button}
                variant="contained"
                onClick={() => setNewAddressOpen(false)}
              >
                Anuluj
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleAddWord()}
              >
                Dodaj
              </Button>
            </Box>
          </Box>
        </Card>
      </Collapse>
      <TablePagination
        component="div"
        rowsPerPage={rowsPerPage}
        rowPerPageOptions={[25, 50, 100]}
        onChangeRowsPerPage={(e) => {
          setRowsPerPage(e.target.value);
          setPage(1);
        }}
        page={page - 1}
        count={count}
        ActionsComponent={PaginationAction}
        classes={usePaginationStyles()}
      />
      <Box>
        {blockedIPs.map((ip) => (
          <BlockedIP
            key={ip.id}
            ip={ip}
            classes={classes}
            handleDelete={handleDelete}
          />
        ))}
      </Box>
    </Box>
  );
};

export default BlockedIPs;
