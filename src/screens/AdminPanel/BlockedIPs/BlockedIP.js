import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";

import deleteBlockedIP from "../../../services/admin/blockedIPs/deleteBlockedIP";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "10px",
    width: "700px",
    borderRadius: "5px",
    backgroundColor: theme.palette.background.paper,
    "&:nth-of-type(even)": {
      backgroundColor: theme.palette.background.default,
    },
  },
  ip: {
    width: "300px",
  },
}));

const BlockedIP = ({ ip, handleDelete }) => {
  const classes = useStyles();
  const [disabled, setDisabled] = useState(false);

  const onDelete = () => {
    setDisabled(true);
    deleteBlockedIP(ip.id)
      .then(() => handleDelete(false))
      .catch(() => {
        setDisabled(false);
        handleDelete(true);
      });
  };

  return (
    <Box className={classes.root}>
      <Typography color="textPrimary" className={classes.ip}>
        {ip.ip}
      </Typography>
      <IconButton disabled={disabled} onClick={() => onDelete()} size="small">
        <CloseIcon />
      </IconButton>
    </Box>
  );
};

export default BlockedIP;
