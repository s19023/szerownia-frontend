import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import Pagination from "@material-ui/lab/Pagination";
import Loader from "../../../../components/Loader/Loader";
import SingleTicket from "./SingleTicket";
import { Switch, Route, useHistory, useLocation } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import { images } from "../../../../config/urls";

const useStyles = makeStyles((theme) => ({
  row: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: "60px",
    "&:nth-of-type(even)": {
      background: theme.palette.background.default,
    },
  },
  avatar: {
    width: "64px",
    display: "flex",
    justifyContent: "center",
  },
  header: {
    flexGrow: "1",
    flexBasis: "25%",
    fontWeight: "bold",
    color: theme.palette.text.primary,
  },
  loading: {
    display: "flex",
    justifyContent: "center",
    padding: "100px",
  },
  name: {
    flexGrow: "1",
    flexBasis: "25%",
    color: theme.palette.text.primary,
  },
  button: {
    flexGrow: "1",
    flexBasis: "10%",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    color: theme.palette.text.primary,
  },
}));

const TicketList = ({ getTickets, url, component, objectKey, type }) => {
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [tickets, setTickets] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (location.pathname.split("/").length !== 4) {
      return;
    }

    setIsLoading(true);
    setTickets([]);
    getTickets(page - 1).then((tickets) => {
      setTotalPages(tickets.totalPages);
      setTickets(tickets.results);
      setIsLoading(false);
    });
  }, [url, page, location]);

  return (
    <Switch>
      <Route path={`/admin/tickets/${url}/:id`}>
        <SingleTicket
          backUrl={`/admin/tickets/${url}`}
          component={component}
          objectKey={objectKey}
          isAd={type === "ad"}
          isUser={type === "user"}
          isOpinion={type === "opinion"}
        />
      </Route>
      <Route path={`/admin/tickets/${url}`}>
        <Box>
          <Box className={classes.row}>
            <Box className={classes.avatar} />
            <Typography className={classes.header}>Zgłaszający</Typography>
            <Typography className={classes.header}>Powód</Typography>
            <Typography className={classes.header}>Data zgłoszenia</Typography>
            <Typography className={classes.button}>Przejdź</Typography>
          </Box>
          {isLoading ||
            (tickets.length === 0 && (
              <Box className={classes.loading}>
                <Loader
                  array={tickets}
                  isLoading={isLoading}
                  label="Nie znaleziono zgłoszeń"
                />
              </Box>
            ))}
          {tickets.map((ticket) => (
            <Box key={ticket.id} className={classes.row}>
              <Box className={classes.avatar}>
                {ticket.notifier && (
                  <Avatar
                    src={images.get + "/" + ticket.notifier.userImageId}
                  />
                )}
              </Box>
              <Typography className={classes.name}>
                {ticket.notifier
                  ? ticket.notifier.firstName + " " + ticket.notifier.lastName
                  : "System"}
              </Typography>
              <Typography className={classes.name}>{ticket.subject}</Typography>
              <Typography className={classes.name}>
                {ticket.reportedDate &&
                  moment(ticket.reportedDate).format("DD.MM.YYYY HH:mm")}
              </Typography>
              <Box className={classes.button}>
                <IconButton
                  color="primary"
                  onClick={() => history.push(`${url}/${ticket.id}`)}
                >
                  <ArrowForwardIcon />
                </IconButton>
              </Box>
            </Box>
          ))}
        </Box>
        {!isLoading && tickets.length > 0 && (
          <Pagination
            page={page}
            count={totalPages}
            showFirstButton
            showLastButton
            onChange={(e, v) => setPage(v)}
          />
        )}
      </Route>
    </Switch>
  );
};

export default TicketList;
