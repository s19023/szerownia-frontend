import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import Avatar from "../../../../components/Avatar/Avatar";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0px 3px 6px #00000029",
    width: "100%",
    minHeight: "100px",
    padding: "20px",
    boxSizing: "border-box",
    display: "flex",
  },
  title: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
  description: {
    fontSize: "18px",
  },
  rating: {
    display: "flex",
    alignItems: "center",
  },
}));

const RatingDetails = ({ object }) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Avatar
        imageId={object.author.userImageId}
        firstName={object.author.firstName}
        lastName={object.author.lastName}
        margin={20}
        size={64}
      />
      <Box flexGrow="1" marginRight="20px">
        <Typography className={classes.title}>
          {object.author.firstName} {object.author.lastName}
        </Typography>
        <Typography className={classes.description}>
          {object.comment}
        </Typography>
      </Box>
      <Box className={classes.rating}>
        <Rating readOnly value={object.rating} max={5} />
      </Box>
    </Box>
  );
};

export default RatingDetails;
