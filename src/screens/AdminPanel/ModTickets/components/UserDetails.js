import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Avatar from "../../../../components/Avatar/Avatar";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: "0px 3px 6px #00000029",
    width: "100%",
    padding: "20px",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
  },
  avatar: {
    width: "64px",
    height: "64px",
    marginRight: "20px",
    boxShadow: "0px 3px 6px #00000029"
  },
  title: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
}));

const UserDetails = ({ object }) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Avatar
        imageId={object.userImageId}
        firstName={object.firstName}
        lastName={object.lastName}
        margin={20}
        size={64}
      />
      <Box flexGrow="1" marginRight="20px">
        <Typography className={classes.title}>
          {object.firstName} {object.lastName}
        </Typography>
      </Box>
    </Box>
  );
};

export default UserDetails;
