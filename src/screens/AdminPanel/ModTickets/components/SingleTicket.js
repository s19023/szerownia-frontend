import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MuiLink from "@material-ui/core/Link";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import TextField from "@material-ui/core/TextField";
import { useParams, useHistory, Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { snackbarAction } from "../../../../redux/modules/snackbar";

import getSingleTicket from "../../../../services/admin/modTickets/getSingleTicket";
import moderateTicket from "../../../../services/admin/modTickets/moderateTicket";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100%",
    padding: "20px",
    boxSizing: "border-box",
  },
  row: {
    display: "flex",
    alignItems: "center",
    marginBottom: "10px",
  },
  button: {
    marginRight: "20px",
  },
  header: {
    fontSize: "18px",
    color: theme.palette.text.primary,
  },
  bold: {
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    color: theme.palette.text.primary,
  },
  loading: {
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  description: {
    color: theme.palette.text.primary,
    fontSize: "18px",
    marginBottom: "10px",
  },
  buttonBox: {
    display: "flex",
    width: "100%",
    marginBottom: (props) => (props.open ? "10px" : "0px"),
    transition: theme.transitions.create("margin-bottom", {
      duration: theme.transitions.duration.standard,
    }),
  },
  bottomButton: {
    padding: "5px 10px",
    "&:not(:last-of-type)": {
      marginRight: "10px",
    },
  },
  collapseRoot: {
    display: "flex",
    alignItems: "center",
  },
  input: {
    maxWidth: "300px",
    marginRight: "5px",
  },
}));

const SingleTicket = ({ backUrl, component, objectKey, isAd, isOpinion }) => {
  const { id } = useParams();
  const history = useHistory();
  const [ticket, setTicket] = useState({});
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [command, setCommand] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const classes = useStyles({ open });
  const dispatch = useDispatch();

  const DetailComponent = component;

  useEffect(() => {
    setIsLoading(true);
    getSingleTicket(id)
      .then((ticket) => {
        setTicket(ticket);
        setIsLoading(false);
      })
      .catch(() => {
        history.push(backUrl);
        setIsLoading(false);
      });
  }, []);

  const handleSend = (command) => {
    setIsLoading(true);
    moderateTicket(id, command, message)
      .then(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Pomyślnie zmoderowano zgłoszenie",
          })
        );
        history.push(backUrl);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const handleOpen = (newCommand) => {
    if (newCommand === command) {
      return;
    }

    setCommand(newCommand);
    setMessage("");
    setOpen(newCommand !== "MODIFIED");

    if (newCommand === "MODIFIED") {
      handleSend(newCommand);
    }
  };

  if (isLoading) {
    return (
      <Box className={classes.loading}>
        <CircularProgress size={64} />
      </Box>
    );
  }

  const reportedLink = isAd
    ? `/ad/${ticket.ad?.descriminator === "RENTALAD" ? "rental" : "search"}/${
        ticket.ad?.id
      }`
    : `/user/${ticket.reported?.id}`;

  return (
    <Box className={classes.root}>
      <Box className={classes.row}>
        <IconButton
          color="primary"
          className={classes.button}
          onClick={() => history.push(backUrl)}
        >
          <ArrowBackIcon />
        </IconButton>
        <Typography className={classes.header}>
          {ticket.notifier ? (
            <React.Fragment>
              Zgłoszenie przesłane przez użytkownika{" "}
              <MuiLink
                component={Link}
                to={`/user/${ticket.notifier.id}`}
                target="blank"
                underline="hover"
                className={classes.bold}
              >
                {ticket.notifier.firstName} {ticket.notifier.lastName}
              </MuiLink>
            </React.Fragment>
          ) : (
            <React.Fragment>
              Zgłoszenie stworzone przez <b className={classes.bold}>System</b>
            </React.Fragment>
          )}
        </Typography>
      </Box>
      <Box className={classes.row}>
        <Typography className={classes.header}>
          <b className={classes.bold}>Powód zgłoszenia: </b>
          {ticket.subject.toLowerCase()}
        </Typography>
      </Box>
      <Box className={classes.row}>
        <Typography className={`${classes.bold} ${classes.header}`}>
          Opis zgłoszenia
        </Typography>
      </Box>
      <Typography className={classes.description}>
        {ticket.description}
      </Typography>
      {component && ticket[objectKey] && (
        <React.Fragment>
          <Box className={classes.row}>
            <Typography className={`${classes.header} ${classes.bold}`}>
              Zgłoszenie dotyczy
            </Typography>
          </Box>
          <Box className={classes.row}>
            <DetailComponent object={ticket[objectKey]} />
          </Box>
        </React.Fragment>
      )}
      <Box className={classes.row}>
        <Typography className={`${classes.header} ${classes.bold}`}>
          Dostępne akcje
        </Typography>
      </Box>
      <Box className={classes.buttonBox}>
        <Button
          color="secondary"
          className={classes.bottomButton}
          onClick={() => handleOpen("HIDDEN")}
        >
          Ukryj
        </Button>
        {!isOpinion && (
          <Button
            className={classes.bottomButton}
            component={Link}
            target="_blank"
            to={reportedLink}
          >
            Przejdź do {isAd ? "ogłoszenia" : "profilu użytkownika"}
          </Button>
        )}
        {isAd && (
          <Button
            color="primary"
            className={classes.bottomButton}
            onClick={() =>
              handleOpen(
                ticket.result === "TO_VERIFY" ? "MODIFIED" : "TO_MODIFY"
              )
            }
          >
            {ticket.result === "TO_VERIFY"
              ? "Zaakceptuj"
              : "Odeślij do poprawy"}
          </Button>
        )}
      </Box>
      <Collapse in={open}>
        <Box className={classes.collapseRoot}>
          <TextField
            label="Dodaj wiadomość"
            variant="outlined"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            className={classes.input}
          />
          <Button
            color="primary"
            onClick={() => handleSend(command)}
            disabled={!message}
          >
            Zatwierdź
          </Button>
        </Box>
      </Collapse>
    </Box>
  );
};

export default SingleTicket;
