import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TicketList from "./components/TicketList";
import { Switch, Route, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import getOpinionTickets from "../../../services/admin/modTickets/getOpinionTickets";
import getAdTickets from "../../../services/admin/modTickets/getAdTickets";
import getUserTickets from "../../../services/admin/modTickets/getUserTickets";

import AdDetails from "../../../components/AdDetails/AdDetails";
import RatingDetails from "./components/RatingDetails";
import UserDetails from "./components/UserDetails";

const urls = ["opinions", "ads", "users"];
const names = ["Opinie", "Ogłoszenia", "Użytkownicy"];

const useStyles = makeStyles((theme) => ({
  buttonRoot: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    height: "400px",
  },
  button: {
    fontSize: "24px",
    padding: "20px",
    borderRadius: "10px",
    width: "220px",
  },
  listRoot: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: theme.palette.background.paper,
    flexGrow: "1",
    borderRadius: "10px",
    boxShadow: theme.shadows[3],
    padding: "15px",
  },
}));

const ModTickets = () => {
  const [selectedMenu, setSelectedMenu] = useState("opinions");
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    history.push("/admin/tickets/opinions");
    setSelectedMenu("opinions");
  }, []);

  const handleClick = (url) => {
    history.push(`/admin/tickets/${url}`);
    setSelectedMenu(url);
  };

  return (
    <Box display="flex" alignItems="strech">
      <Box display="flex" marginRight="20px">
        <Box className={classes.buttonRoot}>
          {urls.map((url, index) => (
            <Button
              className={classes.button}
              key={`url-${index}`}
              variant={selectedMenu === url ? "contained" : "outlined"}
              color="primary"
              onClick={() => handleClick(url)}
            >
              {names[index]}
            </Button>
          ))}
        </Box>
      </Box>
      <Box className={classes.listRoot}>
        <Switch>
          <Route path="/admin/tickets/opinions">
            <TicketList
              getTickets={getOpinionTickets}
              url="opinions"
              component={RatingDetails}
              objectKey="opinion"
              type="opinion"
            />
          </Route>
          <Route path="/admin/tickets/ads">
            <TicketList
              getTickets={getAdTickets}
              url="ads"
              component={AdDetails}
              objectKey="ad"
              type="ad"
            />
          </Route>
          <Route path="/admin/tickets/users">
            <TicketList
              getTickets={getUserTickets}
              url="users"
              component={UserDetails}
              objectKey="reported"
              type="user"
            />
          </Route>
        </Switch>
      </Box>
    </Box>
  );
};

export default ModTickets;
