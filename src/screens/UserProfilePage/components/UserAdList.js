import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import getUserSearchAds from "../../../services/searchAds/getUserSearchAds";
import getSearchAdsListToCorrect from "../../../services/searchAds/getSearchAdsToCorrect";
import getRentalAdsListToCorrect from "../../../services/rentalAds/getRentalAdsToCorrect";
import getUserRentalAds from "../../../services/rentalAds/getUserRentalAds";
import UserAds from "./UserAds";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  header: {
    fontSize: "18px",
    fontWeight: "bold",
    marginBottom: "10px",
  },
}));

const UserAdList = ({ isSearchAd }) => {
  const classes = useStyles();

  return (
    <Box>
      <Typography component="h2" className={classes.header}>
        Ogłoszenia
      </Typography>
      <UserAds
        getAds={isSearchAd ? getUserSearchAds : getUserRentalAds}
        isSearchAd={isSearchAd}
        label="Nie znaleziono ogłoszeń"
      />
      <Typography component="h2" className={classes.header}>
        Ogłoszenia do poprawy
      </Typography>
      <UserAds
        getAds={isSearchAd ? getSearchAdsListToCorrect : getRentalAdsListToCorrect}
        isSearchAd={isSearchAd}
        label="Nie znaleziono ogłoszeń do poprawy"
      />
    </Box>
  );
};
export default UserAdList;
