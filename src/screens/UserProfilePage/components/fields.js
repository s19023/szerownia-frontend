import MaskedInput from "../../../components/MaskedInput/MaskedInput";

export const fields = [
  {
    name: "Imię",
    id: "firstName",
    minLength: 2,
  },
  {
    name: "Nazwisko",
    id: "lastName",
    minLength: 2,
  },
  {
    name: "Adres e-mail",
    id: "email",
    type: "email",
    regex: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    regexError: "Wprowadzono nieprawidłowy adres e-mail",
  },
  {
    name: "Telefon",
    type: "tel",
    id: "telephoneNumber",
    maskedInput: ({ inputRef, ...other }) => {
      return (
        <MaskedInput
          inputRef={inputRef}
          mask={[
            /\d/,
            /\d/,
            /\d/,
            " ",
            /\d/,
            /\d/,
            /\d/,
            " ",
            /\d/,
            /\d/,
            /\d/,
          ]}
          {...other}
        />
      );
    },
    regex: /^(\d{3} ?){2}\d{3}$/,
    regexError: "Wprowadzono nieprawidłowy numer telefonu",
  },
];
