import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";

import updateUser from "../../../services/user/updateUser";
import { hasErrors, validateForm } from "../../../utils/validateForm";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
    [theme.breakpoints.down("md")]: {
      marginBottom: "10px",
      width: "100%",
      alignItems: "center",
    },
  },
  input: {
    width: "100%",
    maxWidth: "300px",
  },
  button: {
    width: "150px",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
}));

const createValues = (fields) => {
  const values = {};
  fields.forEach((field) => {
    values[field.id] = "";
  });

  return values;
};

const EditUserData = ({ values, close }) => {
  const [data, setData] = useState(values);
  const [initialState] = useState(values);
  const [errors, setErrors] = useState(createValues(fields));
  const [isDisabled, setIsDisabled] = useState(true);
  const classes = useStyles();
  const id = useSelector((state) => state.userReducer.id);
  const dispatch = useDispatch();

  const handleUpdate = (key, value) => {
    const tmpData = { ...data };
    tmpData[key] = value;
    let isDisabled = false;
    let isEqual = true;

    fields.forEach((field) => {
      const key = field.id;
      if (tmpData[key] === "" || tmpData[key] === null) {
        isDisabled = true;
        return;
      }

      if (tmpData[key] !== initialState[key]) {
        isEqual = false;
      }
    });

    setData(tmpData);
    setIsDisabled(isDisabled || isEqual);
  };

  const validateData = () => {
    const errors = validateForm(fields, values);
    setErrors(errors);
    return !hasErrors(errors);
  };

  const handleUpload = () => {
    if (!validateData()) {
      return;
    }
    setIsDisabled(true);

    updateUser(id, data)
      .then((userData) => {
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Dane zmienione pomyślnie",
          })
        );
        close(userData);
      })
      .catch(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
          })
        );
        setIsDisabled(false);
      });
  };

  return (
    <React.Fragment>
      <Box className={classes.root}>
        {fields.map((field) => (
          <TextField
            key={field.id}
            className={classes.input}
            error={!!errors[field.id]}
            helperText={errors[field.id]}
            onChange={(e) => handleUpdate(field.id, e.target.value)}
            value={data[field.id]}
            type={field.type}
            label={field.name}
            InputProps={{
              inputComponent: field.maskedInput ? field.maskedInput : "input",
            }}
          />
        ))}
      </Box>
      <Box display="flex" flexDirection="column">
        <Button
          color="primary"
          variant="contained"
          disabled={isDisabled}
          className={classes.button}
          onClick={() => handleUpload()}
        >
          Zapisz
        </Button>
        <Button
          variant="contained"
          onClick={() => close()}
          className={classes.button}
        >
          Anuluj
        </Button>
      </Box>
    </React.Fragment>
  );
};

export default EditUserData;
