import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Box from "@material-ui/core/Box";
import AdDetails from "../../../components/AdDetails/AdDetails";
import Loader from "../../../components/Loader/Loader";
import Pagination from "@material-ui/lab/Pagination";
import { makeStyles } from "@material-ui/core/styles";

import getUserRentalAds from "../../../services/rentalAds/getUserRentalAds";
import getUserSearchAds from "../../../services/searchAds/getUserSearchAds";

const useStyles = makeStyles((theme) => ({
  pagination: {
    marginBottom: "10px",
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      justifyContent: "center",
    },
  },
}));

const UserAds = ({ isSearchAd, getAds, label }) => {
  const classes = useStyles();
  const id = useSelector((state) => state.userReducer.id);
  const [ads, setAds] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setIsLoading(true);
    getAds(id, page - 1)
      .then((ads) => {
        setIsLoading(false);
        setTotalPages(ads.totalPages);
        setAds(ads.results);
      })
      .catch(() => {
        setIsLoading(false);
        setTotalPages(0);
        setAds([]);
      });
  }, [page]);

  return (
    <Box>
      <Box className={classes.pagination}>
        <Pagination
          count={totalPages}
          page={page}
          onChange={(e, v) => setPage(v)}
          showFirstButton
          showLastButton
        />
      </Box>
      <Loader isLoading={isLoading} array={ads} label={label} />
      {ads.map((ad) => (
        <Box key={`ad-${ad.id}`} marginBottom="30px">
          <AdDetails object={ad} isSearchAd={isSearchAd} showEdit />
        </Box>
      ))}
    </Box>
  );
};

export default UserAds;
