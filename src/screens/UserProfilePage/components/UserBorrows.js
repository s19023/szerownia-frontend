import React, { useState, useEffect } from "react";

import getUserBorrows from "../../../services/hires/getUserBorrows";
import ShareRow from "./HireList/components/ShareRow";

const UserBorrows = () => {
  const [borrows, setBorrows] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    getUserBorrows().then((borrows) => setBorrows(borrows));
  };

  return (
    <React.Fragment>
      {borrows.map((borrow) => (
        <ShareRow
          key={borrow.id}
          hire={borrow}
          isBorrow
        />
      ))}
    </React.Fragment>
  );
};

export default UserBorrows;
