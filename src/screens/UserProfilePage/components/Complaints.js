import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Pagination from "@material-ui/lab/Pagination";
import Loader from "../../../components/Loader/Loader";
import getUserComplaints from "../../../services/complaints/getUserComplaints";
import { useComplaintsFields } from "../../../components/useComplaintsFields/useComplaintsFields";
import Table from "../../../components/Table/Table";

const Complaints = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [complaints, setComplaints] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const fields = useComplaintsFields().filter(
    (field) => field.id !== "borrower"
  );

  useEffect(() => {
    setIsLoading(true);
    getUserComplaints(page - 1)
      .then((response) => {
        setTotalPages(response.totalPages);
        setComplaints(response.results);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [page]);

  return (
    <React.Fragment>
      <Box marginBottom="10px">
        <Pagination
          page={page}
          onChange={(e, v) => setPage(v)}
          count={totalPages}
          showFirstButton
          showLastButton
        />
      </Box>
      <Loader
        array={complaints}
        isLoading={isLoading}
        label="Nie znaleziono reklamacji"
      />
      {!isLoading && complaints.length > 0 && (
        <Table
          fields={fields}
          data={complaints}
          defaultSort="reportDate"
          disableStartPadding
          disableEndPadding
        />
      )}
    </React.Fragment>
  );
};

export default Complaints;
