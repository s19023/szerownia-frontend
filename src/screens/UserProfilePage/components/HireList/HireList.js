import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Loader from "../../../../components/Loader/Loader";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import getUserBorrows from "../../../../services/hires/getUserBorrows";
import getUserShares from "../../../../services/hires/getUserShares";
import clsx from "clsx";
import ShareRow from "../../../../components/ShareRow/ShareRow";

const useStyles = makeStyles(() => ({
  flexRow: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    marginBottom: "20px",
  },
  center: {
    justifyContent: "center",
  },
  flexEnd: {
    justifyContent: "flex-end",
  },
  select: {
    width: "300px",
  },
  selectText: {
    fontWeight: "bold",
    marginRight: "10px",
  },
}));

const HireList = ({ borrows, isPremium }) => {
  const classes = useStyles();
  const hireStatuses = useSelector((state) => state.enumReducer.hireStatuses);
  const [hireType, setHireType] = useState("WAITING_FOR_SHIPMENT");
  const [loadingHires, setLoading] = useState(true);
  const [hires, setHires] = useState([]);

  useEffect(() => {
    getData();
  }, [hireType]);

  const getData = () => {
    setLoading(true);
    const getHires = borrows ? getUserBorrows : getUserShares;
    getHires(hireType)
      .then((hires) => {
        setHires(hires);
        setLoading(false);
      })
      .catch(() => {
        setHires([]);
        setLoading(false);
      });
  };

  return (
    <React.Fragment>
      <Box className={clsx(classes.flexRow, classes.flexEnd)}>
        <Typography className={classes.selectText}>
          Status {borrows ? "wypożyczenia" : "użyczenia"}:
        </Typography>
        <Select
          value={hireType}
          onChange={(e) => setHireType(e.target.value)}
          className={classes.select}
        >
          {Object.keys(hireStatuses).map((key) => (
            <MenuItem key={key} value={key}>
              {hireStatuses[key]}
            </MenuItem>
          ))}
        </Select>
      </Box>
      <Loader
        isLoading={loadingHires}
        array={hires}
        label={`Nie znaleziono ${borrows ? "wypożyczeń" : "użyczeń"}`}
      />
      {!loadingHires &&
        hires.map((hire) => (
          <ShareRow
            hire={hire}
            key={hire.id}
            isBorrow={borrows}
            isPremium={isPremium}
            handleRefresh={getData}
          />
        ))}
    </React.Fragment>
  );
};

export default HireList;
