import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Slider from "@material-ui/core/Slider";
import { makeStyles } from "@material-ui/core/styles";
import Dropzone from "react-dropzone";
import Compressor from "compressorjs";
import Cropper from "react-easy-crop";
import { cropImage } from "../../../utils/cropImage";

import updateProfilePicture from "../../../services/user/updateProfilePicture";

const useStyles = makeStyles((theme) => ({
  inputRoot: {
    height: "300px",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "5px",
    border: "1px dashed #707070",
    padding: "5px",
    boxSizing: "border-box",
  },
  text: {
    fontSize: "18px",
    color: theme.palette.text.disabled,
    textAlign: "center",
  },
  cropper: {
    height: "500px",
    width: "100%",
    position: "relative",
    marginBottom: "10px",
  },
  slider: {
    display: "flex",
    alignItems: "center",
  },
  sliderText: {
    fontSize: "24px",
    fontWeight: "bold",
    textAlign: "center",
  },
}));

const ProfilePictureDialog = ({ onClose, handleSuccess, open }) => {
  const classes = useStyles();
  const [src, setSrc] = useState("");
  const [fileName, setFileName] = useState("");
  const [zoom, setZoom] = useState(1);
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [cropPixels, setCropPixels] = useState(null);

  const acceptFile = (files) => {
    const srcFile = files[0];
    setFileName(srcFile.name);
    const reader = new FileReader();
    reader.onload = () => {
      setSrc(reader.result);
    };
    reader.readAsDataURL(srcFile);
  };

  const onCropComplete = (cropArea, cropPixels) => setCropPixels(cropPixels);

  const handleSave = () => {
    cropImage(src, cropPixels).then((img) => {
      updateProfilePicture(img, fileName).then(() => {
        handleSuccess();
      });
    });
  };

  return (
    <Dialog open={open} maxWidth="md" fullWidth onClose={onClose}>
      <DialogTitle>Dodaj zdjęcie profilowe</DialogTitle>
      <DialogContent>
        {src === "" ? (
          <Dropzone
            accept={["image/jpeg", "image/png"]}
            onDropAccepted={acceptFile}
            multiple={false}
            maxSize={5 * 1024 * 1024}
          >
            {({ getRootProps, getInputProps }) => (
              <Box className={classes.inputRoot} {...getRootProps()}>
                <input {...getInputProps()} />
                <Typography className={classes.text}>
                  Kliknij tutaj lub przeciągnij zdjęcie
                </Typography>
              </Box>
            )}
          </Dropzone>
        ) : (
          <Box className={classes.cropper}>
            <Cropper
              image={src}
              zoom={zoom}
              aspect={1}
              cropShape="round"
              crop={crop}
              onCropChange={(crop) => setCrop(crop)}
              onCropComplete={onCropComplete}
            />
          </Box>
        )}
        {src !== "" && (
          <Grid container spacing={2} justify="center">
            <Grid item>
              <Typography className={classes.sliderText}>-</Typography>
            </Grid>
            <Grid item xs={4} className={classes.slider}>
              <Slider
                disabled={src === ""}
                step={0.1}
                min={1}
                max={4}
                onChange={(e, v) => setZoom(v)}
              />
            </Grid>
            <Grid item>
              <Typography className={classes.sliderText}>+</Typography>
            </Grid>
          </Grid>
        )}
      </DialogContent>
      <DialogActions>
        <Button color="secondary" onClick={() => onClose()}>
          Anuluj
        </Button>
        <Button
          color="primary"
          disabled={src === ""}
          onClick={() => handleSave()}
        >
          Zapisz
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ProfilePictureDialog;
