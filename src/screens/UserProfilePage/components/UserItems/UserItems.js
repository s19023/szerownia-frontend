import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Loader from "../../../../components/Loader/Loader";
import Collapse from "@material-ui/core/Collapse";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import AddNewItem from "../../../../components/Item/AddNewItem";
import { makeStyles } from "@material-ui/core/styles";
import getUserProducts from "../../../../services/products/getUserProducts";
import Item from "./components/Item";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  editItem: {
    "&:not(:last-of-type)": {
      marginBottom: "20px",
    },
  },
}));

const UserItems = () => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(true);
  const [items, setItems] = useState([]);
  const [newItemOpen, setNewItemOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(null);

  useEffect(() => {
    downloadData();
  }, []);

  const downloadData = () => {
    getUserProducts().then((items) => {
      setIsLoading(false);
      setItems(items);
    });
  };

  const handleSuccess = (id) => {
    if (id) {
      setOpenEdit(null);
    } else {
      setNewItemOpen(false);
    }
    downloadData();
  };

  return (
    <Box className={classes.root}>
      <Loader
        isLoading={isLoading}
        array={items}
        label="Nie znaleziono przedmiotów"
        margin="20px"
      />
      {!isLoading &&
        items.map((item) =>
          openEdit !== item.idProduct ? (
            <Item
              item={item}
              handleEdit={() => setOpenEdit(item.idProduct)}
              handleSuccess={() => handleSuccess()}
              key={item.idProduct}
            />
          ) : (
            <Box className={classes.editItem} key={item.idProduct}>
              <AddNewItem
                id={item.idProduct}
                handleClose={() => setOpenEdit(null)}
                handleSuccess={() => handleSuccess(item.idProduct)}
              />
            </Box>
          )
        )}
      <Collapse in={!newItemOpen}>
        <Button
          color="primary"
          startIcon={<AddIcon />}
          onClick={() => setNewItemOpen(true)}
        >
          Dodaj nowy przedmiot
        </Button>
      </Collapse>
      <Collapse in={newItemOpen} mountOnEnter unmountOnExit>
        <AddNewItem
          handleClose={() => setNewItemOpen(false)}
          handleSuccess={() => handleSuccess()}
        />
      </Collapse>
    </Box>
  );
};

export default UserItems;
