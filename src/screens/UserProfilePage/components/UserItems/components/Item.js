import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../../../redux/modules/snackbar";
import deleteProduct from "../../../../../services/products/deleteProduct";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: "700px",
    display: "flex",
    alignItems: "center",
    "&:not(:last-of-type)": {
      marginBottom: "20px",
    },
    padding: "5px 20px",
  },
  nameRoot: {
    flexGrow: "1",
    display: "flex",
  },
  name: {
    fontWeight: "bold",
    color: theme.palette.text.primary,
    marginRight: "5px",
  },
  make: {
    color: theme.palette.text.secondary,
  },
  button: {
    marginRight: "10px",
  },
}));

const Item = ({ item, handleEdit, handleSuccess }) => {
  const classes = useStyles();
  const [dialogOpen, setDialogOpen] = useState(false);
  const dispatch = useDispatch();

  const handleDelete = () => {
    setDialogOpen(false);
    deleteProduct(item.idProduct)
      .then(() => {
        handleSuccess();
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message: "Pomyślnie usunięto przedmiot",
          })
        );
      })
      .catch(() => {
        dispatch(snackbarAction.openSnackbar({ error: true }));
      });
  };

  return (
    <React.Fragment>
      <Card elevation={2} className={classes.root}>
        <Box className={classes.nameRoot}>
          <Typography className={classes.name}>{item.name}</Typography>
          <Typography className={classes.make}>
            {item.make} {item.model}
          </Typography>
        </Box>
        <IconButton
          color="primary"
          className={classes.button}
          onClick={() => handleEdit()}
        >
          <EditIcon />
        </IconButton>
        <IconButton color="secondary" onClick={() => setDialogOpen(true)}>
          <DeleteIcon />
        </IconButton>
      </Card>
      <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
        <DialogTitle>Czy na pewno chcesz usunąć przedmiot?</DialogTitle>
        <DialogActions>
          <Button
            className={classes.button}
            onClick={() => setDialogOpen(false)}
            color="primary"
          >
            Anuluj
          </Button>
          <Button onClick={() => handleDelete()} color="secondary">
            Usuń
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default Item;
