import React, { useState, useEffect } from "react";

import getUserShares from "../../../services/hires/getUserShares";
import ShareRow from "./HireList/components/ShareRow";

const UserShares = () => {
  const [shares, setShares] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    getUserShares().then((borrows) => setShares(borrows));
  };

  return (
    <React.Fragment>
      {shares.map((borrow) => (
        <ShareRow
          key={borrow.id}
          hire={borrow}
        />
      ))}
    </React.Fragment>
  );
};

export default UserShares;
