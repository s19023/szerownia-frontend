import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import PremiumIcon from "@material-ui/icons/AttachMoney";
import NoPremiumIcon from "@material-ui/icons/MoneyOff";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../../redux/modules/snackbar";
import { makeStyles } from "@material-ui/core/styles";
import updatePremium from "../../../services/user/updatePremium";
import moment from "moment";
import clsx from "clsx";
import extendPremium from "../../../services/user/extendPremium";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    height: "30px",
    [theme.breakpoints.down("md")]: {
      marginBottom: "20px",
    },
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
      height: "unset",
    },
  },
  icon: {
    marginRight: "5px",
  },
  status: {
    color: theme.palette.text.primary,
  },
  margin: {
    marginRight: "10px",
  },
  success: {
    color: theme.palette.success.main,
  },
  error: {
    color: theme.palette.error.main,
  },
  span: {
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
  price: {
    fontSize: "48px",
    lineHeight: "48px",
    marginBottom: "20px",
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
}));

const paymentError =
  "Wystąpił błąd w trakcie przetwarzania płatności. Spróbuj ponownie później.";

const renderPerkList = () => (
  <ul>
    <li>możliwości promowania ogłoszenia</li>
    <li>dodatkowego ubezpieczenia wypożyczanych przedmiotów</li>
  </ul>
);

const PremiumStatus = ({ isPremium, premiumFrom, premiumTo, onSuccess }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [isOpen, setOpen] = useState(false);
  const [action, setAction] = useState("");
  const [disabled, setDisabled] = useState(false);

  const handleClick = (timeout) => {
    setDisabled(true);
    setTimeout(
      () => {
        handleAction[action]();
      },
      timeout ? 1500 : 0
    );
  };

  const handleDialogOpen = (action) => {
    setAction(action);
    setOpen(true);
  };

  const buyPremium = () => {
    updatePremium()
      .then(() => {
        setOpen(false);
        onSuccess();
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message:
              action === "buy"
                ? "Zakupiłeś konto premium. Super, witaj w gronie najlepszych użytkowników! 😉"
                : "Anulowałeś konto premium. Szkoda, może innym razem 😥",
          })
        );
      })
      .catch(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message:
              action === "buy"
                ? paymentError
                : "Wystąpił błąd podczas anulowania statusu premium. Spróbuj ponownie później.",
          })
        );
      });
  };

  const handleExtendPremium = () => {
    extendPremium()
      .then(() => {
        setOpen(false);
        onSuccess();
        dispatch(
          snackbarAction.openSnackbar({
            error: false,
            message:
              "Przedłużyłeś konto premium. Super, możesz cieszyć się korzyściami jeszcze dłużej! 😉",
          })
        );
      })
      .catch(() => {
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message:
              "Wystąpił błąd w trakcie przetwarzania płatności. Spróbuj ponownie później.",
          })
        );
      });
  };

  const dialogTitle = {
    buy: "Czy chcesz zakupić konto Premium?",
    cancel: "Czy chcesz anulować konto Premium?",
    extend: "Czy chcesz przedłużyć ważność konta?",
  };

  const getDialogContent = () => {
    switch (action) {
      case "buy":
        return (
          <React.Fragment>
            <Typography className={classes.price}>15zł/mies.</Typography>
            <Typography>Za tyle zyskasz dostęp do:</Typography>
            {renderPerkList()}
            <Typography>
              Nie zwlekaj i kup konto Premium w serwisie Szerownia{" "}
              <span className={classes.span}>już dziś!</span> 😉
            </Typography>
          </React.Fragment>
        );
      case "cancel":
        return (
          <React.Fragment>
            <Typography>Anulując konto Premium stracisz dostęp do:</Typography>
            {renderPerkList()}
            <Typography>Czy na pewno chcesz to zrobić?</Typography>
          </React.Fragment>
        );
      default:
        return (
          <React.Fragment>
            <Typography>
              Cieszymy się, że chcesz mieć konto Premium jeszcze dłużej!
            </Typography>
            <Typography>
              Twoje konto Premium będzie ważne do{" "}
              <span className={classes.span}>
                {moment(premiumTo).add(1, "months").format("DD.MM.YYYY")}
              </span>
              .
            </Typography>
          </React.Fragment>
        );
    }
  };

  const handleAction = {
    buy: buyPremium,
    cancel: buyPremium,
    extend: handleExtendPremium,
  };

  return (
    <React.Fragment>
      {isPremium ? (
        <Box className={classes.root}>
          <PremiumIcon className={clsx(classes.icon, classes.success)} />
          <Typography className={clsx(classes.margin, classes.status)}>
            Konto Premium aktywne od{" "}
            <span className={classes.span}>
              {moment(premiumFrom).format("DD.MM.YYYY")}
            </span>{" "}
            do{" "}
            <span className={classes.span}>
              {moment(premiumTo).format("DD.MM.YYYY")}
            </span>
          </Typography>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={() => handleDialogOpen("extend")}
            className={classes.margin}
          >
            Przedłuż Premium
          </Button>
          <Button
            variant="contained"
            color="secondary"
            size="small"
            onClick={() => handleDialogOpen("cancel")}
          >
            Anuluj Premium
          </Button>
        </Box>
      ) : (
        <Box className={classes.root}>
          <NoPremiumIcon className={clsx(classes.icon, classes.error)} />
          <Typography className={classes.margin}>
            Konto Premium jest <span className={classes.span}>nieaktywne</span>
          </Typography>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={() => handleDialogOpen("buy")}
          >
            Zakup Premium
          </Button>
        </Box>
      )}
      <Dialog open={isOpen} onClose={() => setOpen(false)}>
        <DialogTitle>{dialogTitle[action]}</DialogTitle>
        <DialogContent>{getDialogContent()}</DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={() => setOpen(false)}>
            Anuluj
          </Button>
          <Button
            color="primary"
            onClick={() => handleClick(action !== "cancel")}
            disabled={disabled}
            startIcon={disabled && <CircularProgress size={16} />}
          >
            Zatwierdź
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default PremiumStatus;
