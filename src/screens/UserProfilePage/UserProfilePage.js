import React, { useEffect, useState } from "react";
import { Route, useHistory } from "react-router-dom";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import Avatar from "@material-ui/core/Avatar";
import Fade from "@material-ui/core/Fade";
import Typography from "@material-ui/core/Typography";
import Loader from "../../components/Loader/Loader";
import Button from "@material-ui/core/Button";
import ProfilePictureDialog from "./components/ProfilePictureDialog";
import EditUserData from "./components/EditUserData";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";

import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";
import EditIcon from "@material-ui/icons/Edit";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useDispatch } from "react-redux";
import { userAction } from "../../redux/modules/user";

import { images } from "../../config/urls";
import getMyDetails from "../../services/user/getMyDetails";
import PremiumStatus from "./components/PremiumStatus";
import UserRating from "../../components/UserRating/UserRating";
import UserItems from "./components/UserItems/UserItems";
import HireList from "./components/HireList/HireList";
import Complaints from "./components/Complaints";
import UserAdList from "./components/UserAdList";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100%",
  },
  upperRoot: {
    display: "flex",
    alignItems: "center",
    marginBottom: "20px",
    height: "260px",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
      height: "unset",
    },
  },
  avatarRoot: {
    background: theme.palette.background.default,
    boxShadow: theme.shadows[5],
    overflow: "hidden",
    position: "relative",
    borderRadius: "50%",
    marginRight: "20px",
    cursor: "pointer",
    [theme.breakpoints.down("md")]: {
      marginRight: "unset",
      marginBottom: "20px",
    },
  },
  avatar: {
    width: "200px",
    height: "200px",
    [theme.breakpoints.down("sm")]: {
      width: "100px",
      height: "100px",
    },
  },
  overlay: {
    position: "absolute",
    backgroundColor: theme.palette.grey[900] + "AA",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
  },
  icon: {
    color: "#FFFFFF",
    fontSize: "48px",
  },
  detailsRoot: {
    flexGrow: "1",
    [theme.breakpoints.down("md")]: {
      marginBottom: "10px",
    },
  },
  name: {
    color: theme.palette.text.primary,
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
    fontSize: "40px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "24px",
      textAlign: "center",
    },
  },
  button: {
    marginBottom: "10px",
  },
  tabs: {
    marginBottom: "20px",
  },
  tab: {
    color: theme.palette.text.primary,
  },
}));

const pages = [
  {
    link: "/my-profile/ads",
    name: "Ogłoszenia użyczenia",
  },
  {
    link: "/my-profile/search-ads",
    name: "Ogłoszenia poszukiwania",
  },
  {
    link: "/my-profile/shares",
    name: "Twoje użyczenia",
  },
  {
    link: "/my-profile/borrows",
    name: "Twoje wypożyczenia",
  },
  {
    link: "/my-profile/items",
    name: "Twoje przedmioty",
  },
  {
    link: "/my-profile/complaints",
    name: "Twoje reklamacje",
  }
];

const UserProfilePage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [userDetails, setDetails] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [showOverlay, setShowOverlay] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [editUserData, setEditUserData] = useState(false);
  const [value, setValue] = useState(0);

  useEffect(() => {
    updateData();
  }, []);

  const updateData = () => {
    setDialogOpen(false);
    setIsLoading(true);
    getMyDetails()
      .then((details) => {
        setDetails(details);
        setIsLoading(false);
        dispatch(userAction.setPremuim({ isPremium: details.premium }));
      })
      .catch(() => history.push("/"));
  };

  useEffect(() => {
    const currentUrl = history.location.pathname;
    const pageIndex = pages.findIndex((elem) => elem.link === currentUrl);

    if (pageIndex !== -1) {
      setValue(pageIndex);
    } else {
      setValue(0);
      history.push(pages[0].link);
    }
  }, [history, history.location.pathname]);

  const handleLogout = () => {
    dispatch(userAction.logout());
  };

  const handleClose = (data) => {
    setEditUserData(false);
    if (data) {
      const tmpUserData = { ...userDetails };
      Object.keys(data).forEach((key) => {
        tmpUserData[key] = data[key];
      });
      setDetails(tmpUserData);
    }
  };

  const {
    firstName,
    lastName,
    averageRate2Sharer,
    averageRateBorrower,
    userImageId,
    premium,
    premiumFrom,
    premiumTo,
  } = userDetails;

  if (isLoading) {
    return <Loader isLoading />;
  }

  return (
    <Box className={classes.root}>
      <ScrollToTop />
      <Box className={classes.upperRoot}>
        <ProfilePictureDialog
          onClose={() => setDialogOpen(false)}
          handleSuccess={() => updateData()}
          open={dialogOpen}
        />
        <Box
          className={classes.avatarRoot}
          onMouseEnter={() => setShowOverlay(true)}
          onMouseLeave={() => setShowOverlay(false)}
          onClick={() => setDialogOpen(true)}
        >
          <Avatar
            className={classes.avatar}
            src={`${images.get}/${userImageId}`}
            alt={`${firstName} ${lastName}`}
          />
          <Fade in={showOverlay}>
            <Box className={classes.overlay}>
              <PhotoCameraIcon className={classes.icon} />
            </Box>
          </Fade>
        </Box>
        {editUserData ? (
          <EditUserData values={userDetails} close={handleClose} />
        ) : (
          <Box className={classes.detailsRoot}>
            <Typography
              className={classes.name}
            >{`${firstName} ${lastName}`}</Typography>
            <PremiumStatus
              isPremium={premium}
              premiumFrom={premiumFrom}
              premiumTo={premiumTo}
              onSuccess={() => updateData()}
            />
            <UserRating
              rateSharer={averageRate2Sharer}
              rateBorrower={averageRateBorrower}
            />
          </Box>
        )}
        {!editUserData && (
          <React.Fragment>
            <Box display="flex" flexDirection="column">
              <Button
                variant="contained"
                color="primary"
                startIcon={<EditIcon />}
                onClick={() => setEditUserData(true)}
                className={classes.button}
              >
                Edytuj dane
              </Button>
              <Button
                variant="contained"
                onClick={() => handleLogout()}
                startIcon={<ExitToAppIcon />}
              >
                Wyloguj się
              </Button>
            </Box>
          </React.Fragment>
        )}
      </Box>
      <Tabs
        scrollButtons="auto"
        variant="scrollable"
        orientation={isMobile ? "vertical" : "horizontal"}
        value={value}
        className={classes.tabs}
        onChange={(e, v) => {
          history.push(pages[v].link);
          setValue(v);
        }}
      >
        {pages.map((page, index) => (
          <Tab
            className={classes.tab}
            key={`tab-${index}`}
            label={page.name}
            value={index}
          />
        ))}
      </Tabs>
      <Route path="/my-profile/ads">
        <UserAdList />
      </Route>
      <Route path="/my-profile/search-ads">
        <UserAdList isSearchAd />
      </Route>
      <Route path="/my-profile/borrows">
        <HireList borrows isPremium={premium} />
      </Route>
      <Route path="/my-profile/shares">
        <HireList isPremium={premium} />
      </Route>
      <Route path="/my-profile/items">
        <UserItems />
      </Route>
      <Route path="/my-profile/complaints">
        <Complaints />
      </Route>
    </Box>
  );
};

export default UserProfilePage;
