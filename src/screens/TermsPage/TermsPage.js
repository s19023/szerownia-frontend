import React from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import ScrollToTop from "../../components/ScrollToTop/ScrollToTop";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "40px",
    maxWidth: "1000px",
  },
  topHeader: {
    fontSize: "24px",
    fontWeight: "bold",
    textAlign: "center",
  },
  header: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "20px",
    margin: "20px 0",
    color: theme.palette.primary.main,
  },
  content: {
    lineHeight: "19px",
    color: theme.palette.text.primary,
    textAlign: "justify",
    marginBottom: "10px",
  },
}));

const TermsPage = () => {
  const classes = useStyles();
  return (
    <Card elevation={5} className={classes.root}>
      <ScrollToTop />
      <Typography component="h1" className={classes.topHeader}>
        Regulamin serwisu Szerownia
      </Typography>
      <ol>
        <Typography component="h2" className={classes.header}>
          <li>Wstęp</li>
        </Typography>
        <Typography className={classes.content}>
          Serwis umożliwiający wypożyczanie przedmiotów Szerownia prowadzący
          działalność pod adresem internetowym{" "}
          <Link href="http://szerownia.com">http://szerownia.com</Link>{" "}
          prowadzony jest przez Szerownia sp. z o.o., ul. Lorem Ipsum, 11-111
          Warszawa, NIP 000-000-00-00, Regon 000000000
        </Typography>
        <Typography component="h2" className={classes.header}>
          <li>Użyte w regulaminie pojęcia oznaczają:</li>
        </Typography>
        <ul>
          <li className={classes.content}>
            <b>Klient</b> – osoba fizyczna, osoba prawna lub jednostka
            organizacyjna nie będącą osobą prawną, której przepisy szczególne
            przyznają zdolność prawną, która dokonuje Zamówienia w ramach
            Sklepu.
          </li>
          <li className={classes.content}>
            <b>Kodeks Cywilny</b> – ustawa z dnia 23 kwietnia 1964 r. (Dz.U.
            1964 Nr 16, poz. 93 ze zm.).
          </li>
          <li className={classes.content}>
            <b>Regulamin</b> – niniejszy Regulamin świadczenia usług drogą
            elektroniczną w ramach sklepu internetowego{" "}
            <Link href="http://szerownia.com">http://szerownia.com</Link>.
          </li>
          <li className={classes.content}>
            <b>Sklep internetowy Szerownia</b> – serwis internetowy dostępny pod{" "}
            <Link href="http://szerownia.com">http://szerownia.com</Link>, za
            pośrednictwem którego Klient może w szczególności składać
            Zamówienia;
          </li>
          <li className={classes.content}>
            <b>Towar</b> – produkty prezentowane w Sklepie Internetowym.
          </li>
          <li className={classes.content}>
            <b>Umowa sprzedaży</b> – umowa sprzedaży Towarów w rozumieniu
            Kodeksu Cywilnego, zawarta pomiędzy Szerownia sp. z o.o. a Klientem,
            zawierana z wykorzystaniem serwisu internetowego Sklepu.
          </li>
          <li className={classes.content}>
            <b>Ustawa o prawach konsumenta</b> - ustawa z dnia 30 maja 2014 r. O
            prawach konsumenta (Dz. U. 2014, poz 827 ze zm.).
          </li>
          <li className={classes.content}>
            <b>Zamówienie</b> – oświadczenie woli Klienta, zmierzające
            bezpośrednio do zawarcia Umowy sprzedaży, określające w
            szczególności rodzaj i liczbę Towaru.
          </li>
        </ul>
        <Typography component="h2" className={classes.header}>
          <li>Postanowienia ogólne</li>
        </Typography>
        <Typography className={classes.content}>
          Niniejszy Regulamin określa zasady korzystania ze sklepu internetowego
          dostępnego pod{" "}
          <Link href="http://szerownia.com">http://szerownia.com</Link>. Sklep
          internetowy, działający pod{" "}
          <Link href="http://szerownia.com">http://szerownia.com</Link>,
          prowadzony jest przez Szerownia sp. z o.o., ul. Lorem Ipsum, 11-111
          Warszawa, NIP: 000-000-00-00, REGON: 000000000
        </Typography>
        <Typography className={classes.content}>
          Niniejszy Regulamin określa w szczególności:
        </Typography>
        <ol>
          <li className={classes.content}>
            Zasady dokonywania rejestracji i korzystania z konta w ramach sklepu
            internetowego;
          </li>
          <li className={classes.content}>
            Warunki i zasady dokonywania elektronicznej rezerwacji produktów
            dostępnych w ramach sklepu internetowego;
          </li>
          <li className={classes.content}>
            Warunki i zasady składania drogą elektroniczną Zamówień w ramach
            sklepu internetowego;
          </li>
          <li className={classes.content}>
            Zasady zawierania Umów sprzedaży z wykorzystaniem usług świadczonych
            w ramach Sklepu Internetowego.
          </li>
        </ol>
        <Typography component="h2" className={classes.header}>
          <li>Zasady korzystania z serwisu</li>
        </Typography>
        <ul>
          <li className={classes.content}>
            Dokonanie zakupu Produktu nie wymaga rejestracji w Sklepie.
          </li>
          <li className={classes.content}>
            Informacje podane na stronach internetowych Sklepu, w tym informacje
            o prezentowanych produktach, a w szczególności ich opisy, parametry
            techniczne i użytkowe oraz ceny, stanowią zaproszenie do zawarcia
            umowy, w rozumieniu art. 71 Kodeksu Cywilnego.
          </li>
          <li className={classes.content}>
            Składanie zamówienia odbywa się za pomocą formularza, dostępnego na
            stronie sklepu.
          </li>
          <li className={classes.content}>
            Każdy rejestrujący się i/lub dokonujący zamówienia Klient wyraża
            zgodę na otrzymywanie na podany przez siebie adres e-mail informacji
            związanych z przebiegiem transakcji, zawiadomień o zmianach w
            niniejszym Regulaminie.
          </li>
          <li className={classes.content}>
            W celu zapewnienia bezpieczeństwa przekazu komunikatów i danych w
            związku ze świadczonymi w ramach Witryny usługami, Sklep internetowy
            podejmuje środki techniczne i organizacyjne odpowiednie do stopnia
            zagrożenia bezpieczeństwa świadczonych usług, w szczególności środki
            służące zapobieganiu pozyskiwania i modyfikacji przez osoby
            nieuprawnione danych osobowych przesyłanych w Internecie.
          </li>
        </ul>
        <Typography component="h2" className={classes.header}>
          <li>Procedura zawarcia Umowy sprzedaży</li>
        </Typography>
        <Typography className={classes.content}>
          Celem przeprowadzenia jakiejkolwiek aktywności Klienta w Sklepie
          wymagane jest poprawne działanie sieci Internet, używanie przeglądarki
          internetowej, umożliwiającej przeglądanie stron z wykorzystaniem
          JavaScript, w tym także w ramach urządzeń mobilnych oraz posiadanie
          aktywnego konta na wybranym adresie poczty e-mail.
        </Typography>
        <Typography className={classes.content}>
          W celu zawarcia Umowy sprzedaży za pośrednictwem Sklepu internetowego
          należy wejść na stronę internetową{" "}
          <Link href="http://szerownia.com">http://szerownia.com</Link>, dokonać
          wyboru, podejmując kolejne czynności techniczne w oparciu o
          wyświetlane Klientowi komunikaty oraz informacje dostępne na stronie.
        </Typography>
        <Typography className={classes.content}>
          Sklep umożliwia nabywanie produktów i usług przez 24 godziny na dobę,
          7 dni w tygodniu.
        </Typography>
        <Typography className={classes.content}>
          Wybór zamawianych Towarów przez Klienta jest dokonywany poprzez ich
          dodanie do koszyka.
        </Typography>
        <Typography className={classes.content}>
          W trakcie składania Zamówienia – do momentu naciśnięcia przycisku
          „Złóż zamówienie” – Klient ma możliwość modyfikacji wprowadzonych
          danych oraz w zakresie wyboru Towaru. W tym celu należy kierować się
          wyświetlanymi Klientowi komunikatami oraz informacjami dostępnymi na
          stronie.
        </Typography>
        <Typography className={classes.content}>
          Aby sfinalizować zamówienie Klient jest proszony o podanie wszystkich
          niezbędnych danych, potrzebnych do zrealizowania zamówienia tj.
        </Typography>
        <ol>
          <li className={classes.content}>
            imię i nazwisko / Szerownia sp. z o.o.
          </li>
          <li className={classes.content}>adres dostawy</li>
          <li className={classes.content}>e-mail</li>
          <li className={classes.content}>telefon kontaktowy</li>
          <li className={classes.content}>
            NIP – w przypadku prośby o fakturę VAT.
          </li>
        </ol>
        <Typography className={classes.content}>
          Po podaniu przez Klienta korzystającego ze Sklepu internetowego
          wszystkich niezbędnych danych, wyświetlone zostanie podsumowanie
          złożonego Zamówienia. Podsumowanie złożonego Zamówienia będzie
          zawierać informacje dotyczące:
        </Typography>
        <ol>
          <li className={classes.content}>przedmiotu zamówienia,</li>
          <li className={classes.content}>
            jednostkowej oraz łącznej ceny zamawianych produktów lub usług, w
            tym kosztów dostawy oraz dodatkowych kosztów (jeśli występują),
          </li>
          <li className={classes.content}>wybranej metody płatności,</li>
          <li className={classes.content}>wybranego sposobu dostawy.</li>
        </ol>
        <Typography className={classes.content}>
          W celu wysłania Zamówienia konieczne jest dokonanie akceptacji treści
          Regulaminu, podanie danych osobowych oznaczonych jako obowiązkowe oraz
          naciśnięcie przycisku „Złóż zamówienie”.
        </Typography>
        <Typography className={classes.content}>
          Wysłanie przez Klienta Zamówienia stanowi oświadczenie woli zawarcia z
          Szerownia sp. z o.o. Umowy sprzedaży, zgodnie z treścią Regulaminu.
        </Typography>
        <Typography className={classes.content}>
          Po złożeniu i opłaceniu Zamówienia, Klient otrzymuje wiadomość e-mail
          zatytułowaną “Potwierdzenie zamówienia”, zawierającą ostateczne
          potwierdzenie wszystkich istotnych elementów Zamówienia.
        </Typography>
        <Typography className={classes.content}>
          Umowę traktuje się za zawartą z momentem otrzymania przez Klienta
          wiadomości e-mail, o której mowa powyżej.
        </Typography>
        <Typography className={classes.content}>
          Umowa sprzedaży zawierana jest w języku polskim, o treści zgodnej z
          Regulaminem.
        </Typography>
        <Typography component="h2" className={classes.header}>
          <li>Procedura dostawy produktów</li>
        </Typography>
        <Typography className={classes.content}>
          Ceny produktów w Sklepie podawane są w polskich złotych, zawierają
          podatek bez kosztów przesyłki. Koszty przesyłki, doliczane są do sumy
          zamawianych produktów i ponosi je Klient, chyba że spełnia warunki
          uprawniające do bezpłatnej dostawy, tj. wartość zamówienia musi być
          wyższa niż XXX zł brutto. Ceną ostateczną, wiążącą Właściciela i
          Klienta jest cena produktu podana w ofercie w chwili składania
          zamówienia przez Klienta.
        </Typography>
        <Typography className={classes.content}>
          Rodzaje i ceny dostaw:
        </Typography>
        <ol>
          <li className={classes.content}>
            InPost: XX złotych / czas dostawy 1-2 dni robocze
          </li>
          <li className={classes.content}>
            DHL: XX złotych / czas dostawy 1-2 dni robocze
          </li>
          <li className={classes.content}>
            odbiór osobisty po porozumieniu z użyczającym
          </li>
        </ol>
        <Typography className={classes.content}>
          Informacja na temat całkowitej wartości zamówienia (wraz z kosztami
          przesyłki, które szczegółowo wyszczególnione są w zakładce Dostawa i
          zwroty) udostępniana jest w Podsumowaniu Zamówienia na stronie
          sprzedażowej produktu oraz w potwierdzeniu zamówienia.
        </Typography>
        <Typography className={classes.content}>
          Dostawa Towarów odbywa się zarówno na obszarze Rzeczpospolitej
          Polskiej jak i Państw Europy oraz wybranych krajów położonych na
          pozostałych kontynentach pod adres wskazany przez Klienta w trakcie
          składania Zamówienia.
        </Typography>
        <Typography className={classes.content}>
          Dostawa zamówionych Towarów odbywa się przesyłką kurierską.
        </Typography>
        <Typography className={classes.content}>
          Termin realizacji dostawy dla zamówień z dostawą na terytorium
          Rzeczypospolitej Polskiej wynosi trzy dni robocze liczone od dnia
          opłacenia przez Klienta Zamówienia. Dla zamówień z dostawą poza
          granice Rzeczypospolitej termin dostawy podany jest przy informacji
          dotyczącej formy dostawy oraz kraju destynacji.
        </Typography>
        <Typography className={classes.content}>
          Klientowi wraz z przesyłką zostanie przekazany wydruku potwierdzenia i
          specyfikacji Zamówienia oraz dowód sprzedaży. Na wyraźne życzenie
          Klienta może być wystawiona faktura VAT.
        </Typography>
        <Typography component="h2" className={classes.header}>
          <li>Ceny towarów i sposoby płatności</li>
        </Typography>
        <Typography className={classes.content}>
          Ceny Towarów podawane są w złotych polskich i zawierają wszystkie
          składniki, w tym podatek VAT. Klient ma możliwość uiszczenia ceny:
        </Typography>
        <ol>
          <li className={classes.content}>
            przelewem na numer konta bankowego: 00 0000 0000 0000 0000;
          </li>
          <li className={classes.content}>
            płatnością w systemie PayU, PayPal;
          </li>
          <li className={classes.content}>
            zapłata kartą płatniczą: Visa, MasterCard.
          </li>
        </ol>
        Operatorem kart płatniczych jest Definicja od PayU.
        <Typography component="h2" className={classes.header}>
          <li>Prawo odstąpienia od umowy</li>
        </Typography>
        <Typography className={classes.content}>
          Klientowi, będącemu konsumentem w rozumieniu art. 22 Kodeksu
          Cywilnego, przysługuje – na podstawie przepisów prawa – prawo do
          odstąpienia od umowy zawartej na odległość, bez podania przyczyny,
          składając stosowne oświadczenie na piśmie lub mailem na adres{" "}
          <Link href="mailto:kontakt@szerownia.com">kontakt@szerownia.com</Link>
          , w terminie 14 dni i wysyłając je na adres Szerownia sp. z o.o.
          podany w niniejszym Regulaminie.
        </Typography>
        <Typography className={classes.content}>
          W przypadku odstąpienia od umowy zawartej na odległość, umowa jest
          uważana za niezawartą. To, co strony świadczyły, ulega zwrotowi w
          stanie niezmienionym. Zwrot powinien nastąpić niezwłocznie, nie
          później niż w terminie 14 dni. Zakupiony towar należy zwrócić na
          poniższy adres: Szerownia sp. z o.o. ul. NAZWA ULICY, 00-000 MIASTO.
        </Typography>
        <Typography className={classes.content}>
          Zwracany przez Klienta Towar powinien zostać opakowany w odpowiedni
          sposób, zapewniający brak uszkodzeń przesyłki w trakcie transportu.
        </Typography>
        <Typography className={classes.content}>
          W przypadku odstąpienia od niniejszej umowy sklep zwróci Klientowi
          wszystkie otrzymane od niego płatności, w tym koszty dostarczenia
          rzeczy (z wyjątkiem dodatkowych kosztów wynikających z wybranego przez
          Klienta sposobu dostarczenia innego niż najtańszy sposób dostarczenia
          oferowany przez Sklep), niezwłocznie, nie później niż 14 dni od dnia,
          w którym został zwrócony towar. Zwrotu płatności zostanie dokonany
          przy użyciu takich samych sposobów płatności, jakie zostały przez
          Klienta użyte w pierwotnej transakcji, chyba że strony uzgodnią
          inaczej.
        </Typography>
        <Typography className={classes.content}>
          Ważnym elementem regulaminu jest część opisująca politykę zwrotów i
          reklamacji w sklepie internetowym.
        </Typography>
        <Typography className={classes.content}>
          W przypadku gdy zakupiony produkt jest niezgodny z umową lub posiada
          wady fizyczne, Klient ma prawo złożyć reklamacje w oparciu o:
        </Typography>
        <ol>
          <li className={classes.content}>
            regulacje przewidziane przepisami ustawy z dnia 27 lipca 2002 r. o
            szczególnych warunkach sprzedaży konsumenckiej oraz o zmianie
            Kodeksu cywilnego (Dz. U. Nr 141, poz. 1176 z późn. zm.), w
            przypadku gdy produkt jest niezgodny z umową,
          </li>
          <li className={classes.content}>
            regulacje przewidziane przepisami kodeksu cywilnego w zakresie
            rękojmi za wady.
          </li>
        </ol>
        <Typography className={classes.content}>
          Celem złożenia reklamacji Klient zobowiązany jest do poinformowania
          sklepu mailem na adres szerownia@gmail.com, a następnie procedować
          zgodnie z otrzymanymi informacjami.
        </Typography>
        <Typography className={classes.content}>
          Określając żądania w oparciu o tryb przewidziany art. 1 ust a) Klient
          ma prawo wnioskować o doprowadzenie produktu do stanu zgodnego z umową
          poprzez nieodpłatną naprawę albo wymianę, chyba, że naprawa albo
          wymiana są niemożliwe lub wymagają nadmiernych kosztów. Jeżeli Sklep
          nie będzie w stanie wywiązać się z określonych żądań, Klient ma prawo
          domagać się stosowanego obniżenia ceny albo może odstąpić od umowy.
        </Typography>
        <Typography className={classes.content}>
          Określając żądania w oparciu o tryb przewidziany art. 1 ust b) Klient
          ma prawo od umowy odstąpić albo żądać obniżenia ceny. Klient nie może
          od umowy odstąpić, jeżeli Sklep niezwłocznie, najpóźniej w terminie 14
          dni wymieni rzecz wadliwą na rzecz wolną od wad albo niezwłocznie wady
          usunie. Ograniczenie to nie ma zastosowania, jeżeli rzecz była już
          wymieniona przez Sklep lub naprawiana, chyba, że wady są nieistotne.
        </Typography>
        <Typography className={classes.content}>
          Sklep odpowiada za niezgodność produktu konsumpcyjnego z umową (art 1
          ust a), jedynie w przypadkach i na warunkach określonych prawem.
        </Typography>
        <Typography className={classes.content}>
          Sklep odpowiada za wady produktu na podstawie rękojmi (art 1 ust b), w
          przypadkach i na warunkach określonych prawem.
        </Typography>
        <Typography className={classes.content}>
          Sklep rozpatruje reklamacje w terminie 14 dni od daty otrzymania przez
          Sklep zgłoszenia reklamacyjnego wraz z kompletnym, reklamowanym
          produktem.
        </Typography>
        <Typography className={classes.content}>
          Sklep zastrzega sobie prawo do odmowy uznania reklamacji w przypadku
          dokonywania jakichkolwiek modyfikacji produktu przez Klienta.
        </Typography>
        <Typography className={classes.content}>
          Sklep zastrzega sobie prawo do odmowy uznania reklamacji w
          przypadkach: wyłącznej winy użytkownika, uszkodzenia mechanicznego,
          niewłaściwego konserwowania i użytkowania produktu niezgodnego z
          instrukcją.
        </Typography>
        <Typography className={classes.content}>
          Koszty przesyłki reklamowanego produktu w przypadku uznania reklamacji
          zostaną zwrócone Klientowi w terminie 14 dni od daty uznania
          reklamacji.
        </Typography>
        <Typography component="h2" className={classes.header}>
          <li>Polityka prywatności</li>
        </Typography>
        <Typography className={classes.content}>
          Warunkiem sfinalizowania zakupów w Sklepie jest podanie wszelkich
          wymaganych treścią formularza danych Klienta oraz zaznaczenie
          odpowiedniego pola (checkbox) stanowiącego wyrażenie zgody Klienta na
          przetwarzanie jego danych osobowych przez Właściciela oraz pola
          (checkbox) dotyczącego potwierdzenia zaznajomienia się i akceptacji
          niniejszego Regulaminu.
        </Typography>
        <Typography className={classes.content}>
          Wszelkie dane Klienta podawane podczas rejestracji w Sklepie będą
          wykorzystywane tylko i wyłącznie w celu realizacji, zmiany lub
          rozwiązania Umowy bądź innych działań marketingowych, w szczególności
          informowania o nowych produktach i usługach Sklepu, o ile Klient
          wyrazi zgodę poprzez zaznaczenie odpowiedniego pola (checkbox) na
          przesyłanie informacji handlowej, za pomocą środków komunikacji
          elektronicznej w rozumieniu nowego rozporządzenia Parlamentu
          Europejskiego i Rady (UE) 2016/679 z 27.04.2016r.
        </Typography>
        <Typography className={classes.content}>
          Wszelkie dane osobowe Klienta będą przetwarzane przez Właściciela
          zgodnie z treścią ustawy, o której mowa powyżej. Administratorem
          danych osobowych Klienta jest Szerownia sp. z o.o. ul. Lorem Ipsum 11,
          11-111 Warszawa, NIP 000-000-00-00, Regon 000000000
        </Typography>
        <Typography className={classes.content}>
          Gromadzone są jedynie dane osobowe podane przez Klienta dobrowolnie.
          Dane przetwarzane są w celu niezbędnym do wykonania postanowień
          niniejszego Regulaminu, a w szczególności zawarcia umowy sprzedaży
          zamówionego towaru, doręczenia Klientowi towaru, na który Klient
          złożył zamówienie, wystawienia dokumentu potwierdzającego dokonanie
          transakcji sprzedaży towaru, przechowania danych osobowych w
          informatycznym systemie księgowym dla zapewnienia historii transakcji
          handlowych dokonywanych poprzez Sklep.
        </Typography>
        <Typography className={classes.content}>
          Dane podane w trakcie składania zamówienia są przetwarzane również
          przez następujące podmioty w podanym zakresie: imię i nazwisko, adres,
          telefon, e-mail wskazane jako adres dostawy przekazywane są firmom
          przewozowym w postaci etykiety/listu przewozowego będącego
          jednocześnie zleceniem doręczenia przesyłki. W zależności od wybranego
          rodzaju przesyłki przekazywane są:
        </Typography>
        <ol>
          <li className={classes.content}>DHL</li>
          <li className={classes.content}>InPost</li>
        </ol>
        <Typography className={classes.content}>
          Dane przetwarzane są również przez podmioty związane z realizacją
          płatności za zamówienie:
        </Typography>
        <ol>
          <li className={classes.content}>PayU</li>
          <li className={classes.content}>PayPal</li>
        </ol>
        <Typography className={classes.content}>
          W takich przypadkach ilość przekazywanych danych ograniczona jest do
          wymaganego minimum. Ponadto, podane przez Państwa informacje mogą
          zostać udostępnione właściwym organom władzy publicznej, jeżeli
          wymagają tego obowiązujące przepisy prawa.
        </Typography>
        <Typography className={classes.content}>
          Administrator danych nie ponosi odpowiedzialności za podanie
          nieprawdziwych lub niekompletnych danych osobowych przez Klienta.
        </Typography>
        <Typography className={classes.content}>
          Klient w każdym czasie, ma prawo poprzez biuro obsługi Sklepu prawo do
          zmiany, poprawiania lub usunięcia swoich danych osobowych.
        </Typography>
        <Typography className={classes.content}>
          Za dodatkową zgodą udzieloną przez Klienta, zgromadzone dane osobowe
          mogą być również wykorzystywane w celach promocyjnych i
          marketingowych, w tym w szczególności w celu przedstawiania Klientowi
          oferty handlowej sklepu oraz innych informacji marketingowych
          związanych ze Sklepem.
        </Typography>
        <Typography className={classes.content}>
          Każdy użytkownik ma możliwość zapisania się do newslettera –
          informacji handlowej. Zapisanie się na listę następuje poprzez
          zaznaczenie na etapie rejestracji odpowiedniego pola w formularzu (do
          tego celu potrzebny jest tylko adres e-mail). Z prenumeraty
          newslettera można zrezygnować w każdej chwili poprzez link podany w
          biuletynie lub poprzez poinformowanie Sklepu drogą mailową na adres{" "}
          <Link href="mailto:kontakt@szerownia.com">kontakt@szerownia.com</Link>
          . Po odwołaniu, dane adresu e-mail zostaną natychmiast usunięte z bazy
          prenumeraty.
        </Typography>
        <Typography className={classes.content}>
          Sklep używa między innymi tak zwanych „cookies”, które przyczyniają
          się do większej użyteczności, funkcjonalności oraz łatwości obsługi
          naszego sklepu. Instrumenty te ułatwiają Klientom również szybsze
          poruszanie się po stronie Sklepu. Dodatkowo, pliki cookies pozwalają
          zmierzyć częstotliwość odsłon strony oraz najchętniej oglądane
          produkty. Cookies to małe pliki tekstowe, które są przechowywane w
          systemie komputera. Informujemy, że niektóre z tych plików zostają
          przeniesione z serwera Sklepu do systemu komputerowego Klienta.
          Istnieje możliwość zablokowania plików cookies w każdym momencie, o
          ile używana przeglądarka posiada stosowną funkcję. Należy pamiętać, że
          po blokadzie plików cookies możliwość korzystania z niektórych funkcji
          Sklepu zostanie w znacznym stopniu ograniczona lub wyłączona.
        </Typography>
        <Typography component="h2" className={classes.header}>
          <li>Postanowienia końcowe</li>
        </Typography>
        <Typography className={classes.content}>
          Wszelkie treści zawarte w Sklepie podlegają ochronie prawnej i ich
          wykorzystywanie będzie stanowiło naruszenie prawa.
        </Typography>
        <Typography className={classes.content}>
          W sprawach nieuregulowanych w niniejszym Regulaminie mają zastosowanie
          przepisy prawa polskiego, w szczególności Kodeksu Cywilnego oraz
          przepisy Ustawy o szczególnych warunkach sprzedaży konsumenckiej oraz
          zmianie Kodeksu cywilnego z dnia 18 lipca 2002 roku (Dz. U. 2001 Nr
          144 poz. 1204) przepisy ustawy o prawach konsumenta z dnia 30 maja
          2014 r. (Dz.U. 2014 r. poz. 827 ze zm.); oraz inne właściwe przepisy
          powszechnie obowiązującego prawa.
        </Typography>
        <Typography className={classes.content}>
          Aktualna wersja Regulaminu znajduje się pod adresem{" "}
          <Link href="http://szerownia.com/terms">
            http://szerownia.com/terms
          </Link>
        </Typography>
      </ol>
    </Card>
  );
};

export default TermsPage;
