import React from "react";
import { render, waitFor } from "@testing-library/react";
import PriceInfo from "./PriceInfo";
import moment from "moment";

jest.mock("../../../../services/hires/estimateCost");
import estimateCost from "../../../../services/hires/estimateCost";

test("renders", () => {
  render(<PriceInfo />);
});

test("tells user to select a date", () => {
  const { getByText } = render(<PriceInfo />);

  getByText("Wybierz okres wypożyczenia w kalendarzu z lewej strony");
});

test("calls estimate endpoint", async () => {
  const minDate = moment("2021-05-25");
  const maxDate = moment("2021-07-01");
  const rentalAd = 2;
  const pricePerDay = 10;
  const period = 5;
  const cost = 50;
  const commission = 10;
  const depositAmount = 100;

  estimateCost.mockResolvedValueOnce({
    pricePerDay,
    period,
    cost,
    commission,
    depositAmount,
  });

  const { getByText } = render(
    <PriceInfo startDate={minDate} endDate={maxDate} id={rentalAd} />
  );

  await waitFor(() => getByText(`${(cost + commission + depositAmount).toFixed(2)} zł`));
  expect(estimateCost).toBeCalledTimes(1);
  expect(estimateCost).toBeCalledWith(
    rentalAd,
    minDate.format("YYYY-MM-DD"),
    maxDate.format("YYYY-MM-DD")
  );
});
