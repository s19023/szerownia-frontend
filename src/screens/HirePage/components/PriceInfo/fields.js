export const fields = [
  {
    id: "pricePerDay",
    name: "Cena za dzień",
    end: " zł",
    getValue: (price) => (price ? price.toFixed(2) : "0"),
  },
  { id: "period", name: "Ilość dni", getValue: (period) => `x ${period}` },
  {
    id: "usedDiscount",
    name: "Rabat",
    getValue: (discount) => (discount ? `${discount.value}%` : "brak"),
  },
  { id: "depositAmount", name: "Kaucja", end: " zł" },
  {
    id: "commission",
    name: "Prowizja",
    end: " zł",
    getValue: (commission) => (commission ? commission.toFixed(2) : "0"),
  },
];
