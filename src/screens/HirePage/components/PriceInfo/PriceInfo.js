import React, { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";

import estimateCost from "../../../../services/hires/estimateCost";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    width: "250px",
    height: "250px",
    padding: "0 10px",
    boxSizing: "border-box",
    justifyContent: "center",
  },
  disabledText: {
    color: theme.palette.text.disabled,
    textAlign: "center",
  },
  header: {
    marginBottom: "10px",
    fontFamily: "Poppins, sans-serif",
    fontWeight: "bold",
  },
  priceRow: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    "&:not(:last-of-type)": {
      marginBottom: "5px",
    },
  },
  priceName: {
    fontSize: "12px",
  },
  divider: {
    marginBottom: "5px",
  },
}));

const PriceInfo = ({ id, startDate, endDate }) => {
  const classes = useStyles();
  const [cost, setCost] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!startDate || !endDate) {
      return;
    }

    setIsLoading(true);
    estimateCost(
      id,
      startDate.format("YYYY-MM-DD"),
      endDate.format("YYYY-MM-DD")
    )
      .then((cost) => {
        setCost(cost);
        setIsLoading(false);
      })
      .catch(() => {
        setCost({});
        setIsLoading(false);
      });
  }, [startDate, endDate]);

  if (!startDate || !endDate || isLoading) {
    return (
      <Card elevation={2} className={classes.root}>
        <Typography className={classes.disabledText}>
          Wybierz okres wypożyczenia w kalendarzu z lewej strony
        </Typography>
      </Card>
    );
  }

  const { depositAmount, commission } = cost;
  const sum = cost.cost + depositAmount + commission;

  return (
    <Card elevation={2} className={`${classes.root} ${classes.extraPadding}`}>
      <Typography className={classes.header}>Koszt wypożyczenia</Typography>
      {fields.map((field) => (
        <Box key={field.id} className={classes.priceRow}>
          <Typography className={classes.priceName}>{field.name}</Typography>
          <Typography className={classes.price}>
            {field.getValue
              ? field.getValue(cost[field.id])
              : cost[field.id]
              ? cost[field.id]
              : "0"}
            {field.end}
          </Typography>
        </Box>
      ))}
      <Divider className={classes.divider} />
      <Box className={classes.priceRow}>
        <Typography className={classes.priceName}>Razem</Typography>
        <Typography><b>{sum.toFixed(2)} zł</b></Typography>
      </Box>
    </Card>
  );
};

export default PriceInfo;
