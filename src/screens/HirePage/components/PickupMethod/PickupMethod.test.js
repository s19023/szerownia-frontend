import React from "react";
import { render, waitFor } from "@testing-library/react";
import PickupMethod from "./PickupMethod";

jest.mock("../../../../services/rentalAds/getPickupMethods");
import getPickupMethods from "../../../../services/rentalAds/getPickupMethods";

test("renders", () => {
  getPickupMethods.mockResolvedValueOnce([]);
  render(<PickupMethod />);
});

test("renders a specific number of items", async () => {
  getPickupMethods.mockResolvedValueOnce(["PERSONAL_PICKUP", "PARCEL_LOCKER"]);
  const { getByText, getAllByRole } = render(<PickupMethod id={2} />);

  await waitFor(() => getByText("Odbiór osobisty"));
  expect(getAllByRole("label")).toHaveLength(2);
});

test("doesn't call enpoint without ad id", async () => {
  getPickupMethods.mockResolvedValueOnce([]);
  const { getByText } = render(<PickupMethod />);

  await waitFor(() => getByText("Nie podano id ogłoszenia"));
  expect(getPickupMethods).toBeCalledTimes(0);
});
