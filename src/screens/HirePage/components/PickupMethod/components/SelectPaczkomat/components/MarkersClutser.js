import React, { useEffect } from "react";
import L from "leaflet";
import { useMap } from "react-leaflet";
require("leaflet.markercluster");

let mcg = null;

const MarkersCluster = ({ markers, setPaczkomat }) => {
  const map = useMap();

  useEffect(() => {
    mcg = L.markerClusterGroup({
      chunkedLoading: true,
    });
  }, []);

  useEffect(() => {
    mcg.clearLayers();
    const markerList = markers.map((paczkomat) => {
      const pos = new L.LatLng(paczkomat.l.a, paczkomat.l.o);
      const marker = L.marker(pos);
      marker.on("click", () => {
        setPaczkomat(paczkomat);
        map.setView(pos);
      });
      return marker;
    });

    if (markers.length > 0) {
      mcg.addLayers(markerList);
      map.addLayer(mcg);
    }

    return () => map.removeLayer(mcg);
  }, [markers, map]);

  return null;
};

export default MarkersCluster;
