import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { MapContainer, TileLayer, Marker, useMap } from "react-leaflet";
import MarkersCluster from "./components/MarkersClutser";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import getAllPaczkomaty from "../../../../../../services/paczkomaty/getAllPaczkomaty";
import { paczkomaty as paczkomatyUrl } from "../../../../../../config/urls";

import moment from "moment";
import "moment/locale/pl";
moment.locale("pl");

const useStyles = makeStyles((theme) => ({
  root: {
    flexDirection: "row",
    alignItems: "strech",
  },
  map: {
    flexGrow: "1",
    height: "800px",
  },
  detailsRoot: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    width: "350px",
    padding: "10px 20px",
  },
  errorBox: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
  errorText: {
    color: theme.palette.text.disabled,
  },
  image: {
    width: "calc(100% + 40px)",
    height: "250px",
    objectFit: "cover",
    objectPosition: "center",
    margin: "-10px -20px 10px",
  },
  imagePlaceholder: {
    backgroundColor: theme.palette.text.disabled,
  },
  imageErrorText: {
    color: theme.palette.background.main,
  },
  paczkomat: {
    fontSize: "12px",
    color: theme.palette.text.disabled,
  },
  number: {
    fontSize: "32px",
    fontWeight: "bold",
    lineHeight: "32px",
    fontFamily: "Poppins, sans-serif",
  },
  address: {
    fontSize: "20px",
    lineHeight: "24px",
    marginBottom: "5px",
  },
  postalCode: {
    marginBottom: "10px",
  },
  hourRoot: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: "5px",
  },
  hour: {
    fontWeight: "bold",
    fontFamily: "Poppins, sans-serif",
  },
}));

const getStreetName = (streetName) => {
  const hasPrefix = /$\w{2,3}\./gi.test(streetName);
  return hasPrefix ? streetName : "ul. " + streetName;
};

const getTime = (time) => {
  return /\d{2}:\d{2}/.test(time) ? time : `${time}:00`;
};

const SelectPaczkomat = ({ updatePaczkomat }) => {
  const classes = useStyles();
  const [dialogOpen, setDialogOpen] = useState(false);
  const [paczkomaty, setPaczkomaty] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [selectedPaczkomat, setPaczkomat] = useState(null);
  const [confirmedPaczkomat, setConfirmedPaczkomat] = useState(null);
  const [imageError, setImageError] = useState(false);

  useEffect(() => {
    updatePaczkomat(null);
    setPaczkomat(null);
  }, []);

  useEffect(() => {
    if (!dialogOpen || paczkomaty.length !== 0 || isLoading) {
      return;
    }

    setIsLoading(true);
    getAllPaczkomaty().then((paczkomaty) => {
      setPaczkomaty(paczkomaty);
      setIsLoading(false);
    });
  }, [dialogOpen, paczkomaty, isLoading]);

  useEffect(() => {
    setImageError(false);
  }, [selectedPaczkomat]);

  const handleConfirm = () => {
    setConfirmedPaczkomat(selectedPaczkomat);
    updatePaczkomat(selectedPaczkomat);
    setDialogOpen(false);
  };

  return (
    <React.Fragment>
      <Button color="primary" onClick={() => setDialogOpen(true)}>
        Wybierz paczkomat
      </Button>
      {confirmedPaczkomat ? (
        <Typography>
          Wybrano paczkomat:{" "}
          <span className={classes.hour}>{confirmedPaczkomat.n}</span>
        </Typography>
      ) : (
        <Typography>Nie wybrano jeszcze paczkomatu</Typography>
      )}
      <Dialog
        open={dialogOpen}
        onClose={() => setDialogOpen(false)}
        disableBackdropClick
        keepMounted
        maxWidth="lg"
        fullWidth
        classes={{ paper: classes.root }}
      >
        <MapContainer
          className={classes.map}
          center={[52.102288, 19.36203]}
          minZoom={6}
          zoom={6}
        >
          <TileLayer
            attribution='Dane mapy &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <MarkersCluster markers={paczkomaty} setPaczkomat={setPaczkomat} />
        </MapContainer>
        <Box className={classes.detailsRoot}>
          {selectedPaczkomat ? (
            <Box>
              {imageError ? (
                <Box
                  className={clsx(
                    classes.image,
                    classes.errorBox,
                    classes.imagePlaceholder
                  )}
                >
                  <Typography className={classes.imageErrorText}>
                    Brak zdjęcia
                  </Typography>
                </Box>
              ) : (
                <img
                  alt="Zdjęcie paczkomatu"
                  src={`${paczkomatyUrl.image}/${selectedPaczkomat.n}.jpg`}
                  className={classes.image}
                  onError={() => setImageError(true)}
                />
              )}
              <Typography className={classes.paczkomat}>Paczkomat</Typography>
              <Typography className={classes.number}>
                {selectedPaczkomat.n}
              </Typography>
              <Typography className={classes.address}>
                {getStreetName(selectedPaczkomat.e)} {selectedPaczkomat.b}
              </Typography>
              <Typography className={classes.postalCode}>
                {selectedPaczkomat.o} {selectedPaczkomat.c}
              </Typography>
              <Typography className={classes.paczkomat}>
                Godziny otwarcia
              </Typography>
              <Box>
                {moment.weekdays(true).map((day, index) => {
                  const hours = JSON.parse(selectedPaczkomat.i);
                  return (
                    <Box key={index} className={classes.hourRoot}>
                      <Typography>{day}</Typography>
                      <Typography className={classes.hour}>
                        {selectedPaczkomat.h === "24/7"
                          ? "całodobowo"
                          : `${getTime(hours[index + 1][0])} - ${getTime(
                              hours[index + 1][1]
                            )}`}
                      </Typography>
                    </Box>
                  );
                })}
              </Box>
            </Box>
          ) : (
            <Box className={classes.errorBox}>
              <Typography className={classes.errorText}>
                Wybierz paczkomat na mapie
              </Typography>
            </Box>
          )}
          <Box display="flex" justifyContent="flex-end">
            <Button color="secondary" onClick={() => setDialogOpen(false)}>
              Anuluj
            </Button>
            <Button
              disabled={!selectedPaczkomat}
              color="primary"
              onClick={() => handleConfirm()}
            >
              Zatwierdź
            </Button>
          </Box>
        </Box>
      </Dialog>
    </React.Fragment>
  );
};

export default SelectPaczkomat;
