import EmojiPeople from "@material-ui/icons/EmojiPeople";
import Mail from "@material-ui/icons/Mail";
import LocalShipping from "@material-ui/icons/LocalShipping";
import SelectPaczkomat from "./components/SelectPaczkomat/SelectPaczkomat";

export const fields = {
  PERSONAL_PICKUP: {
    name: "Odbiór osobisty",
    icon: EmojiPeople,
  },
  PARCEL_LOCKER: {
    name: "Paczkomat",
    icon: LocalShipping,
    component: SelectPaczkomat,
  },
  COURIER: {
    name: "Kurier",
    icon: Mail,
  },
};
