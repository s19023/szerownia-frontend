import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import { fields } from "./fields";

import getPickupMethods from "../../../../services/rentalAds/getPickupMethods";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  pickupMethod: {
    display: "flex",
    alignItems: "center",
  },
  icon: {
    marginRight: "5px",
  },
}));

const PickupMethod = ({ id, setPickupMethod, updatePaczkomat }) => {
  const classes = useStyles();
  const [pickupMethods, setPickupMethods] = useState([]);
  const [pickupMethod, setMethod] = useState(null);

  const handleChange = (method) => {
    setPickupMethod(method);
    setMethod(method);
  };

  useEffect(() => {
    if (id) {
      getPickupMethods(id).then((pickupMethods) =>
        setPickupMethods(pickupMethods)
      );
    }
  }, [id]);

  const CustomComponent =
    fields[pickupMethod] && fields[pickupMethod].component;

  if (!id) {
    return <Typography>Nie podano id ogłoszenia</Typography>;
  }

  return (
    <Box className={classes.root}>
      <RadioGroup onChange={(e, v) => handleChange(v)}>
        {pickupMethods.map((method) => {
          const Icon = fields[method].icon;
          return (
            <FormControlLabel
              role="label"
              key={method}
              value={method}
              control={<Radio />}
              label={
                <Box className={classes.pickupMethod}>
                  <Icon className={classes.icon} /> {fields[method].name}
                </Box>
              }
            />
          );
        })}
      </RadioGroup>
      {CustomComponent && <CustomComponent updatePaczkomat={updatePaczkomat} />}
    </Box>
  );
};

export default PickupMethod;
