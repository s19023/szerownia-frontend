import React, { useState } from "react";
import FormCard from "../../components/FormCard/FormCard";
import Typography from "@material-ui/core/Typography";
import DateRangePicker from "../../components/DateRangePicker/DateRangePicker";
import SmallAdDetails from "../../components/AdDetails/SmallAdDetails";
import PriceInfo from "./components/PriceInfo/PriceInfo";
import PickupMethod from "./components/PickupMethod/PickupMethod";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import LinearProgress from "@material-ui/core/LinearProgress";
import useQuery from "../../utils/useQuery";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";
import { useHistory } from "react-router-dom";

import newHire from "../../services/hires/newHire";
import simulatePayment from "../../services/simulatePayment";

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: "bold",
    marginBottom: "10px",
    display: "flex",
    justifyContent: "center",
  },
  datePicker: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    marginBottom: "10px",
  },
  button: {
    width: "100%",
    "&:not(:last-of-type)": {
      marginBottom: "10px",
    },
  },
  disabled: {
    color: theme.palette.text.disabled,
    textAlign: "center",
    marginTop: "20px",
  },
  progress: {
    position: "absolute",
    left: "0",
    right: "0",
    bottom: "0",
  },
}));

const HirePage = () => {
  const classes = useStyles();
  const [minDate, setMinDate] = useState(null);
  const [maxDate, setMaxDate] = useState(null);
  const [pickupMethod, setPickupMethod] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [paymentRejected, setPaymentRejected] = useState(false);
  const [selectedPaczkomat, setSelectedPaczkomat] = useState(null);
  const query = useQuery();
  const id = query.get("rentalAd");
  const history = useHistory();
  const dispatch = useDispatch();

  const buttonDisabled =
    !minDate ||
    !maxDate ||
    pickupMethod === "" ||
    (pickupMethod === "PARCEL_LOCKER" && !selectedPaczkomat);

  const handleClick = () => {
    setSubmitted(true);
    setIsLoading(true);
    simulatePayment()
      .then(() => {
        setPaymentRejected(false);
        newHire(id, minDate, maxDate, pickupMethod)
          .then(() => {
            dispatch(
              snackbarAction.openSnackbar({
                error: false,
                message: "Zlecenie wypożyczenia złożone pomyślnie",
              })
            );
            goHome();
          })
          .catch(() => {
            dispatch(
              snackbarAction.openSnackbar({
                error: true,
                message:
                  "Podczas wypożyczania wystąpił błąd. Płatność zostanie anulowana.",
              })
            );
            setIsLoading(false);
          });
      })
      .catch((e) => {
        setPaymentRejected(true);
        setIsLoading(false);
      });
  };

  const goHome = () => {
    history.push("/");
  };

  return (
    <Box display="flex">
      <FormCard title="Dokonaj wypożyczenia" width="400px">
        <Typography className={classes.title}>
          Wybierz kiedy chcesz dokonać wypożyczenia
        </Typography>
        <Box className={classes.datePicker}>
          <DateRangePicker
            id={id}
            updateMinDate={setMinDate}
            updateMaxDate={setMaxDate}
            downloadDates
          />
        </Box>
        <Typography className={classes.title}>
          Wybierz sposób dostawy
        </Typography>
        <Box className={classes.datePicker}>
          <PickupMethod
            id={id}
            setPickupMethod={setPickupMethod}
            updatePaczkomat={(paczkomat) => setSelectedPaczkomat(paczkomat)}
          />
        </Box>
        <Button
          disabled={buttonDisabled || isLoading}
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => handleClick()}
        >
          Wypożycz
        </Button>
        <Button
          disabled={isLoading}
          variant="contained"
          className={classes.button}
          onClick={() => goHome()}
        >
          Anuluj
        </Button>
        <Collapse in={submitted}>
          <Typography className={classes.disabled}>
            {isLoading && "Trwa przetwarzanie płatności..."}
            {paymentRejected &&
              !isLoading &&
              "Płatność odrzucona, spróbuj ponownie"}
            {!isLoading &&
              !paymentRejected &&
              "Wystąpił nieznany błąd, spróbuj ponownie"}
          </Typography>
          {isLoading && <LinearProgress className={classes.progress} />}
        </Collapse>
      </FormCard>
      <Box marginLeft="20px" marginTop="40px">
        <SmallAdDetails id={id} topText="Wypożyczasz" />
        <PriceInfo startDate={minDate} endDate={maxDate} id={id} />
      </Box>
    </Box>
  );
};

export default HirePage;
