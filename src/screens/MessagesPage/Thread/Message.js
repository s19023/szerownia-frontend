import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  messageRoot: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: (props) => (props.sent ? "flex-end" : "flex-start"),
    marginBottom: "10px",
  },
  message: {
    maxWidth: "400px",
    padding: "15px",
    borderRadius: (props) =>
      props.sent ? "10px 10px 0 10px" : "10px 10px 10px 0",
    backgroundColor: (props) =>
      props.sent ? theme.palette.primary.main : theme.palette.background.paper,
    boxShadow: theme.shadows[1],
  },
  content: {
    wordBreak: "break-word",
    color: (props) =>
      props.sent
        ? theme.palette.getContrastText(theme.palette.primary.main)
        : theme.palette.getContrastText(theme.palette.background.paper),
  },
  time: {
    fontSize: "12px",
    color: theme.palette.text.disabled,
  },
}));

const Message = ({ message }) => {
  const userId = useSelector((state) => state.userReducer.id);
  const classes = useStyles({
    sent: message.messageSender
      ? message.messageSender == userId
      : message.authorId == userId,
  });

  const { createdDate: time } = message;

  return (
    <Box className={classes.messageRoot}>
      <Box className={classes.message}>
        <Typography className={classes.content}>
          {message.message ? message.message : message.lastMessage}
        </Typography>
      </Box>
      <Typography className={classes.time}>
        {moment(
          time instanceof Array
            ? `${time.slice(0, 3).join("-")} ${time.slice(3, 6).join(":")}`
            : time
        ).format("DD.MM.YYYY HH:mm")}
      </Typography>
    </Box>
  );
};

export default Message;
