import React, { useState, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import Box from "@material-ui/core/Box";
import LinearProgress from "@material-ui/core/LinearProgress";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import SendIcon from "@material-ui/icons/Send";
import { makeStyles } from "@material-ui/core/styles";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import useWebSocket from "react-use-websocket";

import getMessagesFromThread from "../../../services/messages/getMessagesFromThread";
import postMessage from "../../../services/messages/postMessage";
import Message from "./Message";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    maxHeight: "100%",
    overflow: "auto",
    padding: "0px 10px",
    height: "100%",
  },
  messagesRoot: {
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
    overflow: "auto",
    paddingRight: "5px",
  },
  bottomRoot: {
    width: "100%",
    overflow: "hidden",
    boxSizing: "border-box",
    justifyContent: "center",
    minHeight: "72px",
  },
  progress: {
    width: "100%",
    height: "3px",
    top: (props) => (props.isLoading ? "0px" : "-3px"),
    transition: theme.transitions.create(
      "top",
      theme.transitions.duration.leavingScreen
    ),
  },
  inputRoot: {
    display: "flex",
    padding: "20px 5%",
  },
  inputBox: {
    flexGrow: "1",
    display: "flex",
    justifyContent: "center",
  },
  input: {
    flexGrow: "1",
    maxWidth: "500px",
  },
}));

const Thread = ({ setActive, updateThread }) => {
  const token = useSelector((state) => state.userReducer.accessToken);
  const [messages, setMessages] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isDisabled, setDisabled] = useState(true);
  const [socketUrl, setSocketUrl] = useState(null);
  const [value, setValue] = useState("");
  const classes = useStyles({ isLoading: isLoading });
  const { id } = useParams();
  const history = useHistory();
  const container = useRef();

  const { lastJsonMessage: lastMessage } = useWebSocket(socketUrl);

  useEffect(() => {
    downloadData();
    setActive(id);
  }, [id]);

  useEffect(() => {
    if (!lastMessage) {
      return;
    }

    const message = lastMessage;
    if (message.threadId === id) {
      setMessages(tmpMessages => tmpMessages.concat(message));
    }

    updateThread(message);
  }, [lastMessage])

  useEffect(() => {
    if (!container.current) {
      return;
    }

    container.current.scrollTop = container.current.scrollHeight;
  }, [messages]);

  useEffect(() => {
    setSocketUrl(
      process.env.REACT_APP_MESSAGES_SOCKET + `/messages?id=${token}`
    );
  }, [token]);

  const downloadData = (scroll) => {
    getMessagesFromThread(id)
      .then((messages) => {
        setMessages(messages);
        //scrollToBottom();
        setTimeout(() => setIsLoading(false), 500);
      })
      .catch(() => {
        history.push("/messages");
      });
  };

  const handleChange = (value) => {
    setDisabled(value === "" || value === null);
    setValue(value);
  };

  const sendMessage = () => {
    setDisabled(true);
    setIsLoading(true);
    postMessage(id, value)
      .then(() => {
        setValue("");
        setDisabled(false);
      })
      .catch(() => {
        setValue(value);
        setDisabled(false);
      })
      .finally(() => {
        setTimeout(() => setIsLoading(false), 500);
      });
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.messagesRoot} ref={container}>
        {messages.map((message, idx) => (
          <Message message={message} key={message.id + idx} />
        ))}
      </Box>
      {messages.length > 0 && (
        <Box className={classes.bottomRoot}>
          <LinearProgress className={classes.progress} />
          <Box className={classes.inputRoot}>
            <form className={classes.inputBox}>
              <TextField
                value={value}
                onChange={(e) => handleChange(e.target.value)}
                placeholder="Wpisz wiadomość"
                className={classes.input}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        id="send-message"
                        color="primary"
                        type="submit"
                        disabled={isDisabled}
                        onClick={() => sendMessage()}
                      >
                        <SendIcon />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </form>
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default Thread;
