import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ArrowForward from "@material-ui/icons/ArrowForward";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "15px",
    borderRadius: "4px",
    border: (props) =>
      props.active ? "unset" : `1px solid ${theme.palette.primary.main}`,
    background: (props) =>
      props.active
        ? theme.palette.primary.main
        : theme.palette.background.paper,
    boxShadow: "0px 3px 6px #00000029",
    marginBottom: "15px",
    width: "100%",
    boxSizing: "border-box",
  },
  topic: {
    fontWeight: "bold",
    fontSize: "20px",
    fontFamily: "Poppins, sans-serif",
    color: (props) => (props.active ? "#FAFAFA" : theme.palette.text.primary),
  },
  name: {
    fontSize: "12px",
    color: (props) => (props.active ? "#FAFAFA" : theme.palette.text.primary),
  },
  button: {
    height: "48px",
    color: (props) => (props.active ? "#FAFAFA" : theme.palette.text.primary),
  },
  message: {
    color: (props) => (props.active ? "#FAFAFA" : theme.palette.text.primary),
  },
}));

const MessageRow = ({ thread, active, index }) => {
  const classes = useStyles({ active: active });
  const history = useHistory();
  const { id, lastMessage, lastMessageTime: time } = thread;

  return (
    <Box className={classes.root}>
      <Box>
        <Typography className={classes.topic}>{thread.subject}</Typography>
        <Typography className={classes.name}>
          {moment(
            time instanceof Array
              ? `${time.slice(0, 3).join("-")} ${time.slice(3, 6).join(":")}`
              : time
          ).format("DD.MM.YYYY HH:mm")}
        </Typography>
        <Typography className={classes.message}>{lastMessage}</Typography>
      </Box>
      <IconButton
        className={classes.button}
        onClick={() => history.push(`/messages/thread/${id}`)}
        id={`thread-${index}`}
      >
        <ArrowForward />
      </IconButton>
    </Box>
  );
};

export default MessageRow;
