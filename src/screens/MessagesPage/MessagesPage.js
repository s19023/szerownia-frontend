import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import MessageRow from "./components/MessageRow";
import Thread from "./Thread/Thread";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Switch, Route, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import getAllMessages from "../../services/messages/getAllMessages";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "1500px",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    flexGrow: "1",
    maxHeight: "calc(100vh - 100px)",
  },
  box: {
    maxHeight: "100%",
    flexGrow: "1",
    display: "flex",
    overflow: "auto",
  },
  header: {
    marginBottom: "20px",
  },
  info: {
    fontSize: "16px",
    color: theme.palette.text.secondary,
  },
  messages: {
    width: "35%",
    minHeight: "100px",
    maxHeight: "100%",
    overflow: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  thread: {
    width: "65%",
    maxHeight: "100%",
  },
}));

const MessagesPage = () => {
  const classes = useStyles();
  const [loadingThreads, setLoadingThreads] = useState(true);
  const [threads, setThreads] = useState([]);
  const [active, setActive] = useState(null);
  const history = useHistory();

  useEffect(() => {
    getMessages();
  }, []);

  const getMessages = () => {
    getAllMessages().then((threads) => {
      setThreads(threads);
      setLoadingThreads(false);
    });
  };

  useEffect(() => {
    if (
      threads.length > 0 &&
      history.location.pathname.split("/").length === 2
    ) {
      history.push("/messages/thread/" + threads[0].id);
      setActive(threads[0].id);
    }
  }, [threads]);

  const updateThread = (thread) => {
    const tmpThreads = [...threads].filter(t => t.id !== thread.threadId);
    thread.id = thread.threadId;
    tmpThreads.push(thread);
    setThreads(tmpThreads);
  }

  return (
    <Box className={classes.root}>
      <Typography component="h1" variant="h3" className={classes.header}>
        Wiadomości
      </Typography>
      <Box className={classes.box}>
        <Box className={classes.messages}>
          {loadingThreads && <CircularProgress />}
          {!loadingThreads && threads.length === 0 && (
            <Typography className={classes.info}>
              Nie masz jeszcze żadnych wiadomości
            </Typography>
          )}
          {threads.map((thread, index) => (
            <MessageRow
              active={thread.id == active}
              thread={thread}
              index={index}
              key={thread.id}
            />
          ))}
        </Box>
        <Box className={classes.thread}>
          <Switch>
            <Route path="/messages/thread/:id">
              <Thread setActive={setActive} updateThread={updateThread} />
            </Route>
          </Switch>
        </Box>
      </Box>
    </Box>
  );
};

export default MessagesPage;
