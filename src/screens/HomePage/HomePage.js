import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { snackbarAction } from "../../redux/modules/snackbar";
import { searchAction } from "../../redux/modules/search";
import Search from "../../components/Search/Search";
import SearchResults from "../../components/SearchResults/SearchResults";
import getRentalAdsList from "../../services/rentalAds/getRentalAdsList";
import getSearchAdList from "../../services/searchAds/getSearchAdsList";

const useStyles = makeStyles(() => ({
  tabs: {
    marginBottom: "30px",
  },
  flex: {
    justifyContent: "center",
  },
}));

const HomePage = () => {
  const classes = useStyles();
  const [ads, setAds] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [pages, setPages] = useState(1);
  const [page, setPage] = useState(1);
  const [isLoaded, setIsLoaded] = useState(false);
  const dispatch = useDispatch();
  const isSearchAd = useSelector((state) => state.searchReducer.isSearch);

  useEffect(() => {
    setPage(1);
    if (isLoaded) {
      downloadData();
    }
  }, [isSearchAd]);

  useEffect(() => {
    downloadData();
  }, [page]);

  const setIsSearchAd = (val) => {
    setIsLoading(true);
    dispatch(searchAction.setSearch({ isSearch: val !== 0 }));
  };

  const downloadData = () => {
    setIsLoading(true);
    setAds([]);
    const getAds = isSearchAd ? getSearchAdList : getRentalAdsList;
    getAds(page - 1)
      .then((data) => {
        setAds(data.results);
        setPages(data.totalPages);
        setIsLoading(false);
        setIsLoaded(true);
      })
      .catch(() => {
        setIsLoading(false);
        dispatch(
          snackbarAction.openSnackbar({
            error: true,
            message: "Nie można pobrać ogłoszeń, spróbuj odświeżyć stronę",
          })
        );
      });
  };

  return (
    <Box width="100%" maxWidth="1500px">
      <Search />
      <Tabs
        className={classes.tabs}
        classes={{ flexContainer: classes.flex }}
        value={isSearchAd ? 1 : 0}
        onChange={(e, v) => setIsSearchAd(v)}
      >
        <Tab value={0} label="Ogłoszenia użyczenia" />
        <Tab value={1} label="Ogłoszenia poszukiwania" />
      </Tabs>
      <SearchResults
        results={ads}
        isSearchAd={isSearchAd}
        page={page}
        pages={pages}
        changePage={setPage}
        isLoading={isLoading}
      />
    </Box>
  );
};

export default HomePage;
